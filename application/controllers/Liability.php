<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ('soapclient/SforcePartnerClient.php');
require_once ('soapclient/SforceHeaderOptions.php');

class Liability extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('property_model');
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
            exit();
        }
    }

    /* Start :: Function to list all the Liability */

    public function listLiability($property_id) {
        $property_id = base64_decode($property_id);
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to manage liability!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if (count($this->input->post()) > 0) {
            if ($this->input->post('btn_delete_all') != "") {
                /* getting all ides selected */
                $arr_liability_ids = $this->input->post('checkbox');
                if (count($arr_liability_ids) > 0 && is_array($arr_liability_ids)) {
                    foreach ($arr_liability_ids as $liability_id) {
                        /* getting user details */
                        $arr_liability_detail = $this->common_model->getRecords("trans_debts", "", array("debt_id" => intval($liability_id)));
                        if (count($arr_liability_detail) > 0) {
                            /* Delete details from salesforce */
                            $mySforceConnection = new SforcePartnerClient();
                            $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                            // Simple example of session management - first call will do
                            // login, refresh will use session ID and location cached in
                            // PHP session
                            if (($this->session->userdata('partnerSessionId')) !== null) {
                                $location = $this->session->userdata('partnerLocation');
                                $sessionId = $this->session->userdata('partnerSessionId');
                                $mySforceConnection->setEndpoint($location);
                                $mySforceConnection->setSessionHeader($sessionId);
                            } else {
                                $mySforceConnection->login(USERNAME, PASSWORD);
                                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                            }
                            $salesforceId = $arr_liability_detail[0]['salesforce_debt_id'];
                            if ($salesforceId != null) {
                                $response = $mySforceConnection->delete($salesforceId);
                            }
                        }
                    }
                    if (count($arr_liability_ids) > 0) {
                        /* deleting the property selected */
                        $this->common_model->deleteRows($arr_liability_ids, "trans_debts", "debt_id");
                    }
                    $this->session->set_userdata("msg", "<span class='success'>Loan deleted successfully!</span>");
                }
            }
        }
        /* using the property model */
        $data['title'] = "Manage Liability";
        $data['property_id'] = $property_id;
        $data['arr_liability_list'] = $this->common_model->getRecords('trans_debts', $fields_to_pass = '*', array('property_id' => intval($property_id)), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $this->load->view('backend/debt/list', $data);
    }

    /* Start :: Add Liability function */

    public function addLiability($property_id) {
        $property_id = base64_decode($property_id);
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to manage liability!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        $arr_property_detail = end($this->common_model->getRecords("mst_properties", 'property_id,valuation,user_id,salesforce_property_id', array("property_id" => intval($property_id))));
        $arr_user_detail = end($this->common_model->getRecords("mst_users", 'user_id,salesforce_user_id', array("user_id" => intval($arr_property_detail['user_id']))));
        if (count($this->input->post()) > 0) {
            $user_id = $arr_property_detail['user_id'];
            $salesforce_property_id = $arr_property_detail['salesforce_property_id'];
            $salesforce_user_id = $arr_user_detail['salesforce_user_id'];
            $interest_rate = $this->input->post('interest_rate');
            $loan_amount = str_replace(",", "", $this->input->post('loan_amount'));
            $next_payment_amount = str_replace(",", "", $this->input->post('next_payment_amount'));
            $loan_type = $this->input->post('loan_type');
            $payment_frequency = $this->input->post('payment_frequency');
            $next_repayment_date = $this->input->post('next_repayment_date');
            if ($user_id != "" && $property_id != '') {
                switch ($payment_frequency) {
                    case 'weekly':
                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
                        break;
                    case 'monthly':
                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 30 days'));
                        break;
                    case 'fortnight':
                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 14 days'));
                        break;
                    default:
                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
                }
                /* Insert property dept data */
                $arr_to_property_dept_insert = array(
                    "property_id" => $property_id,
                    "user_id" => $user_id,
                    "loan_amount" => $loan_amount,
                    "interest_rate" => $interest_rate,
                    "loan_type" => $loan_type,
                    "next_payment_amount" => $next_payment_amount,
                    "payment_frequency" => $payment_frequency,
                    "next_payment_date" => $next_payment_date,
                    "added_date" => date("Y-m-d"),
                );
                $debt_id = $this->common_model->insertRow($arr_to_property_dept_insert, "trans_debts");
                $current_equity = ($arr_property_detail['valuation']) - $loan_amount;
                $update_arr = array(
                    "current_equity" => $current_equity,
                );
                $condition_to_pass = array("property_id" => $property_id);
                $this->common_model->updateRow('mst_properties', $update_arr, $condition_to_pass);
                /* Start :: Insert Loan graph value */
                $table = 'trans_graph_details';
                $insert_data = array(
                    'value' => $current_equity,
                    'property_id' => $property_id,
                    'debt_id_fk' => $debt_id,
                    'user_id' => $user_id,
                    'type' => '3',
                    'date' => date("Y-m-d")
                );
                $this->common_model->insertRow($insert_data, $table);
                /* End :: Insert Loan graph value */

                /* Salesforce integration */
                $mySforceConnection = new SforcePartnerClient();
                $mySoapClient = $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                // Simple example of session management - first call will do
                // login, refresh will use session ID and location cached in
                // PHP session
                if (($this->session->userdata('partnerSessionId')) !== null) {
                    $location = $this->session->userdata('partnerLocation');
                    $sessionId = $this->session->userdata('partnerSessionId');
                    $mySforceConnection->setEndpoint($location);
                    $mySforceConnection->setSessionHeader($sessionId);
                } else {
                    $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);
                    $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                    $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                }
                /* Start :: Salesforce Integration */
                $mySforceConnection = new SforcePartnerClient();
                $mySoapClient = $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                // Simple example of session management - first call will do
                // login, refresh will use session ID and location cached in
                // PHP session
                if (($this->session->userdata('partnerSessionId')) !== null) {
                    $location = $this->session->userdata('partnerLocation');
                    $sessionId = $this->session->userdata('partnerSessionId');
                    $mySforceConnection->setEndpoint($location);
                    $mySforceConnection->setSessionHeader($sessionId);
                } else {
                    $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);
                    $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                    $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                }
                /** Insert debt * */
                $arr_debt_fields = array(
                    'User_ID__c' => $salesforce_user_id,
                    'Property_ID__c' => $salesforce_property_id,
                    'Interest_Rate__c' => $interest_rate,
                    'Loan_Amount__c' => $loan_amount,
                    'Loan_Type__c' => $loan_type,
                    'Next_Payment_Date__c' => $next_payment_date,
                    'Payment_Frequency__c' => $payment_frequency,
                    'Next_Payment_Amount__c' => $next_payment_amount,
                    'Added_Date__c' => date("Y-m-d"),
                );
                $sDebtObject = new SObject();
                $sDebtObject->fields = $arr_debt_fields;
                $sDebtObject->type = 'Debt__c';
                $createDebtResponse = $mySforceConnection->create(array($sDebtObject));
                $salesforceDebtId = $createDebtResponse[0]->id;
                /* Update salesforce id in local database for further use.*/
                $table_name = 'trans_debts';
                $update_data = array(
                    'salesforce_debt_id' => $salesforceDebtId
                );
                $condition = array(
                    'debt_id' => $debt_id
                );
                if ($debt_id) {
                    $this->common_model->updateRow($table_name, $update_data, $condition);
                }
                /* End :: Salesforce Integration */
                $this->session->set_userdata("msg", "<span class='success'>Property added successfully!</span>");
                redirect(base_url() . "backend/liability/list/" . base64_encode($property_id));
                exit();
            }
        }
        $arr_privileges = array();
        /* getting all privileges */
        $data['property_id'] = $property_id;
        $data['title'] = "Add Liability";
        $this->load->view('backend/debt/add', $data);
    }
    /* End :: Add Liability function */

    /* Start :: Edit Liability function */

    public function editLiability($edit_id = '') {
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to manage liability!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }

        if ($this->input->post('loan_amount') != '') {
            $debt_id = intval($this->input->post('edit_id'));
            $interest_rate = $this->input->post('interest_rate');
            $loan_amount = $this->input->post('loan_amount');
            $next_payment_amount = $this->input->post('next_payment_amount');
            $loan_type = $this->input->post('loan_type');
            $payment_frequency = strtolower($this->input->post('payment_frequency'));
            $next_repayment_date = $this->input->post('next_repayment_date');
            switch ($payment_frequency) {
                case 'weekly':
                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
                    break;
                case 'monthly':
                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 30 days'));
                    break;
                case 'fortnightly':
                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 14 days'));
                    break;
                default:
                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
            }
            if ($debt_id != '') {
                /* Start :: update property dept data */
                $update_data = array(
                    "loan_amount" => $loan_amount,
                    "interest_rate" => $interest_rate,
                    "loan_type" => $loan_type,
                    "next_payment_amount" => $next_payment_amount,
                    "payment_frequency" => $payment_frequency,
                    "next_payment_date" => $next_payment_date,
                );
                $condition = array("debt_id" => $debt_id);
                $this->common_model->updateRow('trans_debts', $update_data, $condition);
                /* Start :: update property current equity data */
                $arr_debt_detail = end($this->common_model->getRecords("trans_debts", 'property_id,user_id', array("debt_id" => intval($debt_id))));
                $arr_property_detail = end($this->common_model->getRecords("mst_properties", 'property_id,valuation', array("property_id" => intval($arr_debt_detail['property_id']))));
                $current_equity = ($arr_property_detail['valuation']) - $loan_amount;
                $update_arr = array(
                    "current_equity" => $current_equity,
                );
                $condition = array("property_id" => $arr_debt_detail['property_id']);
                $this->common_model->updateRow('mst_properties', $update_arr, $condition);
                /* Start :: Insert Loan graph value */
                $table = 'trans_graph_details';
                $insert_data = array(
                    'value' => $current_equity,
                    'property_id' => $arr_debt_detail['property_id'],
                    'debt_id_fk' => $arr_debt_detail['debt_id'],
                    'user_id' => $arr_debt_detail['user_id'],
                    'type' => '3',
                    'date' => date("Y-m-d")
                );
                $this->common_model->insertRow($insert_data, $table);
                /* End :: Insert Loan graph value */

                /* Start :: Insert Loan graph value */
                $table = 'trans_graph_details';
                $insert_data = array(
                    'value' => $current_equity,
                    'property_id' => $arr_property_detail['property_id'],
                    'user_id' => $arr_debt_detail['user_id'],
                    'type' => '1',
                    'date' => date("Y-m-d")
                );
                $this->common_model->insertRow($insert_data, $table);
                /* End :: Insert Loan graph value */

                /* Start :: Salesforce Integration */
                $mySforceConnection = new SforcePartnerClient();
                $mySoapClient = $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                // Simple example of session management - first call will do
                // login, refresh will use session ID and location cached in
                // PHP session
                if (($this->session->userdata('partnerSessionId')) !== null) {
                    $location = $this->session->userdata('partnerLocation');
                    $sessionId = $this->session->userdata('partnerSessionId');
                    $mySforceConnection->setEndpoint($location);
                    $mySforceConnection->setSessionHeader($sessionId);
                } else {
                    $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);
                    $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                    $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                }
                /** update debt */
                $arr_property_debt = $this->common_model->getRecords('trans_debts', '', array("property_id" => $arr_debt_detail['property_id'], "user_id" => $arr_debt_detail['user_id']));
                $arr_property_debt = end($arr_property_debt);
                $arr_debt_fields = array(
                    'Interest_Rate__c' => $interest_rate,
                    'Loan_Amount__c' => $loan_amount,
                    'Loan_Type__c' => $loan_type,
                    'Next_Payment_Date__c' => $next_payment_date,
                    'Payment_Frequency__c' => $payment_frequency,
                    'Next_Payment_Amount__c' => $next_payment_amount,
                );

                $sDebtObject = new SObject();
                $sDebtObject->fields = $arr_debt_fields;
                $sDebtObject->Id = $arr_property_debt['salesforce_debt_id'];
                $sDebtObject->type = 'Debt__c';
                $createDebtResponse = $mySforceConnection->update(array($sDebtObject));
                $this->session->set_userdata("msg", "<span class='success'>Property updated successfully!</span>");
                redirect(base_url() . "backend/property/list");
                exit();
            }
        }
        $arr_privileges = array();
        /* getting all privileges */
        $data['arr_privileges'] = $this->common_model->getRecords('mst_privileges');
        /* getting user details from $edit_id from function parameter */
        $fields = array('debt_id,loan_amount,interest_rate,loan_type,payment_frequency,next_payment_date,next_payment_amount,property_id');
        $data['arr_liabilities'] = end($this->common_model->getRecords('trans_debts', $fields, array('debt_id' => intval(base64_decode($edit_id))), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $data['title'] = "Edit Property";
        $data['debt_id'] = $edit_id;
        $data['property_id'] = $data['arr_liabilities']['property_id'];
        $this->load->view('backend/debt/edit', $data);
    }
    /* End :: Edit Liability function */

    /* Start :: View Liability function */

    public function viewLiability($debt_id = '') {
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
        }
        /* using the admin model */
        $this->load->model('admin_model');
        $data = $this->common_model->commonFunction();
        $arr_privileges = array();
        /* getting all privileges  */
        /* getting admin details from user id from function parameter */
        $user_account = $this->session->userdata('user_account');
        $fields = array('debt_id,loan_amount,interest_rate,loan_type,payment_frequency,next_payment_date,next_payment_amount,property_id,added_date,salesforce_debt_id');
        $data['arr_liabilities'] = end($this->common_model->getRecords('trans_debts', $fields, array('debt_id' => intval(base64_decode($debt_id))), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $data['property_id'] = $data['arr_liabilities']['property_id'];
        $data['title'] = "View Liability";
        $this->load->view('backend/debt/view', $data);
    }

    /** End :: Get Location wise latitude,longitude & zip code  * */
}
