<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_Account extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("common_model");
        $this->load->model("user_model");
    }

    /* Get user's profile information */

    public function profile() {
        if (!$this->common_model->isLoggedIn()) {
            redirect('signin');
        }
        $data = $this->common_model->commonFunction();
        $data['user_session'] = $this->session->userdata('user_account');

        $arr_user_data = array();
        $table_to_pass = 'mst_users';
        $fields_to_pass = 'user_id,first_name,about_me,last_name,user_email,user_name,user_type,user_status,profile_picture,gender';
        $condition_to_pass = array("user_id" => $data['user_session']['user_id']);
        $arr_user_data = $this->user_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $data['arr_user_data'] = $arr_user_data[0];

        $data['site_title'] = "Profile";
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/user-account/user-profile', $data);
        $this->load->view('front/includes/footer');
    }

    /* Edit user profile information */

    function editProfile() {
        if (!$this->common_model->isLoggedIn()) {
            redirect('signin');
        }
        $data['user_session'] = $this->session->userdata('user_account');
        $user_id = $data['user_session']['user_id'];
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array("user_id" => intval($user_id)));
        $arr_user_detail = end($arr_user_detail);
        $data = $this->common_model->commonFunction();
        if ($this->input->post('user_email') != '') {
            $table_name = 'mst_users';
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $user_email = $this->input->post('user_email');
            $user_name = $this->input->post('user_name');
            $about_me = $this->input->post('about_me');
            $gender = $this->input->post('gender');
            $user_password = base64_decode($arr_user_detail['user_password']);
            if ($this->input->post('user_email') == $this->input->post('user_email_old')) {
                $email_verified = '1';
                $activation_code = $arr_user_detail['activation_code'];
                $user_session = $this->session->userdata('user_account');
                $user_session['user_name'] = $user_name;
                $user_session['user_email'] = $user_email;
                $user_session['first_name'] = $first_name;
                $user_session['last_name'] = $last_name;
                $this->session->set_userdata('user_account', $user_session);
            } else {
                $email_verified = '0';
                $activation_code = time();
            }
            $update_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'user_email' => $user_email,
                'user_name' => $user_name,
                'gender' => $gender,
                'about_me' => $about_me,
                'email_verified' => $email_verified,
                'activation_code' => $activation_code
            );
            $condition = array("user_id" => $user_id);
            $cnf_profile = $this->common_model->updateRow($table_name, $update_data, $condition);
            if ($this->input->post('user_email') == $this->input->post('user_email_old')) {
                $this->session->set_userdata('edit_profile_success', "Your profile has been updated successfully.");
                redirect(base_url() . 'profile');
            } else {
                /*
                 * sending account verification link mail to user 
                 */
                $lang_id = 17;
                $activation_link = '<a href="' . base_url() . 'user-activation/' . $activation_code . '">Activate Account</a>';
                $reserved_words = array
                    ("||SITE_TITLE||" => $data['global']['site_title'],
                    "||SITE_PATH||" => base_url(),
                    "||USER_NAME||" => $this->input->post('user_name'),
                    "||ADMIN_NAME||" => $this->input->post('first_name') . ' ' . $this->input->post('last_name'),
                    "||ADMIN_EMAIL||" => $this->input->post('user_email'),
                    "||PASSWORD||" => $user_password,
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
                /*
                 * getting mail subect and mail message using email template title and lang_id and reserved works 
                 */
                $email_content = $this->common_model->getEmailTemplateInfo('admin-email-updated', 17, $reserved_words);
                /*
                 * sending the mail to deleting user 
                 */
                /*
                 * 1.recipient array. 2.From array containing email and name, 3.Mail subject 4.Mail Body 
                 */
                $mail = $this->common_model->sendEmail(array($this->input->post('user_email')), array("email" => $data['global']['site_email'], "name" => $data['global']['site_title']), $email_content['subject'], $email_content['content']);
                if ($mail) {
                    $this->session->set_userdata('edit_profile_success_with_email', "Your account is deactivated now due to changed email address. Please check your email and activate account from <strong>" . $user_email . "</strong>");
                    $this->session->unset_userdata('user_account');
                    redirect(base_url());
                } else {
                    $this->session->set_userdata('edit_profile_success', "Your profile has been updated successfully.");
                    redirect(base_url() . 'profile');
                }
            }
        }
        $table_to_pass = 'mst_users';
        $fields_to_pass = 'user_id,first_name,about_me,last_name,user_email,user_name,user_type,user_status,profile_picture,gender';
        $condition_to_pass = array("user_id" => $user_id);
        $arr_user_data = array();
        $arr_user_data = $this->user_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $data['arr_user_data'] = $arr_user_data[0];

        $data['site_title'] = "Edit Profile";
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/user-account/edit-user-profile', $data);
        $this->load->view('front/includes/footer');
    }

    /* Check user email for dupliation */

    public function chkEditEmailDuplicate() {

        if ($this->input->post('user_email') == $this->input->post('user_email_old')) {
            echo 'true';
        } else {
            $table_to_pass = 'mst_users';
            $fields_to_pass = array('user_id', 'user_email');
            $condition_to_pass = array("user_email" => $this->input->post('user_email'));
            $arr_login_data = $this->user_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            if (count($arr_login_data)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    /* Check username for dupliation */

    public function chkEditUsernameDuplicate() {

        if ($this->input->post('user_name') == $this->input->post('user_name_old')) {
            echo 'true';
        } else {
            $table_to_pass = 'mst_users';
            $fields_to_pass = array('user_id', 'user_name');
            $condition_to_pass = array("user_email" => $this->input->post('user_name'));
            $arr_login_data = $this->user_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            if (count($arr_login_data)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    /* Change user password */

    public function changePassword() {
        if (!$this->common_model->isLoggedIn()) {
            redirect('signin');
        }
        $data = $this->common_model->commonFunction();
        $data['user_session'] = $this->session->userdata('user_account');
        if ($this->input->post('new_user_password') != '') {
            // generating the password by using hashing technique and applyng salt on it
            //crypt has method is used. 2y is crypt algorith selector
            //12 is workload factor on core processor.
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($this->input->post('new_user_password'), '$2y$12$' . $salt);

            $table_name = 'mst_users';
            $update_data = array('user_password' => $hash_password, "reset_password_code" => '0');
            $condition = array("user_id" => $data['user_session']['user_id']);
            $this->common_model->updateRow($table_name, $update_data, $condition);
            $this->session->set_userdata('edit_profile_success', "Your password has been updated successfully.");
            redirect(base_url() . 'profile');
            exit;
        }
        $table_to_pass = 'mst_users';
        $fields_to_pass = 'user_id,first_name,last_name,user_email,user_name,user_type,user_status,profile_picture,gender';
        $condition_to_pass = array("user_id" => $data['user_session']['user_id']);
        $arr_user_data = array();
        $arr_user_data = $this->user_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $data['arr_user_data'] = $arr_user_data[0];
        $value = $this->generateNewPasswordCode();
        $data['site_title'] = "Change Password";
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/user-account/change-password', $data);
        $this->load->view('front/includes/footer');
    }

    /* It will generate a random code and save in his record  , sending it over email to user. */

    public function generateNewPasswordCode() {
        if (!$this->common_model->isLoggedIn()) {
            redirect('signin');
        }
        $code = rand(9991, 999899);
        $data = $this->common_model->commonFunction();
        $data['user_session'] = $this->session->userdata('user_account');
        if ($data['user_session']['user_id'] != '') {
            $table_name = 'mst_users';
            $update_data = array('password_change_security_code' => $code);
            $condition = array("user_id" => $data['user_session']['user_id']);
            $this->common_model->updateRow($table_name, $update_data, $condition);
            //sending this code to user 
            $lang_id = 17;
            $reserved_words = array
                ("||SITE_TITLE||" => $data['global']['site_title'],
                "||SITE_PATH||" => base_url(),
                "||CODE||" => $code
            );
            /* getting mail subect and mail message using email template title and lang_id and reserved works  */
            $email_content = $this->common_model->getEmailTemplateInfo('send-password-reset-code', 17, $reserved_words);
            $mail = $this->common_model->sendEmail(array($data['user_session']['user_email']), array("email" => $data['global']['site_email'], "name" => $data['global']['site_title']), $email_content['subject'], $email_content['content']);
            return json_encode(array("success" => '1', "success_msg" => 'Security code has been sent on your registered email!!'));
            exit;
        }
        return json_encode(array("success" => 0, "error_msg" => 'Error in request0'));
        exit;
    }

    /* It will check the security code. */

    public function checkForValidCodeOnchangePassword() {
        if (!$this->common_model->isLoggedIn()) {
            echo "false";
            exit;
        }
        $code = $this->input->post('security_code');
        $data = $this->common_model->commonFunction();
        $data['user_session'] = $this->session->userdata('user_account');
        if ($data['user_session']['user_id'] != '') {
            $table_name = 'mst_users';
            $select_data = array('user_id,password_change_security_code');
            $condition = array("user_id" => $data['user_session']['user_id'], 'password_change_security_code' => $code);
            $data['user_info'] = $this->common_model->getRecords($table_name, $select_data, $condition);
            if (count($data['user_info']) > 0) {
                if ($data['user_info'][0]['password_change_security_code'] != '0' && $data['user_info'][0]['password_change_security_code'] != '') {
                    echo "true";
                    exit;
                } else {
                    echo "false";
                    exit;
                }
            } else {
                echo "false";
                exit;
            }
        }
        echo "false";
        exit;
    }

    public function changeProfilePicture() {
        $data = $this->common_model->commonFunction();
        $data['user_session'] = $this->session->userdata('user_account');
        $user_id = $data['user_session']['user_id'];
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['upload_path'] = './media/front/img/user-profile-pictures/';
        $config['max_size'] = 1024 * 3;
        $config['file_name'] = rand();
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($_FILES['uploadprofile']['name'] != '') {
            if (!$this->upload->do_upload('uploadprofile')) {
                $error = $this->upload->display_errors();
                $this->session->set_userdata('image_error', $error);
            } else {
                $thumb_file = explode('.', $this->input->post('old_logo'));
                $absolute_path = $this->common_model->absolutePath();
                $data = $this->upload->data();
                $logo_name = $data['file_name'];
                /* create thumbnail start here */
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = './media/front/img/user-profile-pictures/' . $data['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 100;
                $config['height'] = 100;
                $config['new_image'] = './media/front/img/user-profile-pictures/thumb/' . $data['file_name'];
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    $error = $this->image_lib->display_errors();
                    $this->session->set_userdata('image_error', $error);
                }
                $profile_image = array("profile_picture" => $data['file_name']);
                //condition to update record	for the user status
                $condition_array = array('user_id' => $user_id);
                $this->common_model->updateRow('mst_users', $profile_image, $condition_array);
            }
        }
        $profile_image = array("profile_picture" => $data['file_name']);
        //condition to update record	for the user status
        $condition_array = array('user_id' => $user_id);
        $this->common_model->updateRow('mst_users', $profile_image, $condition_array);
    }

    /* Destroy the user session */
}
