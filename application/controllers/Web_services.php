<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//error_reporting(E_ALL);
//ini_set("error_reporting", E_ALL);
require_once ('soapclient/SforceEnterpriseClient.php');
require_once ('soapclient/SforceHeaderOptions.php');

class Web_Services extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->_url = "";
        $this->load->model('common_model');
        $this->load->model('web_services_model');
        $this->load->helper('custom_method');
    }

    /* Start:: PRE App Login */

    public function wsAppLogin($email = "", $pass = "") {
        $data['global'] = $this->common_model->getGlobalSettings();
        $client_id = $data['global']['client_id'];
        $client_secret = $data['global']['client_secret'];
        $email = $this->input->post('user_email');
        $pass = $this->input->post('user_password');
        $arrToReturn = array();
        $arr_login_data = array();
        if ($email != '') {
            $table_to_pass = 'mst_users';
            $fields_to_pass = array('user_id', 'salesforce_user_id', 'first_name', 'last_name', 'user_name', 'user_email', 'user_type', 'email_verified', 'user_status', 'user_password', 'role_id');
            $condition_to_pass = "user_email = '" . addslashes($email) . "' AND user_type = 1";
            $arr_login_data = $this->web_services_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            if (count($arr_login_data)) {
                $crypt = crypt($pass, $arr_login_data[0]['user_password']);
                if ($crypt != $arr_login_data[0]['user_password']) {
                    $arrToReturn = array('error' => 1, 'message' => "Please enter correct password.");
                } elseif ($arr_login_data[0]['email_verified'] == 1) {
                    if ($arr_login_data[0]['user_status'] == 2) {
                        $arrToReturn = array('error' => 1, 'message' => "Your account has been blocked by administrator.");
                    } else {
                        $user_data['user_id'] = $arr_login_data[0]['user_id'];
                        $user_data['user_email'] = $arr_login_data[0]['user_email'];
                        $user_data['first_name'] = $arr_login_data[0]['first_name'];
                        $user_data['last_name'] = $arr_login_data[0]['last_name'];
                        $user_data['user_type'] = $arr_login_data[0]['user_type'];
                        $user_data['role_id'] = $arr_login_data[0]['role_id'];
                        $user_data['salesforce_user_id'] = $arr_login_data[0]['salesforce_user_id'];
                        $api_keys['client_id'] = $client_id;
                        $api_keys['client_secret'] = $client_secret;
                        $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $arr_login_data[0]['role_id']));
                        /* serializing the user privilegse and setting into the session. While ussing user privileges use unserialize this session to get user privileges */
                        if (count($arr_privileges) > 0) {
                            foreach ($arr_privileges as $privilege) {
                                $user_privileges[] = $privilege['privilege_id'];
                            }
                        } else {
                            $user_privileges = array();
                        }
                        $user_data['user_privileges'] = serialize($user_privileges);
                        /* Start :: Set the user's session */
                        $this->session->set_userdata('user_account', $user_data);
                        /* End :: Set the user's session */
                        $arrToReturn = array('error' => 0, 'message' => "Login successfull.", "data" => $arr_login_data[0], "api_keys" => $api_keys);
                    }
                } else {
                    $arrToReturn = array('error' => 1, 'message' => "Please activate your account.");
                }
            } else {
                $arrToReturn = array('error' => 1, 'message' => "Invalid email address.");
            }
        } else {
            $arrToReturn = array('error' => 1, 'message' => "Opps !!! Something went wrong. Please try again.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Forgot Password */

    public function wsForgotPassword() {
        $data = $this->common_model->commonFunction();
        $data['global'] = $this->common_model->getGlobalSettings();
        $user_email = $this->input->post('user_email');
        if ($user_email != '') {
            /* get user information to send password detail */
            $table = 'mst_users';
            $fields = array('user_id', 'first_name', 'last_name', 'user_email', 'user_type', 'email_verified', 'user_status', 'user_password', 'role_id');
            $condition = "user_email = '" . $user_email . "'AND user_type= 1";
            $arr_password_data = $this->common_model->getRecords($table, $fields, $condition, $order_by = '', $limit = '', $debug = 0);

            $data_global = $this->common_model->getGlobalSettings();

            if (count($arr_password_data)) {
                $code = rand(9991, 999899);
                $activation_code = time();
                $table_name = 'mst_users';
                $update_data = array('reset_password_code' => $activation_code, 'password_change_security_code' => $code);
                $condition_to_pass = array("user_id" => $arr_password_data[0]['user_id']);
                $this->common_model->updateRow($table_name, $update_data, $condition_to_pass);
                $reset_password_link = '<a href="' . base_url() . 'reset-password/' . base64_encode($activation_code) . '">Click here</a>';
                $lang_id = '17';
                $site_url = '<a href="' . base_url() . '">' . base_url() . '</a>';
                $reserved_words = array
                    (
                    "||USER_NAME||" => $arr_password_data[0]['first_name'] . " " . $arr_password_data[0]['last_name'],
                    "||FIRST_NAME||" => $arr_password_data[0]['first_name'],
                    "||LAST_NAME||" => $arr_password_data[0]['last_name'],
                    "||USER_EMAIL||" => $arr_password_data[0]['user_email'],
                    "||RESET_PASSWORD_LINK||" => $reset_password_link,
                    "||SITE_TITLE||" => $data['global']['site_title'],
                    "||SITE_URL||" => $site_url
                );
                $template_title = 'forgot-password';
                $arr_emailtemplate_data = $this->common_model->getEmailTemplateInfo($template_title, $lang_id, $reserved_words);
                $recipeinets = $arr_password_data[0]['user_email'];
                $from = array("email" => $data_global['site_email']);
                $subject = $arr_emailtemplate_data['subject'];
                $message = $arr_emailtemplate_data['content'];
                $mail = $this->common_model->sendEmail($recipeinets, $from, $subject, $message);

                if ($user_email) {
                    //$arr_to_return = array('error_code' => "0", 'msg' => "We have sent reset password link on your email " . $user_email);
                    $arr_to_return = array('error_code' => "0", 'msg' => "Thanks! We have just sent you a password reset link by email.");
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail');
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                }
            } else {
                $arr_to_return = array('error_code' => "1", 'msg' => 'This email address is not registered with system.');
                $response_arr = array('Response' => $arr_to_return);
                echo json_encode($response_arr);
            }
        } else {
            $arr_to_return = array('msg' => 'no input data');
            $response_arr = array('Response' => $arr_to_return);
            echo json_encode($response_arr);
        }
    }

    /* Start :: PRE App Cms */

    public function wsCms($page_name) {
        $arrToReturn = array();
        $lang_id = '17';
        $condition = array('A.page_alias' => $page_name, 'A.status' => 'Published', "B.lang_id" => $lang_id);
        $arrCms = $this->web_services_model->getCmsPageDetailsFront($condition);
        $arr_return['page_alias'] = isset($arrCms[0]['page_alias']) ? $arrCms[0]['page_alias'] : '';
        $arr_return['page_title'] = isset($arrCms[0]['page_title']) ? $arrCms[0]['page_title'] : '';
        $arr_return['page_content'] = isset($arrCms[0]['page_content']) ? strip_tags(nl2br((stripslashes(preg_replace("/[\\n\\r]+/", " ", $arrCms[0]['page_content']))))) : '';
        if (is_array($arrCms) && count($arrCms)) {
            $arrToReturn = array('error' => 0, 'data' => $arr_return);
        } else {
            $arrToReturn = array('error' => 1, 'message' => "This cms page not found.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Contact Us */

    public function wsClientContactUs() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $message = $this->input->post('message');
        $data = $this->common_model->commonFunction();
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        if ($user_id != '') {
            $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
            /* single row fix */
            $arr_user_detail = end($arr_user_detail);
            /* insert contact details */
            $arr_fields = array(
                "first_name" => $arr_user_detail['first_name'],
                "last_name" => $arr_user_detail['last_name'],
                "subject" => "I Need Help With My Strategy",
                "phone" => "",
                "mail_id" => $arr_user_detail['user_email'],
                "message" => addslashes($message),
                "user_id_fk" => addslashes($user_id),
                "reply_status" => '0',
                "help_with_strategy" => "1",
                "contact_us_flag" => "1",
                "date" => date('Y-m-d H:i:s')
            );
            $last_insert_id = $this->web_services_model->insertContactUs($arr_fields);
            if ($last_insert_id > 0) {
                $arr_admin_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => '1', "user_type" => '2'));
                /* single row fix */
                $arr_admin_detail = end($arr_admin_detail);
                $admin_login_link = '<a href="' . base_url() . 'backend/login" target="_new">Log in to ' . base_url() . 'administration</a>';

                $macros_array_detail = array();
                $macros_array_detail = $this->common_model->getRecords('mst_email_template_macros', 'macros,value', $condition_to_pass = '', $order_by = '', $limit = '', $debug = 0);
                $macros_array = array();
                foreach ($macros_array_detail as $row) {
                    $macros_array[$row['macros']] = $row['value'];
                }
                $reserved_words = array();

                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($data['global']['site_title']),
                    "||SITE_PATH||" => base_url(),
                    "||ADMIN_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
                    "||ADMIN_EMAIL||" => $arr_admin_detail['user_email'],
                    "||MESSAGE||" => $message,
                    "||ADMIN_LOGIN_LINK||" => $admin_login_link
                );
                $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
                /* getting mail subect and mail message using email template title and lang_id and reserved works */
                $email_content = $this->common_model->getEmailTemplateInfo('client-contact-us', 17, $reserved_words);
                /* sending the mail to deleting user */
                /* 1.recipient array. 2.From array containing email and name, 3.Mail subject 4.Mail Body */
                $mail = $this->common_model->sendEmail(array($data['global']['contact_email']), array("email" => $arr_user_detail['user_email'], "name" => stripslashes($data['global']['site_title'])), $email_content['subject'], $email_content['content']);

                $arrToReturn = array('error' => 0, 'message' => "Thanks for sending your message to PRE. We will respond to you shortly.");
            } else {
                $arrToReturn = array('error' => 1, 'message' => "Could not send the message, please try again.");
            }
        } else {
            $arrToReturn = array('error' => 1, 'message' => "Please enter required fields.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start:: PRE App Add Property */

    public function wsAddPreProperty() {
        $arrToReturn = array();

        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $salesforce_user_id = $this->input->post('salesforce_user_id');
        $property_nickname = $this->input->post('property_nickname');
        $property_address = $this->input->post('property_address');
        $full_address = $this->input->post('full_address');
        $property_suburb = $this->input->post('property_suburb');
        $property_state = $this->input->post('property_state');
        $property_postcode = $this->input->post('property_postcode');
        $ownership_type = $this->input->post('ownership_type');
        $number_of_bedrooms = $this->input->post('number_of_bedrooms');
        $number_of_bathrooms = $this->input->post('number_of_bathrooms');
        $number_of_carparks = $this->input->post('number_of_carparks');
        $property_type = $this->input->post('property_type');
        $property_specification = $this->input->post('property_specification');
        $land_size = str_replace(",", "", $this->input->post('land_size'));
        $dwelling_footprint = str_replace(",", "", $this->input->post('dwelling_footprint'));
        $property_purchase_price = str_replace(",", "", $this->input->post('property_purchase_price'));
        $property_purchase_date = $this->input->post('property_purchase_date');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $old_property_img = $this->input->post('old_property_img');
        $corelogic_property_id = $this->input->post('corelogic_property_id');
        $locality_id = $this->input->post('locality_id');
        $locality = $this->input->post('locality');
        $interest_rate = $this->input->post('interest_rate');
        $loan_amount = str_replace(",", "", $this->input->post('loan_amount'));

        /** New Parameter * */
        $entity_name = $this->input->post('entity_name');
        $ownership_status = $this->input->post('ownership_status');
        $purchased_via = $this->input->post('purchased_via');
        $lender = $this->input->post('lender');
        $total_loan_limit = str_replace(",", "", $this->input->post('total_loan_limit'));
        $current_loan_balance = str_replace(",", "", $this->input->post('current_loan_balance'));
        /** New Parameter * */
        if ($property_specification == 'Investment') {
            $ppor_status = 'Yes';
            $weekly_rent = str_replace(",", "", $this->input->post('weekly_rent'));
        } else {
            $ppor_status = $this->input->post('ppor_status');
            $weekly_rent = "0";
        }

        $valuation = str_replace(",", "", $this->input->post('valuation')); //$coreLogicSQM*$this->input->post('land_size');
//        if (str_replace(",", "", $this->input->post('loan_amount')) != '' && str_replace(",", "", $this->input->post('loan_amount')) != '0') {
//            $loan_amount = str_replace(",", "", $this->input->post('loan_amount'));
//        } else {
//            $loan_amount = "0";
//        }

        $purchase_date = str_replace('/', '-', $property_purchase_date);
        $purchase_date = date('Y-m-d', strtotime($purchase_date));

//        $next_repayment_date = str_replace('/', '-', $next_repayment_date);
//        $next_repayment_date = date('Y-m-d', strtotime($next_repayment_date));

        $datetime1 = new DateTime(date("Y-m-d"));
        $datetime2 = new DateTime($purchase_date);
        $difference = $datetime1->diff($datetime2);
        $years_woned = ($difference->y * 365);
        $years_woned = $years_woned + ($difference->m * 30);
        $years_woned = round(($years_woned + $difference->d) / 365, 1);

        if ($land_size != '0') {
            $purchase_SQM = 0;
            $value_SQM = 0;
            if (strtoupper($property_type) == 'LAND') {
                //calculating purchase sqm
                if ($property_purchase_price != 0 && $property_purchase_price != '') {
                    $purchase_SQM = ($property_purchase_price) / ($land_size);
                }
                if ($valuation != 0 && $valuation != '') {
                    $value_SQM = ($valuation) / ($land_size);
                }
            } else {
                if ($property_purchase_price != 0 && $property_purchase_price != '') {
                    $purchase_SQM = ($property_purchase_price * 0.3) / ($land_size);
                }
                if ($valuation != 0 && $valuation != '') {
                    $value_SQM = ($valuation * 0.3) / ($land_size);
                }
            }
        } else {
            $purchase_SQM = 0;
            $value_SQM = 0;
        }

        $flag = "true";
        /* Check same property added */
        if (($full_address) != '' && ($property_nickname) != '') {

//            $chk_name_condition = "(property_name='" . trim($property_nickname) . "' || full_address='" . trim($full_address) . "')";
//            $check_property_added1 = $this->common_model->getRecords('mst_properties','*',$chk_name_condition);

            $chk_name_condition = "((property_name='" . trim($property_nickname) . "' || full_address='" . trim($full_address) . "') && (user_id=" . $user_id . "))";
            $check_property_added = $this->web_services_model->getCheckRecordsExists($chk_name_condition, $user_id);

            if (count($check_property_added) == 0) {
                $flag = "true";
            } else {
                $flag = "false";
            }
        }

        if ($flag == 'true') {
            if ($user_id) {
                /* insert contact details */
                $arr_fields = array(
                    "user_id" => $user_id,
                    "property_name" => addslashes($property_nickname),
                    "property_type" => $property_type,
                    "corelogic_property_id" => $corelogic_property_id,
                    "locality_id" => $locality_id,
                    "locality" => addslashes($locality),
                    "property_specification" => $property_specification,
                    "ppor_status" => $ppor_status,
                    "property_address" => addslashes($property_address),
                    "full_address" => addslashes($full_address),
                    "property_suburb" => addslashes($property_suburb),
                    "property_state" => addslashes($property_state),
                    "property_postcode" => $property_postcode,
                    "ownership_type" => $ownership_type,
                    "land_size" => $land_size,
                    "dwelling_footprint" => $dwelling_footprint,
                    "bedrooms" => $number_of_bedrooms,
                    "bathrooms" => $number_of_bathrooms,
                    "carparks" => $number_of_carparks,
                    "purchase_price" => $property_purchase_price,
                    "valuation" => $valuation,
                    "value_SQM" => $value_SQM,
                    "purchase_SQM" => $purchase_SQM,
                    "current_equity" => ($valuation) - $loan_amount,
                    "capital_growth" => round(((($valuation - $property_purchase_price) / $property_purchase_price) * 100), 1),
                    "LVR" => round(($loan_amount / $valuation) * 100, 1),
                    "IRR" => round(((($valuation - $property_purchase_price) / $years_woned) / (($property_purchase_price * 0.2))) * 100, 1),
                    "ROI" => round((($valuation - $property_purchase_price) / (($property_purchase_price * 0.2))) * 100, 1),
                    "years_woned" => $years_woned,
                    "weekly_rent" => $weekly_rent,
                    "purchase_date" => $purchase_date,
                    "latitude" => $latitude,
                    "longitude" => $longitude,
                    "created_at" => date('Y-m-d H:i:s'),
                    "income_cron_updated" => date("Y-m-d", strtotime('+ 7 days')),
                    "cron_updated_status" => '0',
                    "entity_name" => $entity_name,
                    "ownership_status" => $ownership_status,
                    "purchased_via" => $purchased_via,
                    "lender" => $lender,
                    "total_loan_limit" => $total_loan_limit,
                    "current_loan_balance" => $current_loan_balance
                );

                $property_insert_id = $this->web_services_model->insertProperty($arr_fields);
                if ($loan_amount != '') {
                    /* Start :: Insert property dept data */
                    $arr_to_property_dept_insert = array(
                        "property_id" => $property_insert_id,
                        "user_id" => $user_id,
                        "loan_amount" => $loan_amount,
                        "initial_loan_amount" => $loan_amount,
                        "interest_rate" => $interest_rate,
//                    "rate_term" => $rate_term,
                        "loan_type" => "Fixed",
//                    "payment_frequency" => strtolower($payment_frequency),
//                    "next_payment_date" => date("Y-m-d", strtotime($next_payment_date . '+ 7 days')),
                        "added_date" => date("Y-m-d"),
                    );

                    $debt_id = $this->common_model->insertRow($arr_to_property_dept_insert, 'trans_debts');
                }
                /* Start :: Insert property income data */
                if ($weekly_rent != '') {
                    $arr_to_property_income_insert = array(
                        "property_id" => $property_insert_id,
                        "user_id" => $user_id,
                        "weekly_rent" => $weekly_rent,
                        "added_date" => date("Y-m-d")
                    );
                    $income_id = $this->common_model->insertRow($arr_to_property_income_insert, 'trans_incomes');
                }
                /* End :: Insert property income data */
                if ($property_insert_id > 0) {
                    if ($_FILES['property_image'] != '') {
                        if (isset($_FILES['property_image']['tmp_name'])) {
                            $property_image = rand() . ".jpg";
                            move_uploaded_file($_FILES['property_image']['tmp_name'], "media/front/img/property-images/" . $property_image);
                            $property_imgs = $property_image;
                        } else if (($this->input->post('property_image') != '')) {
                            $base = $this->input->post('property_image');
                            $binary = base64_decode($base);
                            $property_image = uniqid() . rand() . '.jpg';
                            $file = fopen("media/front/img/property-images/" . $property_image, 'wb');
                            fwrite($file, $binary);
                            fclose($file);
                            $property_imgs = $property_image;
                        } else {
                            if ($old_property_img != '') {
                                $property_imgs = $old_property_img;
                            } else {
                                $property_imgs = '';
                            }
                        }

                        $fields = array(
                            'property_id' => $property_insert_id,
                            'image_name' => $property_imgs,
                            'status' => '1',
                            'sequence' => '1',
                        );
                        $table = "trans_property_images";
                        $this->common_model->insertRow($fields, $table);
                    } else {
                        $fields = array(
                            'property_id' => $property_insert_id,
                            'image_name' => 'default_img.png',
                            'status' => '1',
                            'sequence' => '1',
                        );
                        $table = "trans_property_images";
                        $this->common_model->insertRow($fields, $table);
                    }

                    /** Capital Growth Graph Generate * */
                    $table = 'trans_graph_details';
                    $insert_land_data = array(
                        'value' => $valuation,
                        'property_id' => $property_insert_id,
                        'user_id' => $user_id,
                        'type' => '2',
                        'date' => date("Y-m-d")
                    );
                    $this->common_model->insertRow($insert_land_data, $table);

                    /** Capital Growth Graph Generate * */
                    /* Start:: Selesforce Integration */

                    $mySforceConnection = new SforceEnterpriseClient();
                    $mySoapClient = $mySforceConnection->createConnection("soapclient/enterprise.wsdl.xml");

                    // Simple example of session management - first call will do
                    // login, refresh will use session ID and location cached in
                    // PHP session
                    if (($this->session->userdata('partnerSessionId')) !== null) {
                        $location = $this->session->userdata('partnerLocation');
                        $sessionId = $this->session->userdata('partnerSessionId');

                        $mySforceConnection->setEndpoint($location);
                        $mySforceConnection->setSessionHeader($sessionId);
                    } else {
                        $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);

                        $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                        $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                    }

                    $sPropertyObject = new stdclass();
                    $sPropertyObject->Name = $property_nickname;
                    $sPropertyObject->Property_Owner__c = $salesforce_user_id;
                    $sPropertyObject->Bathrooms__c = $number_of_bathrooms;
                    $sPropertyObject->Bedrooms__c = $number_of_bedrooms;
                    $sPropertyObject->Capital_Growth__c = round(((($valuation - $property_purchase_price) / $property_purchase_price) * 100), 1);
                    $sPropertyObject->Carparks__c = $number_of_carparks;
                    $sPropertyObject->Created_At__c = date("Y-m-d") . 'T' . date("H:i:s");
                    $sPropertyObject->Current_Equity__c = ($valuation) - $loan_amount;
                    $sPropertyObject->Dwelling_Footprint__c = $dwelling_footprint;
                    $sPropertyObject->IRR__c = ((($valuation - $property_purchase_price) / $years_woned) / (($property_purchase_price * 20) / 10000));
                    $sPropertyObject->Land_Size__c = $land_size;
                    $sPropertyObject->LVR__c = ($loan_amount / $valuation) * 100;
                    $sPropertyObject->Property_Address__c = htmlentities($property_address);
                    $sPropertyObject->Property_Postcode__c = $property_postcode;
                    $sPropertyObject->Property_Specification__c = $property_specification;
                    $sPropertyObject->Property_State__c = htmlentities($property_state);
                    $sPropertyObject->Property_Suburb__c = htmlentities($property_suburb);
                    $sPropertyObject->Property_Type__c = htmlentities($property_type);
                    $sPropertyObject->Ownership_Type__c = htmlentities($ownership_type);
                    $sPropertyObject->Purchase_Date__c = date("Y-m-d", strtotime($purchase_date)) . 'T' . date("H:i:s");
                    $sPropertyObject->Purchase_Price__c = $property_purchase_price;
                    $sPropertyObject->ROI__c = (($valuation - $property_purchase_price) / (($property_purchase_price * 20))) * 10000;
                    $sPropertyObject->User_ID__c = $salesforce_user_id;
                    $sPropertyObject->Valuation__c = $valuation;
                    $sPropertyObject->Value_SQM__c = $value_SQM;
                    $sPropertyObject->Purchase_SQM__c = $purchase_SQM;
                    $sPropertyObject->Years_Woned__c = $years_woned;


                    $createPropertyResponse = $mySforceConnection->create(array($sPropertyObject), 'PRE_APP_Properties__c');
                    //print_r($createResponse);
                    $salesforcePropertyId = $createPropertyResponse[0]->id;

                    /* Start:: Update salesforce id in local database for further use. */
                    $table_name = 'mst_properties';
                    $update_data = array(
                        'salesforce_property_id' => $salesforcePropertyId
                    );
                    $condition = array(
                        'property_id' => $property_insert_id
                    );
                    if ($property_insert_id) {
                        $this->common_model->updateRow($table_name, $update_data, $condition);
                    }
                    /* End:: Update salesforce id in local database for further use. */

                    if ($loan_amount != '') {
                        /* Start:: Insert debt */
                        $sDebtObject = new stdclass();
                        $sDebtObject->User_ID__c = $salesforce_user_id;
                        $sDebtObject->Property_ID__c = $salesforcePropertyId;
                        $sDebtObject->Interest_Rate__c = $interest_rate;
                        $sDebtObject->Loan_Amount__c = $loan_amount;
                        $sDebtObject->Initial_Loan_Amount__c = $loan_amount;
                        $sDebtObject->Loan_Type__c = 'Fixed';
                        $sDebtObject->Added_Date__c = date('Y-m-d');


                        $createDebtResponse = $mySforceConnection->create(array($sDebtObject), 'PRE_Debt__c');
                        //print_r($createResponse);
                        $salesforceDebtId = $createDebtResponse[0]->id;
                        /*
                         * Update salesforce id in local database for further use.
                         */
                        $table_name = 'trans_debts';
                        $update_data = array(
                            'salesforce_debt_id' => $salesforceDebtId
                        );
                        $condition = array(
                            'property_id' => $property_insert_id,
                            'user_id' => $user_id
                        );
                        if ($property_insert_id) {
                            $this->common_model->updateRow($table_name, $update_data, $condition);
                        }
                    }
                    /* End:: Insert debt */

                    /* Start:: Insert Income */
                    if ($property_specification == 'Investment') {
                        
                        $sIncomeObject = new stdclass();
                        $sIncomeObject->PRE_Property_Income__c = $salesforcePropertyId;
                        $sIncomeObject->User_ID__c = $salesforce_user_id;
                        $sIncomeObject->Property_ID__c = $salesforcePropertyId;
                        $sIncomeObject->Weekly_Rent__c = $weekly_rent;

                        $createIncomeResponse = $mySforceConnection->create(array($sIncomeObject), 'PRE_Income__c');
                        $salesforceIncomeId = $createIncomeResponse[0]->id;

                        $table_name = 'trans_incomes';

                        $update_data = array(
                            'salesforce_income_id' => $salesforceIncomeId
                        );
                        $condition = array(
                            'property_id' => $property_insert_id,
                            'user_id' => $user_id
                        );
                        if ($property_insert_id) {
                            $this->common_model->updateRow($table_name, $update_data, $condition);
                        }
                    }
                    /* End:: Insert Income */

                    /* End  :: Selesforce Integration */
                    $arrToReturn = array('error' => 0, 'message' => "Property has been added successfully.");
                } else {
                    $arrToReturn = array('error' => 1, 'message' => "Property is not added successfully, please try again.");
                }
            } else {
                $arrToReturn = array('error' => 1, 'message' => "Please enter required fields.");
            }
        } else {
            $arrToReturn = array('error' => 2, 'message' => "This property already exist in the system. Add a different one.");
        }

        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Edit Property */

    public function wsEditPreProperty() {

        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $arrToReturn = array();
        $property_id = intval($this->input->post('property_id'));
        $salesforce_user_id = $this->input->post('salesforce_user_id');
        $property_nickname = $this->input->post('property_nickname');
        $old_property_nickname = $this->input->post('old_property_nickname');
        $property_address = $this->input->post('property_address');
        $old_property_address = $this->input->post('old_property_address');
        $full_address = $this->input->post('full_address');
        $old_full_address = $this->input->post('old_full_address');
        $property_suburb = $this->input->post('property_suburb');
        $property_state = $this->input->post('property_state');
        $property_postcode = $this->input->post('property_postcode');
        $number_of_bedrooms = $this->input->post('number_of_bedrooms');
        $number_of_bathrooms = $this->input->post('number_of_bathrooms');
        $number_of_carparks = $this->input->post('number_of_carparks');
        $property_type = $this->input->post('property_type');
        $property_specification = $this->input->post('property_specification');
        $land_size = str_replace(',', "", $this->input->post('land_size'));
        $dwelling_footprint = str_replace(',', "", $this->input->post('dwelling_footprint'));
        $property_purchase_price = str_replace(',', "", $this->input->post('property_purchase_price'));
        $property_purchase_date = $this->input->post('property_purchase_date');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $old_property_img = $this->input->post('old_property_img');
        $next_repayment_date = $this->input->post('next_repayment_date');
        $corelogic_property_id = $this->input->post('corelogic_property_id');
        $locality_id = $this->input->post('locality_id');
        $locality = $this->input->post('locality');
        $valuation = str_replace(',', "", $this->input->post('valuation')); //$coreLogicSQM*$this->input->post('land_size');
        $ownership_type = $this->input->post('ownership_type');

        $loan_amount = str_replace(',', "", $this->input->post('loan_amount'));
        $interest_rate = $this->input->post('interest_rate');

        /** New Parameter * */
        $entity_name = $this->input->post('entity_name');
        $ownership_status = $this->input->post('ownership_status');
        $purchased_via = $this->input->post('purchased_via');
        $lender = $this->input->post('lender');
        $total_loan_limit = str_replace(",", "", $this->input->post('total_loan_limit'));
        $current_loan_balance = str_replace(",", "", $this->input->post('current_loan_balance'));
        /** New Parameter * */
        if ($property_specification == 'Investment') {
            $ppor_status = 'Yes';
            $weekly_rent = str_replace(",", "", $this->input->post('weekly_rent'));
        } else {
            $ppor_status = $this->input->post('ppor_status');
            $weekly_rent = "0";
        }

        //calculaing the purchse and current sqm value.
        if ($land_size != '0') {
            $purchase_SQM = 0;
            $value_SQM = 0;
            if (strtoupper($property_type) == 'LAND') {

                //calculating purchase sqm
                if ($property_purchase_price != 0 && $property_purchase_price != '') {
                    $purchase_SQM = ($property_purchase_price) / ($land_size);
                }
                if ($valuation != 0 && $valuation != '') {
                    $value_SQM = ($valuation) / ($land_size);
                }
            } else {
                if ($property_purchase_price != 0 && $property_purchase_price != '') {
                    $purchase_SQM = ($property_purchase_price * 0.3) / ($land_size);
                }
                if ($valuation != 0 && $valuation != '') {
                    $value_SQM = ($valuation * 0.3) / ($land_size);
                }
            }
        } else {
            $purchase_SQM = 0;
            $value_SQM = 0;
        }
        $purchase_date = str_replace('/', '-', $property_purchase_date);
        $purchase_date = date('Y-m-d', strtotime($purchase_date));

//        $next_repayment_date = str_replace('/', '-', $next_repayment_date);
//        $next_repayment_date = date('Y-m-d', strtotime($next_repayment_date));

        $datetime1 = new DateTime(date("Y-m-d"));
        $datetime2 = new DateTime($purchase_date);
        $difference = $datetime1->diff($datetime2);

        $years_woned = ($difference->y * 365);
        $years_woned = $years_woned + ($difference->m * 30);
        $years_woned = round(($years_woned + $difference->d) / 365, 1);

        $arr_property = $this->common_model->getRecords('mst_properties', '', array("property_id" => $property_id));
        $arr_property = end($arr_property);

        /* Check same property added */
        if (($full_address) == ($old_full_address) && ($property_nickname) == ($old_property_nickname)) {
            $flag = "true";
        } elseif ($property_nickname != $old_property_nickname && $full_address == $old_full_address) {
            $chk_name_condition = "((user_id='" . $user_id . "') AND (property_name='" . trim($property_nickname) . "'))";
            $check_property_added = $this->web_services_model->getCheckRecordsExists($chk_name_condition);
            if (count($check_property_added) == 0) {
                $flag = "true";
            } else {
                $flag = "false";
            }
        } elseif ($property_nickname == $old_property_nickname && $full_address != $old_full_address) {
            $chk_name_condition = "((user_id='" . $user_id . "') AND (full_address='" . trim($full_address) . "'))";

            $check_property_added = $this->web_services_model->getCheckRecordsExists($chk_name_condition);
            if (count($check_property_added) == 0) {
                $flag = "true";
            } else {
                $flag = "false";
            }
        } elseif (($full_address) != ($old_full_address) && ($property_nickname) != ($old_property_nickname)) {
            $chk_name_condition = "((user_id='" . $user_id . "') AND (property_name='" . trim($property_nickname) . "') || (full_address='" . trim($full_address) . "'))";
            $check_property_added = $this->web_services_model->getCheckRecordsExists($chk_name_condition);
            if (count($check_property_added) == 0) {
                $flag = "true";
            } else {
                $flag = "false";
            }
        }

        $property_nickname = $this->input->post('property_nickname');
        $property_address = $this->input->post('property_address');

        if ($flag == 'true') {

            if (count($arr_property) && $property_id) {
                /* update property details */
                $salesforce_property_id = $arr_property['salesforce_property_id'];

//                switch (strtolower($this->input->post('payment_frequency'))) {
//                    case 'weekly':
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
//                        break;
//                    case 'monthly':
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 30 days'));
//                        break;
//                    case 'fortnightly':
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 14 days'));
//                        break;
//                    default:
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
//                }

                $arr_to_update = array(
                    "property_name" => ($property_nickname),
                    "user_id" => $user_id,
                    "property_address" => addslashes($property_address),
                    "full_address" => addslashes($full_address),
                    "property_type" => $property_type,
                    "ownership_type" => $ownership_type,
                    "corelogic_property_id" => $corelogic_property_id,
                    "locality_id" => $locality_id,
                    "locality" => $locality,
                    "property_specification" => $property_specification,
                    "ppor_status" => $ppor_status,
                    "property_suburb" => addslashes($property_suburb),
                    "property_state" => addslashes($property_state),
                    "property_postcode" => addslashes($property_postcode),
                    "land_size" => $land_size,
                    "dwelling_footprint" => $dwelling_footprint,
                    "bedrooms" => $number_of_bedrooms,
                    "bathrooms" => $number_of_bathrooms,
                    "carparks" => $number_of_carparks,
                    "purchase_price" => $property_purchase_price,
                    "valuation" => $valuation,
                    "value_SQM" => $value_SQM,
                    "purchase_SQM" => $purchase_SQM,
                    "current_equity" => ($valuation) - $loan_amount,
                    "capital_growth" => round(((($valuation - $property_purchase_price) / $property_purchase_price) * 100), 1),
                    "LVR" => ($loan_amount / $valuation) * 100,
                    "IRR" => ((($valuation - $property_purchase_price) / $years_woned) / (($property_purchase_price * 20) / 10000)),
                    "ROI" => (($valuation - $property_purchase_price) / (($property_purchase_price * 20))) * 10000,
                    "years_woned" => $years_woned,
                    "weekly_rent" => $weekly_rent,
                    "purchase_date" => $purchase_date,
                    "entity_name" => $entity_name,
                    "ownership_status" => $ownership_status,
                    "purchased_via" => $purchased_via,
                    "lender" => $lender,
                    "total_loan_limit" => $total_loan_limit,
                    "current_loan_balance" => $current_loan_balance,
                    "latitude" => $latitude,
                    "longitude" => $longitude,
                    'updated_at' => date("Y-m-d H:i:s")
                );

                $is_updated = false;
                if ($property_id != '') {
                    $is_updated = $this->common_model->updateRow("mst_properties", $arr_to_update, array("property_id" => $property_id));
                }

                if ($loan_amount != '') {
                    /*
                     * Update property dept data
                     */
                    $arr_to_property_dept_update = array(
                        "loan_amount" => $loan_amount,
                        "initial_loan_amount" => $loan_amount,
                        "interest_rate" => $interest_rate,
                    );
                    $this->common_model->updateRow("trans_debts", $arr_to_property_dept_update, array("property_id" => $property_id, "user_id" => $user_id));
                }
                /*
                 * Update property income data
                 */

                $arr_to_property_income_update = array(
                    "weekly_rent" => $weekly_rent
                );
                $this->common_model->updateRow("trans_incomes", $arr_to_property_income_update, array("property_id" => $property_id, "user_id" => $user_id));

                if ($is_updated) {
                    $this->common_model->deleteRows(array($property_id), "trans_property_images", "property_id");
                    if ($_FILES['property_image'] != '') {
                        if (isset($_FILES['property_image']['tmp_name'])) {
                            $property_image = rand() . ".jpg";
                            move_uploaded_file($_FILES['property_image']['tmp_name'], "media/front/img/property-images/" . $property_image);
                            $property_imgs = $property_image;
                        } else if (($this->input->post('property_image') != '')) {
                            $base = $this->input->post('property_image');
                            $binary = base64_decode($base);
                            $property_image = uniqid() . rand() . '.jpg';
                            $file = fopen("media/front/img/property-images/" . $property_image, 'wb');
                            fwrite($file, $binary);
                            fclose($file);
                            $property_imgs = $property_image;
                        } else {
                            if ($old_property_img != '') {
                                $property_imgs = $old_property_img;
                            } else {
                                $property_imgs = '';
                            }
                        }

                        $arr_to_update = array(
                            'property_id' => $property_id,
                            'image_name' => $property_imgs,
                            'status' => '1',
                            'sequence' => '1',
                        );
//                        $is_updated = $this->common_model->updateRow("trans_property_images", $arr_to_update, array("property_id" => $property_id));
                        $this->common_model->insertRow($arr_to_update, "trans_property_images");
                    }

                    /** Capital Growth Graph Generate * */
                    $table = 'trans_graph_details';
                    $insert_land_data = array(
                        'value' => $valuation,
                        'date' => date("Y-m-d")
                    );
                    $this->common_model->updateRow($table, $insert_land_data, array("property_id" => $property_id, "user_id" => $user_id));

                    /** Capital Growth Graph Generate * */
                    /* Start :: Salesforce Integration */


                    $mySforceConnection = new SforceEnterpriseClient();
                    $mySforceConnection->createConnection("soapclient/enterprise.wsdl.xml");

                    // Simple example of session management - first call will do
                    // login, refresh will use session ID and location cached in
                    // PHP session
                    if (($this->session->userdata('partnerSessionId')) !== null) {
                        $location = $this->session->userdata('partnerLocation');
                        $sessionId = $this->session->userdata('partnerSessionId');

                        $mySforceConnection->setEndpoint($location);
                        $mySforceConnection->setSessionHeader($sessionId);
                    } else {
                        $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);

                        $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                        $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                    }


                    $sPropertyObject = new stdclass();
                    $sPropertyObject->Name = $property_nickname;
                    $sPropertyObject->Bathrooms__c = $number_of_bathrooms;
                    $sPropertyObject->Bedrooms__c = $number_of_bedrooms;
                    $sPropertyObject->Capital_Growth__c = round(((($valuation - $property_purchase_price) / $property_purchase_price) * 100), 1);
                    $sPropertyObject->Carparks__c = $number_of_carparks;
                    $sPropertyObject->Current_Equity__c = ($valuation) - $loan_amount;
                    $sPropertyObject->Dwelling_Footprint__c = $dwelling_footprint;
                    $sPropertyObject->IRR__c = ((($valuation - $property_purchase_price) / $years_woned) / (($property_purchase_price * 20) / 10000));
                    $sPropertyObject->Land_Size__c = $land_size;
                    $sPropertyObject->LVR__c = ($loan_amount / $valuation) * 100;
                    $sPropertyObject->Property_Address__c = htmlentities($property_address);
                    $sPropertyObject->Property_Postcode__c = htmlentities($property_postcode);
                    $sPropertyObject->Property_Specification__c = $property_specification;
                    $sPropertyObject->Property_State__c = htmlentities($property_state);
                    $sPropertyObject->Property_Suburb__c = htmlentities($property_suburb);
                    $sPropertyObject->Property_Type__c = htmlentities($property_type);
                    $sPropertyObject->Ownership_Type__c = htmlentities($ownership_type);
                    $sPropertyObject->Purchase_Date__c = date("Y-m-d", strtotime($purchase_date)) . 'T' . date("H:i:s");
                    $sPropertyObject->Purchase_Price__c = $property_purchase_price;
                    $sPropertyObject->ROI__c = (($valuation - $property_purchase_price) / (($property_purchase_price * 20))) * 10000;
                    $sPropertyObject->Updated_At__c = date("Y-m-d") . 'T' . date("H:i:s");
                    $sPropertyObject->User_ID__c = $salesforce_user_id;
                    $sPropertyObject->Valuation__c = $valuation;
                    $sPropertyObject->Value_SQM__c = $value_SQM;
                    $sPropertyObject->Purchase_SQM__c = $purchase_SQM;
                    $sPropertyObject->Years_Woned__c = $years_woned;

                    $sPropertyObject->Id = $salesforce_property_id;

                    $mySforceConnection->update(array($sPropertyObject), 'PRE_APP_Properties__c');

                    $arrToReturn = array('error' => 0, 'message' => "Property has been updated successfully.");

                    /** update debt */
                    if ($loan_amount != '') {
                        $arr_property_debt = $this->common_model->getRecords('trans_debts', '', array("property_id" => $property_id, "user_id" => $user_id));

                        if (count($arr_property_debt) > 0) {

                            $sDebtObject = new stdclass();
                            $sDebtObject->Interest_Rate__c = $interest_rate;
                            $sDebtObject->Loan_Amount__c = $loan_amount;
                            $sDebtObject->Loan_Type__c = 'Fixed';
                            $sDebtObject->Id = $arr_property_debt[0]['salesforce_debt_id'];
                            $createDebtResponse = $mySforceConnection->update(array($sDebtObject), 'PRE_Debt__c');
                        } else {

                            $sDebtObject = new stdclass();
                            $sDebtObject->User_ID__c = $salesforce_user_id;
                            $sDebtObject->Property_ID__c = $salesforce_property_id;
                            $sDebtObject->Interest_Rate__c = $interest_rate;
                            $sDebtObject->Loan_Amount__c = $loan_amount;
                            $sDebtObject->Initial_Loan_Amount__c = $loan_amount;
                            $sDebtObject->Loan_Type__c = 'Fixed';
                            $sDebtObject->Added_Date__c = date('Y-m-d');

                            $createDebtResponse = $mySforceConnection->create(array($sDebtObject), 'PRE_Debt__c');

                            $salesforceDebtId = $createDebtResponse[0]->id;
                            /*
                             * insert salesforce id in local database for further use.
                             */
                            $arr_to_property_dept_insert = array(
                                "loan_amount" => $loan_amount,
                                "property_id" => $property_id,
                                "salesforce_debt_id" => $salesforceDebtId,
                                "user_id" => $user_id,
                                "interest_rate" => $interest_rate,
                                "added_date" => date("Y-m-d"),
                            );
                            $this->common_model->insertRow($arr_to_property_dept_insert, "trans_debts");
                        }
                    }
                    /** Update Income */
                    $arr_property_income = $this->common_model->getRecords('trans_incomes', '', array("property_id" => $property_id, "user_id" => $user_id));
                    if (count($arr_property_income) > 0) {

                        $sIncomeObject = new stdclass();
                        $sIncomeObject->Weekly_Rent__c = $weekly_rent;
                        $sIncomeObject->Id = $arr_property_income[0]['salesforce_income_id'];
                        $createDebtResponse = $mySforceConnection->update(array($sIncomeObject), 'PRE_Income__c');
                    } else {

                        $sIncomeObject = new stdclass();
                        $sIncomeObject->User_ID__c = $salesforce_user_id;
                        $sIncomeObject->PRE_Property_Income__c = $salesforce_property_id;
                        $sIncomeObject->Property_ID__c = $salesforce_property_id;
                        $sIncomeObject->Interest_Rate__c = $interest_rate;

                        $createIncomeResponse = $mySforceConnection->create(array($sIncomeObject), 'PRE_Income__c');

                        $salesforceIncomeId = $createIncomeResponse[0]->id;

                        $arr_to_property_income_insert = array(
                            "user_id" => $user_id,
                            "property_id" => $property_id,
                            "salesforce_income_id" => $salesforceIncomeId,
                            "weekly_rent" => $weekly_rent,
                            "added_date" => date('Y-m-d')
                        );
                        $this->common_model->insertRow($arr_to_property_income_insert, "trans_incomes");
                    }
                    /* End :: Salesforce Integration */
                } else {
                    $arrToReturn = array('error' => 1, 'message' => "Property is not added successfull, please try again.");
                }
            } else {
                $arrToReturn = array('error' => 1, 'message' => "Please enter required fields.");
            }
        } else {
            $arrToReturn = array('error' => 2, 'message' => "This property already exist in the system. Add a different one.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App List Property */

    public function wsGetPrePropertyListByType() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        if ($this->input->post('property_type') != "") {
            $property_type = $this->input->post('property_type');
            $condition_to_pass = array('p.property_type' => $property_type, "p.user_id" => intval($user_id));
        } else {
            $condition_to_pass = array("p.user_id" => intval($user_id));
        }
        $fields_to_pass = array('p.property_id', 'p.property_type', 'pi.image_name', 'p.property_name', 'p.property_address', 'p.property_suburb', 'p.property_state', 'p.property_postcode');
        $arrProperties = $this->web_services_model->getPropertyDetailsByType($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        if (count($arrProperties) > 0) {
            foreach ($arrProperties as $key => $property_data) {
                $arr_return[$key]['property_id'] = isset($property_data['property_id']) ? $property_data['property_id'] : '';
                $arr_return[$key]['property_type'] = isset($property_data['property_type']) ? $property_data['property_type'] : '';
                $arr_return[$key]['property_image'] = isset($property_data['image_name']) ? $property_data['image_name'] : '';
                $arr_return[$key]['property_name'] = isset($property_data['property_name']) ? stripslashes($property_data['property_name']) : '';
                $arr_return[$key]['property_address'] = isset($property_data['property_address']) ? stripslashes($property_data['property_address']) : '';
                $arr_return[$key]['property_suburb'] = isset($property_data['property_suburb']) ? stripslashes($property_data['property_suburb']) : '';
                $arr_return[$key]['property_state'] = isset($property_data['property_state']) ? stripslashes($property_data['property_state']) : '';
                $arr_return[$key]['property_postcode'] = isset($property_data['property_postcode']) ? stripslashes($property_data['property_postcode']) : '';
            }
            $arrToReturn = array('error' => 0, 'msg' => 'success', 'property_data' => $arr_return);
        } else {
            $arrToReturn = array('error' => 1, 'message' => "Property not found.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Edit Property Details */

    public function wsGetPrePropertyEditDetailById() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $property_id = $this->input->post('property_id');

        $condition_to_pass = array('p.property_id' => $property_id, "p.user_id" => intval($user_id));
        $fields_to_pass = array('p.*,pi.image_name,td.salesforce_debt_id,td.debt_id,td.loan_amount,td.initial_loan_amount,td.interest_rate,td.loan_type,td.rate_term,td.payment_frequency,td.next_payment_date,td.next_payment_amount,ti.weekly_rent');
        $arrProperties = end($this->web_services_model->getPropertyDetailsByType($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));

        $current_yield = (($arrProperties['weekly_rent'] * 52) / ($arrProperties['valuation'])) * 100;
        $rent_per_month = ((($arrProperties['weekly_rent'] / 7) * 365) / 12);

        if (count($arrProperties) > 0) {
            $current_equity = $this->formatCash($arrProperties['current_equity']);
//            if ($arrProperties['current_equity'] < 1000) {
//                $current_equity = round($arrProperties['current_equity']);
//            } else if ($arrProperties['current_equity'] < 100000) {
//                $current_equity = round($arrProperties['current_equity'] / 1000, 1) . 'K';
//            } else {
//                $current_equity = round($arrProperties['current_equity'] / 1000000, 1) . 'M';
//            }

            $final_rent_per_month = $this->formatCash($rent_per_month);
//            if ($rent_per_month < 1000) {
//                $final_rent_per_month = round($rent_per_month);
//            } else if ($rent_per_month < 100000) {
//                $final_rent_per_month = round($rent_per_month / 1000, 1) . 'K';
//            } else {
//                $final_rent_per_month = round($rent_per_month / 1000000, 1) . 'M';
//            }

            $arr_return['property_id'] = isset($arrProperties['property_id']) ? $arrProperties['property_id'] : '';
            $arr_return['user_id'] = isset($arrProperties['user_id']) ? $arrProperties['user_id'] : '';
            $arr_return['property_type'] = isset($arrProperties['property_type']) ? $arrProperties['property_type'] : '';
            $arr_return['ownership_type'] = isset($arrProperties['ownership_type']) ? $arrProperties['ownership_type'] : '';
            $arr_return['property_image'] = isset($arrProperties['image_name']) ? $arrProperties['image_name'] : '';
            $arr_return['property_name'] = isset($arrProperties['property_name']) ? stripslashes($arrProperties['property_name']) : '';
            $arr_return['property_address'] = isset($arrProperties['property_address']) ? stripslashes($arrProperties['property_address']) : '';
            $arr_return['full_address'] = isset($arrProperties['full_address']) ? stripslashes($arrProperties['full_address']) : '';
            $arr_return['property_suburb'] = isset($arrProperties['property_suburb']) ? stripslashes($arrProperties['property_suburb']) : '';
            $arr_return['property_state'] = isset($arrProperties['property_state']) ? stripslashes($arrProperties['property_state']) : '';
            $arr_return['property_postcode'] = isset($arrProperties['property_postcode']) ? stripslashes($arrProperties['property_postcode']) : '';
            $arr_return['property_specification'] = isset($arrProperties['property_specification']) ? $arrProperties['property_specification'] : '';
            $arr_return['land_size'] = isset($arrProperties['land_size']) ? number_format($arrProperties['land_size'], 0, ".", ",") : '';
            $arr_return['dwelling_footprint'] = isset($arrProperties['dwelling_footprint']) ? number_format($arrProperties['dwelling_footprint'], 0, ".", ",") : '';
            $arr_return['bedrooms'] = isset($arrProperties['bedrooms']) ? $arrProperties['bedrooms'] : '';
            $arr_return['bathrooms'] = isset($arrProperties['bathrooms']) ? $arrProperties['bathrooms'] : '';
            $arr_return['carparks'] = isset($arrProperties['carparks']) ? $arrProperties['carparks'] : '';
            $arr_return['purchase_price'] = isset($arrProperties['purchase_price']) ? number_format($arrProperties['purchase_price'], 0, ".", ",") : '0';
            $arr_return['weekly_rent'] = isset($arrProperties['weekly_rent']) ? round($arrProperties['weekly_rent']) : '0';
            $arr_return['edit_valuation'] = isset($arrProperties['valuation']) ? number_format($arrProperties['valuation'], 0, ".", ",") : '0';
            $arr_return['valuation'] = isset($arrProperties['valuation']) ? number_format($arrProperties['valuation'], 0, ".", ",") : '0';
            $arr_return['edit_value_SQM'] = isset($arrProperties['value_SQM']) ? number_format($arrProperties['value_SQM'], 0, ".", ",") : '0';
            $arr_return['value_SQM'] = isset($arrProperties['value_SQM']) ? number_format($arrProperties['value_SQM'], 0, ".", ",") : '0';
            $arr_return['current_equity'] = isset($current_equity) ? ($current_equity) : '0';
            $arr_return['capital_growth'] = isset($arrProperties['capital_growth']) ? $arrProperties['capital_growth'] : '0';
            $arr_return['LVR'] = isset($arrProperties['LVR']) ? $arrProperties['LVR'] : '0';
            $arr_return['IRR'] = isset($arrProperties['IRR']) ? round($arrProperties['IRR']) : '0';
            $arr_return['ROI'] = isset($arrProperties['ROI']) ? round($arrProperties['ROI']) : '0';
            $arr_return['years_woned'] = isset($arrProperties['years_woned']) ? $arrProperties['years_woned'] : '';
            $arr_return['purchase_date'] = isset($arrProperties['purchase_date']) ? date("d/m/Y", strtotime($arrProperties['purchase_date'])) : '';
            $arr_return['entity_name'] = isset($arrProperties['entity_name']) ? ($arrProperties['entity_name']) : '';
            $arr_return['ownership_status'] = isset($arrProperties['ownership_status']) ? ($arrProperties['ownership_status']) : '';
            $arr_return['purchased_via'] = isset($arrProperties['purchased_via']) ? ($arrProperties['purchased_via']) : '';
            $arr_return['lender'] = isset($arrProperties['lender']) ? ($arrProperties['lender']) : '';
            $arr_return['total_loan_limit'] = isset($arrProperties['total_loan_limit']) ? number_format($arrProperties['total_loan_limit'], 0, ".", ",") : '';
            $arr_return['current_loan_balance'] = isset($arrProperties['current_loan_balance']) ? number_format($arrProperties['current_loan_balance'], 0, ".", ",") : '';

            $arr_return['current_yield'] = isset($current_yield) ? round($current_yield, 2) : '0';
            $arr_return['rent_per_month'] = isset($final_rent_per_month) ? ($final_rent_per_month) : '0';


            if ($arrProperties['loan_amount'] != '0') {
                $arr_return['loan_amount'] = isset($arrProperties['loan_amount']) ? number_format($arrProperties['loan_amount'], 0, ".", ",") : '';
                $arr_return['interest_rate'] = isset($arrProperties['interest_rate']) ? $arrProperties['interest_rate'] : '';
                $arr_return['next_repayment_date'] = isset($arrProperties['next_payment_date']) ? date("d/m/Y", strtotime($arrProperties['next_payment_date'])) : '';
                $arr_return['rate_term'] = isset($arrProperties['rate_term']) ? $arrProperties['rate_term'] : '';
                $arr_return['payment_frequency'] = isset($arrProperties['payment_frequency']) ? ucfirst($arrProperties['payment_frequency']) : '';
            } else {
                $arr_return['loan_amount'] = '';
                $arr_return['interest_rate'] = '';
                $arr_return['next_repayment_date'] = '';
                $arr_return['rate_term'] = '';
                $arr_return['payment_frequency'] = '';
            }

            $arr_return['next_payment_amount'] = isset($arrProperties['next_payment_amount']) ? $arrProperties['next_payment_amount'] : '';
            $arr_return['locality'] = isset($arrProperties['locality']) ? ucfirst($arrProperties['locality']) : '';
            $arr_return['corelogic_property_id'] = isset($arrProperties['corelogic_property_id']) ? ucfirst($arrProperties['corelogic_property_id']) : '';
            $arr_return['locality_id'] = isset($arrProperties['locality_id']) ? ucfirst($arrProperties['locality_id']) : '';

            /** Graph Value Arjun Changes By 05-12-2016  * */
            $capital_growth_value = $this->web_services_model->getCapitalGraphRecords(intval($user_id), '2', $property_id);
            /** Graph Value * */
            foreach ($capital_growth_value as $key => $capital_graph) {
                $capital_growth_graph[$key]['value'] = isset($capital_graph['value_total']) ? $capital_graph['value_total'] : '';
                $capital_growth_graph[$key]['date'] = isset($capital_graph['date']) ? date("Y", strtotime($capital_graph['date'])) : '';
            }
//            $land_growth_percentage = end($this->web_services_model->getGoalsDetails($table_to_pass = '', $fields_to_pass = 'target_achieved', array('user_id' => intval($user_id), 'goal' => '2'), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
//            $land_growth['land_growth_percentage'] = isset($arrProperties['capital_growth']) ? round($arrProperties['capital_growth']) : '0';

            $purchase_value['value'] = isset($arrProperties['purchase_price']) ? round($arrProperties['purchase_price']) : '0';
            $purchase_value['date'] = isset($arrProperties['purchase_date']) ? date("Y", strtotime($arrProperties['purchase_date'])) : '';


            $arrToReturn = array('error' => 0, 'msg' => 'success', 'property_data' => $arr_return, 'capital_growth_graph' => $capital_growth_graph, 'purchase_value' => $purchase_value);
        } else {
            $arrToReturn = array('error' => 1, 'message' => "Property not found.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Delete Property */

    public function wsDeletePreProperty($property_id) {
        $arrToReturn = array();
        if (!$property_id) {
            $property_id = $this->input->post('property_id');
        }
        $arr_property_detail = $this->common_model->getRecords("mst_properties", "", array("property_id" => intval($property_id)));

        if (count($arr_property_detail) > 0) {
            /* Start:: Delete details from salesforce */
            $mySforceConnection = new SforceEnterpriseClient();
            $mySoapClient = $mySforceConnection->createConnection("soapclient/enterprise.wsdl.xml");
            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (($this->session->userdata('partnerSessionId')) !== null) {
                $location = $this->session->userdata('partnerLocation');
                $sessionId = $this->session->userdata('partnerSessionId');

                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);
            } else {
                $mySforceConnection->login(USERNAME, PASSWORD);

                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
            }
            $salesforceId = $arr_property_detail[0]['salesforce_property_id'];

            if ($salesforceId != "") {
                $response = $mySforceConnection->delete($salesforceId);
            }
            /* End:: Delete details from salesforce */
            $this->web_services_model->deleteProperty($property_id);
            $this->common_model->deleteRows($property_id, 'trans_graph_details', 'property_id');
            $arr_property_images = $this->common_model->getRecords("trans_property_images", "", array("property_id" => intval($property_id)));
            if (count($arr_property_images)) {
                foreach ($arr_property_images as $image) {
                    $this->web_services_model->deleteImage($property_id, $image['image_id']);
                    $image_path = 'media/front/img/property-images/';
                    @unlink($image_path . $image['image_name']);
                    @unlink($image_path . 'small-thumbs' . $image['image_name']);
                    @unlink($image_path . 'thumbs' . $image['image_name']);
                }
            }
            $arrToReturn = array('error' => 0, 'message' => "Property deleted successfully.");
        } else {
            $arrToReturn = array('error' => 1, 'message' => "Property not found.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Get Loan,Rent,Equity List */

    public function wsPreGetLoanRentEquityReviewList() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $check_flag = $this->input->post('check_flag');

        switch ($check_flag) {
            case 'Loan':
                $condition_to_pass = array('p.user_id' => intval($user_id), 'p.ppor_status' => 'Yes', 'loan_amount<>' => '0');
                $fields_to_pass = array('p.property_id,pi.image_name,p.property_name,p.property_address,p.valuation,p.property_suburb,property_state,property_postcode,td.loan_amount');
                $arrProperties = $this->web_services_model->getPropertyDetailsByType($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

                if (count($arrProperties) > 0) {
                    foreach ($arrProperties as $key => $property_data) {
                        $arr_return[$key]['property_id'] = isset($property_data['property_id']) ? $property_data['property_id'] : '';
                        $arr_return[$key]['property_image'] = isset($property_data['image_name']) ? $property_data['image_name'] : '';
                        $arr_return[$key]['property_name'] = isset($property_data['property_name']) ? stripslashes($property_data['property_name']) : '';
                        $arr_return[$key]['property_address'] = isset($property_data['property_address']) ? stripslashes($property_data['property_address']) : '';
                        $arr_return[$key]['property_suburb'] = isset($property_data['property_suburb']) ? stripslashes($property_data['property_suburb']) : '';
                        $arr_return[$key]['property_state'] = isset($property_data['property_state']) ? stripslashes($property_data['property_state']) : '';
                        $arr_return[$key]['property_postcode'] = isset($property_data['property_postcode']) ? stripslashes($property_data['property_postcode']) : '';
                        $arr_return[$key]['valuation'] = isset($property_data['loan_amount']) ? number_format($property_data['loan_amount'], 0, ".", ",") : '0';
                    }
                    $arrToReturn = array('error' => 0, 'msg' => 'success', 'property_data' => $arr_return);
                } else {
                    $arrToReturn = array('error' => 1, 'message' => "Property not found.");
                }

                echo json_encode($arrToReturn);

                break;

            case 'Rent' :
                $condition_to_pass = array('p.user_id' => intval($user_id), "p.property_specification" => 'Investment', 'p.weekly_rent<>' => '0.0');
                $fields_to_pass = array('p.property_id,pi.image_name,p.property_name,p.property_address,p.weekly_rent,p.property_suburb,property_state,property_postcode');
                $arrProperties = $this->web_services_model->getPropertyDetailsByType($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

                if (count($arrProperties) > 0) {
                    foreach ($arrProperties as $key => $property_data) {
                        $arr_return[$key]['property_id'] = isset($property_data['property_id']) ? $property_data['property_id'] : '';
                        $arr_return[$key]['property_image'] = isset($property_data['image_name']) ? $property_data['image_name'] : '';
                        $arr_return[$key]['property_name'] = isset($property_data['property_name']) ? stripslashes($property_data['property_name']) : '';
                        $arr_return[$key]['property_address'] = isset($property_data['property_address']) ? stripslashes($property_data['property_address']) : '';
                        $arr_return[$key]['property_suburb'] = isset($property_data['property_suburb']) ? stripslashes($property_data['property_suburb']) : '';
                        $arr_return[$key]['property_state'] = isset($property_data['property_state']) ? stripslashes($property_data['property_state']) : '';
                        $arr_return[$key]['property_postcode'] = isset($property_data['property_postcode']) ? stripslashes($property_data['property_postcode']) : '';
                        $arr_return[$key]['weekly_rent'] = isset($property_data['weekly_rent']) ? number_format($property_data['weekly_rent'], 0, ".", ",") : '0';
                    }
                    $arrToReturn = array('error' => 0, 'msg' => 'success', 'property_data' => $arr_return);
                } else {
                    $arrToReturn = array('error' => 1, 'message' => "Property not found.");
                }
                echo json_encode($arrToReturn);
                break;
            case 'Equity' :
                $condition_to_pass = array('p.user_id' => intval($user_id));
                $fields_to_pass = array('p.property_id,pi.image_name,p.property_name,p.property_address,p.weekly_rent,p.current_equity,p.property_suburb,property_state,property_postcode');
                $arrProperties = $this->web_services_model->getPropertyDetailsByType($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

                if (count($arrProperties) > 0) {
                    foreach ($arrProperties as $key => $property_data) {
                        $arr_return[$key]['property_id'] = isset($property_data['property_id']) ? $property_data['property_id'] : '';
                        $arr_return[$key]['property_image'] = isset($property_data['image_name']) ? $property_data['image_name'] : '';
                        $arr_return[$key]['property_name'] = isset($property_data['property_name']) ? stripslashes($property_data['property_name']) : '';
                        $arr_return[$key]['property_address'] = isset($property_data['property_address']) ? stripslashes($property_data['property_address']) : '';
                        $arr_return[$key]['property_suburb'] = isset($property_data['property_suburb']) ? stripslashes($property_data['property_suburb']) : '';
                        $arr_return[$key]['property_state'] = isset($property_data['property_state']) ? stripslashes($property_data['property_state']) : '';
                        $arr_return[$key]['property_postcode'] = isset($property_data['property_postcode']) ? stripslashes($property_data['property_postcode']) : '';
                        $arr_return[$key]['current_equity'] = isset($property_data['current_equity']) ? number_format($property_data['current_equity'], 0, ".", ",") : '0';
                    }
                    $arrToReturn = array('error' => 0, 'msg' => 'success', 'property_data' => $arr_return);
                } else {
                    $arrToReturn = array('error' => 1, 'message' => "Property not found.");
                }
                echo json_encode($arrToReturn);
                break;
        }
    }

    /* Start :: PRE App Get Loan,Rent,Equity Review */

    public function wsPreGetLoanRentEquityReview() {
        $data['global'] = $this->common_model->getGlobalSettings();
        $arrToReturn = array();
        $property_ids = $this->input->post('property_id');
        $review_type = $this->input->post('review_type');

        $str = '';
        if ($property_ids != '') {
            $arrProperties = $this->web_services_model->getPropertyDetails($property_ids);

            $str = '<link href="' . base_url() . 'media/backend/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />';
            $str .= '<link href="' . base_url() . 'media/backend/css/bootstrap.min.css" rel="stylesheet" type="text/css" />';
            switch ($review_type) {
                case 'Loan' :
                    $str .= '<div class="box-body table-responsive" style=padding:10px>';
                    $str .= '<div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">';
                    $str .= '<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">';
                    $str .= '<thead>';
                    $str .= '<tr role="row">';
                    $str .= '<th style="width:10%"  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Property Id">Property Id</th>';
                    $str .= '<th style="width:20%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Property Name">Property Name</th>';
                    $str .= '<th style="width:15%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Full Name">Full Name</th>';
                    $str .= '<th style="width:15%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Loan Amount">Loan Amount</th>';
                    $str .= '<th style="width:10%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Interest Rate">Interest Rate</th>';
                    $str .= '<th style="width:15%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Loan Type">Loan Type</th>';
                    $str .= '</tr>';
                    $str .= '</thead>';

                    $str .= '<tbody>';

                    for ($i = 0; $i < count($arrProperties); $i++) {

                        $str .= '<tr>';
                        $str .= '<td>' . $arrProperties[$i]['property_id'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['property_name'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['first_name'] . " " . $arrProperties[$i]['last_name'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['loan_amount'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['interest_rate'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['loan_type'] . '</td>';
                        $str .= '</tr>';
                    }
                    $str .= '</tbody> ';
                    $str .= '</table>';
                    $str .= '</div>';
                    $str .= '</div>';
                    $str .= '<div style="padding:10px">Thanks,</div>';
                    $str .= '<div style="padding:10px">' . stripslashes($data['global']['site_title']) . '</div>';
                    break;

                case 'Rent' :

                    $str .= '<div class="box-body table-responsive" style=padding:10px>';
                    $str .= '<div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">';
                    $str .= '<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">';
                    $str .= '<thead>';
                    $str .= '<tr role="row">';
                    $str .= '<th style="width:10%"  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Property Id">Property Id</th>';
                    $str .= '<th style="width:20%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Property Name">Property Name</th>';
                    $str .= '<th style="width:20%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Full Name">Full Name</th>';
                    $str .= '<th style="width:15%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Weekly Rent">Weekly Rent</th>';
                    $str .= '<th style="width:15%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Address">Address</th>';
                    $str .= '</tr>';
                    $str .= '</thead>';

                    $str .= '<tbody>';
                    for ($i = 0; $i < count($arrProperties); $i++) {
                        $str .= '<tr>';
                        $str .= '<td>' . $arrProperties[$i]['property_id'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['property_name'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['first_name'] . " " . $arrProperties[$i]['last_name'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['weekly_rent'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['property_address'] . '</td>';
                        $str .= '</tr>';
                    }
                    $str .= '</tbody> ';
                    $str .= '</table>';
                    $str .= '</div>';
                    $str .= '</div>';
                    $str .= '<div style="padding:10px">Thanks,</div>';
                    $str .= '<div style="padding:10px">' . stripslashes($data['global']['site_title']) . '</div>';

                    break;
                case 'Equity' :
                    $str .= '<div class="box-body table-responsive" style=padding:10px>';
                    $str .= '<div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">';
                    $str .= '<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">';
                    $str .= '<thead>';
                    $str .= '<tr role="row">';
                    $str .= '<th style="width:10%"  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Property Id">Property Id</th>';
                    $str .= '<th style="width:20%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Property Name">Property Name</th>';
                    $str .= '<th style="width:20%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Full Name">Full Name</th>';
                    $str .= '<th style="width:15%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Equity">Equity</th>';
                    $str .= '<th style="width:15%" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Address">Address</th>';
                    $str .= '</tr>';
                    $str .= '</thead>';

                    $str .= '<tbody>';
                    for ($i = 0; $i < count($arrProperties); $i++) {
                        $str .= '<tr>';
                        $str .= '<td>' . $arrProperties[$i]['property_id'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['property_name'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['first_name'] . " " . $arrProperties[$i]['last_name'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['current_equity'] . '</td>';
                        $str .= '<td>' . $arrProperties[$i]['property_address'] . '</td>';
                        $str .= '</tr>';
                    }
                    $str .= '</tbody> ';
                    $str .= '</table>';
                    $str .= '</div>';
                    $str .= '</div>';
                    $str .= '<div style="padding:10px">Thanks,</div>';
                    $str .= '<div style="padding:10px">' . stripslashes($data['global']['site_title']) . '</div>';
                    break;
            }
            /* getting admin details */
            $arr_admin_detail = $this->common_model->getRecords("mst_users", "", array("user_id" => '1'));
            if (count($arr_admin_detail) > 0) {
                /* setting reserved_words for email content */
                $lang_id = 17;
                $macros_array_detail = array();
                $macros_array_detail = $this->common_model->getRecords('mst_email_template_macros', 'macros,value', $condition_to_pass = '', $order_by = '', $limit = '', $debug = 0);
                $macros_array = array();
                foreach ($macros_array_detail as $row) {
                    $macros_array[$row['macros']] = $row['value'];
                }
                $reserved_words = array();

                $reserved_arr = array(
                    "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
                    "||ADMIN_NAME||" => $arr_admin_detail[0]['first_name'] . ' ' . $arr_admin_detail[0]['last_name']
                );

                $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
                $template_title = 'common-message';
                /* getting mail subject and mail message using email template title and lang_id and reserved works */
                $email_content = $this->common_model->getEmailTemplateInfo($template_title, 17, $reserved_words);

                $temaplate_content = $email_content['content'];
                $message_body = $temaplate_content . $str;
                $mail = $this->common_model->sendEmail(array($arr_admin_detail[0]['user_email']), array("email" => $data['global']['site_email'], "name" => stripslashes($data['global']['site_title'])), $email_content['subject'], $message_body);
            }

            $arrToReturn = array('error' => 0, 'msg' => 'Thanks for sending your request to PRE. One of our friendly team members will contact you shortly.');
        } else {
            $arrToReturn = array('error' => 1, 'msg' => "Property not found.");
        }

        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Goals List */

    public function wsPreGoals() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        /** Get Goals Count Start * */
        $goals_update_details = $this->web_services_model->getGoalsDetails($table_to_pass = '', $fields_to_pass = '*', array('user_id' => intval($user_id)), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

        foreach ($goals_update_details as $goals) {
            $arr_property_detail = end($this->common_model->getRecords("mst_properties", "sum(valuation) as total_valuation,sum(land_size) as total_land_size,count(property_id) as total_property", array("user_id" => intval($goals['user_id']), 'ppor_status' => 'Yes')));
            $arr_debt_detail = end($this->common_model->getRecords("trans_debts", "sum(loan_amount) as total_loan_amount", array("user_id" => intval($goals['user_id']))));

            $income_details = end($this->common_model->getRecords('trans_incomes', $fields_to_pass = 'sum(weekly_rent) as total_rent', array("user_id" => intval($goals['user_id'])), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));


            switch ($goals['goal']) {
                case 1:
                    $edit_value['edit_current_status'] = isset($arr_property_detail['total_valuation']) ? round($arr_property_detail['total_valuation']) : '0';
                    $target_amount['target_amount'] = isset($goals['target_amount']) ? number_format($goals['target_amount'], 0, ".", ",") : '0';
                    break;
                case 2:
                    $edit_value['edit_current_status'] = isset($arr_property_detail['total_property']) ? ($arr_property_detail['total_property']) : '0';
                    $target_amount['target_amount'] = isset($goals['target_amount']) ? number_format($goals['target_amount'], 0, ".", ",") : '0';
                    break;
                case 3:
                    $edit_value['edit_current_status'] = isset($income_details['total_rent']) ? round($income_details['total_rent']) : '0';
                    $target_amount['target_amount'] = isset($goals['target_amount']) ? number_format($goals['target_amount'], 0, ".", ",") : '0';
                    break;
                case 4:
                    $LVR = ($arr_debt_detail['total_loan_amount'] / $arr_property_detail['total_valuation']) * 100;
//                    $LVR_avg = ($LVR / $arr_property_detail['total_property']);
                    $target_amount['target_amount'] = isset($goals['target_amount']) ? number_format($goals['target_amount'], 0, ".", ",") : '0';
                    $edit_value['edit_current_status'] = isset($LVR) ? round($LVR) : '0';
                    break;
            }

            $target_achieved = ($edit_value['edit_current_status'] / round($goals['target_amount'])) * 100;

            $update_data = array(
                "current_status" => $edit_value['edit_current_status'],
                "target_achieved" => round($target_achieved),
            );
            $condition = array('goal_id' => intval($goals['goal_id']), 'user_id' => intval($user_id));
            $this->common_model->updateRow('trans_goals', $update_data, $condition);
        }

        $arr_property_detail = end($this->common_model->getRecords("mst_properties", "sum(valuation) as total_valuation,sum(land_size) as total_land_size,sum(LVR) as total_LVR", array("user_id" => intval($user_id), 'ppor_status' => 'Yes')));
        $properties_count = end($this->common_model->getRecords("mst_properties", "count(property_id) as total_properties", array("user_id" => intval($user_id), 'ppor_status' => 'Yes')));
        $arr_debt_detail = end($this->common_model->getRecords("trans_debts", "sum(loan_amount) as total_loan_amount", array("user_id" => intval($user_id))));
        $current_status_networth = $arr_property_detail['total_valuation'] - $arr_debt_detail['total_loan_amount'];
        /** Get Goals Count Start * */
        $goals_details = $this->web_services_model->getGoalsDetails($table_to_pass = '', $fields_to_pass = '*', array('user_id' => intval($user_id)), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        foreach ($goals_details as $goals) {
            $income_details = end($this->common_model->getRecords('trans_incomes', $fields_to_pass = 'sum(weekly_rent) as total_rent', array("user_id" => intval($goals['user_id'])), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));

            switch ($goals['goal']) {

                case 1:
                    if ($arr_property_detail['total_valuation'] < 1000) {
                        $current_status = round($arr_property_detail['total_valuation']);
                    } else if ($arr_property_detail['total_valuation'] < 100000) {
                        $current_status = round($arr_property_detail['total_valuation'] / 1000, 1) . 'K';
                    } else {
                        $current_status = round($arr_property_detail['total_valuation'] / 1000000, 1) . 'M';
                    }

                    if ($goals['target_amount'] < 1000) {
                        $Target_Amount = round($goals['target_amount']);
                    } else if ($goals['target_amount'] < 100000) {
                        $Target_Amount = round($goals['target_amount'] / 1000, 1) . 'K';
                    } else {
                        $Target_Amount = round($goals['target_amount'] / 1000000, 1) . 'M';
                    }

                    $assets['goal_id'] = isset($goals['goal_id']) ? ($goals['goal_id']) : '0';
                    $assets['user_id'] = isset($goals['user_id']) ? ($goals['user_id']) : '0';
                    $assets['goal'] = isset($goals['goal']) ? ($goals['goal']) : '0';
                    if ($goals['target_date'] == '0000-00-00' || $goals['target_date'] == '1970-01-01') {
                        $assets['target_date'] = '00/00/0000';
                    } else {
                        $assets['target_date'] = isset($goals['target_date']) ? date("d/m/Y", strtotime($goals['target_date'])) : '0000/00/00';
                    }
                    $assets['target_amount'] = isset($Target_Amount) ? $Target_Amount : '0';
                    $assets['current_status'] = isset($current_status) ? ($current_status) : '0';
                    $assets['target_achieved'] = isset($goals['target_achieved']) ? round($goals['target_achieved']) : '0';
                    break;

                case 2:

                    if ($goals['target_amount'] < 1000) {
                        $Target_Amount = round($goals['target_amount']);
                    } else if ($goals['target_amount'] < 100000) {
                        $Target_Amount = round($goals['target_amount'] / 1000, 1) . 'K';
                    } else {
                        $Target_Amount = round($goals['target_amount'] / 1000000, 1) . 'M';
                    }

                    $properties['goal_id'] = isset($goals['goal_id']) ? ($goals['goal_id']) : '0';
                    $properties['user_id'] = isset($goals['user_id']) ? ($goals['user_id']) : '0';
                    $properties['goal'] = isset($goals['goal']) ? ($goals['goal']) : '0';
                    if ($goals['target_date'] == '0000-00-00' || $goals['target_date'] == '1970-01-01') {
                        $properties['target_date'] = '00/00/0000';
                    } else {
                        $properties['target_date'] = isset($goals['target_date']) ? date("d/m/Y", strtotime($goals['target_date'])) : '0000/00/00';
                    }

                    $properties['target_amount'] = isset($Target_Amount) ? ($Target_Amount) : '0';
                    $properties['current_status'] = isset($properties_count['total_properties']) ? ($properties_count['total_properties']) : '0';
                    $properties['target_achieved'] = isset($goals['target_achieved']) ? round($goals['target_achieved']) : '0';
                    break;

                case 3:
                    if ($goals['current_status'] < 1000) {
                        $current_status = round($income_details['total_rent']);
                    } else if ($goals['current_status'] < 100000) {
                        // Anything less than a million
                        $current_status = round($income_details['total_rent'] / 1000, 1) . 'K';
                    } else {
                        // Anything less than a million
                        $current_status = round($income_details['total_rent'] / 1000000, 1) . 'M';
                    }

                    if ($goals['target_amount'] < 1000) {
                        $target_amount = round($goals['target_amount']);
                    } else if ($goals['target_amount'] < 100000) {
                        // Anything less than a million
                        $target_amount = round($goals['target_amount'] / 1000, 1) . 'K';
                    } else {
                        // Anything less than a million
                        $target_amount = round($goals['target_amount'] / 1000000, 1) . 'M';
                    }

                    $rental_income['goal_id'] = isset($goals['goal_id']) ? ($goals['goal_id']) : '0';
                    $rental_income['user_id'] = isset($goals['user_id']) ? ($goals['user_id']) : '0';
                    $rental_income['goal'] = isset($goals['goal']) ? ($goals['goal']) : '0';
                    if ($goals['target_date'] == '0000-00-00' || $goals['target_date'] == '1970-01-01') {
                        $rental_income['target_date'] = '00/00/0000';
                    } else {
                        $rental_income['target_date'] = isset($goals['target_date']) ? date("d/m/Y", strtotime($goals['target_date'])) : '0000/00/00';
                    }
                    $rental_income['target_amount'] = isset($target_amount) ? ($target_amount) : '0';
                    $rental_income['current_status'] = isset($current_status) ? ($current_status) : '0';
                    $rental_income['target_achieved'] = isset($goals['target_achieved']) ? round($goals['target_achieved']) : '0';

                    break;

                case 4:
//                    if ($goals['target_amount'] < 1000) {
                    $Target_Amount = round($goals['target_amount']);
//                    } else if ($goals['target_amount'] < 100000) {
//                        $Target_Amount = round($goals['target_amount'] / 1000, 1) . 'K';
//                    } else {
//                        $Target_Amount = round($goals['target_amount'] / 1000000, 1) . 'M';
//                    }

                    $LVR_Goal['goal_id'] = isset($goals['goal_id']) ? ($goals['goal_id']) : '0';
                    $LVR_Goal['user_id'] = isset($goals['user_id']) ? ($goals['user_id']) : '0';
                    $LVR_Goal['goal'] = isset($goals['goal']) ? ($goals['goal']) : '0';
                    if ($goals['target_date'] == '0000-00-00' || $goals['target_date'] == '1970-01-01') {
                        $LVR_Goal['target_date'] = '00/00/0000';
                    } else {
                        $LVR_Goal['target_date'] = isset($goals['target_date']) ? date("d/m/Y", strtotime($goals['target_date'])) : '0000/00/00';
                    }

                    $LVR_AVG = round($arr_property_detail['total_LVR'] / $properties_count['total_properties']);
                    $LVR_Goal['target_amount'] = isset($Target_Amount) ? ($Target_Amount) : '0';
                    $LVR_Goal['current_status'] = isset($LVR_AVG) ? ($LVR_AVG) : '0';
                    $LVR_Goal['target_achieved'] = isset($goals['target_achieved']) ? round($goals['target_achieved']) : '0';
                    break;
            }
        }

        if ($goals_details > 0) {
            $arrToReturn = array('error' => 0, 'msg' => 'success', 'assets' => $assets, 'Properties' => $properties, 'rental_income' => $rental_income, 'LVR' => $LVR_Goal);
        } else {
            $arrToReturn = array('error' => 1, 'message' => "Property not found.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Goals Add */

    public function wsPreGoalsAdd() {
//        error_reporting(E_ALL);
//        ini_set("display_errors","on");
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');


        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $goal_type = $this->input->post('goal_type');
        $target_amount = str_replace(",", "", $this->input->post('target_amount'));
        $current_status = $this->input->post('current_status');
        $comments = $this->input->post('comments');
        $salesforce_user_id = $this->input->post('salesforce_user_id');

        $target_date = str_replace('/', '-', $this->input->post('target_date'));
        $target_date = date('Y-m-d', strtotime($target_date));

        $target_achieved = ($current_status / $target_amount) * 100;

        if ($user_id) {

            /* insert contact details */
            $arr_fields = array(
                "user_id" => $user_id,
                "goal" => $goal_type,
                "target_date" => $target_date,
                "target_amount" => $target_amount,
                "current_status" => $current_status,
                "target_achieved" => $target_achieved,
                "comments" => addslashes($comments),
            );
            $insert_id = $this->web_services_model->insertGoals($arr_fields);

            setlocale(LC_MONETARY, 'en_US.UTF-8');
            switch ($goal_type) {
                case 1:
                    $goal_name = "Assets";

                    $target_achieved = money_format('%.2n', $target_achieved);
                    $target_amount = money_format('%.2n', $target_amount);
                    $current_status = money_format('%.2n', $current_status);
                    break;
                case 2:
                    $goal_name = "Properties";
                    $target_achieved = $target_achieved;
                    $target_amount = $target_amount;
                    $current_status = $current_status;
                    break;
                case 3:
                    $goal_name = "Rental Income";
                    $target_achieved = money_format('%.2n', $target_achieved);
                    $target_amount = money_format('%.2n', $target_amount);
                    $current_status = money_format('%.2n', $current_status);
                    break;
                case 4:
                    $goal_name = "LVR";
                    $target_achieved = $target_achieved . '%';
                    $target_amount = $target_amount . '%';
                    $current_status = $current_status . '%';
                    break;
            }

            /* Start :: Salesforce Integration */
            $mySforceConnection = new SforceEnterpriseClient();
            $mySforceConnection->createConnection("soapclient/enterprise.wsdl.xml");

            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (($this->session->userdata('partnerSessionId')) !== null) {
                $location = $this->session->userdata('partnerLocation');
                $sessionId = $this->session->userdata('partnerSessionId');

                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);
            } else {
                $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);

                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
            }

            /** Insert Goal */
            $sGoalsObject = new stdclass();
            $sGoalsObject->Name = $goal_name;
            $sGoalsObject->Goal_Owner__c = $salesforce_user_id;
            $sGoalsObject->User_ID__c = $salesforce_user_id;
            //$sGoalsObject->goal__c = $goal_name;
            $sGoalsObject->target_date__c = $target_date;
            $sGoalsObject->target_amount__c = $target_amount;
            $sGoalsObject->current_status__c = $current_status;
            $sGoalsObject->target_achieved__c = $target_achieved;


            $createGoalResponse = $mySforceConnection->create(array($sGoalsObject), 'PRE_Goal__c');

            $salesforceGoalId = $createGoalResponse[0]->id;

            $table_name = 'trans_goals';
            $update_data = array(
                'salesforce_goal_id' => $salesforceGoalId
            );
            $condition = array(
                'goal_id' => $insert_id,
                'user_id' => $user_id
            );
            if ($insert_id) {
                $this->common_model->updateRow($table_name, $update_data, $condition);
            }
            /* End :: Salesforce Integration */
            if ($insert_id != '') {
                $arrToReturn = array('error' => 0, 'msg' => "Goal set successfully.");
            } else {
                $arrToReturn = array('error' => 1, 'msg' => "Goal is not set, please try again.");
            }
        } else {
            $arrToReturn = array('error' => 1, 'msg' => "Please enter required fields.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Goals Edit */

    public function wsPreGoalsEdit() {
        $this->load->model('common_model');
        $arrToReturn = array();
        $goal_id = $this->input->post('goal_id');
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $goal_type = $this->input->post('goal_type');
        $target_amount = str_replace(",", "", $this->input->post('target_amount'));
        $current_status = $this->input->post('current_status');
        $comments = $this->input->post('comments');

        $target_date = str_replace('/', '-', $this->input->post('target_date'));
        $target_date = date('Y-m-d', strtotime($target_date));

        $target_achieved = ($current_status / $target_amount) * 100;

        $arr_debt_detail = end($this->common_model->getRecords("trans_debts", "sum(loan_amount) as total_loan_amount", array("user_id" => intval($user_id))));
        $current_status_networth = $current_status - $arr_debt_detail['total_loan_amount'];

        if ($goal_type == '1') {
            $edit_value['current_status'] = isset($current_status_networth) ? ($current_status_networth) : '0';
        }
        if ($goal_type == '2') {
            $edit_value['current_status'] = isset($current_status) ? ($current_status) : '0';
        }
        if ($goal_type == '3') {
            $edit_value['current_status'] = isset($current_status) ? ($current_status) : '0';
        }
        if ($goal_type == '4') {
            $edit_value['current_status'] = isset($current_status) ? ($current_status) : '0';
        }

        if ($user_id) {
            /* insert contact details */
            $update_data = array(
                "user_id" => $user_id,
                "goal" => $goal_type,
                "target_date" => $target_date,
                "target_amount" => $target_amount,
                "current_status" => $edit_value['current_status'],
                "target_achieved" => $target_achieved,
                "comments" => addslashes($comments),
            );

            $condition = array('goal_id' => intval($goal_id), 'user_id' => intval($user_id));
            $this->common_model->updateRow('trans_goals', $update_data, $condition);

            if ($user_id != '') {
                $arrToReturn = array('error' => 0, 'msg' => "Goal updated successfully.");
            } else {
                $arrToReturn = array('error' => 1, 'msg' => "Goal is not updated, please try again.");
            }

            //echo json_encode($arrToReturn);
            //exit();

            /* Salesforce integration */
            $mySforceConnection = new SforceEnterpriseClient();
            $mySoapClient = $mySforceConnection->createConnection("soapclient/enterprise.wsdl.xml");

            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (($this->session->userdata('partnerSessionId')) !== null) {
                $location = $this->session->userdata('partnerLocation');
                $sessionId = $this->session->userdata('partnerSessionId');

                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);
            } else {
                $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);

                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
            }

            /** Update Goal */
            $arr_PRE_goal = $this->common_model->getRecords('trans_goals', 'salesforce_goal_id', array('goal_id' => intval($goal_id), 'user_id' => intval($user_id)));
            $arr_PRE_goal = end($arr_PRE_goal);

            setlocale(LC_MONETARY, 'en_US.UTF-8');
            switch ($goal_type) {
                case 1:
                    $goal_name = "Assets";

                    $target_achieved = money_format('%.2n', $target_achieved);
                    $target_amount = money_format('%.2n', $target_amount);
                    $current_status = money_format('%.2n', $current_status);
                    break;
                case 2:
                    $goal_name = "Properties";
                    $target_achieved = $target_achieved;
                    $target_amount = $target_amount;
                    $current_status = $current_status;
                    break;
                case 3:
                    $goal_name = "Rental Income";
                    $target_achieved = money_format('%.2n', $target_achieved);
                    $target_amount = money_format('%.2n', $target_amount);
                    $current_status = money_format('%.2n', $current_status);
                    break;
                case 4:
                    $goal_name = "LVR";
                    $target_achieved = $target_achieved . '%';
                    $target_amount = $target_amount . '%';
                    $current_status = $current_status . '%';
                    break;
            }

            /** Insert Goal */
            $sGoalObject = new stdclass();
            $sGoalObject->target_date__c = $target_date;
            $sGoalObject->target_amount__c = $target_amount;
            $sGoalObject->current_status__c = $edit_value['current_status'];
            $sGoalObject->target_achieved__c = $target_achieved . '%';

            $sGoalObject->Id = $arr_PRE_goal['salesforce_goal_id'];
            $mySforceConnection->update(array($sGoalObject), 'PRE_Goal__c');


            if ($user_id != '') {
                $arrToReturn = array('error' => 0, 'msg' => "Goal updated successfully.");
            } else {
                $arrToReturn = array('error' => 1, 'msg' => "Goal is not updated, please try again.");
            }
        } else {
            $arrToReturn = array('error' => 1, 'msg' => "Please enter required fields.");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Goals Delete */

    public function wsPreDeleteGoal() {
        $arrToReturn = array();
        $goal_id = $this->input->post('goal_id');
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $arr_goal_detail = $this->common_model->getRecords("trans_goals", "", array("goal_id" => intval($goal_id), 'user_id' => intval($user_id)));
        if (count($arr_goal_detail) > 0) {
            /* Delete Goal from salesforce */
            $mySforceConnection = new SforceEnterpriseClient();
            $mySforceConnection->createConnection("soapclient/enterprise.wsdl.xml");

            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (($this->session->userdata('partnerSessionId')) !== null) {
                $location = $this->session->userdata('partnerLocation');
                $sessionId = $this->session->userdata('partnerSessionId');

                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);
            } else {
                $mySforceConnection->login(USERNAME, PASSWORD);

                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
            }
            $salesforceId = $arr_goal_detail[0]['salesforce_goal_id'];
            if ($salesforceId != "") {
                $mySforceConnection->delete($salesforceId);
            }


            $this->web_services_model->deleteGoal($goal_id);
            $arrToReturn = array('error' => 0, 'msg' => "Goal deleted successfully.");
        } else {
            $arrToReturn = array('error' => 1, 'msg' => "Goal not found.");
        }
        echo json_encode($arrToReturn);
        exit();
    }

    /* Start :: PRE App Get Goals Edit Value */

    public function wsPreGetGoalValueForEdit() {
        $arrToReturn = array();
        $goal_id = $this->input->post('goal_id');

        $arr_goal_detail = end($this->common_model->getRecords("trans_goals", "", array("goal_id" => intval($goal_id))));

        $arr_property_detail = end($this->common_model->getRecords("mst_properties", "sum(valuation) as total_valuation,sum(LVR) as total_lvr, count(property_id) as total_property_count", array("user_id" => intval($arr_goal_detail['user_id']), 'ppor_status' => 'Yes')));

        $arr_income_detail = end($this->common_model->getRecords("trans_incomes", "sum(weekly_rent) as total_weekly_rent", array("user_id" => intval($arr_goal_detail['user_id']))));

        $arr_debt_detail = end($this->common_model->getRecords("trans_debts", "sum(loan_amount) as total_loan_amount", array("user_id" => intval($arr_goal_detail['user_id']))));
        $current_status_assets = $arr_property_detail['total_valuation'];

        switch ($arr_goal_detail['goal']) {
            case 1:
                $edit_value['current_status'] = isset($current_status_assets) ? number_format($current_status_assets, 0, ".", ",") : '0';
                $edit_value['edit_current_status'] = isset($current_status_assets) ? round($current_status_assets) : '0';
                $edit_value['goal_type'] = '1';
                break;

            case 2:
                $edit_value['current_status'] = isset($arr_property_detail['total_property_count']) ? ($arr_property_detail['total_property_count']) : '0';
                $edit_value['edit_current_status'] = isset($arr_property_detail['total_property_count']) ? round($arr_property_detail['total_property_count']) : '0';
                $edit_value['goal_type'] = '2';
                break;

            case 3:
                $edit_value['current_status'] = isset($arr_income_detail['total_weekly_rent']) ? number_format($arr_income_detail['total_weekly_rent'], 0, ".", ",") : '0';
                $edit_value['edit_current_status'] = isset($arr_income_detail['total_weekly_rent']) ? round($arr_income_detail['total_weekly_rent']) : '0';
                $edit_value['goal_type'] = '3';
                break;

            case 4:
                $avg_LVR = ($arr_property_detail['total_lvr'] / $arr_property_detail['total_property_count']);
                $edit_value['current_status'] = isset($avg_LVR) ? number_format($avg_LVR, 0, ".", ",") : '0';
                $edit_value['edit_current_status'] = isset($avg_LVR) ? round($avg_LVR) : '0';
                $edit_value['goal_type'] = '4';
                break;
        }

        $edit_value['goal_id'] = isset($arr_goal_detail['goal_id']) ? ($arr_goal_detail['goal_id']) : '0';

        if ($arr_goal_detail['target_date'] == '1970-01-01') {
            $edit_value['target_date'] = '00/00/0000';
        } else {
            $edit_value['target_date'] = isset($arr_goal_detail['target_date']) ? date("d/m/Y", strtotime($arr_goal_detail['target_date'])) : '0';
        }
        $edit_value['target_amount'] = isset($arr_goal_detail['target_amount']) ? number_format($arr_goal_detail['target_amount'], 0, ".", ",") : '0';

        if (count($arr_goal_detail) > 0) {
            $arrToReturn = array('error' => 0, "edit_goal_value" => $edit_value);
        } else {
            $arrToReturn = array('error' => 1, 'msg' => "fail");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Get Goals Value For Current Status */

    public function wsPreGetGoalValueCurrentStatus() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $goal_type = $this->input->post('goal_type');
        $arr_property_detail = end($this->common_model->getRecords("mst_properties", "sum(valuation) as total_valuation,sum(land_size) as total_land_size, count(property_id) as total_property_count,sum(LVR) as total_lvr", array("user_id" => intval($user_id), 'ppor_status' => 'Yes')));

        $arr_income_detail = end($this->common_model->getRecords("trans_incomes", "sum(weekly_rent) as total_weekly_rent", array("user_id" => intval($user_id))));

        $arr_debt_detail = end($this->common_model->getRecords("trans_debts", "sum(loan_amount) as total_loan_amount", array("user_id" => intval($user_id))));
        $current_status_assets = $arr_property_detail['total_valuation'];

        switch ($goal_type) {
            case 1:
                $edit_value['current_status'] = isset($current_status_assets) ? number_format($current_status_assets, 0, ".", ",") : '0';
                $edit_value['without_comma_cs'] = isset($current_status_assets) ? round($current_status_assets) : '0';
                $edit_value['goal_type'] = '1';
                break;

            case 2:
                $edit_value['current_status'] = isset($arr_property_detail['total_property_count']) ? ($arr_property_detail['total_property_count']) : '0';
                $edit_value['without_comma_cs'] = isset($arr_property_detail['total_property_count']) ? round($arr_property_detail['total_property_count']) : '0';
                $edit_value['goal_type'] = '2';
                break;

            case 3:
                $edit_value['current_status'] = isset($arr_income_detail['total_weekly_rent']) ? number_format($arr_income_detail['total_weekly_rent'], 0, ".", ",") : '0';
                $edit_value['without_comma_cs'] = isset($arr_income_detail['total_weekly_rent']) ? round($arr_income_detail['total_weekly_rent']) : '0';
                $edit_value['goal_type'] = '3';
                break;

            case 4:
                $avg_LVR = ($arr_property_detail['total_lvr'] / $arr_property_detail['total_property_count']);
                $edit_value['current_status'] = isset($avg_LVR) ? number_format($avg_LVR, 0, ".", ",") : '0';
                $edit_value['without_comma_cs'] = isset($avg_LVR) ? round($avg_LVR) : '0';
                $edit_value['goal_type'] = '4';
                break;
        }

        if (count($arr_property_detail) > 0) {

            $arrToReturn = array('error' => 0, "edit_goal_value" => $edit_value);
        } else {
            $arrToReturn = array('error' => 1, 'msg' => "fail");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Get All Goal Value For wealth Details */

    public function wsPreGetPreAllPropertyWealthDetails() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $condition_to_pass = array('p.user_id' => intval($user_id), 'ppor_status' => 'Yes');
        $fields_to_pass = array('p.property_name,p.current_equity,p.property_id,count(p.property_id) as total_property,sum(p.LVR) as total_LVR');
        $arrProperties = $this->web_services_model->getPropertyDetailsByType($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);



        $condition = array('user_id' => intval($user_id), 'ppor_status' => 'Yes');
        $valution = end($this->common_model->getRecords('mst_properties', $fields_to_pass = 'sum(valuation) as valuation,count(property_id) as total_property', $condition, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $total_debt_value = end($this->web_services_model->getAllDebtRecords(intval($user_id)));
        $assets_goal = end($this->common_model->getRecords('trans_goals', $fields_to_pass = 'target_achieved', array('user_id' => intval($user_id), 'goal' => '1'), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $properties_goal = end($this->common_model->getRecords('trans_goals', $fields_to_pass = 'target_achieved', array('user_id' => intval($user_id), 'goal' => '2'), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $total_lvr = ($total_debt_value['debt_value'] / $valution['valuation']) * 100;
//        $LVR_AVG = ($total_lvr / $valution['total_property']);

        $current_equity = $valution['valuation'] - $total_debt_value['debt_value'];

        if (count($arrProperties) > 0) {
            foreach ($arrProperties as $key => $property) {
                $equity_graph[$key]['property_name'] = isset($property['property_name']) ? stripslashes($property['property_name']) : '';
                $equity_graph[$key]['current_equity'] = isset($property['current_equity']) ? stripslashes($property['current_equity']) : '';
                $equity_graph[$key]['property_id'] = isset($property['property_id']) ? stripslashes($property['property_id']) : '';
            }

            if ($valution['valuation'] < 1000) {
                $asset_valuation = round($valution['valuation']);
            } else if ($valution['valuation'] < 100000) {
                $asset_valuation = round($valution['valuation'] / 1000, 1) . 'K';
            } else {
                $asset_valuation = round($valution['valuation'] / 1000000, 1) . 'M';
            }

            $details['asset_valuation'] = isset($asset_valuation) ? $asset_valuation : '0';
            $details['LVR'] = isset($total_lvr) ? round($total_lvr) : '0';
            $details['assets_count'] = isset($arrProperties) ? count($arrProperties) : '0';
            $details['assets_goal'] = isset($assets_goal['target_achieved']) ? round($assets_goal['target_achieved']) : '0';
            $details['properties_goal'] = isset($properties_goal['target_achieved']) ? round($properties_goal['target_achieved']) : '0';


            $arrToReturn = array('error' => 0, 'msg' => 'success', 'equity_graph_arr' => $equity_graph, 'net_worth_details' => $details);
        } else {
            $arrToReturn = array('error' => 1, 'message' => "fail");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Get All News,Alerts,Events List */

    public function wsPreNewsEventList() {
        $arr_to_return = array();
//        $user_id = $this->input->post('user_id');
        $click_flag = $this->input->post('click_flag');

        switch ($click_flag) {
            case 'News':
                $arr_article_data = $this->common_model->getRecords('mst_news_article', $fields = 'article_id,article_name,article_short_description,article_description,added_date,article_image', $condition = '', $order_by = '', $limit = '', $debug = 0);
                if (count($arr_article_data) > 0) {
                    foreach ($arr_article_data as $key => $article_data) {

                        $arr_return[$key]['id'] = isset($article_data['article_id']) ? $article_data['article_id'] : '';
                        $arr_return[$key]['name'] = isset($article_data['article_name']) ? stripslashes($article_data['article_name']) : '';
                        $arr_return[$key]['short_description'] = isset($article_data['article_description']) ? stripslashes($article_data['article_short_description']) : '';
                        $arr_return[$key]['image'] = isset($article_data['article_image']) ? $article_data['article_image'] : '';
                        $arr_return[$key]['date'] = isset($article_data['added_date']) ? date("d/m/Y", strtotime($article_data['added_date'])) : '';
                    }
                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'article_data' => $arr_return);
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail', 'article_data' => "");
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                }
                break;

            case 'Events':
                $arr_event_data = $this->common_model->getRecords('mst_events', $fields = 'event_id,event_name,event_date_time,event_description,added_date,event_image,posted_by', array('posted_by' => '1'), $order_by = '', $limit = '', $debug = 0);
                if (count($arr_event_data) > 0) {
                    foreach ($arr_event_data as $key => $event_data) {
                        $arr_return[$key]['id'] = isset($event_data['event_id']) ? $event_data['event_id'] : '';
                        $arr_return[$key]['name'] = isset($event_data['event_name']) ? stripslashes($event_data['event_name']) : '';
//                        $arr_return[$key]['short_description'] = isset($event_data['event_description']) ? stripslashes($event_data['event_description']) : '';
                        $arr_return[$key]['short_description'] = isset($event_data['event_description']) ? strip_tags(nl2br((stripslashes(preg_replace("/[\\n\\r]+/", " ", $event_data['event_description']))))) : '';
                        $arr_return[$key]['image'] = isset($event_data['event_image']) ? $event_data['event_image'] : '';
                        $arr_return[$key]['date'] = isset($event_data['event_date_time']) ? date("d/m/Y", strtotime($event_data['event_date_time'])) : '';
                    }
                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'event_data' => $arr_return);
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail', 'event_data' => "");
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                }
                break;

            case 'Alerts':
                $arr_alert_data = $this->common_model->getRecords('mst_alerts', $fields = 'alert_id,alert_name,alert_short_description,added_date', $condition = '', $order_by = '', $limit = '', $debug = 0);
                if (count($arr_alert_data) > 0) {
                    foreach ($arr_alert_data as $key => $alert_data) {
                        $arr_return[$key]['id'] = isset($alert_data['alert_id']) ? $alert_data['alert_id'] : '';
                        $arr_return[$key]['name'] = isset($alert_data['alert_name']) ? stripslashes($alert_data['alert_name']) : '';
                        $arr_return[$key]['short_description'] = isset($alert_data['alert_short_description']) ? stripslashes($alert_data['alert_short_description']) : '';
                        $arr_return[$key]['date'] = isset($alert_data['added_date']) ? date("d/m/Y", strtotime($alert_data['added_date'])) : '';
                    }
                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'alerts_data' => $arr_return);
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail', 'alerts_data' => "");
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                }
                break;
        }
    }

    /* Start :: PRE App Get News,Alerts,Events Details */

    public function wsPreGetNewsEventsDetailsById() {
        $id = $this->input->post('id');
        $click_flag = $this->input->post('click_flag');
        switch ($click_flag) {
            case 'Alerts':
                $alert_data = end($this->common_model->getRecords('mst_alerts', $fields = 'alert_id,alert_name,alert_short_description,added_date', array('alert_id' => $id, 'posted_by' => '1'), $order_by = '', $limit = '', $debug = 0));
                if (count($alert_data) > 0) {
                    $arr_return['id'] = isset($alert_data['alert_id']) ? $alert_data['alert_id'] : '';
                    $arr_return['name'] = isset($alert_data['alert_name']) ? stripslashes($alert_data['alert_name']) : '';
                    $arr_return['description'] = isset($alert_data['alert_short_description']) ? (nl2br(stripslashes($alert_data['alert_short_description']))) : '';
                    $arr_return['date'] = isset($alert_data['added_date']) ? date("d/m/Y", strtotime($alert_data['added_date'])) : '';
                    $arr_to_return = array('error_code' => 0, 'message' => "Success", 'alert_details' => $arr_return);
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                } else {
                    $arr_to_return = array('error_code' => 1, 'message' => "Fail");
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                }
                break;
            case 'Events':

                $event_data = end($this->common_model->getRecords('mst_events', $fields = 'event_id,event_name,event_date_time,event_description,event_address,parking_status,added_date,event_image,event_address,registration_open_time,posted_by', array('event_id' => $id, 'posted_by' => '1'), $order_by = '', $limit = '', $debug = 0));
                if (count($event_data) > 0) {
                    $arr_return['id'] = isset($event_data['event_id']) ? $event_data['event_id'] : '';
                    $arr_return['name'] = isset($event_data['event_name']) ? stripslashes($event_data['event_name']) : '';
                    $arr_return['description'] = isset($event_data['event_description']) ? strip_tags(nl2br((stripslashes(preg_replace("/[\\n\\r]+/", " ", $event_data['event_description']))))) : '';
                    $arr_return['image'] = isset($event_data['event_image']) ? $event_data['event_image'] : '';
                    $arr_return['date'] = isset($event_data['event_date_time']) ? date("d/m/Y", strtotime($event_data['event_date_time'])) : '';
                    $arr_return['registration_open_time'] = isset($event_data['registration_open_time']) ? date("h:i A", strtotime($event_data['registration_open_time'])) : '';
                    $arr_return['event_start_time'] = isset($event_data['event_date_time']) ? date("h:i A", strtotime($event_data['event_date_time'])) : '';
                    $arr_return['event_venue'] = isset($event_data['event_address']) ? ($event_data['event_address']) : '';
                    $arr_return['parking_status'] = isset($event_data['parking_status']) ? ($event_data['parking_status']) : '';
                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'event_data' => $arr_return);
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail', 'event_data' => "");
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                }
                break;
            case 'News':
                $article_data = end($this->common_model->getRecords('mst_news_article', $fields = 'article_id,article_name,article_short_description,article_description,added_date,article_image', array('article_id' => $id, 'posted_by' => '1'), $order_by = '', $limit = '', $debug = 0));
                if (count($article_data) > 0) {
                    $arr_return['id'] = isset($article_data['article_id']) ? $article_data['article_id'] : '';
                    $arr_return['name'] = isset($article_data['article_name']) ? stripslashes($article_data['article_name']) : '';
                    $arr_return['description'] = isset($article_data['article_description']) ? strip_tags(nl2br((stripslashes(preg_replace("/[\\n\\r]+/", " ", $article_data['article_description']))))) : '';
                    $arr_return['image'] = isset($article_data['article_image']) ? $article_data['article_image'] : '';
                    $arr_return['date'] = isset($article_data['added_date']) ? date("d/m/Y", strtotime($article_data['added_date'])) : '';
                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'article_data' => $arr_return);
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail', 'article_data' => "");
                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($response_arr);
                }
                break;
        }
    }

    /* Start :: PRE App Get All Property Details */

    public function getAllPropertyIncomeDetails() {
        $arrToReturn = array();
        $growth = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }

        $condition_to_pass = array('p.user_id' => intval($user_id), 'property_specification' => 'Investment');
        $fields_to_pass = array('p.property_name,p.property_id,sum(p.valuation) as total_valuation');
        $arrProperties = $this->web_services_model->getPropertyDetailsByType($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

        /** Get Total Property Valuation * */
        $get_annual_income_first = end($this->common_model->getRecords('mst_properties', $fields_to_pass = 'sum(weekly_rent) as total_weekly_rent', array('user_id' => intval($user_id), 'property_specification' => 'Investment'), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $total_week = end($this->common_model->getRecords('trans_incomes', $fields_to_pass = 'sum(weekly_rent)as total_week,count(income_id) as total_property_count', array('user_id' => intval($user_id)), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));

        $condition = array('user_id' => intval($user_id), 'ppor_status' => 'Yes');
        $valution = end($this->common_model->getRecords('mst_properties', $fields_to_pass = 'sum(valuation) as valuation,count(property_id) as total_property', $condition, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));

        $rental_income_goal = end($this->common_model->getRecords('trans_goals', $fields_to_pass = 'target_achieved', array('user_id' => intval($user_id), 'goal' => '3'), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $rent_per_annual = ($total_week['total_week'] * 52 );
        $current_yield = ((($total_week['total_week'] * 52 ) / ($valution['valuation'])) * 100);
        $rent_per_week = ($total_week['total_week']) / ($total_week['total_property_count']);

        /** Get Total Property Valuation * */
        if (count($arrProperties) > 0) {
            /** Property Valuation * */
            if ($valution['valuation'] < 1000) {
                $asset_valuation = round($valution['valuation']);
            } else if ($valution['valuation'] < 100000) {
                $asset_valuation = round($valution['valuation'] / 1000, 1) . 'K';
            } else {
                $asset_valuation = round($valution['valuation'] / 1000000, 1) . 'M';
            }

            /** Rent per annum value * */
            if ($rent_per_annual < 1000) {
                $rent_per_annum = round($rent_per_annual);
            } else if ($rent_per_annual < 100000) {
                $rent_per_annum = round($rent_per_annual / 1000, 1) . 'K';
            } else {
                $rent_per_annum = round($rent_per_annual / 1000000, 1) . 'M';
            }

            /** Rent per annum value * */
            if ($rent_per_week < 1000) {
                $avg_rent_per_week = round($rent_per_week);
            } else if ($rent_per_week < 100000) {
                $avg_rent_per_week = round($rent_per_week / 1000, 1) . 'K';
            } else {
                $avg_rent_per_week = round($rent_per_week / 1000000, 1) . 'M';
            }

            $details['asset_valuation'] = isset($asset_valuation) ? $asset_valuation : '0';
            $details['income_per_annum'] = isset($rent_per_annum) ? $rent_per_annum : '0';
            $details['yield'] = isset($current_yield) ? round($current_yield) : '0';
            $details['avg_rent_per_week'] = isset($avg_rent_per_week) ? $avg_rent_per_week : '0';
            $details['rental_income_goal'] = isset($rental_income_goal['target_achieved']) ? round($rental_income_goal['target_achieved']) : '0';

            $overall_income = 0;
            foreach ($arrProperties as $key => $property) {
                $total_income = 0;
                $total_percentage_income = 0;
                $total_weekly_rent = end($this->common_model->getRecords('trans_incomes', $fields_to_pass = 'sum(weekly_rent)as total,added_date', array('user_id' => intval($user_id), 'property_id' => intval($property['property_id'])), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));

                $now = date('Y-m-d'); // or your date as well
                $your_date = date("Y-m-d", strtotime($total_weekly_rent['added_date']));
                $datediff = strtotime($now) - strtotime($your_date);
                $Days = floor($datediff / (60 * 60 * 24));

                $total_weeks = round($Days / 7);
                $total_annual_income = ($total_weekly_rent['total'] * 52);
                $total_income = ($total_weekly_rent['total']);

                $growth[$key]['property_id'] = isset($property['property_id']) ? ($property['property_id']) : '';
                $growth[$key]['property_name'] = isset($property['property_name']) ? stripslashes($property['property_name']) : '';
                $growth[$key]['graph_annual_rental_income'] = isset($total_annual_income) ? round($total_annual_income) : '0';
                $growth[$key]['graph_anual_income'] = isset($total_income) ? round($total_income) : '0';
            }

            $annual_rent = ($get_annual_income_first['total_weekly_rent'] * 52);
            $anual_income_percentage = round((($total_week['total_week'] * 100) / $annual_rent), 2);
            $graph_annual_percentage['graph_annual_percentage'] = isset($anual_income_percentage) ? round($anual_income_percentage) : '0';

            $arrToReturn = array('error' => 0, 'msg' => 'success', 'income_graph_arr' => $growth, 'income_details' => $details, 'graph_annual_percentage' => $graph_annual_percentage);
        } else {
            $arrToReturn = array('error' => 1, 'message' => "fail");
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Mortage Calculator */

    public function getMortageCalculator() {

        $sw = '1366';
//        $sw = $this->input->post('sw');
        $purchase_price = $this->input->post('purchase_price'); //ma
        $down_payment = $this->input->post('down_payment'); //dp
        $mortgage_term = $this->input->post('mortgage_term'); //mt
        $interest_rate = $this->input->post('interest_rate'); //ir
        $property_tax = $this->input->post('property_tax'); //pt $ per year
        $property_insurance = $this->input->post('property_insurance'); //pi $ per year
        $PMI = $this->input->post('PMI'); //mi Private Mortgage Insurance
        $month = $this->input->post('month'); //sm First payment month
        $year = $this->input->post('year'); //sy First payment year

        if ($purchase_price != '') {
            $url = "http://www.mlcalc.com/";

            if ($down_payment < 20) {
                $data_string = "ml=mortgage&cl=true&sw=" . $sw . "&ma=" . $purchase_price . "&dp=" . $down_payment . "&mt=" . $mortgage_term . "&ir=" . $interest_rate . "&pt=" . $property_tax . "&pi=" . $property_insurance . "&mi=" . $PMI . "&sm=" . $month . "&sy=" . $year . "&as=none";
            } else {
                $data_string = "ml=mortgage&cl=true&sw=" . $sw . "&ma=" . $purchase_price . "&dp=" . $down_payment . "&mt=" . $mortgage_term . "&ir=" . $interest_rate . "&pt=" . $property_tax . "&pi=" . $property_insurance . "&sm=" . $month . "&sy=" . $year . "&as=year";
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);               
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);
            error_reporting(E_ALL);
            ini_set("display_errors", "on");
            include('simple_html_dom.php');
            $html = str_get_html($response);
//            echo '<pre>';print_r($html);die;
            ini_set('memory_limit', '-1');
            $row_count = 0;
            $new_arr = array();
            $new_arr1 = array();

            foreach ($html->find('table[id=summary] tr') as $k => $row) {
                if ($k == 0) {
                    if ($down_payment < 20) {
                        $value1 = ($row->find('td', 1));
                        $value2 = $row->find('td', 2);
                        $value3 = $row->find('td', 3);
                        $value4 = $row->find('td', 4);

                        $r1 = (explode("$", $value1));
                        $r2 = (explode("$", $value2));
                        $r3 = (explode("$", $value3));
                        $r4 = (explode("$", $value4));

                        $new_arr['total_monthly_payment_until'] = strip_tags(implode("", $r1));
                        $new_arr['total_monthly_payment_after'] = strip_tags(implode("", $r2));
                        $new_arr['total_days_payment'] = strip_tags(implode("", $r3));
                        $new_arr['payoff_date'] = strip_tags(implode("", $r4));
                    } else {
                        $value1 = ($row->find('td', 1));
                        $value2 = $row->find('td', 2);
                        $value3 = $row->find('td', 3);


                        $r1 = (explode("$", $value1));
                        $r2 = (explode("$", $value2));
                        $r3 = (explode("$", $value3));

                        $new_arr['total_monthly_payment'] = strip_tags(implode("", $r1));
                        $new_arr['total_days_payment'] = strip_tags(implode("", $r2));
                        $new_arr['payoff_date'] = strip_tags(implode("", $r3));
                    }
                }
                if ($k == 2) {

                    if ($down_payment < 20) {

                        $secondvalue1 = ($row->find('td', 0));
                        $secondvalue2 = $row->find('td', 1);
                        $secondvalue3 = $row->find('td', 2);
                        $secondvalue4 = $row->find('td', 3);

                        $ro1 = (explode("$", $secondvalue1));
                        $ro2 = (explode("$", $secondvalue2));
                        $ro3 = (explode("$", $secondvalue3));
                        $ro4 = (explode("$", $secondvalue4));

                        $total_monthly_payment_until = strip_tags(implode("", $ro1));
                        $new_arr1['total_monthly_payment_until'] = implode(" ", str_split($total_monthly_payment_until, 21)) . " ";

                        $total_monthly_payment_after = strip_tags(implode("", $ro2));
                        $new_arr1['total_monthly_payment_after'] = implode(" ", str_split($total_monthly_payment_after, 21)) . " ";

                        $new_arr1['total_days_payment'] = strip_tags(implode("", $ro3));
                        $new_arr1['payoff_date'] = strip_tags(implode("", $ro4));
                    } else {
                        $secondvalue1 = ($row->find('td', 0));
                        $secondvalue2 = $row->find('td', 1);
                        $secondvalue3 = $row->find('td', 2);

                        $ro1 = (explode("$", $secondvalue1));
                        $ro2 = (explode("$", $secondvalue2));
                        $ro3 = (explode("$", $secondvalue3));

                        $new_arr1['total_monthly_payment'] = strip_tags(implode("", $ro1));
                        $new_arr1['total_days_payment'] = strip_tags(implode("", $ro2));
                        $new_arr1['payoff_date'] = strip_tags(implode("", $ro3));
                    }
                }
            }
            $arrToReturn = array('error_code' => 0, 'msg' => 'success', "json_data" => $new_arr, "json_data1" => $new_arr1);
        } else {
            $arrToReturn = array('error_code' => 0, 'msg' => 'Please enter required fields.');
        }
        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Tax Calculator */

    public function getTaxCalculator() {
        $income = $this->input->post('income');
        $year = $this->input->post('year');
        $includessuper = $this->input->post('includessuper');
        $resident = $this->input->post('resident');
        $help = $this->input->post('help');

        if ($income != '') {
            $url = "http://www.taxcalc.com.au/calcs.php?income=" . $income . "&year=" . $year . "&lang=en&includessuper=" . $includessuper . "&resident=" . $resident . "&help=" . $help . "&response=json";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);               
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);
            $response = simplexml_load_string($response);
            $arrToReturn = array('error_code' => 0, 'msg' => "success", 'tax_data' => $response);
        } else {
            $arrToReturn = array('error_code' => 1, 'msg' => "Please enter required fields.");
        }

        echo json_encode($arrToReturn);
    }

    /* Start :: PRE App Depreciation Calculator */

    public function getDepreciationCalculator() {
        $property_type = $this->input->post('property_type');
        $construction_type = $this->input->post('construction_type');
        $units = $this->input->post('units');
        $levels = $this->input->post('levels');
        $quality_of_finish = $this->input->post('quality_of_finish');
        $squares = $this->input->post('squares');
        $floor_area = $this->input->post('floor_area');
        $cons_completion_year = $this->input->post('cons_completion_year');
        $purchase_year = $this->input->post('purchase_year');
        $city = $this->input->post('city');
        $marginal_tax_rate = $this->input->post('marginal_tax_rate');
        $resp_flag = $this->input->post('resp_flag');
//        echo '<pre>';print_r($_POST);die;
        if ($resp_flag != '') {
            $url = "https://www.bmtqs.com.au/tax-depreciation-calculator";
            if ($resp_flag == '0') {
                $data = array('PropertyType' => "Standard House", 'ConstructionType' => 'Commercial', 'Units' => '1', 'QualityOfFinish' => 'Medium', 'Squares' => 'sqm', 'FloorArea' => '180', 'ConsCompletionYear' => '2016', 'PurchaseYear' => '2016', 'City' => 'Sydney', 'MarginalTaxRate' => '37');
            } else {
                $data = array('PropertyType' => "$property_type", 'ConstructionType' => "$construction_type", 'Units' => "$units", 'Levels' => $levels, 'QualityOfFinish' => "$quality_of_finish", 'Squares' => "$squares", 'FloorArea' => "$floor_area", 'ConsCompletionYear' => "$cons_completion_year", 'PurchaseYear' => "$purchase_year", 'City' => "$city", 'MarginalTaxRate' => "$marginal_tax_rate");
            }
//                echo '<pre>';print_r($data);die;            
            $data_string = json_encode($data);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);               
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $response = json_decode(curl_exec($ch));

            if ($resp_flag == '0') {
                $PropertyType = $response->PropertyTypes; // Get All Property Types Array 
                $ConstructionTypes = $response->ConstructionTypes; // Get All Construction Types Array 
                $QualityOfFinishes = $response->QualityOfFinishes; // Get All Quality Of Finishes Array 
                $Cities = $response->Cities; // Get All Cities Array 
                $Years = $response->Years; // Get All Years Array 

                $new_array = array("PropertyType" => $PropertyType, "ConstructionTypes" => $ConstructionTypes, "QualityOfFinishes" => $QualityOfFinishes, "Cities" => $Cities, "Years" => $Years);
                $arrToReturn = array('error_code' => 0, 'msg' => "success", 'response' => $new_array);
            } else {
                $Maximum = $response->Result->Maximum;
                $MaximumTotal = $response->Result->MaximumTotal;
                $Minimum = $response->Result->Minimum;
                $MinimumTotal = $response->Result->MinimumTotal;
                $ReducedTaxPayabaleMaxTotal = $response->Result->ReducedTaxPayabaleMaxTotal;
                $ReducedTaxPayabaleMinTotal = $response->Result->ReducedTaxPayabaleMinTotal;
                $result['PropertyType'] = isset($response->PropertyType) ? $response->PropertyType : '';
                $result['ConstructionType'] = isset($response->ConstructionType) ? $response->ConstructionType : '';
                $result['QualityOfFinish'] = isset($response->QualityOfFinish) ? $response->QualityOfFinish : '';
                $result['QualityOfFinish'] = isset($response->Units) ? $response->Units : '';
                $result['Levels'] = isset($response->Levels) ? $response->Levels : '';
                $result['FloorArea'] = isset($response->FloorArea) ? $response->FloorArea : '';
                $result['MaxFloorArea'] = isset($response->MaxFloorArea) ? $response->MaxFloorArea : '';
                $result['MinFloorArea'] = isset($response->MinFloorArea) ? $response->MinFloorArea : '';
                $result['ConsCompletionYear'] = isset($response->ConsCompletionYear) ? $response->ConsCompletionYear : '';
                $result['PurchaseYear'] = isset($response->PurchaseYear) ? $response->PurchaseYear : '';
                $result['City'] = isset($response->City) ? $response->City : '';
                $result['Squares'] = isset($response->Squares) ? $response->Squares : '';
                $result['Disclaimer'] = isset($response->Disclaimer) ? $response->Disclaimer : '';
                $result['NumberOfLevels'] = isset($response->NumberOfLevels) ? $response->NumberOfLevels : '';
                $result['CobrandedDetails'] = isset($response->CobrandedDetails) ? $response->CobrandedDetails : '';
                $result['MarginalTaxRate'] = isset($response->MarginalTaxRate) ? $response->MarginalTaxRate : '';

                $new_array1 = array("success_result" => $result, "Maximum" => $Maximum, "MaximumTotal" => $MaximumTotal, "Minimum" => $Minimum, "MinimumTotal" => $MinimumTotal, "ReducedTaxPayabaleMaxTotal" => $ReducedTaxPayabaleMaxTotal, "ReducedTaxPayabaleMinTotal" => $ReducedTaxPayabaleMinTotal);
                $arrToReturn = array('error_code' => 0, 'msg' => "success", 'response' => $new_array1);
            }
        } else {
            $arrToReturn = array('error_code' => 1, 'msg' => "Please enter required fields.");
        }

        echo stripslashes(json_encode($arrToReturn));
    }

    /* Start :: PRE App Current Date */

    public function getCurrentDate() {
        $arrToReturn = array();
        $current_year = date("Y");
        $arr_return['current_year'] = isset($current_year) ? $current_year : '';
        if ($arr_return) {
            $arrToReturn = array('error_code' => 0, 'current_year' => $arr_return);
        } else {
            $arrToReturn = array('error_code' => 1);
        }
        echo json_encode($arrToReturn);
    }

    public function getPreNewAssetPFS() {
//        $arrToReturn = array();

        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $furniture = $this->input->post('furniture');
        $personal_effects = $this->input->post('personal_effects');
        $other_assets = $this->input->post('other_assets');
        $shares_bonds = $this->input->post('shares_bonds');
        $total_savings = $this->input->post('total_savings');

        if ($user_id != '' && $furniture == '' && $personal_effects == '' && $other_assets == '' && $total_savings == '' && $shares_bonds == '') {

            $new_asset_pfs = $this->common_model->getRecords('mst_assets_PFS', '', array("user_id_fk" => $user_id));
            $new_assets['user_id_fk'] = isset($new_asset_pfs[0]['user_id_fk']) ? $new_asset_pfs[0]['user_id_fk'] : '0';
            $new_assets['furniture'] = isset($new_asset_pfs[0]['furniture']) ? $new_asset_pfs[0]['furniture'] : '0';
            $new_assets['personal_effects'] = isset($new_asset_pfs[0]['personal_effects']) ? $new_asset_pfs[0]['personal_effects'] : '0';
            $new_assets['other_assets'] = isset($new_asset_pfs[0]['other_assets']) ? $new_asset_pfs[0]['other_assets'] : '0';
            $new_assets['shares_bonds'] = isset($new_asset_pfs[0]['shares_bonds']) ? $new_asset_pfs[0]['shares_bonds'] : '0';
            $new_assets['total_savings'] = isset($new_asset_pfs[0]['total_savings']) ? $new_asset_pfs[0]['total_savings'] : '0';

//            echo '<pre>';print_r($new_assets);die;
            if (count($new_asset_pfs) > 0) {
                $arrToReturn = array('error' => 0, 'msg' => 'success', 'assets_PFS' => $new_assets);
            } else {
                $arrToReturn = array('error' => 1, 'msg' => 'fail');
            }
            echo json_encode($arrToReturn);
        } else {
//            echo '<pre>else';
            $new_asset_pfs = $this->common_model->getRecords('mst_assets_PFS', '', array("user_id_fk" => $user_id));
            if (count($new_asset_pfs) == 0) {
                $array_to_insert = array(
                    "user_id_fk" => $user_id,
                    "furniture" => $furniture,
                    "personal_effects" => $personal_effects,
                    "other_assets" => $other_assets,
                    "shares_bonds" => $shares_bonds,
                    "total_savings" => $total_savings,
                    "added_date" => date('Y-m-d'),
                );
                $this->common_model->insertRow($array_to_insert, 'mst_assets_PFS');
            } else {
                $array_to_update = array(
                    "user_id_fk" => $user_id,
                    "furniture" => $furniture,
                    "personal_effects" => $personal_effects,
                    "other_assets" => $other_assets,
                    "shares_bonds" => $shares_bonds,
                    "total_savings" => $total_savings,
                    "added_date" => date('Y-m-d'),
                );
                $condition = array('user_id_fk' => intval($user_id));
                $this->common_model->updateRow('mst_assets_PFS', $array_to_update, $condition);
            }
            if ($user_id != '') {
                $arrToReturn = array('error' => 0, 'msg' => 'success');
            } else {
                $arrToReturn = array('error' => 1);
            }
            echo json_encode($arrToReturn);
        }
    }

    public function wsPrePersonAddEdit() {

        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $person_type = $this->input->post('person_type');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $dob = $this->input->post('dob');
        $email_id = $this->input->post('email_id');
        $country_code = $this->input->post('country_code');
        $phone_number = $this->input->post('phone_number');
        $mobile_number = $this->input->post('mobile_number');
        $street_address = $this->input->post('street_address');
        $town_city = $this->input->post('town_city');
        $postcode = $this->input->post('postcode');
        $country = $this->input->post('country');
        $cb_mobile = $this->input->post('cb_mobile');
        $cb_phone = $this->input->post('cb_phone');
        $cb_email = $this->input->post('cb_email');

        if ($user_id != '' && $person_type != '') {
            if ($user_id != '' && $person_type != '' && $first_name == '' && $last_name == '') {

                $new_person_pfs = $this->common_model->getRecords('mst_persons_PFS', '', array("user_id_fk" => intval($user_id), 'person_type' => $person_type));
                $user_data = $this->common_model->getRecords('mst_users', '', array("user_id" => intval($user_id)));
                if ($person_type == "P1") {
                    $first_name = $user_data[0]['first_name'];
                    $last_name = $user_data[0]['last_name'];
                } elseif ($person_type == "P2") {
                    $first_name = $new_person_pfs[0]['first_name'];
                    $last_name = $new_person_pfs[0]['last_name'];
                }

//            echo '<pre>';print_R($user_data);die;
                $new_person['person_id'] = isset($new_person_pfs[0]['person_id']) ? $new_person_pfs[0]['person_id'] : '0';
                $new_person['user_id_fk'] = isset($new_person_pfs[0]['user_id_fk']) ? $new_person_pfs[0]['user_id_fk'] : '0';
                $new_person['person_type'] = isset($new_person_pfs[0]['person_type']) ? $new_person_pfs[0]['person_type'] : '0';
                $new_person['first_name'] = isset($first_name) ? $first_name : '';
                $new_person['last_name'] = isset($last_name) ? $last_name : '';
                $new_person['birth_date'] = isset($new_person_pfs[0]['birth_date']) ? $new_person_pfs[0]['birth_date'] : '';
                $new_person['email_id'] = isset($new_person_pfs[0]['email_id']) ? $new_person_pfs[0]['email_id'] : '';
                $new_person['country_code'] = isset($new_person_pfs[0]['country_code']) ? $new_person_pfs[0]['country_code'] : '';
                $new_person['phone'] = isset($new_person_pfs[0]['phone']) ? $new_person_pfs[0]['phone'] : '';
                $new_person['mobile_number'] = isset($new_person_pfs[0]['mobile_number']) ? $new_person_pfs[0]['mobile_number'] : '';
                $new_person['street_address'] = isset($new_person_pfs[0]['street_address']) ? $new_person_pfs[0]['street_address'] : '';
                $new_person['town_city'] = isset($new_person_pfs[0]['town_city']) ? $new_person_pfs[0]['town_city'] : '';
                $new_person['post_code'] = isset($new_person_pfs[0]['post_code']) ? $new_person_pfs[0]['post_code'] : '';
                $new_person['country'] = isset($new_person_pfs[0]['country']) ? $new_person_pfs[0]['country'] : '';
                $new_person['prefer_mobile'] = isset($new_person_pfs[0]['prefer_mobile']) ? $new_person_pfs[0]['prefer_mobile'] : '';
                $new_person['prefer_phone'] = isset($new_person_pfs[0]['prefer_phone']) ? $new_person_pfs[0]['prefer_phone'] : '';
                $new_person['prefer_email'] = isset($new_person_pfs[0]['prefer_email']) ? $new_person_pfs[0]['prefer_email'] : '';
                $new_person['added_date'] = isset($new_person_pfs[0]['added_date']) ? $new_person_pfs[0]['added_date'] : '0';

                if (count($new_person_pfs) > 0) {
                    $arrToReturn = array('error' => 0, 'msg' => 'success', 'person' => $new_person);
                } else {
                    $arrToReturn = array('error' => 1, 'msg' => 'fail');
                }
            } else {
                $new_person_pfs = $this->common_model->getRecords('mst_persons_PFS', '', array("user_id_fk" => intval($user_id), 'person_type' => $person_type));
                $new_income_pfs = $this->common_model->getRecords('mst_income_dependants_PFS', '', array("user_id_fk" => intval($user_id), 'person_type' => $person_type));
                $new_smsf_insurances_pfs = $this->common_model->getRecords('mst_smsf_insurances_PFS', '', array("user_id_fk" => intval($user_id), 'person_type' => $person_type));

                if (count($new_person_pfs) == 0) {
                    $array_to_insert = array(
                        "user_id_fk" => $user_id,
                        "person_type" => $person_type,
                        "first_name" => $first_name,
                        "last_name" => $last_name,
                        "birth_date" => $dob,
                        "email_id" => $email_id,
                        "country_code" => $country_code,
                        "phone" => $phone_number,
                        "mobile_number" => $mobile_number,
                        "street_address" => $street_address,
                        "town_city" => $town_city,
                        "post_code" => $postcode,
                        "country" => $country,
                        "prefer_mobile" => $cb_mobile,
                        "prefer_phone" => $cb_phone,
                        "prefer_email" => $cb_email,
                        "added_date" => date("Y-m-d"),
                    );

                    $last_id = $this->common_model->insertRow($array_to_insert, 'mst_persons_PFS');

                    if (count($new_income_pfs) == 0) {
                        $insert_income = array(
                            "user_id_fk" => $user_id,
                            "person_id_fk" => $last_id,
                            "person_type" => $person_type,
                            "added_date" => date("Y-m-d"),
                        );

                        $this->common_model->insertRow($insert_income, 'mst_income_dependants_PFS');
                    }

                    if (count($new_smsf_insurances_pfs) == 0) {
                        $insert_smsf = array(
                            "user_id_fk" => $user_id,
                            "person_id_fk" => $last_id,
                            "person_type" => $person_type,
                            "added_date" => date("Y-m-d"),
                        );
                        $this->common_model->insertRow($insert_smsf, 'mst_smsf_insurances_PFS');
                    }
                } else {
                    $array_to_update = array(
                        "user_id_fk" => $user_id,
                        "person_type" => $person_type,
                        "first_name" => $first_name,
                        "last_name" => $last_name,
                        "birth_date" => $dob,
                        "email_id" => $email_id,
                        "country_code" => $country_code,
                        "phone" => $phone_number,
                        "mobile_number" => $mobile_number,
                        "street_address" => $street_address,
                        "town_city" => $town_city,
                        "post_code" => $postcode,
                        "country" => $country,
                        "prefer_mobile" => $cb_mobile,
                        "prefer_phone" => $cb_phone,
                        "prefer_email" => $cb_email,
                        "added_date" => date("Y-m-d"),
                    );
                    $condition = array("user_id_fk" => intval($user_id), 'person_type' => $person_type);
                    $this->common_model->updateRow('mst_persons_PFS', $array_to_update, $condition);
                }


                $arrToReturn = array('error' => 0, 'msg' => 'success');
            }
        } else {
            $arrToReturn = array('error' => 1);
        }
        echo json_encode($arrToReturn);
    }

    public function wsPreSMSFInsuranceAddEdit() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error_code' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
//        $arr_json = '{"main_array": [{"P1":{"person_type":"P1","person_id":"1","name":"VIN DIESEL","current_super_balance":"300,000","cb_smsf":"1","cb_defined_benefit_superannuation":"1","cb_income_insurance":"1","cb_life_insurance":"0","cb_tpd_insurance":"0","cb_trauma_insurance":"0"}},{"P2":{"person_id":"2","person_type":"P2","name":"JOHN RAMBO","current_super_balance":"250,000","cb_smsf":"0","cb_defined_benefit_superannuation":"0","cb_income_insurance":"0","cb_life_insurance":"1","cb_tpd_insurance":"1","cb_trauma_insurance":"1"}}]}';
//        $post_main_array = json_decode($arr_json, true);
        $post_main_array = json_decode($_REQUEST['main_array'], true);
        $main_array = $post_main_array['main_array'];

        if ($user_id) {
            if ($user_id != '' && (count($main_array) == 0)) {
                $person_pfs = $this->common_model->getRecords('mst_persons_PFS', 'first_name,last_name,person_type,person_id', array("user_id_fk" => intval($user_id)));

                foreach ($person_pfs as $person_name) {
                    if ($person_name['person_type'] == 'P1') {
                        $full_name = $person_name['first_name'] . " " . $person_name['last_name'];
                        $name[$person_name['person_type']] = isset($full_name) ? stripslashes($full_name) : '';
                        $name[$person_name['person_type'] . "_Id"] = isset($person_name['person_id']) ? stripslashes($person_name['person_id']) : '';
                    } else {
                        $full_name = $person_name['first_name'] . " " . $person_name['last_name'];
                        $name[$person_name['person_type']] = isset($full_name) ? stripslashes($full_name) : '';
                        $name[$person_name['person_type'] . "_Id"] = isset($person_name['person_id']) ? stripslashes($person_name['person_id']) : '';
                    }
                }

                $new_SMSF_insurance_pfs = $this->common_model->getRecords('mst_smsf_insurances_PFS', '', array("user_id_fk" => intval($user_id)));
                if (count($new_SMSF_insurance_pfs) > 0) {
                    $arr_return = array();
                    foreach ($new_SMSF_insurance_pfs as $key => $new_SMSF) {

                        $arr_return[$new_SMSF['person_type']]['smsf_id'] = isset($new_SMSF['smsf_id']) ? $new_SMSF['smsf_id'] : '';
                        $arr_return[$new_SMSF['person_type']]['person_id_fk'] = isset($new_SMSF['person_id_fk']) ? stripslashes($new_SMSF['person_id_fk']) : '';
                        $arr_return[$new_SMSF['person_type']]['user_id_fk'] = isset($new_SMSF['user_id_fk']) ? stripslashes($new_SMSF['user_id_fk']) : '';
                        $arr_return[$new_SMSF['person_type']]['person_type'] = isset($new_SMSF['person_type']) ? $new_SMSF['person_type'] : '';
                        $arr_return[$new_SMSF['person_type']]['current_super_balance'] = isset($new_SMSF['current_super_balance']) ? $new_SMSF['current_super_balance'] : '';
                        $arr_return[$new_SMSF['person_type']]['CB_SMSF'] = isset($new_SMSF['CB_SMSF']) ? $new_SMSF['CB_SMSF'] : '';
                        $arr_return[$new_SMSF['person_type']]['CB_benefit'] = isset($new_SMSF['CB_benefit']) ? $new_SMSF['CB_benefit'] : '';
                        $arr_return[$new_SMSF['person_type']]['CB_income_insurance'] = isset($new_SMSF['CB_income_insurance']) ? $new_SMSF['CB_income_insurance'] : '';
                        $arr_return[$new_SMSF['person_type']]['CB_life_insurance'] = isset($new_SMSF['CB_life_insurance']) ? $new_SMSF['CB_life_insurance'] : '';
                        $arr_return[$new_SMSF['person_type']]['CB_TPD_insurance'] = isset($new_SMSF['CB_TPD_insurance']) ? $new_SMSF['CB_TPD_insurance'] : '';
                        $arr_return[$new_SMSF['person_type']]['CB_trauma_insurance'] = isset($new_SMSF['CB_trauma_insurance']) ? $new_SMSF['CB_trauma_insurance'] : '';
                        $arr_return[$new_SMSF['person_type']]['person_type'] = isset($new_SMSF['person_type']) ? $new_SMSF['person_type'] : '';
                        $arr_return[$new_SMSF['person_type']]['added_date'] = isset($new_SMSF['added_date']) ? date("d/m/Y", strtotime($new_SMSF['added_date'])) : '';
                    }
                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'main_array' => array($arr_return), 'name' => $name);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail');
                }
                echo json_encode($arr_to_return);
            } else {
                if (count($main_array) > 0) {
                    $this->common_model->deleteRows(array($user_id), "mst_smsf_insurances_PFS", "user_id_fk");
                    for ($i = 0; $i <= 1; $i++) {
                        if ($main_array[$i]['P1']['person_type'] == 'P1') {
                            $insert_data = array(
                                'person_id_fk' => $main_array[0]['P1']['person_id'],
                                'user_id_fk' => $user_id,
                                'person_type' => $main_array[0]['P1']['person_type'],
                                'current_super_balance' => $main_array[0]['P1']['current_super_balance'],
                                'CB_SMSF' => $main_array[0]['P1']['cb_smsf'],
                                'CB_benefit' => $main_array[0]['P1']['cb_defined_benefit_superannuation'],
                                'CB_income_insurance' => $main_array[0]['P1']['cb_income_insurance'],
                                'CB_life_insurance' => $main_array[0]['P1']['cb_life_insurance'],
                                'CB_TPD_insurance' => $main_array[0]['P1']['cb_tpd_insurance'],
                                'CB_trauma_insurance' => $main_array[0]['P1']['cb_trauma_insurance'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_smsf_insurances_PFS');
                        } else {
                            $insert_data = array(
                                'person_id_fk' => $main_array[$i]['P2']['person_id'],
                                'user_id_fk' => $user_id,
                                'person_type' => $main_array[$i]['P2']['person_type'],
                                'current_super_balance' => $main_array[$i]['P2']['current_super_balance'],
                                'CB_SMSF' => $main_array[$i]['P2']['cb_smsf'],
                                'CB_benefit' => $main_array[$i]['P2']['cb_defined_benefit_superannuation'],
                                'CB_income_insurance' => $main_array[$i]['P2']['cb_income_insurance'],
                                'CB_life_insurance' => $main_array[$i]['P2']['cb_life_insurance'],
                                'CB_TPD_insurance' => $main_array[$i]['P2']['cb_tpd_insurance'],
                                'CB_trauma_insurance' => $main_array[$i]['P2']['cb_trauma_insurance'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_smsf_insurances_PFS');
                        }
                    }

                    $arr_to_return = array('error_code' => "0", 'msg' => 'success');
//                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($arr_to_return);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail');
//                    $response_arr = array('Response' => $arr_to_return);
                    echo json_encode($arr_to_return);
                }
            }
        } else {
            $arr_to_return["msg"] = "no input data";
            $response_arr = array('Response' => $arr_to_return);
            echo json_encode($response_arr);
        }
    }

    public function wsPreIncomeDependantAddEdit() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error_code' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $total_dependants = $this->input->post('total_dependants');
//        $arr_json = '[{"P1":{"person_type":"P1","person_id":"1","employment_type":"Self Employed","company_name":"Abc Company","gross_income":"10,000"}},{"P2":{"person_id":"2","person_type":"P2","employment_type":"Self Employed","company_name":"Xyz Company","gross_income":"15,000"}}]';
//        $main_array = json_decode($arr_json, true);
        $post_main_array = json_decode($_REQUEST['main_array'], true);
        $main_array = $post_main_array['main_array'];
//echo '<pre>'; print_r($main_array);
        if ($user_id) {
            if ($user_id != '' && (count($main_array) == 0)) {


                $new_income_dependant_pfs = $this->common_model->getRecords('mst_income_dependants_PFS', '', array("user_id_fk" => intval($user_id)));

                if (count($new_income_dependant_pfs) > 0) {
                    $arr_return = array();
                    foreach ($new_income_dependant_pfs as $key => $new_income_dependant) {
                        $arr_return[$new_income_dependant['person_type']]['income_dependant_id'] = isset($new_income_dependant['income_dependant_id']) ? $new_income_dependant['income_dependant_id'] : '';
                        $arr_return[$new_income_dependant['person_type']]['person_id'] = isset($new_income_dependant['person_id_fk']) ? stripslashes($new_income_dependant['person_id_fk']) : '';
                        $arr_return[$new_income_dependant['person_type']]['user_id'] = isset($new_income_dependant['user_id_fk']) ? stripslashes($new_income_dependant['user_id_fk']) : '';
                        $arr_return[$new_income_dependant['person_type']]['person_type'] = isset($new_income_dependant['person_type']) ? $new_income_dependant['person_type'] : '';
                        $arr_return[$new_income_dependant['person_type']]['employment_type'] = isset($new_income_dependant['employment_type']) ? $new_income_dependant['employment_type'] : '';
                        $arr_return[$new_income_dependant['person_type']]['company_name'] = isset($new_income_dependant['company_name']) ? $new_income_dependant['company_name'] : '';
                        $arr_return[$new_income_dependant['person_type']]['gross_income'] = isset($new_income_dependant['gross_income']) ? $new_income_dependant['gross_income'] : '';
                    }

                    $person_pfs = $this->common_model->getRecords('mst_persons_PFS', 'first_name,last_name,person_type,person_id', array("user_id_fk" => intval($user_id)));

                    foreach ($person_pfs as $person_name) {
                        if ($person_name['person_type'] == 'P1') {
                            $full_name = $person_name['first_name'] . " " . $person_name['last_name'];
                            $name[$person_name['person_type']] = isset($full_name) ? stripslashes($full_name) : '';
                            $name[$person_name['person_type'] . "_Id"] = isset($person_name['person_id']) ? stripslashes($person_name['person_id']) : '';
                            $name['total_dependants'] = isset($new_income_dependant_pfs[0]['total_dependants']) ? $new_income_dependant_pfs[0]['total_dependants'] : '';
                        } else {
                            $full_name = $person_name['first_name'] . " " . $person_name['last_name'];
                            $name[$person_name['person_type']] = isset($full_name) ? stripslashes($full_name) : '';
                            $name[$person_name['person_type'] . "_Id"] = isset($person_name['person_id']) ? stripslashes($person_name['person_id']) : '';
                            $name['total_dependants'] = isset($new_income_dependant_pfs[0]['total_dependants']) ? $new_income_dependant_pfs[0]['total_dependants'] : '';
                        }
                    }

                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'main_array' => array($arr_return), 'name' => $name);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail');
                }
                echo json_encode($arr_to_return);
            } else {
                if (count($main_array) > 0) {
                    $this->common_model->deleteRows(array($user_id), "mst_income_dependants_PFS", "user_id_fk");
                    for ($i = 0; $i <= 1; $i++) {
                        if ($main_array[$i]['P1']['person_type'] == 'P1') {
                            $insert_data = array(
                                'person_id_fk' => $main_array[$i]['P1']['person_id'],
                                'user_id_fk' => $user_id,
                                'person_type' => $main_array[$i]['P1']['person_type'],
                                'employment_type' => $main_array[$i]['P1']['employment_type'],
                                'company_name' => $main_array[$i]['P1']['company_name'],
                                'gross_income' => $main_array[$i]['P1']['gross_income'],
                                'total_dependants' => $total_dependants,
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_income_dependants_PFS');
                        } else {
                            $insert_data = array(
                                'person_id_fk' => $main_array[$i]['P2']['person_id'],
                                'user_id_fk' => $user_id,
                                'person_type' => $main_array[$i]['P2']['person_type'],
                                'employment_type' => $main_array[$i]['P2']['employment_type'],
                                'company_name' => $main_array[$i]['P2']['company_name'],
                                'gross_income' => $main_array[$i]['P2']['gross_income'],
                                'total_dependants' => $total_dependants,
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_income_dependants_PFS');
                        }
                    }

                    $arr_to_return = array('error_code' => "0", 'msg' => 'success');
                    echo json_encode($arr_to_return);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail');
                    echo json_encode($arr_to_return);
                }
            }
        } else {
            $arr_to_return["msg"] = "no input data";
            echo json_encode($arr_to_return);
        }
    }

    public function wsPreLiabilitiesAddEdit() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error_code' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        $rent_per_month = $this->input->post('rent_per_month');
//        $arr_json = '[{"CAR_1":{"type":"C1","current_value":"10,000","lender":"ABC","loan_limit":"56,000","amount_owing":"1,020","monthly_payments":"500"}},{"CAR_2":{"type":"C2","current_value":"50,000","lender":"QWE","loan_limit":"500,000","amount_owing":"5,000","monthly_payments":"250"}},{"CREDIT_CARD_1":{"type":"CC1","lender":"ZAX","card_limit":"600,000","amount_owing":"26,000","monthly_payments":"350"}},{"CREDIT_CARD_2":{"type":"CC2","lender":"VBN","card_limit":"450,000","amount_owing":"50,000","monthly_payments":"450"}},{"OTHER_LOAN_1":{"type":"OL1","lender":"CVB","loan_limit":"456,000","amount_owing":"2,540","monthly_payments":"150"}},{"OTHER_LOAN_2":{"type":"OL2","lender":"ASD","loan_limit":"650,000","amount_owing":"2,500","monthly_payments":"350"}},{"LHP":{"type":"LHP","lender":"ERT","loan_limit":"65,000","amount_owing":"1,520","monthly_payments":"245"}}]';
//        $main_array = json_decode($arr_json, true);
        $post_main_array = json_decode($_REQUEST['main_array'], true);
        $main_array = $post_main_array['main_array'];

        if ($user_id) {
            if ($user_id != '' && (count($main_array) == 0)) {

                $new_liabilities_pfs = $this->common_model->getRecords('mst_liabilities_PFS', '', array("user_id_fk" => intval($user_id)));

                if (count($new_liabilities_pfs) > 0) {
                    $name['rent_per_month'] = isset($new_liabilities_pfs[0]['rent_per_month']) ? $new_liabilities_pfs[0]['rent_per_month'] : '';
                    $arr_return = array();
                    foreach ($new_liabilities_pfs as $key => $liabilities_pfs) {

                        $arr_return[$liabilities_pfs['loan_type']]['liability_id'] = isset($liabilities_pfs['liability_id']) ? $liabilities_pfs['liability_id'] : '';
                        $arr_return[$liabilities_pfs['loan_type']]['user_id_fk'] = isset($liabilities_pfs['user_id_fk']) ? stripslashes($liabilities_pfs['user_id_fk']) : '';
                        $arr_return[$liabilities_pfs['loan_type']]['loan_type'] = isset($liabilities_pfs['loan_type']) ? stripslashes($liabilities_pfs['loan_type']) : '';
                        $arr_return[$liabilities_pfs['loan_type']]['current_value'] = isset($liabilities_pfs['current_value']) ? $liabilities_pfs['current_value'] : '';
                        $arr_return[$liabilities_pfs['loan_type']]['lender'] = isset($liabilities_pfs['lender']) ? $liabilities_pfs['lender'] : '';
                        $arr_return[$liabilities_pfs['loan_type']]['loan_limit'] = isset($liabilities_pfs['loan_limit']) ? $liabilities_pfs['loan_limit'] : '';
                        $arr_return[$liabilities_pfs['loan_type']]['card_limit'] = isset($liabilities_pfs['card_limit']) ? $liabilities_pfs['card_limit'] : '';
                        $arr_return[$liabilities_pfs['loan_type']]['amount_owing'] = isset($liabilities_pfs['amount_owing']) ? $liabilities_pfs['amount_owing'] : '';
                        $arr_return[$liabilities_pfs['loan_type']]['monthly_payments'] = isset($liabilities_pfs['monthly_payments']) ? $liabilities_pfs['monthly_payments'] : '';
                    }
                    $arr_to_return = array('error_code' => "0", 'msg' => 'success', 'main_array' => array($arr_return), 'rent_per_month' => $name);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail');
                }
                echo json_encode($arr_to_return);
            } else {
                if (count($main_array) > 0) {
                    $this->common_model->deleteRows(array($user_id), "mst_liabilities_PFS", "user_id_fk");
                    for ($i = 0; $i <= 6; $i++) {
                        if ($main_array[$i]['CAR_1']['type'] == 'C1') {
                            $insert_data = array(
                                'user_id_fk' => $user_id,
                                'rent_per_month' => $rent_per_month,
                                'loan_type' => $main_array[$i]['CAR_1']['type'],
                                'current_value' => $main_array[$i]['CAR_1']['current_value'],
                                'lender' => $main_array[$i]['CAR_1']['lender'],
                                'loan_limit' => $main_array[$i]['CAR_1']['loan_limit'],
                                'amount_owing' => $main_array[$i]['CAR_1']['amount_owing'],
                                'monthly_payments' => $main_array[$i]['CAR_1']['monthly_payments'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_liabilities_PFS');
                        } else if ($main_array[$i]['CAR_2']['type'] == 'C2') {
                            $insert_data = array(
                                'user_id_fk' => $user_id,
                                'rent_per_month' => $rent_per_month,
                                'loan_type' => $main_array[$i]['CAR_2']['type'],
                                'current_value' => $main_array[$i]['CAR_2']['current_value'],
                                'lender' => $main_array[$i]['CAR_2']['lender'],
                                'loan_limit' => $main_array[$i]['CAR_2']['loan_limit'],
                                'amount_owing' => $main_array[$i]['CAR_2']['amount_owing'],
                                'monthly_payments' => $main_array[$i]['CAR_2']['monthly_payments'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_liabilities_PFS');
                        } else if ($main_array[$i]['CREDIT_CARD_1']['type'] == 'CC1') {
                            $insert_data = array(
                                'user_id_fk' => $user_id,
                                'rent_per_month' => $rent_per_month,
                                'loan_type' => $main_array[$i]['CREDIT_CARD_1']['type'],
                                'current_value' => $main_array[$i]['CREDIT_CARD_1']['current_value'],
                                'lender' => $main_array[$i]['CREDIT_CARD_1']['lender'],
                                'card_limit' => $main_array[$i]['CREDIT_CARD_1']['card_limit'],
                                'amount_owing' => $main_array[$i]['CREDIT_CARD_1']['amount_owing'],
                                'monthly_payments' => $main_array[$i]['CREDIT_CARD_1']['monthly_payments'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_liabilities_PFS');
                        } else if ($main_array[$i]['CREDIT_CARD_2']['type'] == 'CC2') {
                            $insert_data = array(
                                'user_id_fk' => $user_id,
                                'rent_per_month' => $rent_per_month,
                                'loan_type' => $main_array[$i]['CREDIT_CARD_2']['type'],
                                'current_value' => $main_array[$i]['CREDIT_CARD_2']['current_value'],
                                'lender' => $main_array[$i]['CREDIT_CARD_2']['lender'],
                                'card_limit' => $main_array[$i]['CREDIT_CARD_2']['card_limit'],
                                'amount_owing' => $main_array[$i]['CREDIT_CARD_2']['amount_owing'],
                                'monthly_payments' => $main_array[$i]['CREDIT_CARD_2']['monthly_payments'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_liabilities_PFS');
                        } else if ($main_array[$i]['OTHER_LOAN_1']['type'] == 'OL1') {
                            $insert_data = array(
                                'user_id_fk' => $user_id,
                                'rent_per_month' => $rent_per_month,
                                'loan_type' => $main_array[$i]['OTHER_LOAN_1']['type'],
                                'current_value' => $main_array[$i]['OTHER_LOAN_1']['current_value'],
                                'lender' => $main_array[$i]['OTHER_LOAN_1']['lender'],
                                'loan_limit' => $main_array[$i]['OTHER_LOAN_1']['loan_limit'],
                                'amount_owing' => $main_array[$i]['OTHER_LOAN_1']['amount_owing'],
                                'monthly_payments' => $main_array[$i]['OTHER_LOAN_1']['monthly_payments'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_liabilities_PFS');
                        } else if ($main_array[$i]['OTHER_LOAN_2']['type'] == 'OL2') {
                            $insert_data = array(
                                'user_id_fk' => $user_id,
                                'rent_per_month' => $rent_per_month,
                                'loan_type' => $main_array[$i]['OTHER_LOAN_2']['type'],
                                'current_value' => $main_array[$i]['OTHER_LOAN_2']['current_value'],
                                'lender' => $main_array[$i]['OTHER_LOAN_2']['lender'],
                                'loan_limit' => $main_array[$i]['OTHER_LOAN_2']['loan_limit'],
                                'amount_owing' => $main_array[$i]['OTHER_LOAN_2']['amount_owing'],
                                'monthly_payments' => $main_array[$i]['OTHER_LOAN_2']['monthly_payments'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_liabilities_PFS');
                        } else if ($main_array[$i]['LHP']['type'] == 'LHP') {
                            $insert_data = array(
                                'user_id_fk' => $user_id,
                                'rent_per_month' => $rent_per_month,
                                'loan_type' => $main_array[$i]['LHP']['type'],
                                'current_value' => $main_array[$i]['LHP']['current_value'],
                                'lender' => $main_array[$i]['LHP']['lender'],
                                'loan_limit' => $main_array[$i]['LHP']['loan_limit'],
                                'amount_owing' => $main_array[$i]['LHP']['amount_owing'],
                                'monthly_payments' => $main_array[$i]['LHP']['monthly_payments'],
                                'added_date' => date("Y-m-d H:i:s"),
                            );
                            $this->common_model->insertRow($insert_data, 'mst_liabilities_PFS');
                        }
                    }

                    $arr_to_return = array('error_code' => "0", 'msg' => 'success');
                    echo json_encode($arr_to_return);
                } else {
                    $arr_to_return = array('error_code' => "1", 'msg' => 'fail');
                    echo json_encode($arr_to_return);
                }
            }
        } else {
            $arr_to_return["msg"] = "no input data";
            echo json_encode($arr_to_return);
        }
    }

    public function wsGetDashboardDetails() {
        $arrToReturn = array();
        $user_id = $this->input->post('user_id');
        $arr_user_detail = $this->common_model->getRecords("mst_users", "", array('user_id' => $user_id));
        if (count($arr_user_detail) == 0) {
            $arrToReturn = array('error' => 3, 'message' => "Sorry you are not registered with this site.");
            echo json_encode($arrToReturn);
            exit();
        }
        /** Get Income PA * */
        $income_condition = array('user_id_fk' => $user_id);
        $get_income_data = $this->common_model->getRecords('mst_income_dependants_PFS', $fields = 'gross_income,person_type,added_date', $income_condition, $order_by = '', $limit = '', $debug = 0);
        $get_total_income = end($this->common_model->getRecords('mst_income_dependants_PFS', $fields = 'sum(gross_income) as total_gross_income', $income_condition, $order_by = '', $limit = '', $debug = 0));

//        echo '<pre>';print_r($get_persone_arr);die;
        foreach ($get_income_data as $income) {

            $get_persone_arr = end($this->common_model->getRecords('mst_persons_PFS', $fields = 'first_name,last_name', array('user_id_fk' => $user_id, 'person_type' => $income['person_type']), $order_by = '', $limit = '', $debug = 0));

            $gross_income = $this->formatCash($income['gross_income']);
            $total_gross_income = $this->formatCash($get_total_income['total_gross_income']);
//            if ($income['gross_income'] < 1000) {
//                $gross_income = round($income['gross_income']);
//            } else {
//                $gross_income = round($income['gross_income'] / 1000000, 2) . 'M';
//            }
//
//            if ($get_total_income['total_gross_income'] <= 1000) {
//                $total_gross_income = round($get_total_income['total_gross_income']);
//            } else {
//                $total_gross_income = round($get_total_income['total_gross_income'] / 1000000, 2) . 'M';
//            }

            $new_income['gross_income_' . $income['person_type']] = isset($gross_income) ? $gross_income : '0';
            $new_income['total_gross_income'] = isset($total_gross_income) ? $total_gross_income : '0';
            $new_income['first_name_' . $income['person_type']] = isset($get_persone_arr['first_name']) ? $get_persone_arr['first_name'] : '';
            $new_income['last_name_' . $income['person_type']] = isset($get_persone_arr['last_name']) ? $get_persone_arr['last_name'] : '';
            $new_income['added_date'] = isset($income['added_date']) ? date("d M y H:i", strtotime($income['added_date'])) : '';
        }
        /** Get Income PA * */
        /** Get Superannoation * */
        $superannuation_condition = array('user_id_fk' => $user_id);
        $get_superannuation_data = $this->common_model->getRecords('mst_smsf_insurances_PFS', $fields = 'current_super_balance,person_type,added_date', $superannuation_condition, $order_by = '', $limit = '', $debug = 0);
        $get_total_super = end($this->common_model->getRecords('mst_smsf_insurances_PFS', $fields = 'sum(current_super_balance) as total_super', $superannuation_condition, $order_by = '', $limit = '', $debug = 0));

        foreach ($get_superannuation_data as $super) {
            $get_persone_arr = end($this->common_model->getRecords('mst_persons_PFS', $fields = 'first_name,last_name', array('user_id_fk' => $user_id, 'person_type' => $super['person_type']), $order_by = '', $limit = '', $debug = 0));
            $CSB = $this->formatCash($super['current_super_balance']);

//            if ($super['current_super_balance'] <= 1000) {
//                $CSB = round($super['current_super_balance']);
//            } else {
//                $CSB = round($super['current_super_balance'] / 1000000, 2) . 'M';
//            }

            $total_super = $this->formatCash($get_total_super['total_super']);
//            if ($get_total_super['total_super'] <= 1000) {
//                $total_super = round($get_total_super['total_super']);
//            } else {
//                $total_super = round($get_total_super['total_super'] / 1000000, 2) . 'M';
//            }

            $new_superannuation['current_super_balance_' . $super['person_type']] = isset($CSB) ? $CSB : '0';
            $new_superannuation['total_super'] = isset($total_super) ? $total_super : '0';
            $new_superannuation['first_name_' . $super['person_type']] = isset($get_persone_arr['first_name']) ? $get_persone_arr['first_name'] : '';
            $new_superannuation['last_name_' . $super['person_type']] = isset($get_persone_arr['last_name']) ? $get_persone_arr['last_name'] : '';
            $new_superannuation['added_date'] = isset($super['added_date']) ? date("d M y H:i", strtotime($super['added_date'])) : '';
        }
        /** Get Superannoation * */
        /** Property Market Value * */
        $property_condition = array('user_id' => $user_id);
        $get_property = $this->common_model->getRecords('mst_properties', $fields = 'sum(valuation) as total_market_value,count(property_id) as properties_owned', $property_condition, $order_by = '', $limit = '', $debug = 0);

        $properties_owned = $get_property[0]['properties_owned'];
        $total_market_value = ($get_property[0]['total_market_value']);

        $market_value = $this->formatCash($total_market_value);
//        if ($total_market_value <= 1000) {
//            $market_value = round($total_market_value);
//        } else {
//            $market_value = round($total_market_value / 1000000, 2) . 'M';
//        }

        $properties_value['total_market_value'] = isset($market_value) ? $market_value : '0';
        $properties_value['properties_owned'] = isset($properties_owned) ? $properties_owned : '0';

        /** Property Market Value * */
        /** LVR And Equity * */
        $property_lvr_equity_condition = array('user_id' => $user_id);
        $get_property_lvr_equity = $this->common_model->getRecords('mst_properties', $fields = 'sum(LVR) as total_LVR,count(property_id) as total_properties, sum(current_equity) as total_current_equity', $property_lvr_equity_condition, $order_by = '', $limit = '', $debug = 0);

        $total_LVR = round($get_property_lvr_equity[0]['total_LVR'] / $get_property_lvr_equity[0]['total_properties']);
        $total_current_equity = round($get_property_lvr_equity[0]['total_current_equity'] * 0.8);
//        $total_market_value = ($get_property[0]['total_market_value']);
//        if ($total_market_value <= 1000) {
//            $market_value = round($total_market_value);
//        } else {
//            $market_value = round($total_market_value / 1000000, 2) . 'M';
//        }

        $LVR_Equity['total_LVR'] = isset($total_LVR) ? $total_LVR : '0';

        $amount_current_equity = $this->formatCash($total_current_equity);
//        if ($total_current_equity <= 1000) {
//            $amount_current_equity = round($total_current_equity);
//        } else {
//            $amount_current_equity = round($total_current_equity / 1000000, 2) . 'M';
//        }
        $LVR_Equity['total_current_equity_at'] = isset($amount_current_equity) ? $amount_current_equity : '0';
//        $properties_value['properties_owned'] = isset($properties_owned) ? $properties_owned : '0';

        /** LVR And Equity * */
        /** Property Loans * */
        $property_loan_condition = array('user_id_fk' => $user_id);
//        $get_property_loans = end($this->common_model->getRecords('mst_liabilities_PFS', $fields = 'sum(amount_owing) as total_amount_owing', $property_loan_condition, $order_by = '', $limit = '', $debug = 0));
        $get_property_loans = end($this->common_model->getRecords('mst_properties', $fields = 'sum(current_loan_balance) as total_amount_owing', array('user_id' => $user_id), $order_by = '', $limit = '', $debug = 0));

        $get_property_loan_limit = end($this->common_model->getRecords('mst_properties', $fields = 'sum(total_loan_limit) as properties_loan_limit', array('user_id' => $user_id), $order_by = '', $limit = '', $debug = 0));
// echo '<pre>';print_r($get_property_loan_limit);die;
        $total_amount_owing = round($get_property_loans['total_amount_owing']);
        $total_loan_limit = round($get_property_loan_limit['properties_loan_limit']);

        $amount_owing = $this->formatCash($total_amount_owing);
//        if ($total_amount_owing <= 1000) {
//            $amount_owing = round($total_amount_owing);
//        } else {
//            $amount_owing = round($total_amount_owing / 1000000, 2) . 'M';
//        }
        $loan_limit = $this->formatCash($total_loan_limit);
//        if ($total_loan_limit <= 1000) {
//            $loan_limit = round($total_loan_limit);
//        } else {
//            $loan_limit = round($total_loan_limit / 1000000, 2) . 'M';
//        }

        $property_loan['total_amount_owing'] = isset($amount_owing) ? $amount_owing : '0';
        $property_loan['total_loan_limit'] = isset($loan_limit) ? $loan_limit : '0';

        /** Property Loans * */
        /*         * Property Yield and Rent* */
        $property_yield_condition = array('user_id' => $user_id);
        $get_property_yield = end($this->common_model->getRecords('mst_properties', $fields = 'sum(weekly_rent) as total_weekly_rent,sum(valuation) as total_valuation', $property_yield_condition, $order_by = '', $limit = '', $debug = 0));

        $current_yield = (($get_property_yield['total_weekly_rent'] * 52) / ($get_property_yield['total_valuation'])) * 100;
        $rent_per_annual = (($get_property_yield['total_weekly_rent'] / 7) * 365);
        $rent_per_week = ($get_property_yield['total_weekly_rent']);

        $rent_annual = $this->formatCash($rent_per_annual);
//        if ($rent_per_annual <= 1000) {
//            $rent_annual = round($rent_per_annual);
//        } else {
//            $rent_annual = round($rent_per_annual / 1000000, 2) . 'M';
//        }

        $rent_week = $this->formatCash($rent_per_week);

//        if ($rent_per_week <= 1000) {
//            $rent_week = round($rent_per_week);
//        } else {
//            $rent_week = round($rent_per_week / 1000000, 2) . 'M';
//        }

        $property_yield_rent['gross_yield'] = isset($current_yield) ? round($current_yield) : '0';
        $property_yield_rent['total_rent_PW'] = isset($rent_week) ? ($rent_week) : '0';
        $property_yield_rent['total_rent_PA'] = isset($rent_annual) ? ($rent_annual) : '0';

        /** Property Yield and Rent* */
        /** Get Cars Value * */
        $car_condition = "((user_id_fk='" . $user_id . "') AND (loan_type='C1' OR loan_type='C2'))";
        $get_cars = end($this->common_model->getRecords('mst_liabilities_PFS', $fields = 'sum(current_value) as total_current_value,sum(amount_owing) as total_amount_owing,sum(monthly_payments) as total_monthly_payments', $car_condition, $order_by = '', $limit = '', $debug = 0));

        $total_current_value = ($get_cars['total_current_value']);
        $total_amount_owing = ($get_cars['total_amount_owing']);
        $total_monthly_payments = ($get_cars['total_monthly_payments']);

        $total_value = $this->formatCash($total_current_value);
//        if ($total_current_value <= 1000) {
//            $total_value = round($total_current_value);
//        } else {
//            $total_value = round($total_current_value / 1000000, 2) . 'M';
//        }

        $total_owing = $this->formatCash($total_amount_owing);
//        if ($total_amount_owing <= 1000) {
//            $total_owing = round($total_amount_owing);
//        } else {
//            $total_owing = round($total_amount_owing / 1000000, 2) . 'M';
//        }

        $total_payment_PM = $this->formatCash($total_monthly_payments);
//        if ($total_monthly_payments <= 1000) {
//            $total_payment_PM = round($total_monthly_payments);
//        } else {
//            $total_payment_PM = round($total_monthly_payments / 1000000, 2) . 'M';
//        }

        $car_loan['total_value'] = isset($total_value) ? $total_value : '0';
        $car_loan['total_owing'] = isset($total_owing) ? $total_owing : '0';
        $car_loan['total_payment_PM'] = isset($total_payment_PM) ? $total_payment_PM : '0';
        /** Get Cars Value * */
        /** Get Other Loans Value * */
        $other_condition = "((user_id_fk='" . $user_id . "') AND (loan_type='OL1' OR loan_type='OL2'))";
        $get_other_loan = end($this->common_model->getRecords('mst_liabilities_PFS', $fields = 'sum(loan_limit) as total_loan_limit,sum(amount_owing) as total_amount_owing,sum(monthly_payments) as total_monthly_payments', $other_condition, $order_by = '', $limit = '', $debug = 0));


        $total_loan_limits = ($get_other_loan['total_loan_limit']);
        $total_amount_owing = ($get_other_loan['total_amount_owing']);
        $total_monthly_payments = ($get_other_loan['total_monthly_payments']);

        $total_loan_limit = $this->formatCash($total_loan_limits);
//        if ($total_loan_limits <= 1000) {
//            $total_loan_limit = round($total_loan_limits);
//        } else {
//            $total_loan_limit = round($total_loan_limits / 1000000, 2) . 'M';
//        }

        $total_owing = $this->formatCash($total_amount_owing);

//        if ($total_amount_owing <= 1000) {
//            $total_owing = round($total_amount_owing);
//        } else {
//            $total_owing = round($total_amount_owing / 1000000, 2) . 'M';
//        }

        $total_payment_PM = $this->formatCash($total_monthly_payments);
//        if ($total_monthly_payments <= 1000) {
//            $total_payment_PM = round($total_monthly_payments);
//        } else {
//            $total_payment_PM = round($total_monthly_payments / 1000000, 2) . 'M';
//        }

        $other_loan['total_loan_limit'] = isset($total_loan_limit) ? $total_loan_limit : '0';
        $other_loan['total_owing'] = isset($total_owing) ? $total_owing : '0';
        $other_loan['total_payment_PM'] = isset($total_payment_PM) ? $total_payment_PM : '0';

        /** Get Other Loans Value Value * */
        /** Get Lease/Hire Purchases Value * */
        $LHP_condition = array('user_id_fk' => $user_id, 'loan_type' => 'LHP');
        $get_LHP = end($this->common_model->getRecords('mst_liabilities_PFS', $fields = 'sum(loan_limit) as total_loan_limit,sum(amount_owing) as total_amount_owing,sum(monthly_payments) as total_monthly_payments', $LHP_condition, $order_by = '', $limit = '', $debug = 0));

        $total_loan_limits = ($get_LHP['total_loan_limit']);
        $total_amount_owing = ($get_LHP['total_amount_owing']);
        $total_monthly_payments = ($get_LHP['total_monthly_payments']);

        $total_loan_limit = $this->formatCash($total_loan_limits);
//        if ($total_loan_limits <= 1000) {
//            $total_loan_limit = round($total_loan_limits);
//        } else {
//            $total_loan_limit = round($total_loan_limits / 1000000, 2) . 'M';
//        }

        $total_owing = $this->formatCash($total_amount_owing);
//        if ($total_amount_owing <= 1000) {
//            $total_owing = round($total_amount_owing);
//        } else {
//            $total_owing = round($total_amount_owing / 1000000, 2) . 'M';
//        }

        $total_payment_PM = $this->formatCash($total_monthly_payments);
//        if ($total_monthly_payments <= 1000) {
//            $total_payment_PM = round($total_monthly_payments);
//        } else {
//            $total_payment_PM = round($total_monthly_payments / 1000000, 2) . 'M';
//        }
//        
        $gross_income = $this->formatCash($income['gross_income']);
        $LHP_loan['total_loan_limit'] = isset($total_loan_limit) ? $total_loan_limit : '0';
        $LHP_loan['total_owing'] = isset($total_owing) ? $total_owing : '0';
        $LHP_loan['total_payment_PM'] = isset($total_payment_PM) ? $total_payment_PM : '0';

        /** Get Lease/Hire Purchases Value * */
        /** Get Assets Value * */
        $assets_condition = array('user_id_fk' => $user_id);
        $get_assets = end($this->common_model->getRecords('mst_assets_PFS', $fields = 'other_assets,shares_bonds,total_savings', $assets_condition, $order_by = '', $limit = '', $debug = 0));

        $other_assets = $this->formatCash($get_assets['other_assets']);
//        if ($get_assets['other_assets'] <= 1000) {
//            $other_assets = round($get_assets['other_assets']);
//        } else {
//            $other_assets = round($get_assets['other_assets'] / 1000000, 2) . 'M';
//        }

        $shares_bonds = $this->formatCash($get_assets['shares_bonds']);
//        if ($get_assets['shares_bonds'] <= 1000) {
//            $shares_bonds = round($get_assets['shares_bonds']);
//        } else {
//            $shares_bonds = round($get_assets['shares_bonds'] / 1000000, 2) . 'M';
//        }

        $total_savings = $this->formatCash($get_assets['total_savings']);
//        if ($get_assets['total_savings'] <= 1000) {
//            $total_savings = round($get_assets['total_savings']);
//        } else {
//            $total_savings = round($get_assets['total_savings'] / 1000000, 2) . 'M';
//        }

        $gross_income = $this->formatCash($income['gross_income']);
        $assets['total_other_assets'] = isset($other_assets) ? $other_assets : '0';
        $assets['total_shares_bonds'] = isset($shares_bonds) ? $shares_bonds : '0';
        $assets['total_savings'] = isset($total_savings) ? $total_savings : '0';
        /** Get Assets Value * */
        if ($user_id != '') {
            $arrToReturn = array('error' => 0, 'message' => "Success", 'income_PA' => $new_income, 'superannuation' => $new_superannuation, 'property_market_value' => $properties_value, 'lvr_and_equity' => $LVR_Equity, 'property_loans' => $property_loan, 'property_yield_rent' => $property_yield_rent, 'cars' => $car_loan, 'other_loan' => $other_loan, 'lease_hire_purchases' => $LHP_loan, 'Assets' => $assets);
        } else {
            $arrToReturn = array('error' => 1, 'message' => "Fail");
        }
        echo json_encode($arrToReturn);
    }

    function formatCash($cash) {
// strip any commas
        $cash = (0 + str_replace(',', '', $cash));
// make sure it's a number...
        if (!is_numeric($cash)) {
            return false;
        }

        if ($cash >= 0) {
            if ($cash >= 1000000) {
                return round(($cash / 1000000), 2) . 'M';
            } elseif ($cash >= 1000) {
                return round(($cash / 1000)) . 'K';
            } else {
                return number_format($cash);
            }
        } else {
            $cash = trim(str_replace('-', '', $cash));
            if ($cash >= 1000000) {
                return '-' . round(($cash / 1000000), 2) . 'M';
            } elseif ($cash >= 1000) {
                return '-' . round(($cash / 1000)) . 'K';
            } else {
                return '-' . number_format($cash);
            }
        }
    }

}
