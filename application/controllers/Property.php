<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ('soapclient/SforcePartnerClient.php');
require_once ('soapclient/SforceHeaderOptions.php');

class Property extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('property_model');
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
            exit();
        }
    }

    /* Start :: Function to list all the properties */

    public function listProperty() {
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('9', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("msg", "<span class='error'>You doesn't have priviliges to  manage property!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if (count($this->input->post()) > 0) {
            if ($this->input->post('btn_delete_all') != "") {
                /* getting all ides selected */
                $arr_property_ids = $this->input->post('checkbox');
                if (count($arr_property_ids) > 0 && is_array($arr_property_ids)) {
                    foreach ($arr_property_ids as $property_id) {
                        /* getting user details */
                        $arr_property_detail = $this->common_model->getRecords("mst_properties", "", array("property_id" => intval($property_id)));
                        if (count($arr_property_detail) > 0) {
                            /* Delete details from salesforce */
                            $mySforceConnection = new SforcePartnerClient();
                            $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                            // Simple example of session management - first call will do
                            // login, refresh will use session ID and location cached in
                            // PHP session
                            if (($this->session->userdata('partnerSessionId')) !== null) {
                                $location = $this->session->userdata('partnerLocation');
                                $sessionId = $this->session->userdata('partnerSessionId');
                                $mySforceConnection->setEndpoint($location);
                                $mySforceConnection->setSessionHeader($sessionId);
                            } else {
                                $mySforceConnection->login(USERNAME, PASSWORD);
                                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                            }
                            $salesforceId = $arr_property_detail[0]['salesforce_property_id'];
                            if ($salesforceId != null) {
                                $response = $mySforceConnection->delete($salesforceId);
                            }
                        }
                    }
                    if (count($arr_property_ids) > 0) {
                        /* deleting the property selected */
                        $this->common_model->deleteRows($arr_property_ids, "mst_properties", "property_id");
                    }
                    $this->session->set_userdata("msg", "<span class='success'>Property deleted successfully!</span>");
                }
            }
        }
        /* using the property model */
        $data['title'] = "Manage Property";
        $data['arr_property_list'] = $this->property_model->getPropertyDetails($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        for ($i = 0; $i < count($data['arr_property_list']); $i++) {
            $data['arr_property_list'][$i]['media'] = $this->property_model->getPropertyImageDetails($table_to_pass = '', $fields_to_pass = '', array('property_id' => $data['arr_property_list'][$i]['property_id']), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        }
        $this->load->view('backend/property/list', $data);
    }

    /* Start :: Add property function */

    public function addProperty() {
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('9', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("msg", "<span class='error'>You doesn't have priviliges to  manage property!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if (count($this->input->post()) > 0) {
            if ($this->input->post('property_name') != "") {

                /* property record to add */
                $userData = explode("###", $this->input->post('user_id'));
                $user_id = $userData[0];
                $salesforce_user_id = $userData[1];

                $property_name = $this->input->post('property_name');
                $locality_id = $this->input->post('locality_id');
                $property_address = $this->input->post('property_address');
                $corelogic_property_id = $this->input->post('corelogic_property_id');
                $property_type = $this->input->post('property_type');
                $ownership_type = $this->input->post('ownership_type');
                $property_specification = $this->input->post('property_specification');
                $property_suburb = $this->input->post('property_suburb');
                $property_state = $this->input->post('property_state');
                $property_postcode = $this->input->post('property_postcode');
                $land_size = $this->input->post('land_size');
                $dwelling_footprint = $this->input->post('dwelling_footprint');
                if(strtoupper($property_type) == 'LAND'){
                    $bedrooms = '0';
                    $bathrooms = '0';
                    $carparks = '0';
                }else{
                    $bedrooms = $this->input->post('bedrooms');
                    $bathrooms = $this->input->post('bathrooms');
                    $carparks = $this->input->post('carparks');
                }
                $purchase_price = $this->input->post('purchase_price');
                $valuation = $this->input->post('valuation');
                $loan_amount = $this->input->post('loan_amount');
                $property_purchase_date = $this->input->post('purchase_date');
                $property_purchase_price = $this->input->post('purchase_price');
//                $next_repayment_date = $this->input->post('next_repayment_date');
//                $payment_frequency = $this->input->post('payment_frequency');
                $interest_rate = $this->input->post('interest_rate');
//                $rate_term = $this->input->post('rate_term');
//                $loan_payment = $this->input->post('loan_payment');

                if ($land_size > 0) {
                    $coreLogicSQM = round($purchase_price / $land_size);
                }

                if ($property_specification == 'Investment') {
                    $ppor_status = 'Yes';
                    $weekly_rent = str_replace(",", "", $this->input->post('weekly_rent'));
                } else {
                    $ppor_status = $this->input->post('ppor_status');
                    $weekly_rent = "0";
                }
                
                $purchase_date = date("Y-m-d", strtotime($property_purchase_date));
                $datetime1 = new DateTime(date("Y-m-d"));
                $datetime2 = new DateTime($purchase_date);
                $difference = $datetime1->diff($datetime2);
                $years_woned = ($difference->y * 365);
                $years_woned = $years_woned + ($difference->m * 30);
                $years_woned = round(($years_woned + $difference->d) / 365, 1);

                $purchase_SQM = 0;
                $value_SQM = 0;
                if (strtoupper($property_type) == 'LAND') {
                    //calculating purchase sqm
                    if ($property_purchase_price != 0 && $property_purchase_price != '') {
                        $purchase_SQM = ($property_purchase_price) / ($land_size);
                    }
                    if ($valuation != 0 && $valuation != '') {
                        $value_SQM = ($valuation) / ($land_size);
                    }
                } else {
                    if ($property_purchase_price != 0 && $property_purchase_price != '') {
                        $purchase_SQM = ($property_purchase_price * 0.3) / ($land_size);
                    }
                    if ($valuation != 0 && $valuation != '') {
                        $value_SQM = ($valuation * 0.3) / ($land_size);
                    }
                }

//                switch ($payment_frequency) {
//                    case 'weekly':
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
//                        break;
//                    case 'monthly':
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 30 days'));
//                        break;
//                    case 'fortnight':
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 14 days'));
//                        break;
//                    default:
//                        $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
//                }

                $arr_to_insert = array(
                    "property_name" => addslashes($property_name),
                    "locality_id" => addslashes($locality_id),
                    "property_address" => addslashes($property_address),
                    "user_id" => $user_id,
                    "corelogic_property_id" => $corelogic_property_id,
                    "property_type" => $property_type,
                    "ownership_type" => $ownership_type,
                    "property_specification" => $property_specification,
                    "locality" => addslashes($property_suburb),
                    "ppor_status" => $ppor_status,
                    "property_state" => addslashes($property_state),
                    "property_postcode" => addslashes($property_postcode),
                    "property_suburb" => addslashes($property_suburb),
                    "land_size" => $land_size,
                    "dwelling_footprint" => $dwelling_footprint,
                    "bedrooms" => $bedrooms,
                    "bathrooms" => $bathrooms,
                    "carparks" => $carparks,
                    "purchase_price" => $purchase_price,
                    "valuation" => $valuation,
                    "value_SQM" => $value_SQM,
                    "purchase_SQM" => $purchase_SQM,
                    "current_equity" => ($valuation) - $loan_amount,
                    "capital_growth" => round(((($valuation - $purchase_price) / $purchase_price) * 100), 1),
                    "LVR" => ($loan_amount / $valuation) * 100,
                    "IRR" => ((($valuation - $purchase_price) / $years_woned) / (($purchase_price * 20) / 10000)),
                    "ROI" => (($valuation - $purchase_price) / (($purchase_price * 20))) * 10000,
                    "years_woned" => $years_woned,
                    "latitude" => $this->input->post('latitude'),
                    "longitude" => $this->input->post('longitude'),
                    "purchase_date" => $purchase_date,
                    'created_at' => date("Y-m-d H:i:s"),
                    "income_cron_updated" => date("Y-m-d", strtotime('+ 7 days')),
                    "cron_updated_status" => '0'
                );
               
                /* Insert :: inserting property details into the database */
                $property_insert_id = $this->common_model->insertRow($arr_to_insert, "mst_properties");
                if ($loan_amount > 0 && $loan_amount != '0' && $loan_amount != '') {
                    /* Insert :: property dept data */
                    $arr_to_property_dept_insert = array(
                        "loan_amount" => $loan_amount,
                        "initial_loan_amount" => $loan_amount,
                        "property_id" => $property_insert_id,
                        "user_id" => $user_id,
                        "interest_rate" => $interest_rate,
//                        "rate_term" => $rate_term,
//                        "payment_frequency" => $payment_frequency,
//                        "next_payment_date" => $next_payment_date,
//                        "next_payment_amount" => $loan_payment,
                        "added_date" => date("Y-m-d"),
                    );
                    $debt_id = $this->common_model->insertRow($arr_to_property_dept_insert, "trans_debts");
                }
                /** Insert :: property income data */
                $arr_to_property_income_insert = array(
                    "property_id" => $property_insert_id,
                    "user_id" => $user_id,
                    "weekly_rent" => $weekly_rent,
                    "added_date" => date("Y-m-d")
                );
                $income_id = $this->common_model->insertRow($arr_to_property_income_insert, "trans_incomes");

                ## Adding multiple image for property start
                $count = count($_FILES['property_images']['name']);
                $counter = 1;
                if ($_FILES['property_images']['name'][0] != '') {
                    for ($s = 0; $s <= $count - 1; $s++) {
                        $_FILES['property_images' . $s] = array();
                        $_FILES['property_images' . $s]['name'] = $_FILES['property_images']['name'][$s];
                        $_FILES['property_images' . $s]['type'] = $_FILES['property_images']['type'][$s];
                        $_FILES['property_images' . $s]['tmp_name'] = $_FILES['property_images']['tmp_name'][$s];
                        $config['file_name'] = rand() . time() . $s;
                        $_FILES['property_images' . $s]['error'] = $_FILES['property_images']['error'][$s];
                        $_FILES['property_images' . $s]['size'] = $_FILES['property_images']['size'][$s];
                        $config['upload_path'] = './media/front/img/property-images/';
                        $config['allowed_types'] = '*';
                        $config['max_size'] = '5000';
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload("property_images" . $s)) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_userdata('msg', $error);
                            redirect(base_url() . 'backend/property/list');
                        } else {
                            $this->load->library('image_lib');
                            $data = array('upload_data' => $this->upload->data());
                            $image_data = $this->upload->data();
                            $file_name = $image_data['file_name'];
                            $width = 300;
                            $height = 300;
                            $config = array(
                                'source_image' => $image_data['full_path'],
                                'task_images' => './media/front/img/property-images/thumbs/',
                                'maintain_ration' => false,
                                'width' => $width,
                                'height' => $height
                            );
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $this->image_lib->clear();

                            $absolute_path = $this->common_model->absolutePath();
                            $image_path = $absolute_path . "media/front/img/property-images/";
                            $image_main = $image_path . "/" . $file_name;

                            $thumbs_image1 = $image_path . "/thumbs/" . $file_name;
                            $thumbs_image2 = $image_path . "/small-thumbs/" . $file_name;

                            $str_console1 = "convert " . $image_main . " -resize 394!X284! " . $thumbs_image1;
                            exec($str_console1);
                            $str_console = "convert " . $image_main . " -resize 198!X163! " . $thumbs_image2;
                            exec($str_console);
                        }
                        $image_data = $this->upload->data();
                        $fields = array(
                            'property_id' => $property_insert_id,
                            'image_name' => $file_name,
                            'status' => '1',
                            'sequence' => $counter,
                        );
                        $table = "trans_property_images";
                        $this->common_model->insertRow($fields, $table);
                        $counter++;
                    }
                }

                /* Graph value insert here */
//                ============================
                /* Graph value insert here */

                $this->session->set_userdata("msg", "<span class='success'>Property added successfully!</span>");

                /* Start :: Salesforce integration */
                $mySforceConnection = new SforcePartnerClient();
                $mySoapClient = $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                // Simple example of session management - first call will do
                // login, refresh will use session ID and location cached in
                // PHP session
                if (($this->session->userdata('partnerSessionId')) !== null) {
                    $location = $this->session->userdata('partnerLocation');
                    $sessionId = $this->session->userdata('partnerSessionId');

                    $mySforceConnection->setEndpoint($location);
                    $mySforceConnection->setSessionHeader($sessionId);
                } else {
                    $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);
                    $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                    $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                }

                $IRR = round((($valuation - $purchase_price) / $years_woned) / (($purchase_price * 20) / 10000));
                $LVR = round(($loan_amount / $valuation) * 100);
                $ROI = round((($valuation - $purchase_price) / (($purchase_price * 20))) * 10000);
                $purchase_date = date("Y-m-d", strtotime());

                $arr_property_fields = array(
                    'Name' => $property_name,
                    'User_ID__c' => $salesforce_user_id,
                    'Bathrooms__c' => $bathrooms,
                    'Bedrooms__c' => $bedrooms,
                    'Capital_Growth__c' => round(((($valuation - $purchase_price) / $purchase_price) * 100), 1),
                    'Carparks__c' => $carparks,
                    'Created_At__c' => date("Y-m-d") . 'T' . date("H:i:s"),
                    'Current_Equity__c' => ($valuation) - $loan_amount,
                    'Dwelling_Footprint__c' => $dwelling_footprint,
                    'IRR__c' => $IRR,
                    'Land_Size__c' => $land_size,
                    'Latitude__c' => $this->input->post('latitude'),
                    'Longitude__c' => $this->input->post('longitude'),
                    'LVR__c' => $LVR,
                    'Property_Address__c' => htmlentities(addslashes($property_address)),
                    'Property_Specification__c' => $property_specification,
                    'Property_State__c' => htmlentities($this->input->post('property_state')),
                    'Property_Suburb__c' => htmlentities($this->input->post('property_suburb')),
                    'PropertyType__c' => htmlentities($property_type),
                    'Purchase_Date__c' => $purchase_date,
                    'Purchase_Price__c' => $purchase_price,
                    'ROI__c' => $ROI,
                    'Valuation__c' => $valuation,
                    'Value_SQM__c' => $value_SQM,
                    'Purchase_SQM__c' => $purchase_SQM,
                    'Years_Woned__c' => $years_woned
                );

                $sObject = new SObject();
                $sObject->fields = $arr_property_fields;
                $sObject->type = 'Property__c';
                $createResponse = $mySforceConnection->create(array($sObject));
                //print_r($createResponse);
                $salesforcePropertyId = $createResponse[0]->id;
                /* Update :: salesforce id in local database for further use. */
                $table_name = 'mst_properties';
                $update_data = array(
                    'salesforce_property_id' => $salesforcePropertyId
                );
                $condition = array(
                    'property_id' => $property_insert_id
                );
                if ($property_insert_id) {
                    $this->common_model->updateRow($table_name, $update_data, $condition);
                }
                /* Start :: Insert debt */
                $arr_debt_fields = array(
                    'User_ID__c' => $salesforce_user_id,
                    'Property_ID__c' => $salesforce_property_id,
                    'Interest_Rate__c' => $this->input->post('interest_rate'),
                    'Loan_Amount__c' => $loan_amount,
                    'Initial_Loan_Amount__c' => $loan_amount,
                    'Loan_Type__c' => 'Fixed',
//                    'Next_Payment_Date__c' => date("Y-m-d", strtotime($next_payment_date)) . 'T' . date("H:i:s"),
//                    'Payment_Frequency__c' => $this->input->post('payment_frequency'),
//                    'Rate_Term__c' => $this->input->post('rate_term'),
                    'Added_Date__c' => date('Y-m-d'),
                );

                $sDebtObject = new SObject();
                $sDebtObject->fields = $arr_debt_fields;
                $sDebtObject->type = 'Debt__c';
                $createDebtResponse = $mySforceConnection->create(array($sDebtObject));
//                print_r($createDebtResponse);
                $salesforceDebtId = $createDebtResponse[0]->id;
                /* Update :: salesforce id in local database for further use. */
                $table_name = 'trans_debts';
                $update_data = array(
                    'salesforce_debt_id' => $salesforceDebtId
                );
                $condition = array(
                    'property_id' => $property_insert_id,
                    'user_id' => $user_id
                );
                if ($property_insert_id) {
                    $this->common_model->updateRow($table_name, $update_data, $condition);
                }

                /** Insert Income */
                $arr_income_fields = array(
                    'User_ID__c' => $salesforce_user_id,
                    'Property_ID__c' => $salesforcePropertyId,
                    'Weekly_Rent__c' => round($weekly_rent),
                );

                $sIncomeObject = new SObject();
                $sIncomeObject->fields = $arr_income_fields;
                $sIncomeObject->type = 'Income__c';
                $createIncomeResponse = $mySforceConnection->create(array($sIncomeObject));
                //print_r($createIncomeResponse);
                $salesforceIncomeId = $createIncomeResponse[0]->id;

                $table_name = 'trans_incomes';
                $update_data = array(
                    'salesforce_income_id' => $salesforceIncomeId
                );
                $condition = array(
                    'property_id' => $property_insert_id,
                    'user_id' => $user_id
                );
                if ($property_insert_id) {
                    $this->common_model->updateRow($table_name, $update_data, $condition);
                }

                redirect(base_url() . "backend/property/list");
                exit();
            }
        }
        $arr_privileges = array();
        /* getting all privileges */
        $data['arr_privileges'] = $this->common_model->getRecords('mst_privileges');
        $data['title'] = "Add Property";
        $data['arr_roles'] = $this->common_model->getRecords("mst_role");
        $data['arrUers'] = $this->common_model->getRecords("mst_users");
        $this->load->view('backend/property/add', $data);
    }

    /* Start :: Edit property function */

    public function editProperty($edit_id = '') {
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('9', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("msg", "<span class='error'>You doesn't have priviliges to  manage property!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        $property_id = intval($this->input->post('edit_id'));
        
        $arr_property = $this->common_model->getRecords('mst_properties', '', array("property_id" => $property_id));
        $arr_property = end($arr_property);
        if (count($arr_property) && $property_id) {
            /* if password need to change */
            $userData = explode("###", $this->input->post('user_id'));
            $user_id = intval($userData[0]);
            $salesforce_user_id = $userData[1];
            $salesforce_property_id = $arr_property['salesforce_property_id'];

            $property_name = $this->input->post('property_name');
            $locality_id = $this->input->post('locality_id');
            $property_address = $this->input->post('property_address');
            $corelogic_property_id = $this->input->post('corelogic_property_id');
            $property_type = $this->input->post('property_type');
            $ownership_type = $this->input->post('ownership_type');
            $property_specification = $this->input->post('property_specification');
            $property_suburb = $this->input->post('property_suburb');
            $property_state = $this->input->post('property_state');
            $property_postcode = $this->input->post('property_postcode');
            $land_size = $this->input->post('land_size');
            $dwelling_footprint = $this->input->post('dwelling_footprint');
            
            if(strtoupper($property_type) == 'LAND'){
                $bedrooms = '0';
                $bathrooms = '0';
                $carparks = '0';
            }else{
                $bedrooms = $this->input->post('bedrooms');
                $bathrooms = $this->input->post('bathrooms');
                $carparks = $this->input->post('carparks');
            }
            $purchase_price = $this->input->post('purchase_price');
            $valuation = $this->input->post('valuation');
            $loan_amount = $this->input->post('loan_amount');
            $property_purchase_date = $this->input->post('purchase_date');
//            $next_repayment_date = $this->input->post('next_repayment_date');
//            $payment_frequency = $this->input->post('payment_frequency');
            $interest_rate = $this->input->post('interest_rate');
//            $rate_term = $this->input->post('rate_term');
//            $loan_payment = $this->input->post('loan_payment');

            if ($property_specification == 'Investment') {
                $ppor_status = 'Yes';
                $weekly_rent = str_replace(",", "", $this->input->post('weekly_rent'));
            } else {
                $ppor_status = $this->input->post('ppor_status');
                $weekly_rent = "0";
            }

            $purchase_SQM = 0;
            $value_SQM = 0;

            $purchase_date = date("Y-m-d", strtotime($property_purchase_date));
            $datetime1 = new DateTime(date("Y-m-d"));
            $datetime2 = new DateTime($purchase_date);
            $difference = $datetime1->diff($datetime2);
            $years_woned = ($difference->y * 365);
            $years_woned = $years_woned + ($difference->m * 30);
            $years_woned = round(($years_woned + $difference->d) / 365, 1);

            //calculaing the purchse and current sqm value.

            if (strtoupper($property_type) == 'LAND') {
                //calculating purchase sqm
                if ($purchase_price != 0 && $purchase_price != '') {
                    $purchase_SQM = ($purchase_price) / ($land_size);
                }
                if ($valuation != 0 && $valuation != '') {
                    $value_SQM = ($valuation) / ($land_size);
                }
            } else {
                if ($purchase_price != 0 && $purchase_price != '') {
                    $purchase_SQM = ($purchase_price * 0.3) / ($land_size);
                }
                if ($valuation != 0 && $valuation != '') {
                    $value_SQM = ($valuation * 0.3) / ($land_size);
                }
            }

//            switch ($payment_frequency) {
//                case 'weekly':
//                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
//                    break;
//                case 'monthly':
//                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 30 days'));
//                    break;
//                case 'fortnight':
//                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 14 days'));
//                    break;
//                default:
//                    $next_payment_date = date("Y-m-d", strtotime($next_repayment_date . '+ 7 days'));
//            }

            $arr_to_property_update = array(
                "property_name" => addslashes($property_name),
                "locality_id" => addslashes($locality_id),
                "property_address" => addslashes($property_address),
                "user_id" => $user_id,
                "corelogic_property_id" => $corelogic_property_id,
                "property_type" => $property_type,
                "ownership_type" => $ownership_type,
                "property_specification" => $property_specification,
                "locality" => addslashes($property_suburb),
                "ppor_status" => $ppor_status,
                "property_state" => addslashes($property_state),
                "property_postcode" => addslashes($property_postcode),
                "property_suburb" => addslashes($property_suburb),
                "land_size" => $land_size,
                "dwelling_footprint" => $dwelling_footprint,
                "bedrooms" => $bedrooms,
                "bathrooms" => $bathrooms,
                "carparks" => $carparks,
                "purchase_price" => $purchase_price,
                "valuation" => $valuation,
                "value_SQM" => $value_SQM,
                "purchase_SQM" => $purchase_SQM,
                "current_equity" => ($valuation) - $loan_amount,
                "capital_growth" => round(((($valuation - $purchase_price) / $purchase_price) * 100), 1),
                "LVR" => ($loan_amount / $valuation) * 100,
                "IRR" => ((($valuation - $purchase_price) / $years_woned) / (($purchase_price * 20) / 10000)),
                "ROI" => (($valuation - $purchase_price) / (($purchase_price * 20))) * 10000,
                "years_woned" => $years_woned,
                "latitude" => $this->input->post('latitude'),
                "longitude" => $this->input->post('longitude'),
                "purchase_date" => date("Y-m-d", strtotime($property_purchase_date)),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            );
           
            /* updating the property details */
            $this->common_model->updateRow("mst_properties", $arr_to_property_update, array("property_id" => $property_id));

            /* Update property dept data */
            $arr_to_property_dept_update = array(
                "loan_amount" => $loan_amount,
                'initial_loan_amount' => $loan_amount,
                "interest_rate" => $interest_rate,
//                "rate_term" => $rate_term,
//                "payment_frequency" => $payment_frequency,
//                "next_payment_date" => $next_payment_date,
//                "next_payment_amount" => $loan_payment
            );
            $this->common_model->updateRow("trans_debts", $arr_to_property_dept_update, array("property_id" => $property_id));

            /* Update property income data */
            $income_details = $this->common_model->getRecords('trans_incomes', $fields_to_pass = 'weekly_rent', array('property_id' => intval($property_id)), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            $arr_to_property_income_update = array(
                "weekly_rent" => $weekly_rent
            );
            $this->common_model->updateRow("trans_incomes", $arr_to_property_income_update, array("property_id" => $property_id));

            ## Adding multiple image for property start
            $count = count($_FILES['property_images']['name']);
            if (isset($_FILES['property_images']) && $_FILES['property_images']['name'][0] != '') {
                $this->property_model->deleteImages($property_id);
                $counter = 1;
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['property_images' . $s] = array();
                    $_FILES['property_images' . $s]['name'] = $_FILES['property_images']['name'][$s];
                    $_FILES['property_images' . $s]['type'] = $_FILES['property_images']['type'][$s];
                    $_FILES['property_images' . $s]['tmp_name'] = $_FILES['property_images']['tmp_name'][$s];
                    $config['file_name'] = time() . $s;
                    $_FILES['property_images' . $s]['error'] = $_FILES['property_images']['error'][$s];
                    $_FILES['property_images' . $s]['size'] = $_FILES['property_images']['size'][$s];
                    $config['upload_path'] = './media/front/img/property-images/';
                    $config['allowed_types'] = '*';
                    $config['max_size'] = '5000';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload("property_images" . $s)) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_userdata('msg', 'There is a problem in uploading. Please check and try again.');
                        redirect(base_url() . 'backend/property/list');
                    } else {
                        $this->load->library('image_lib');
                        $data = array('upload_data' => $this->upload->data());
                        $image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $width = 300;
                        $height = 300;
                        $config = array(
                            'source_image' => $image_data['full_path'],
                            'task_images' => './media/front/img/property-images/thumbs/',
                            'maintain_ration' => false,
                            'width' => $width,
                            'height' => $height
                        );
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                        $this->image_lib->clear();

                        $absolute_path = $this->common_model->absolutePath();
                        $image_path = $absolute_path . "media/front/img/property-images/";
                        $image_main = $image_path . "/" . $file_name;

                        $thumbs_image1 = $image_path . "/thumbs/" . $file_name;
                        $thumbs_image2 = $image_path . "/small-thumbs/" . $file_name;

                        $str_console1 = "convert " . $image_main . " -resize 394!X284! " . $thumbs_image1;
                        exec($str_console1);
                        $str_console = "convert " . $image_main . " -resize 198!X163! " . $thumbs_image2;
                        exec($str_console);
                    }
                    $image_data = $this->upload->data();
                    $fields = array(
                        'property_id' => $this->input->post('edit_id'),
                        'image_name' => $file_name,
                        'status' => '1',
                        'sequence' => $counter,
                    );
                    $table = "trans_property_images";
                    $this->common_model->insertRow($fields, $table);
                    $counter++;
                }
            }

            /* Salesforce integration */
            $mySforceConnection = new SforcePartnerClient();
            $mySoapClient = $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (($this->session->userdata('partnerSessionId')) !== null) {
                $location = $this->session->userdata('partnerLocation');
                $sessionId = $this->session->userdata('partnerSessionId');

                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);
            } else {
                $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);

                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
            }

            $IRR = round((($valuation - $purchase_price) / $years_woned) / (($purchase_price * 20) / 10000));
            $LVR = round(($loan_amount / $valuation) * 100);
            $ROI = round((($valuation - $purchase_price) / (($purchase_price * 20))) * 10000);
//            $purchase_date = date("Y-m-d", strtotime());

            $arr_update_property_fields = array(
                'Name' => $property_name,
                'User_ID__c' => $salesforce_user_id,
                'Bathrooms__c' => $bathrooms,
                'Bedrooms__c' => $bedrooms,
                'Capital_Growth__c' => round(((($valuation - $purchase_price) / $purchase_price) * 100), 1),
                'Carparks__c' => $carparks,
                'Created_At__c' => date("Y-m-d") . 'T' . date("H:i:s"),
                'Current_Equity__c' => ($valuation) - $loan_amount,
                'Dwelling_Footprint__c' => $dwelling_footprint,
                'IRR__c' => $IRR,
                'Land_Size__c' => $land_size,
                'Latitude__c' => $this->input->post('latitude'),
                'Longitude__c' => $this->input->post('longitude'),
                'LVR__c' => $LVR,
                'Property_Address__c' => htmlentities(addslashes($property_address)),
                'Property_Specification__c' => $property_specification,
                'Property_State__c' => htmlentities($this->input->post('property_state')),
                'Property_Suburb__c' => htmlentities($this->input->post('property_suburb')),
                'PropertyType__c' => htmlentities($property_type),
                'Ownership_Type__c' => htmlentities($ownership_type),
                'Purchase_Date__c' => date("Y-m-d", strtotime($property_purchase_date)),
                'Purchase_Price__c' => $purchase_price,
                'ROI__c' => $ROI,
                'Valuation__c' => $valuation,
                'Value_SQM__c' => $value_SQM,
                'Purchase_SQM__c' => $purchase_SQM,
                'Years_Woned__c' => $years_woned
            );

            $sObject = new SObject();
            $sObject->Id = $salesforce_property_id;
            $sObject->fields = $arr_update_property_fields;

            $sObject->type = 'Property__c';
            $createResponse = $mySforceConnection->update(array($sObject));

            /** update debt */
            if ($loan_amount != '' && $loan_amount != '0') {
                $arr_property_debt = $this->common_model->getRecords('trans_debts', '', array("property_id" => $property_id));

                if (count($arr_property_debt) > 0) {
                    $arr_debt_fields = array(
                        'Interest_Rate__c' => $this->input->post('interest_rate'),
                        'Loan_Amount__c' => $loan_amount,
                        'Initial_Loan_Amount__c' => $loan_amount,
                        'Loan_Type__c' => 'Fixed',
//                        'Next_Payment_Date__c' => date("Y-m-d", strtotime($next_payment_date)) . 'T' . date("H:i:s"),
//                        'Payment_Frequency__c' => strtolower($this->input->post('payment_frequency')),
//                        'Rate_Term__c' => $this->input->post('rate_term')
                    );

                    $sDebtObject = new SObject();

                    $sDebtObject->fields = $arr_debt_fields;
                    $sDebtObject->Id = $arr_property_debt[0]['salesforce_debt_id'];
                    $sDebtObject->type = 'Debt__c';

                    $createDebtResponse = $mySforceConnection->update(array($sDebtObject));
                } else {

                    $arr_debt_fields = array(
                        'User_ID__c' => $salesforce_user_id,
                        'Property_ID__c' => $salesforce_property_id,
                        'Interest_Rate__c' => $this->input->post('interest_rate'),
                        'Loan_Amount__c' => $loan_amount,
                        'Initial_Loan_Amount__c' => $loan_amount,
                        'Loan_Type__c' => 'Fixed',
//                        'Next_Payment_Date__c' => date("Y-m-d", strtotime($next_payment_date)) . 'T' . date("H:i:s"),
//                        'Payment_Frequency__c' => $this->input->post('payment_frequency'),
//                        'Rate_Term__c' => $this->input->post('rate_term'),
                        'Added_Date__c' => date('Y-m-d'),
                    );

                    $sDebtObject = new SObject();

                    $sDebtObject->fields = $arr_debt_fields;

                    $sDebtObject->type = 'Debt__c';

                    $createDebtResponse = $mySforceConnection->create(array($sDebtObject));
//                      print_r($createDebtResponse);
                    $salesforceDebtId = $createDebtResponse[0]->id;

                    /*
                     * insert salesforce id in local database for further use.
                     */
                    $arr_to_property_dept_insert = array(
                        "loan_amount" => $loan_amount,
                        "property_id" => $property_id,
                        "salesforce_debt_id" => $salesforceDebtId,
                        "user_id" => $user_id,
                        "interest_rate" => $this->input->post('interest_rate'),
//                        "rate_term" => $this->input->post('rate_term'),
//                        "payment_frequency" => $this->input->post('payment_frequency'),
//                        "next_payment_date" => $next_payment_date,
//                        "next_payment_amount" => $this->input->post('loan_payment'),
                        "added_date" => date("Y-m-d"),
                    );
                    $this->common_model->insertRow($arr_to_property_dept_insert, "trans_debts");
                }
            }
            /** Update Income */
            $arr_property_income = $this->common_model->getRecords('trans_incomes', '', array("property_id" => $property_id));

            if (count($arr_property_income) > 0) {
                $arr_income_fields = array(
                    'Weekly_Rent__c' => $weekly_rent,
                );

                $sIncomeObject = new SObject();

                $sIncomeObject->fields = $arr_income_fields;
                $sDebtObject->Id = $arr_property_income[0]['salesforce_income_id'];
                $sIncomeObject->type = 'Income__c';

                $createIncomeResponse = $mySforceConnection->update(array($sIncomeObject));
                $salesforceIncomeId = $createIncomeResponse[0]->id;
            } else {
                $arr_income_fields = array(
                    'User_ID__c' => $salesforce_user_id,
                    'Property_ID__c' => $salesforce_property_id,
                    'Weekly_Rent__c' => $weekly_rent,
                );

                $sIncomeObject = new SObject();

                $sIncomeObject->fields = $arr_income_fields;

                $sIncomeObject->type = 'Income__c';

                $createIncomeResponse = $mySforceConnection->create(array($sIncomeObject));
                //print_r($createIncomeResponse);
                $salesforceIncomeId = $createIncomeResponse[0]->id;

                $arr_to_property_income_insert = array(
                    "user_id" => $user_id,
                    "property_id" => $property_id,
                    "salesforce_income_id" => $salesforceIncomeId,
                    "weekly_rent" => $weekly_rent,
                    "added_date" => date('Y-m-d')
                );
                $this->common_model->insertRow($arr_to_property_income_insert, "trans_incomes");
            }
            $this->session->set_userdata("msg", "<span class='success'>Property updated successfully!</span>");
            redirect(base_url() . "backend/property/list");
            exit();
        }
        $arr_privileges = array();
        /* getting all privileges */
        $data['arr_privileges'] = $this->common_model->getRecords('mst_privileges');
        /* getting user details from $edit_id from function parameter */
        $data['arr_property_detail'] = $this->property_model->getPropertyDetailsById("mst_properties", $fields_to_pass = "", array("p.property_id" => intval(base64_decode($edit_id))));
        $data['arr_property_detail'] = end($data['arr_property_detail']);
        $data['product_image'] = $this->common_model->getRecords("trans_property_images", "", array("property_id" => intval(base64_decode($edit_id))));
        $data['title'] = "Edit Property";
        $data['edit_id'] = $edit_id;
        $data['arrUers'] = $this->common_model->getRecords("mst_users");
        $this->load->view('backend/property/edit', $data);
    }

    /* End :: Edit property function */
    
    function getUserCityLatLng() {
        $user_address = $this->input->post('selected_address');
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($user_address) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
//        return $result3[0];
        echo json_encode(array("current_city" => $user_address, "lat" => $result3[0]['lat'], "lng" => $result3[0]['lng']));
    }

    function getUserGeoLocation() {
        $data = $this->common_model->commonFunction();
        $address = $this->input->post('user_address');
        $address = str_replace(" ", "+", $address);
        $address_url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";

        $result = file_get_contents("$address_url");
        $json_data = json_decode($result, TRUE);
        foreach ($json_data['results'] as $address_data) {
            foreach ($address_data['address_components'] as $addressPart) {
                if ((in_array('locality', $addressPart['types'])) && (in_array('political', $addressPart['types']))) {
                    $city = $addressPart['long_name'];
                } else if ((in_array('administrative_area_level_1', $addressPart['types'])) && (in_array('political', $addressPart['types']))) {
                    $state = $addressPart['long_name'];
                } else if ((in_array('country', $addressPart['types'])) && (in_array('political', $addressPart['types']))) {
                    $country = $addressPart['long_name'];
                    $country_iso = $addressPart['short_name'];
                } else if ((in_array('postal_code', $addressPart['types']))) {
                    $zipcode = $addressPart['long_name'];
                }
            }
        }
        if (($zipcode != '') && ($city != '') && ($state != '') && ($country != '')) {
            $address = $zipcode . ', ' . $city . ', ' . $state . ', ' . $country;
        } else if (($zipcode != '') && ($city != '')) {
            $address = $zipcode . ', ' . $city;
        } else if (($city != '') && ($state != '')) {
            $address = $city . ', ' . $state;
        } else if (($state != '') && ($country != '')) {
            $address = $state . ', ' . $country;
        } else if ($country != '') {
            $address = $country;
        }
        $lat = $json_data['results'][0]['geometry']['location']['lat'];
        $lang = $json_data['results'][0]['geometry']['location']['lng'];
        $zipcode = $this->getPostcode($lat, $lang);
        if ($country != '' && $country != null) {
            $arr_to_return = array('user_country' => $country, 'user_state' => $state, 'user_city' => $city, 'user_postal' => $zipcode, 'country_iso' => $country_iso);
        } else {
            $arr_to_return = array('msg' => 'Please enter valid suburb name and postcode..!');
        }
        echo json_encode($arr_to_return);
    }

    function getPostcode($lat, $lng) {
        $returnValue = NULL;
        $ch = curl_init();
        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=false";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        $json = json_decode($result, TRUE);

        if (isset($json['results'])) {
            foreach ($json['results'] as $result) {
                foreach ($result['address_components'] as $address_component) {
                    $types = $address_component['types'];
                    if (in_array('postal_code', $types) && sizeof($types) == 1) {
                        $returnValue = $address_component['short_name'];
                    }
                }
            }
        }
        return $returnValue;
    }
    
    public function chkPropertyNameExist() {
        $property_name = $this->input->post('property_name');
        $table_to_pass = 'mst_properties';
        $fields_to_pass = array('property_id', 'property_name');
        $condition_to_pass = array("property_name" => $property_name);
        $product_details = $this->common_model->getRecords($table_to_pass, $fields_to_pass, $condition_to_pass);
        if (count($product_details)) {
            echo 'false';
        } else {
            echo 'true';
        }
    }
    
    public function chkEditPropertyNameExist() {

        if ($this->input->post('property_name') == $this->input->post('property_name_old')) {
            echo 'true';
        } else {
            $table_to_pass = 'mst_properties';
            $fields_to_pass = array('property_id', 'property_name');
            $condition_to_pass = array("property_name" => $this->input->post('property_name'));
            $product_details = $this->common_model->getRecords($table_to_pass, $fields_to_pass, $condition_to_pass);
            if (count($product_details)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }
}
