<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('home_model');
        $this->load->model('seo_page_banner_model');
        $this->load->helper('cookie');
    }

    public function index() {
        $data['global'] = $this->common_model->getGlobalSettings();
        /** Here you can add the code to load header, slider banner  and footer pages. * */
        /** loading the home model * */
        $lang_id = 17;/** 17 for english * */
        if ($this->session->userdata('language_id') != '') {
            $lang_id = $this->session->userdata('language_id');
        }
        $data['site_title'] = "Home";
        $data['header'] = array("title" => "Welcome to PRE", "keywords" => "Welcome to PRE", "description" => "Welcome to PRE");
        $arr_state_data = $this->seo_page_banner_model->getStateData($table_to_pass = '', $fields_to_pass = '', '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $data['state'] = strtoupper($state);
        $data['arr_seo_banner'] = $this->seo_page_banner_model->getBanner();
        $data['site_title'] = ucfirst($state);
        $data['arr_state_data'] = $arr_state_data;
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/includes/top-nav', $data);
        $this->load->view('front/home', $data);
        $this->load->view('front/includes/footer', $data);
    }

    public function seoPage() {
//        $client_id = "f24ugvace46pmdsbeqg4bk6h";
//        $client_secret = "SHK7QDXrJC";
//        $this->output->cache(60);

        $link = $_SERVER['REDIRECT_QUERY_STRING'];
//        echo $link;die;
        $expl_link = explode("/", $link);
        $state = strtoupper(str_replace("+", " ", $expl_link[1]));
        $suburb = strtoupper(str_replace("+", " ", $expl_link[2]));
        $postcode = strtoupper(str_replace("+", " ", $expl_link[3]));
         //echo " state=>".$state." sub=>".$suburb;die;
        
        if ($state != '' && $suburb != '' && $postcode) {
            
            $data['global'] = $this->common_model->getGlobalSettings();
            /** Here you can add the code to load header, slider banner  and footer pages. * */
            /** loading the home model * */
            $data['site_title'] = "Home";
            $data['header'] = array("title" => "Welcome to PRE", "keywords" => "Welcome to PRE", "description" => "Welcome to PRE");
            $data['arr_seo_banner'] = $this->seo_page_banner_model->getBanner();

            $flag = "true";
            if (($suburb) != '') {

                $demographic_data = $this->common_model->getRecords('mst_demographic_data', '*', array("suburb" => strtoupper(stripslashes($suburb)), "state" => $state, "postcode" => $postcode));

                if (count($demographic_data) > 0) {
                    $maxValue = 0.0;
                    $maxKey = '';
                    if (count($demographic_data)) {
                        foreach ($demographic_data[0] as $column => $val) {
                            if (substr($column, 0, 4) === 'age_') {
                                if ($maxValue < floatval($val)) {
                                    $maxValue = floatval($val);
                                    $maxKey = $column;
                                }
                            }
                        }
                    }
                    $demographic_data[0]['agegroup'] = $maxKey;
                }
                $data['single_state_data'] = end($this->common_model->getRecords('mst_state_demographic_data', '*', array("state_name" => strtoupper(stripslashes($demographic_data['state'])))));
                if (count($demographic_data) > 0) {
                    $flag = "true";
                } else {
                    $flag = "false";
                }
                
            }
            
            if ($flag == 'false') {
                $demographic_data = $this->common_model->getRecords('mst_state_demographic_data', '*', array("state_name" => strtoupper(stripslashes($suburb))));
            }

            if ($flag == 'true') {
                
                $data_str = "grant_type=client_credentials&scope=api_addresslocators_read%20api_demographics_read%20api_properties_read%20api_suburbperformance_read";
                $url = "https://auth.domain.com.au/v1/connect/token";
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($curl, CURLOPT_USERPWD, "f24ugvace46pmdsbeqg4bk6h:SHK7QDXrJC");
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_str);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $tokon_response = curl_exec($curl);
                
                
                $final_tokon_response = json_decode($tokon_response);
                $token = $final_tokon_response->access_token;
                curl_close($curl);

                $suburb1 = str_replace(' ', '', $suburb);

                /** Get Suburb Id * */
                $url1 = "https://api.domain.com.au/v1/addressLocators?searchLevel=Suburb&suburb=" . $suburb1 . "&state=" . $state . "";

                $su = curl_init();
                $headr = array('Authorization: Bearer ' . $token, 'X-Originating-Ip: 52.62.83.84');
                curl_setopt($su, CURLOPT_URL, $url1);
                curl_setopt($su, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($su, CURLOPT_USERPWD, "f24ugvace46pmdsbeqg4bk6h:SHK7QDXrJC");
                curl_setopt($su, CURLOPT_HTTPHEADER, $headr);
                curl_setopt($su, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($su, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($su, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($su);
                $info = curl_getinfo($su, CURLINFO_HTTP_CODE);
                $suburb_data = json_decode($result);
                
                curl_close($su);
                /** Get Unit Records * */
                $subur_id = $suburb_data[0]->ids[0]->id;

                if ($subur_id != '') {
                    // following url to get 10 years records
                    $unit_url = "https://api.domain.com.au/v1/suburbPerformanceStatistics?state=" . $state . "&suburbId=" . $subur_id . "&propertyCategory=unit&chronologicalSpan=12&tPlusFrom=1&tPlusTo=10";

                    // following url to get 1 years records
//                    $unit_url = "https://api.domain.com.au/v1/suburbPerformanceStatistics?state=" . $state . "&suburbId=" . $subur_id . "&propertyCategory=unit&chronologicalSpan=3&tPlusFrom=1&tPlusTo=4";

                    $unit = curl_init();
                    $headr = array('Authorization: Bearer ' . $token, 'X-Originating-Ip: 52.62.83.84');
                    curl_setopt($unit, CURLOPT_URL, $unit_url);
                    curl_setopt($unit, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($unit, CURLOPT_USERPWD, "f24ugvace46pmdsbeqg4bk6h:SHK7QDXrJC");
                    curl_setopt($unit, CURLOPT_HTTPHEADER, $headr);
                    curl_setopt($unit, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($unit, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($unit, CURLOPT_SSL_VERIFYPEER, false);
                    $unit_result = curl_exec($unit);
                    $info = curl_getinfo($unit, CURLINFO_HTTP_CODE);
                    $unit_response_data = json_decode($unit_result);
                    //print_r($unit_result);die;
//                    echo '<pre>';print_r($unit_response_data);die;
                    // following commented code is implemented for 1 records
//                    $data['from_year'] = $unit_response_data->series->seriesInfo[0]->year;
//                    $data['to_year'] = $unit_response_data->series->seriesInfo[3]->year;
//                    $from_month = $unit_response_data->series->seriesInfo[0]->month;
//                    $to_month = $unit_response_data->series->seriesInfo[3]->month;
//
//                    switch ($to_month) {
//                        case 1:
//                            $mon_name = "Jan";
//                            break;
//                        case 2:
//                            $mon_name = "Feb";
//                            break;
//                        case 3:
//                            $mon_name = "Mar";
//                            break;
//                        case 4:
//                            $mon_name = "April";
//                            break;
//                        case 5:
//                            $mon_name = "May";
//                            break;
//                        case 6:
//                            $mon_name = "Jun";
//                            break;
//                        case 7:
//                            $mon_name = "July";
//                            break;
//                        case 8:
//                            $mon_name = "Aug";
//                            break;
//                        case 9:
//                            $mon_name = "Sept";
//                            break;
//                        case 10:
//                            $mon_name = "Oct";
//                            break;
//                        case 11:
//                            $mon_name = "Nov";
//                            break;
//                        case 12:
//                            $mon_name = "Dec";
//                            break;
//                    }
//                    switch ($from_month) {
//                        case 1:
//                            $mon_name1 = "Jan";
//                            break;
//                        case 2:
//                            $mon_name1 = "Feb";
//                            break;
//                        case 3:
//                            $mon_name1 = "Mar";
//                            break;
//                        case 4:
//                            $mon_name1 = "April";
//                            break;
//                        case 5:
//                            $mon_name1 = "May";
//                            break;
//                        case 6:
//                            $mon_name1 = "Jun";
//                            break;
//                        case 7:
//                            $mon_name1 = "July";
//                            break;
//                        case 8:
//                            $mon_name1 = "Aug";
//                            break;
//                        case 9:
//                            $mon_name1 = "Sept";
//                            break;
//                        case 10:
//                            $mon_name1 = "Oct";
//                            break;
//                        case 11:
//                            $mon_name1 = "Nov";
//                            break;
//                        case 12:
//                            $mon_name1 = "Dec";
//                            break;
//                    }
//                    $data['to_month'] = $mon_name;
//                    $data['from_month'] = $mon_name1;

                    $latest_unit_demographic_data = $unit_response_data->series->seriesInfo[9];
                    $prev_unit_demographic_data = $unit_response_data->series->seriesInfo[8];
                    $first_unit_demographic_data = $unit_response_data->series->seriesInfo[0];

                    $data['unit_response_data'] = $unit_response_data->series->seriesInfo;

                    $data['latest_unit_demographic_data'] = $latest_unit_demographic_data->values;

                    $data['first_unit_demographic_data'] = $first_unit_demographic_data->values;

                    $data['latest_unit_demographic_year'] = $latest_unit_demographic_data->year;

                    $data['prev_unit_demographic_data'] = $prev_unit_demographic_data->values;

                    $data['unit_past_year_change'] = round(($prev_unit_demographic_data->values->medianSaleListingPrice / $latest_unit_demographic_data->values->medianSaleListingPrice) * 100);

                    $data['unit_10_year_change'] = round(($first_unit_demographic_data->values->medianSaleListingPrice / $latest_unit_demographic_data->values->medianSaleListingPrice) * 100);

//                    $data['unit_median_rental_yield'] = round(((($latest_unit_demographic_data->values->medianRentListingPrice) * 52 ) / ($latest_unit_demographic_data->values->medianSaleListingPrice)) * 100, 2);
                    $data['unit_median_rental_yield'] = round(((($latest_unit_demographic_data->values->medianRentListingPrice) * 52 ) / ($latest_unit_demographic_data->values->medianSoldPrice)) * 100, 2);

                    $data['unit_days_on_market_change'] = round((($latest_unit_demographic_data->values->daysOnMarket) / ($prev_unit_demographic_data->values->daysOnMarket)) * 100);

                    $data['unit_auction_success'] = round((($latest_unit_demographic_data->values->auctionNumberSold) / ($latest_unit_demographic_data->values->auctionNumberAuctioned)) * 100);

                    $data['unit_vendor_discount'] = round($latest_unit_demographic_data->values->discountPercentage);

                    $data['unit_your_depoist'] = round($first_unit_demographic_data->values->medianSoldPrice * 0.2);

                    $data['unit_cash_on_cash_return'] = round((($latest_unit_demographic_data->values->medianSoldPrice - $first_unit_demographic_data->values->medianSoldPrice) / $data['unit_your_depoist']) * 100);

                    $data['unit_cash_on_cash_return_value'] = round($latest_unit_demographic_data->values->medianSoldPrice - $first_unit_demographic_data->values->medianSoldPrice);

                    $data['unit_today_value'] = round($latest_unit_demographic_data->values->medianSoldPrice);

                    $latestunitmedianSoldPrice = $latest_unit_demographic_data->values->medianSoldPrice;
                    $firstunitmedianSoldPrice = $first_unit_demographic_data->values->medianSoldPrice;
                    $diffunitmedianSoldPrice = $latestunitmedianSoldPrice - $firstunitmedianSoldPrice;
                    $tempunit_today_value_in_per = ($diffunitmedianSoldPrice / $firstunitmedianSoldPrice);
                    $data['today_unit_value_in_per'] = $tempunit_today_value_in_per * 100;

                    $prevunitmedianSoldPrice = $prev_unit_demographic_data->values->medianSoldPrice;
                    $diffunitmedianSoldPriceper = $latestunitmedianSoldPrice - $prevunitmedianSoldPrice;
                    $tempunit_prev_value_in_per = ($diffunitmedianSoldPriceper / $latestunitmedianSoldPrice);
                    $data['tempunit_prev_value_in_per'] = $tempunit_prev_value_in_per * 100;

                    $unit_sum = 0;
                    $unit_rent_sum = 0;
                    foreach ($unit_response_data->series->seriesInfo as $key => $unit_graph) {
                        $unit_sum+= $unit_graph->values->medianSaleListingPrice;
                        $unit_rent_sum+= $unit_graph->values->medianRentListingPrice;
                    }
                    $data['unit_graph_sell'] = round($unit_sum / 10);
                    $data['unit_graph_rent'] = round($unit_rent_sum / 10);
                    curl_close($unit);
                    /** Get Unit Records * */
                    /** Get House Records * */
                    // following url to get 10 years records
                    $house_url = "https://api.domain.com.au/v1/suburbPerformanceStatistics?state=" . $state . "&suburbId=" . $subur_id . "&propertyCategory=house&chronologicalSpan=12&tPlusFrom=1&tPlusTo=10";

                    // following url to get 1 years records
//                    $house_url = "https://api.domain.com.au/v1/suburbPerformanceStatistics?state=" . $state . "&suburbId=" . $subur_id . "&propertyCategory=house&chronologicalSpan=3&tPlusFrom=1&tPlusTo=4";
                    $house = curl_init();
                    $headr = array('Authorization: Bearer ' . $token, 'X-Originating-Ip: 52.62.83.84');
                    curl_setopt($house, CURLOPT_URL, $house_url);
                    curl_setopt($house, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($house, CURLOPT_USERPWD, "f24ugvace46pmdsbeqg4bk6h:SHK7QDXrJC");
                    curl_setopt($house, CURLOPT_HTTPHEADER, $headr);
                    curl_setopt($house, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($house, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($house, CURLOPT_SSL_VERIFYPEER, false);
                    $house_result = curl_exec($house);
                    $info = curl_getinfo($house, CURLINFO_HTTP_CODE);
                    $house_response_data = json_decode($house_result);

                    $latest_house_demographic_data = $house_response_data->series->seriesInfo[9];
                    $prev_house_demographic_data = $house_response_data->series->seriesInfo[8];
                    $first_house_demographic_data = $house_response_data->series->seriesInfo[0];

                    $data['house_response_data'] = $house_response_data->series->seriesInfo;

                    $data['latest_house_demographic_data'] = $latest_house_demographic_data->values;

                    $data['first_house_demographic_data'] = $first_house_demographic_data->values;

                    $data['prev_house_demographic_data'] = $prev_house_demographic_data->values;

                    $data['house_past_year_change'] = round(($prev_house_demographic_data->values->medianSaleListingPrice / $latest_house_demographic_data->values->medianSaleListingPrice) * 100);

                    $data['house_10_year_change'] = round(($first_house_demographic_data->values->medianSaleListingPrice / $latest_house_demographic_data->values->medianSaleListingPrice) * 100);

//                    $data['house_median_rental_yield'] = round(((($latest_house_demographic_data->values->medianRentListingPrice) * 52) / ($latest_house_demographic_data->values->medianSaleListingPrice)) * 100, 2);
                    $data['house_median_rental_yield'] = round(((($latest_house_demographic_data->values->medianRentListingPrice) * 52) / ($latest_house_demographic_data->values->medianSoldPrice)) * 100, 2);

                    $data['home_days_on_market_change'] = round((($latest_house_demographic_data->values->daysOnMarket) / ($prev_house_demographic_data->values->daysOnMarket)) * 100);

                    $data['house_auction_success'] = round((($latest_house_demographic_data->values->auctionNumberSold) / ($latest_house_demographic_data->values->auctionNumberAuctioned)) * 100);

                    $data['house_vendor_discount'] = round($latest_house_demographic_data->values->discountPercentage);

                    $data['house_your_depoist'] = round(($first_house_demographic_data->values->medianSoldPrice) * 0.2);

                    $data['house_cash_on_cash_return'] = round(((($latest_house_demographic_data->values->medianSoldPrice) - ($first_house_demographic_data->values->medianSoldPrice)) / $data['house_your_depoist']) * 100);

                    $data['house_cash_on_cash_return_value'] = round($latest_house_demographic_data->values->medianSoldPrice) - ($first_house_demographic_data->values->medianSoldPrice);

                    $data['house_today_value'] = round($latest_house_demographic_data->values->medianSoldPrice);


                    $data['latest_house_demographic_year'] = $latest_house_demographic_data->year;

                    $data['first_house_demographic_year'] = $first_house_demographic_data->year;

                    $data['first_house_demographic_month'] = $first_house_demographic_data->month;

                    $latesthousemedianSoldPrice = $latest_house_demographic_data->values->medianSoldPrice;
                    $firstmedianSoldPrice = $first_house_demographic_data->values->medianSoldPrice;
                    $diffmedianSoldPrice = $latesthousemedianSoldPrice - $firstmedianSoldPrice;
                    $temphouse_today_value_in_per = ($diffmedianSoldPrice / $firstmedianSoldPrice);
                    $data['today_house_value_in_per'] = $temphouse_today_value_in_per * 100;

                    $prevhousemedianSoldPrice = $prev_house_demographic_data->values->medianSoldPrice;
                    $diffprevmedianSoldPrice = $latesthousemedianSoldPrice - $prevhousemedianSoldPrice;
                    $temphouse_prev_value_in_per = ($diffprevmedianSoldPrice / $latesthousemedianSoldPrice);
                    $data['temphouse_prev_value_in_per'] = $temphouse_prev_value_in_per * 100;


                    switch ($latest_house_demographic_data->month) {
                        case 1:
                            $data['latest_month'] = "Jan";
                            break;
                        case 2:
                            $data['latest_month'] = "Feb";
                            break;
                        case 3:
                            $data['latest_month'] = "Mar";
                            break;
                        case 4:
                            $data['latest_month'] = "Apr";
                            break;
                        case 5:
                            $data['latest_month'] = "May";
                            break;
                        case 6:
                            $data['latest_month'] = "Jun";
                            break;
                        case 7:
                            $data['latest_month'] = "Jul";
                            break;
                        case 8:
                            $data['latest_month'] = "Aug";
                            break;
                        case 9:
                            $data['latest_month'] = "Sep";
                            break;
                        case 10:
                            $data['latest_month'] = "Oct";
                            break;
                        case 11:
                            $data['latest_month'] = "Nov";
                            break;
                        case 12:
                            $data['latest_month'] = "Dec";
                            break;
                    }

                    switch ($first_house_demographic_data->month) {
                        case 1:
                            $data['first_month'] = "Jan";
                            break;
                        case 2:
                            $data['first_month'] = "Feb";
                            break;
                        case 3:
                            $data['first_month'] = "Mar";
                            break;
                        case 4:
                            $data['first_month'] = "Apr";
                            break;
                        case 5:
                            $data['first_month'] = "May";
                            break;
                        case 6:
                            $data['first_month'] = "Jun";
                            break;
                        case 7:
                            $data['first_month'] = "Jul";
                            break;
                        case 8:
                            $data['first_month'] = "Aug";
                            break;
                        case 9:
                            $data['first_month'] = "Sep";
                            break;
                        case 10:
                            $data['first_month'] = "Oct";
                            break;
                        case 11:
                            $data['first_month'] = "Nov";
                            break;
                        case 12:
                            $data['first_month'] = "Dec";
                            break;
                    }

                    $house_sum = 0;
                    $house_rent_sum = 0;
                    foreach ($house_response_data->series->seriesInfo as $key => $house_graph) {
                        $house_sum+= $house_graph->values->medianSaleListingPrice;
                        $house_rent_sum+= $house_graph->values->medianRentListingPrice;
                    }

                    $data['house_graph_sell'] = round($house_sum / 10);
                    $data['house_graph_rent'] = round($house_rent_sum / 10);
                    curl_close($house);
                    /** Get House Records * */
                }
            }

            $data['demographic_data'] = ($demographic_data[0]);

            $data['flag'] = $flag;

            $data['arr_state_data'] = $this->seo_page_banner_model->getStateData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            $condition = '(`suburb`="' . $suburb . '" OR `state`="' . $suburb . '")';
            $state_data = end($this->seo_page_banner_model->getStateDetails($table_to_pass = '', $fields_to_pass = '', $condition, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));

            if ($flag == 'true') {
                $data['state'] = isset($demographic_data[0]['state']) ? ucfirst($demographic_data[0]['state']) : "QLD";
            } else {
                $data['state'] = isset($suburb) ? ucfirst($suburb) : "QLD";
            }

            if ($flag == 'false') {
                $data['select_state'] = isset($state_data['state']) ? $state_data['state'] : "QLD";
            }

            $data['selected_suburb'] = isset($suburb) ? ucfirst(strtolower($suburb)) : "";
            $data['postcode'] = isset($postcode) ? $postcode : "";
            $data['country'] = isset($state_data['country']) ? ($state_data['country']) : "Australia";
            $data['meta_description'] = "Investment Property " . $suburb . ", " . $state_data['state'] . ", " . $postcode . " - Best Investment Properties " . $suburb . ", " . $state_data['state'] . ", " . $postcode . " - Property Investment Tools including Property Investments Calculators, Tax Deductions, How to Buy and Invest in Property, Real Estate Investment Case Studies and Free Best Investment Suburb Reports for each state of Australia.";
            $data['site_title'] = "Investment Property " . $suburb;
            //$data['arr_demographic_data'] = $this->common_model->getRecords('mst_demographic_data', 'state,postcode,suburb,demographic_id', '', $order_by = '', $limit = '', $debug = 0);
            $this->load->view('front/includes/header', $data);
            $this->load->view('front/includes/top-nav', $data);
            $this->load->view('front/seo-page/seo-page-details', $data);
            $this->load->view('front/includes/footer', $data);
        } else {
            $data['global'] = $this->common_model->getGlobalSettings();
            $data['header'] = array("title" => "Welcome to PRE", "keywords" => "Welcome to PRE", "description" => "Welcome to PRE");
            $data['arr_seo_banner'] = $this->seo_page_banner_model->getBanner();
//            $arr_state_data = $this->seo_page_banner_model->getStateData($table_to_pass = '', $fields_to_pass = '', array('state' => $state), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            $arr_state_data = $this->seo_page_banner_model->getStateData($table_to_pass = '', $fields_to_pass = '', '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            $data['state'] = strtoupper($state);
            $data['site_title'] = ucfirst($state);
            if (count($arr_state_data) > 0) {
                $data['arr_state_data'] = $arr_state_data;
                $this->load->view('front/includes/header', $data);
                $this->load->view('front/includes/top-nav', $data);
                $this->load->view('front/seo-page/all-suburb-list', $data);
                $this->load->view('front/includes/footer', $data);
            } else {
                redirect(base_url() . 'page-not-found');
            }
        }
    }

    public function getSeoPageData() {
        $data['global'] = $this->common_model->getGlobalSettings();
        $location = $this->input->post("location");

        $this->load->driver('cache');
        if ($cacheKey = $this->cache->file->get(md5($location))) {
            $search_data = unserialize($cacheKey);
        } else {
            $api_key = $data['global']['seo_api_key'];
            $url = "https://api.planningalerts.org.au/applications.js?count=10&key=" . $api_key . "&suburb=" . $location . "";
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $url);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch1);

            curl_close($ch1);
            $final_response = json_decode($response);

            $search_data = array();
            foreach ($final_response as $key => $suburbs) {
                /*
                 * No more used as distance is not displayed on the page
                  $current_location = $this->geoCheckIP();
                  $default_location = $this->getCityLnt($current_location['town'] . ' ' . $current_location['state']);

                  $point1_lat = $suburbs->application->lat;
                  $point1_long = $suburbs->application->lng;

                  $point2_lat = $default_location['lat'];
                  $point2_long = $default_location['lng']; */

                $date = date("Y-m-d H:i:s", strtotime($suburbs->application->date_scraped));
                $date_scraped = $this->time_elapsed_string($date);
//                $dis = $this->distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 1);
                $search_data[$key]['id'] = isset($suburbs->application->id) ? $suburbs->application->id : "";
                $search_data[$key]['address'] = isset($suburbs->application->address) ? $suburbs->application->address : "";
                $search_data[$key]['description'] = isset($suburbs->application->description) ? $suburbs->application->description : "";
//                $search_data[$key]['date_scraped'] = isset($date_scraped) ? $date_scraped . ", " . round($dis) . " km away" : "";
                $search_data[$key]['date_scraped'] = isset($date_scraped) ? $date_scraped : "";
                $search_data[$key]['date_received'] = isset($suburbs->application->date_received) ? $suburbs->application->date_received : "";
                $search_data[$key]['lat'] = isset($suburbs->application->lat) ? $suburbs->application->lat : "";
                $search_data[$key]['lng'] = isset($suburbs->application->lng) ? $suburbs->application->lng : "";
            }
            $this->cache->file->save(md5($location), serialize($search_data), 86400 * 7);
        }
        echo json_encode(array('Response' => $search_data));
    }

    public function getAllState() {
        $selected_state = $this->input->post('selected_state');
        $condition_to_pass = array('state' => $selected_state);
        $selected_state_suburb = $this->seo_page_banner_model->getSuburbData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '50', $debug_to_pass = 0);
        /* Get SUBURBS From  Start */
        $arr_state_data = $this->seo_page_banner_model->getStateData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $state_data = array();
        foreach ($arr_state_data as $key => $states) {
            $state_data[$key]['state_name'] = isset($states['state']) ? strtolower($states['state']) : "-";
        }

        $selected_state_suburb_data = array();
        foreach ($selected_state_suburb as $key => $suburbs) {
            $selected_state_suburb_data[$key]['suburb_name'] = isset($suburbs['suburb']) ? strtolower($suburbs['suburb']) : "-";
            $selected_state_suburb_data[$key]['state_name'] = isset($suburbs['state']) ? strtolower($suburbs['state']) : "-";
            $selected_state_suburb_data[$key]['postcode'] = isset($suburbs['postcode']) ? strtolower($suburbs['postcode']) : "-";
        }
        $country_data['country_name'] = isset($arr_state_data[0]['country']) ? $arr_state_data[0]['country'] : "-";

        echo json_encode(array('Response' => $state_data, 'country' => $country_data, 'selected_state_suburb' => $selected_state_suburb_data));
    }

    public function getParticularStateSuburbs() {
        $selected_state = $this->input->post('selected_state');
        $condition_to_pass = array('state' => $selected_state);
        $selected_state_suburb = $this->seo_page_banner_model->getParticularStateSuburbData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        /* Get SUBURBS From  Start */
        $arr_state_data = $this->seo_page_banner_model->getStateData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $state_data = array();
        foreach ($arr_state_data as $key => $states) {
            $state_data[$key]['state_name'] = isset($states['state']) ? strtolower($states['state']) : "-";
        }
        $selected_state_suburb_data = array();
        foreach ($selected_state_suburb as $key => $suburbs) {
            $selected_state_suburb_data[$key]['suburb_name'] = isset($suburbs['suburb']) ? strtolower($suburbs['suburb']) : "-";
            $selected_state_suburb_data[$key]['state_name'] = isset($suburbs['state']) ? strtolower($suburbs['state']) : "-";
            $selected_state_suburb_data[$key]['postcode'] = isset($suburbs['postcode']) ? strtolower($suburbs['postcode']) : "-";
        }
        $country_data['country_name'] = isset($arr_state_data[0]['country']) ? $arr_state_data[0]['country'] : "-";

        echo json_encode(array('Response' => $state_data, 'country' => $country_data, 'selected_state_suburb' => $selected_state_suburb_data));
    }

    public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 1) {
        // Calculate the distance in degrees
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));
        // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
        switch ($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                break;
            case 'mi':
                $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
                break;
            case 'nmi':
                $distance = $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
        }
        return round($distance, $decimals);
    }

    // Function to get the client ip address
    public function getClientIPEnv() {

        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['HTTP_HOST'];
        else
            $ipaddress = '52.62.83.84';
        return $ipaddress;
    }

    public function geoCheckIP() {
        $ip = $this->getClientIPEnv();
        //check, if the provided ip is valid
        if (!filter_var($ip, FILTER_VALIDATE_IP)) {
            throw new InvalidArgumentException("IP is not valid");
        }
        //contact ip-server
        $response = @file_get_contents('http://www.netip.de/search?query=' . $ip);
        if (empty($response)) {
            throw new InvalidArgumentException("Error contacting Geo-IP-Server");
        }
        //Array containing all regex-patterns necessary to extract ip-geoinfo from page
        $patterns = array();
        $patterns["ip"] = '#Ip: (.*?)&nbsp;#i';
        $patterns["country"] = '#Country: (.*?)&nbsp;#i';
        $patterns["state"] = '#State/Region: (.*?)<br#i';
        $patterns["town"] = '#City: (.*?)<br#i';
        //Array where results will be stored
        $ipInfo = array();
        //check response from ipserver for above patterns
        foreach ($patterns as $key => $pattern) {
            //store the result in array
            $ipInfo[$key] = preg_match($pattern, $response, $value) && !empty($value[1]) ? $value[1] : 'not found';
        }

        return $ipInfo;
    }

    public function getCityLnt($search_key) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($search_key) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
        return $result3[0];
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    function getUserCityLatLng() {
        $user_address = $this->input->post('selected_address');
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($user_address) . "&sensor=false";
        $result_string = file_get_contents($url);
        $json_data = json_decode($result_string, true);

        foreach ($json_data['results'] as $address_data) {
            foreach ($address_data['address_components'] as $addressPart) {
                if ((in_array('locality', $addressPart['types'])) && (in_array('political', $addressPart['types']))) {
                    $suburb = $addressPart['long_name'];
                } else if ((in_array('administrative_area_level_1', $addressPart['types'])) && (in_array('political', $addressPart['types']))) {
                    $state = $addressPart['short_name'];
                } else if ((in_array('postal_code', $addressPart['types']))) {
                    $postcode = $addressPart['long_name'];
                }
            }
        }

        $lat = $json_data['results'][0]['geometry']['location']['lat'];
        $lang = $json_data['results'][0]['geometry']['location']['lng'];

        if ($suburb != '' && $suburb != null) {
            $arr_to_return = array('suburb' => $suburb, 'state' => $state, 'postcode' => $postcode);
        } else {
            $arr_to_return = array('error_code' => 1, 'msg' => 'Please enter valid suburb name and postcode..!');
        }
        echo json_encode($arr_to_return);
    }

    function getSuburbStatesZipcode() {
        $selected_suburb = $this->input->post('selected_address');
        $this->load->driver('cache');

        if ($cacheKey = $this->cache->file->get(md5("searchsubrub" . $selected_suburb))) {

            $arr_to_return = unserialize($cacheKey);
        } else {

            $theCondition = $this->validateEnteredSubrub($selected_suburb);

            if ($theCondition["error"]) {
                $arr_to_return = array('error_code' => 1, 'msg' => 'Please enter valid suburb name and postcode..!');
            } else {

                if ($theCondition["hasboth"]) {

                    $condition = "(suburb='" . strtoupper(stripslashes($theCondition["subrub"])) . "' and postcode='" . $theCondition["postcode"] . "')";
                } elseif (!empty($theCondition["subrub"])) {
                    $condition = "(suburb='" . strtoupper(stripslashes($theCondition["subrub"])) . "')";
                } elseif (!empty($theCondition["postcode"])) {
                    $condition = "(postcode='" . $theCondition["postcode"] . "')";
                }


                $demographic_data = end($this->common_model->getRecords('mst_demographic_data', 'suburb,state,postcode', $condition));

                $suburb = $demographic_data['suburb'];
                $state = $demographic_data['state'];
                $postcode = $demographic_data['postcode'];
                if ($suburb != '' && $suburb != null) {
                    $arr_to_return = array('suburb' => strtolower($suburb), 'state' => strtolower($state), 'postcode' => $postcode);
                } else {
                    $arr_to_return = array('error_code' => 1, 'msg' => 'Please enter valid suburb name and postcode..!');
                }
            }
            $this->cache->file->save(md5("searchsubrub" . $selected_suburb), serialize($arr_to_return), 86400 * 30);
        }
        echo json_encode($arr_to_return);
    }

    private function validateEnteredSubrub($inputStr) {
        if (empty($inputStr)) {
            return array("error" => 1);
        }

        $textMatch = "";
        $numberMatch = "";
        // Check if it has both entered
        preg_match("/[a-zA-Z\s]+/", $inputStr, $textMatch);
        preg_match("/[0-9]+/", $inputStr, $numberMatch);

        if (!empty($textMatch[0]) && !empty($numberMatch[0])) {
            // This is text and subrub
            return array("error" => 0, "hasboth" => 1, "subrub" => $textMatch[0], "postcode" => $numberMatch[0]);
        }

        if (!empty($textMatch[0])) {
            // This is text and subrub
            return array("error" => 0, "hasboth" => 0, "subrub" => $textMatch[0], "postcode" => "");
        }

        if (!empty($numberMatch[0])) {
            // This is text and subrub
            return array("error" => 0, "hasboth" => 0, "subrub" => "", "postcode" => $numberMatch[0]);
        }
    }

    public function seoPageDataCron() {
        $_SERVER["HTTP_CONNECTION"] = "keep-alive";

        $cron_data = $this->common_model->getRecords('mst_cron_details', '*', $condition_to_pass = '', $order_by_to_pass = 'cron_id DESC', $limit_to_pass = 1, $debug_to_pass = 0);
        if (count($cron_data) > 0) {
            $cron_id = $cron_data[0]['cron_id'];
            $suburbs_data = $this->common_model->getRecords($table_to_pass = 'mst_demographic_data', $fields_to_pass = 'state,postcode,suburb', $condition_to_pass = array('demographic_id >' => $cron_id), $order_by_to_pass = '', $limit_to_pass = 5, $debug_to_pass = 0);
        } else {
            $suburbs_data = $this->common_model->getRecords($table_to_pass = 'mst_demographic_data', $fields_to_pass = 'state,postcode,suburb', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = 5, $debug_to_pass = 0);
        }

        foreach ($suburbs_data as $key => $suburbs) {
            $link = base_url() . strtolower($suburbs['state']) . "/" . strtolower(str_replace(" ", "+", $suburbs['suburb'])) . "/" . strtolower($suburbs['postcode']);

            $condition_to_pass = array('page_link' => $link, 'status' => '0');
            $cron_record = $this->seo_page_banner_model->getCronRecords($table_to_pass = '', $fields_to_pass = '', $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

            if (count($cron_record) == 0) {
                file_get_contents($link);
                $arr_fields = array(
                    "page_link" => $link,
                    "status" => '1',
                    "cron_updated_date" => date("Y-m-d")
                );
                $this->common_model->insertRow($arr_fields, 'mst_cron_details');
                sleep(30);
            }
        }
        echo "Successfully fetch data..!, Hit link again to fetch next batch of data...!";
    }

    public function searchStateData() {
        $search_val = $this->input->post('search_val');
        if ($search_val != '') {
            $arr_state_data = $this->seo_page_banner_model->getSearchStateRecords($search_val);
            if (count($arr_state_data) > 0) {
                echo json_encode($arr_state_data);
            }
        }
    }

    public function CTAUrls() {
        $data['global'] = $this->common_model->getGlobalSettings();

        $arr_return = array();
        $arr_return['cta1_height'] = $data['global']['cta1_height'];
        $arr_return['cta1_src'] = $data['global']['cta1_src'];
        $arr_return['cta2_height'] = $data['global']['cta2_height'];
        $arr_return['cta2_src'] = $data['global']['cta2_src'];
        $arr_return['cta3_height'] = $data['global']['cta3_height'];
        $arr_return['cta3_src'] = $data['global']['cta3_src'];
        echo json_encode($arr_return);
    }

    public function generateAPNCache() {

        // Get All Locations / Subrubs in batch
        $cron_location_data = $this->common_model->getRecords('mst_locationcron_details', '*', $condition_to_pass = '', $order_by_to_pass = 'cron_id DESC', $limit_to_pass = 1, $debug_to_pass = 0);

        if (count($cron_location_data) > 0) {
            $cron_id = $cron_location_data[0]['cron_id'];
            $suburbs_data = $this->common_model->getRecords($table_to_pass = 'mst_demographic_data', $fields_to_pass = 'state,postcode,suburb', $condition_to_pass = array('demographic_id >' => $cron_id), $order_by_to_pass = '', $limit_to_pass = 5, $debug_to_pass = 0);
        } else {
            $suburbs_data = $this->common_model->getRecords($table_to_pass = 'mst_demographic_data', $fields_to_pass = 'state,postcode,suburb', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = 5, $debug_to_pass = 0);
        }
        // Call to private function
        if (count($suburbs_data) > 0) {
            foreach ($suburbs_data as $val) {
                $location = $val['suburb'];
                $this->generateCacheForLocationAPN($location);


                $cron_records = $this->common_model->getRecords($table_to_pass = 'mst_locationcron_details', $fields_to_pass = '*', $condition_to_pass = array('subhurb' => $location), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
                if (count($cron_records) == 0) {
                    $arr_fields = array(
                        "subhurb" => $location,
                        "status" => '1',
                        "cron_updated_date" => date("Y-m-d")
                    );
                    $this->common_model->insertRow($arr_fields, 'mst_locationcron_details');
                    sleep(30);
                }
            }
            echo "Successfully created cache..!Hit link again to create cache of next batch of records";
        } else {
            echo "Something went wrong..Please try agian..!";
        }

        // Save Current location in temp table
    }

    private function generateCacheForLocationAPN($location) {
        $data['global'] = $this->common_model->getGlobalSettings();
        $this->load->driver('cache');
        if ($cacheKey = $this->cache->file->get(md5($location))) {
            $search_data = unserialize($cacheKey);
        } else {
            $api_key = $data['global']['seo_api_key'];
            $url = "https://api.planningalerts.org.au/applications.js?count=10&key=" . $api_key . "&suburb=" . $location . "";
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $url);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch1);

            curl_close($ch1);
            $final_response = json_decode($response);

            $search_data = array();
            foreach ($final_response as $key => $suburbs) {
                /*
                 * No more used as distance is not displayed on the page
                  $current_location = $this->geoCheckIP();
                  $default_location = $this->getCityLnt($current_location['town'] . ' ' . $current_location['state']);

                  $point1_lat = $suburbs->application->lat;
                  $point1_long = $suburbs->application->lng;

                  $point2_lat = $default_location['lat'];
                  $point2_long = $default_location['lng']; */

                $date = date("Y-m-d H:i:s", strtotime($suburbs->application->date_scraped));
                $date_scraped = $this->time_elapsed_string($date);
                //$dis = $this->distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 1);
                $search_data[$key]['id'] = isset($suburbs->application->id) ? $suburbs->application->id : "";
                $search_data[$key]['address'] = isset($suburbs->application->address) ? $suburbs->application->address : "";
                $search_data[$key]['description'] = isset($suburbs->application->description) ? $suburbs->application->description : "";
//                $search_data[$key]['date_scraped'] = isset($date_scraped) ? $date_scraped . ", " . round($dis) . " km away" : "";
                $search_data[$key]['date_scraped'] = isset($date_scraped) ? $date_scraped : "";
                $search_data[$key]['date_received'] = isset($suburbs->application->date_received) ? $suburbs->application->date_received : "";
                $search_data[$key]['lat'] = isset($suburbs->application->lat) ? $suburbs->application->lat : "";
                $search_data[$key]['lng'] = isset($suburbs->application->lng) ? $suburbs->application->lng : "";
            }
            $this->cache->file->save(md5($location), serialize($search_data), 86400 * 7);
        }
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


