<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Suburb extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('suburbs_model');
    }

    /* Start :: Function to list all the properties */

    public function suburbList($pg = '') {
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("msg", "<span class='error'>You doesn't have priviliges to  manage user!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if (count($this->input->post()) > 0) {
            if ($this->input->post('btn_delete_all') != "") {
                /* getting all ides selected */
                $arr_suburb_id = $this->input->post('checkbox');
                if (count($arr_suburb_id) > 0 && is_array($arr_suburb_id)) {
                    if (count($arr_suburb_id) > 0) {
                        /* deleting the property selected */
                        $this->common_model->deleteRows($arr_suburb_id, "mst_suburbs", "suburb_id");
                    }
                    $this->session->set_userdata("msg", "<span class='success'>Suburb deleted successfully!</span>");
                }
            }
        }
        /* using the property model */
        $data['title'] = "Manage Promoted Suburbs";
        $data['arr_suburb_list'] = $this->common_model->getRecords('mst_suburbs', 'suburb_name,promoted_suburbs,suburb_id', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

        #Start:: pagination
        $totalrows = count($data['arr_suburb_list']);
        #load pagination library
        $this->load->library('pagination');
        $config['base_url'] = base_url() . "backend/suburb-list";
        $config['total_rows'] = $totalrows;
        $config['per_page'] = 25;
        $config['prev_link'] = "&laquo;";
        $config['next_link'] = "&raquo;";
        $config['cur_page'] = $pg;
        $data['cur_page'] = $pg;
        $config['num_links'] = 5;
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['full_tag_open'] = '<div class="dataTables_paginate paging_bootstrap"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<div class="dataTables_paginate paging_bootstrap"><ul class="pagination"><li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($config);
        $data['create_links'] = $this->pagination->create_links();
        $data['arr_suburb_list'] = $this->suburbs_model->getSuburbsList($config['per_page'], $pg);
        $data['page'] = $pg;
        #END Pagination :
        $this->load->view('backend/suburbs/list', $data);
    }

    public function suburbAdd() {
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("msg", "<span class='error'>You doesn't have priviliges to  manage user!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if ($this->input->post('suburb_name') != "") {
            $arr_to_insert_trans = array(
                "suburb_name" => mysql_real_escape_string($this->input->post('suburb_name')),
                "promoted_suburbs" => mysql_real_escape_string($this->input->post('promoted_suburbs')),
                "latitude" => mysql_real_escape_string($this->input->post('latitude')),
                "longitude" => mysql_real_escape_string($this->input->post('longitude'))
            );
            $this->common_model->insertRow($arr_to_insert_trans, "mst_suburbs");
            $this->session->set_userdata("msg", "<span class='success'>Records added successfully!</span>");
            /* getting mail subect and mail message using email template title and lang_id and reserved works */
            redirect(base_url() . "backend/suburbs");
            exit;
        }
        /* using the property model */
        $data['title'] = "Manage Add Promoted Suburbs";
        $this->load->view('backend/suburbs/add', $data);
    }

    public function checkSuburbName() {
        #checking admin is logged in or not
        $data = $this->common_model->commonFunction();
        $suburb_name = $this->input->post('suburb_name');
        $old_suburb_name = $this->input->post('old_suburb_name');
        $data['get_suburbs_list'] = $this->suburbs_model->getNamesList($suburb_name);
        if ($old_suburb_name != '') {
            if (count($data['get_suburbs_list']) > 0 && ($data['get_suburbs_list'][0]['suburb_name'] != $old_suburb_name)) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            if (count($data['get_suburbs_list']) > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    function suburbEdit($suburb_id) {
        $suburb_id = base64_decode($suburb_id);
        #Getting Common data
        $data = $this->common_model->commonFunction();
        #using the admin model        
        $arr_privileges = array();
        /*         * getting all privileges ** */
        $data['arr_privileges'] = $this->common_model->getRecords('mst_privileges');
        if ($data['user_account']['role_id'] != 1) {
            /* checking user has privilege to access the manage testimonial */
            $user_account = $this->session->userdata('user_account');
            /* getting user Privileges from the session array */
            $user_priviliges = ($user_account['user_privileges']);
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $user_account['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (!in_array(7, $arr_login_admin_privileges)) {
                /* setting session for displaying notiication message. */
                $this->session->set_userdata("msg", "<span class='error'>You doesn't have priviliges to  manage suburb!</span>");
                redirect(base_url() . "backend/home");
            }
        }
        if (!$this->suburbs_model->checkEditId("mst_suburbs", $suburb_id, 'suburb_id')) {
            $this->session->set_userdata("msg", "<span class='error'>Record not found!</span>");
            redirect(base_url() . "backend/home");
            exit;
        }
        if ($this->input->post()) {
            $suburb_name = addslashes($this->input->post('suburb_name'));
            $latitude = addslashes($this->input->post('latitude'));
            $longitude = addslashes($this->input->post('longitude'));
            $promoted_suburbs = addslashes($this->input->post('promoted_suburbs'));
            $arr_set_trans_fileds = array(
                "suburb_name" => $suburb_name,
                "latitude" => $latitude,
                "longitude" => $longitude,
                "promoted_suburbs" => $promoted_suburbs
            );
            $this->common_model->updateRow('mst_suburbs', $arr_set_trans_fileds, array("suburb_id" => $suburb_id));
            $this->session->set_userdata("msg", "<span class='success'>Record updated successfully!</span>");
            redirect(base_url() . 'backend/suburb-list');
        }
        #using the admin model      
        $data['title'] = "Edit Suburb";
        $data['arr_suburb_details'] = $this->suburbs_model->getSuburbDetails($suburb_id);
        $this->load->view('backend/suburbs/edit', $data);
    }
}
