<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Corelogic extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->_client_id = "1e71fe1b";
        $this->_client_secret = "21488a13c77fade5bf3553ed82a5567a";
        $this->_url = "";
        $this->load->model('common_model');
        $this->load->model('user_model');
        $this->load->helper('custom_method');
        
        $data['global'] = $this->common_model->getGlobalSettings();
        $client_id = $data['global']['client_id'];
        $client_secret = $data['global']['client_secret'];
         
    }

    public function accessLocality() {
        
        $data['global'] = $this->common_model->getGlobalSettings();
        $client_id = $data['global']['client_id'];
        $client_secret = $data['global']['client_secret'];
        $url = "https://access-uat-api.corelogic.asia/access/oauth/token?client_id=" . $client_id . "&client_secret=" . $client_secret . "&grant_type=client_credentials&env_access_restrict=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $response = json_decode($response);
        $token = $response->access_token;
        curl_close($ch);
        $kerword = $this->input->get("term");
        
        $url1 = "https://property-uat-api.corelogic.asia/bsg-au/v1/suggest.json?suggestionTypes=address&q=" . $kerword . "&access_token=" . $token;
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $url1);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
        $response1 = curl_exec($ch1);
        curl_close($ch);
        $response1 = json_decode($response1);
        $final_response =(array)$response1;
        $result = array();
        $search_data = array();
        foreach ($final_response['suggestions'] as $k=> $address) {
            $search_data[$k]["label"] = $address->suggestion;
            $search_data[$k]["value"] = $address->localityId;
            $search_data[$k]["property_id"] = $address->propertyId;
        }
        foreach ($search_data as $address) {
            $addressLabel = $address["label"];
            if (strpos(strtoupper($addressLabel), strtoupper($kerword)) !== false) {
                array_push($result, $address);
            }
        }
        echo json_encode($result);
    }
    
    public function getPropertyDetails() {
        $data['global'] = $this->common_model->getGlobalSettings();
        $client_id = $data['global']['client_id'];
        $client_secret = $data['global']['client_secret'];

        $url = "https://access-uat-api.corelogic.asia/access/oauth/token?client_id=" . $client_id . "&client_secret=" . $client_secret . "&grant_type=client_credentials&env_access_restrict=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $tokon_response = curl_exec($ch);
        $final_tokon_response = json_decode($tokon_response);
        
        $token = $final_tokon_response->access_token;
        curl_close($ch);
        $propertyId = $this->input->post("term");
        $url1 = "https://property-uat-api.corelogic.asia/bsg-au/v1/property/" . $propertyId . ".json?returnFields=address%2Cattributes%2CavmDetailList%2CcurrentOwnershipList%2CcontactList%2C%20developmentApplicationList%2CexternalReferenceList%2CfeatureList%2CforRentPropertyCampaignList%2CforSaleAgencyCampaignList%2CforSalePropertyCampaignList%2Clegal%2CparcelList%2CpropertyPhotoList%2CsaleList%2Csite%2Ctitle&access_token=" . $token;
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $url1);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch1);
        curl_close($ch);
        $final_response = json_decode($response);
        
        $search_data = array();

        foreach ($final_response as $key => $property) {

            $search_data[$key]['propertyName'] = isset($property->address->buildingComplexName) ? $property->address->buildingComplexName : "";
            $search_data[$key]['address'] = isset($property->address->singleLine) ? $property->address->singleLine : "";
            $search_data[$key]['suburb'] = isset($property->address->street->locality->name) ? $property->address->street->locality->name : "";
            $search_data[$key]['state'] = isset($property->address->street->locality->postcode->state) ? $property->address->street->locality->postcode->state : "";
            $search_data[$key]['postcode'] = isset($property->address->street->locality->postcode->name) ? $property->address->street->locality->postcode->name : "";
            $search_data[$key]['bathrooms'] = isset($property->attributes->bathrooms) ? $property->attributes->bathrooms : "";
            $search_data[$key]['bedrooms'] = isset($property->attributes->bedrooms) ? $property->attributes->bedrooms : "";
            $search_data[$key]['carSpaces'] = isset($property->attributes->carSpaces) ? $property->attributes->carSpaces : "";
            $search_data[$key]['ownership_type'] = isset($property->title->ownerCode) ? $property->title->ownerCode : "";
            $search_data[$key]['landArea'] = isset($property->attributes->landArea) ? $property->attributes->landArea : "";
            $search_data[$key]['estimate'] = isset($property->avmDetailList[0]->estimate) ? $property->avmDetailList[0]->estimate : "";
            $search_data[$key]['sqm'] = isset($property->avmDetailList[0]->score) ? $property->avmDetailList[0]->score : "";
            $search_data[$key]['corelogicPropertyId'] = isset($property->id) ? $property->id : "";
            $search_data[$key]['propertyPhotoList'] = isset($property->propertyPhotoList[0]->mediumPhotoUrl) ? $property->propertyPhotoList[0]->mediumPhotoUrl : "";
            $search_data[$key]['propertyType'] = isset($property->propertyType) ? $property->propertyType : "";
            $search_data[$key]['purchase_date'] = isset($property->saleList[0]->contractDate) ? $property->saleList[0]->contractDate : "";
            $search_data[$key]['purchase_price'] = isset($property->saleList[0]->price) ? $property->saleList[0]->price : "";
        }
        header("Content-type: application/json");


        echo json_encode($search_data);
    }

}