<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banner extends CI_Controller {
    /* construction function used to load all the models used in the controller.	   */

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('Seo_page_banner_model');
    }

    // function to list all the home page sections parameter

    public function listHomePageSettings() {

        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
            exit;
        }
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        //checking for admin privilages
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('10', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage seo page banner!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if ($this->input->post() != '') {
            if (count($this->input->post('checkbox')) > 0) {
                /* getting all ids selected */
                $arr_banner_ids = $this->input->post('checkbox');
                if (count($arr_banner_ids) > 0) {
                    /* deleting the testimonial from the backend */
                    $this->common_model->deleteRows($arr_banner_ids, "mst_home_page_sections", "section_name_id");
                    $this->session->set_userdata("msg", "<span class='success'>SEO banner page deleted successfully!</span>");
                }
            }
        }
        $data['title'] = "Manage SEO Page Banner";
        $data['arr_seo_banner'] = $this->Seo_page_banner_model->getBanner();
        $this->load->view('backend/seo-page-banner/list', $data);
    }

    //This function will help to change the status of any home page section enabled/disabled
    public function changeStatus() {
        if ($this->input->post('banner_id') != "" && $this->input->post('status') != "") {
            $arr_to_update = array("status" => $this->input->post('status'));
            $condition_array = array('section_val_id' => intval($this->input->post('banner_id')));
            $this->common_model->updateRow('trans_home_page_settings', $arr_to_update, $condition_array);

            echo json_encode(array("error" => "0", "error_message" => "Update has been successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, update can not be fulfilled this time. Please try again later"));
        }
    }

    //function to edit the global settings parameter
    public function editSeoPageBanner($edit_id = '') {
        /* checking admin is logged in or not */
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
        }
        /* getting commen data required */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the global settings */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('10', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage seo page banner!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if (count($_POST) > 0) {
            if ($this->input->post('description') != "" && $this->input->post('name') != "") {
                if ($this->input->post('edit_id') != '') {
                    $arr_to_update = array(
                        "name" => ($this->input->post('name'))
                    );
                    $condition_array = array('section_name_id' => intval(base64_decode($this->input->post('edit_id'))));
                    $this->common_model->updateRow('mst_home_page_sections', $arr_to_update, $condition_array);



//                    if ($this->input->post('section_type') == '1') {
//                        if ($_FILES['banner_video']['name'] != '') {
//                            $image_name_one = time() . '.' . $arr_file['ext'];
//                            $upload_dir = './media/front/home-page-videos/';
//                            $config['upload_path'] = $upload_dir;
//                            $config['allowed_types'] = '*';
//                            $config['max_size'] = '9000000000';
//                            $config['max_width'] = '12024';
//                            $config['max_height'] = '7268';
//                            $config['file_name'] = $image_name_one;
//                            $this->load->library('upload', $config);
//                            $this->upload->initialize($config);
//                            if (!$this->upload->do_upload('banner_video')) {
//                                $error = array('error' => $this->upload->display_errors());
//                                $this->session->set_userdata('msg', $error['error']);
//                                redirect(base_url() . 'backend/seo-page-banner/list');
//                            } else {
//                                $image_name = time() . ".mp4";
//                                $absolute_path = $this->common_model->absolutePath();
//                                $upload_path = $absolute_path . "media/front/home-page-videos/";
//                                $config['file_name'] = $image_name;
//                                $command = "ffmpeg -i " . $upload_path . $image_name_one . " -s 500x400 -vcodec libx264 -strict -2 " . $upload_path . $image_name;
//                                exec($command);
//                                $this->load->library('upload', $config);
//                                $this->upload->initialize($config);
//                                $this->upload->do_upload('banner_video');
//                                $data = array('upload_data' => $this->upload->data());
//                            }
//                        }
//                        if ($_FILES['banner_video']['name'] != '') {
//                            $image_data = $this->upload->data();
//                            $banner_image = $image_name;
//                        } else {
//                            $banner_image = $this->input->post('old_banner_video');
//                        }
//                        
//                        $arr_to_update2 = array(
//                                "description" => ($this->input->post('description')),
//                                "media" => $banner_image,
//                            );
//
//
//                        $condition_array = array('section_name_id' => intval(base64_decode($this->input->post('edit_id'))));
//                        $this->common_model->updateRow('trans_home_page_section', $arr_to_update2, $condition_array);
//                        
//                    } else {
//                    echo '<pre>';print_r($_FILES['banner_image']);die;
                    if ($_FILES['banner_image']['name'] != "") {
                        $image_data = array();
                        $config['upload_path'] = './media/front/home-page-images/';
                        $config['allowed_types'] = 'pdf|jpeg|jpg|png|gif|ico|bmp|GIF|PNG|JPG|JPEG';
                        $config['file_name'] = "image" . time();
                        $config['max_size'] = 1024 * 3;
                        $this->upload->initialize($config);
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('banner_image')) {
                            $error = array('error' => $this->upload->display_errors());
                        } else {
                            $data = array('upload_data' => $this->upload->data());
                            $image_data = $this->upload->data();
                            $banner_image = $image_data['file_name'];
                        }
                    }

                    if ($_FILES['banner_image']['name'] != "") {
                        $arr_to_update2 = array(
                            "description" => ($this->input->post('description')),
                            "media" => $banner_image,
                        );

                        $condition_array = array('section_name_id' => intval(base64_decode($this->input->post('edit_id'))));
                        $this->common_model->updateRow('trans_home_page_section', $arr_to_update2, $condition_array);
                    } else {
                        $arr_to_update2 = array(
                            "description" => ($this->input->post('description')),
                        );
                        $condition_array = array('section_name_id' => intval(base64_decode($this->input->post('edit_id'))));
                        $this->common_model->updateRow('trans_home_page_section', $arr_to_update2, $condition_array);
                    }


                    $this->session->set_userdata("msg", "<span class='success'>Section updated successfully!</span>");
                }
                redirect(base_url() . "backend/seo-page-banner/list");
            }
        }
        $data['title'] = "Edit Home Page Section";
        if (($edit_id != '')) {
            $data['edit_id'] = $edit_id;
            $arr_banner_details = $this->Seo_page_banner_model->getBannerDetails(intval(base64_decode($edit_id)));
            /* single row fix */
            $data['arr_banner_details'] = end($arr_banner_details);
            $this->load->view('backend/seo-page-banner/edit', $data);
        } else {
            /* go to the page not found */
        }
    }

    public function ViewSeoPageBanner($view_id = '') {
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
            exit;
        }
        $data = $this->common_model->commonFunction();
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('10', $arr_login_admin_privileges) == FALSE) {
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage seo page banner!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        $data['title'] = "Manage SEO Page Banner";
        $data['arr_banner_details'] = $this->Seo_page_banner_model->getBannerDetails(intval(base64_decode($view_id)));
        $this->load->view('backend/seo-page-banner/view', $data);
    }

    public function checkBannerName() {
        /* checking admin user name already exist or not for edit Admin */
        if (strtolower($this->input->post('name')) == strtolower($this->input->post('old_name'))) {
            echo "true";
        } else {
            $arr_banner_detail = $this->Seo_page_banner_model->getBannerDetailsByName($this->input->post('name'));
            if (count($arr_banner_detail) == 0) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

}
