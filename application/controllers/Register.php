<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("common_model");
        $this->load->model("register_model");
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper(array('url', 'html', 'form'));
        
    }

    public function customerSignUp() {
        $data = $this->common_model->commonFunction();
        if ($this->common_model->isLoggedIn()) {
            if ($data['user_account']['user_type'] == '2') {
                $this->session->unset_userdata('user_account');
                $this->session->set_userdata("msg_success", "You are not authorized to access this area.");
                redirect(base_url());
            } else {
                redirect(base_url() . 'my-dashboard');
            }
        }
        $data['site_title'] = "Create an Account";
        $social_logged_data = $this->session->userdata('social_userdata');
        $twitter_data = $this->session->userdata('twitter_data');
        if ($twitter_data) {

            $name = explode(' ', $twitter_data->name);
            $data['user_name'] = $twitter_data->screen_name;
            $data['first_name'] = $name[0];
            $data['last_name'] = $name[1];
            $data['tw_id'] = $twitter_data->id;
        } elseif ($this->session->userdata('social_userdata') != '') {

            $data['user_name'] = $social_logged_data['user_name'];
            $data['fb_flag'] = $social_logged_data['fb_flag'];
            $data['fb_user_id'] = $social_logged_data['fb_user_id'];
            $data['user_email'] = $social_logged_data['user_email'];
            $data['first_name'] = $social_logged_data['first_name'];
            $data['last_name'] = $social_logged_data['last_name'];
            if ($social_logged_data['fb_id'] != '') {
                $data['fb_id'] = $social_logged_data['fb_id'];
            } elseif ($social_logged_data['google_id'] != '') {
                $data['google_id'] = $social_logged_data['google_id'];
            } elseif ($social_logged_data['tw_id'] != '') {
                $data['twitter_id'] = $social_logged_data['tw_id'];
            }
        } else {
            $data['email_address'] = '';
            $data['first_name'] = '';
            $data['last_name'] = '';
        }
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/registration/customer-register', $data);
        $this->load->view('front/includes/footer');
    }
    /* Employer registration process */

    // Below function is called for validating select option field.
    public function selectValidate($abcd) {
// 'none' is the first option that is default "-------Choose City-------"
        if ($abcd == "") {
            $this->form_validation->set_message('selectValidate', 'Please Select Your Country.');
            return false;
        } else {
// Customer picked something.
            return true;
        }
    }
    //select date of birth
    public function DOBValidate($abcd) {
// 'none' is the first option that is default "-------Choose City-------"
        if ($abcd == "") {
            $this->form_validation->set_message('selectValidate', 'Please Select Date of birth.');
            return false;
        } else {
// Customer picked something.
            return true;
        }
    }

    public function employerRegister() {
        $data = $this->common_model->commonFunction();
        if ($this->common_model->isLoggedIn()) {
            if ($data['user_account']['user_type'] == '2') {
                $this->session->unset_userdata('user_account');
                $this->session->set_userdata("msg_success", "You are not authorized to access this area.");
                redirect(base_url());
            } else {
                redirect(base_url() . 'my-dashboard');
            }
        }

        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|trim|is_unique[mst_users.user_email]');
        $this->form_validation->set_rules('user_password', 'Password', 'required|matches[cnf_user_password]');
        $this->form_validation->set_rules('cnf_user_password', 'Confirm password', 'required');
        $this->form_validation->set_rules('first_name', 'Name', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        $this->form_validation->set_message('required', 'Enter %s');
        if ($this->form_validation->run() == FALSE) {
            $data['site_title'] = "Create an Account";
            $this->load->view('front/includes/header', $data);
            $this->load->view('front/registration/employer-register', $data);
            $this->load->view('front/includes/footer');
        } else {
            if ($this->input->post('user_email') != '') {
                $selected_city = reverseGeocode($this->input->post('city_name'));
                $table = 'mst_users';
                $activation_code = time();
                $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
                $hash_password = crypt($this->input->post('user_password'), '$2y$12$' . $salt);
                $user_name = explode('@', $this->input->post('user_email'));
                echo $this->input->post('fb_app_id');
                if ($this->input->post('fb_flag') == '1') {
                    $img_name = $this->saveFBImage('http://graph.facebook.com/' . $this->input->post('fb_user_id') . '/picture?type=large', $this->input->post('fb_app_id') . '_' . uniqid());
                } else {
                    $img_name = "";
                }
                $str = $this->input->post('first_name');
                $array1 = explode(" ", $str);
                if ($this->input->post('social_flag') == '1') {
                    $fields = array(
                        'user_email' => mysql_real_escape_string($this->input->post('user_email')),
                        'user_password' => $hash_password,
                        'first_name' => mysql_real_escape_string($array1[0]),
                        'last_name' => mysql_real_escape_string($array1[1]),
                        'tw_id' => mysql_real_escape_string($this->input->post('tw_id')),
                        'user_type' => '1',
                        'user_status' => '1',
                        'activation_code' => $activation_code,
                        'email_verified' => '1',
                        'profile_picture' => $img_name,
                        'register_date' => date("Y-m-d H:i:s"),
                        'ip_address' => $_SERVER['REMOTE_ADDR']
                    );
                } else {

                    $fields = array(
                        'user_email' => mysql_real_escape_string($this->input->post('user_email')),
                        'user_password' => $hash_password,
                        'first_name' => mysql_real_escape_string($array1[0]),
                        'last_name' => mysql_real_escape_string($array1[1]),
                        'user_type' => '1',
                        'user_status' => '1',
                        'activation_code' => $activation_code,
                        'email_verified' => '1',
                        'register_date' => date("Y-m-d H:i:s"),
                        'ip_address' => $_SERVER['REMOTE_ADDR']
                    );
                }
                $condition = '';
                $insert_id = $this->common_model->insertRow($fields, $table);
                if ($insert_id != '') {
                    $table_to_pass = 'mst_users';
                    $fields_to_pass = array('user_id', 'first_name', 'last_name', 'user_name', 'user_email', 'user_type', 'email_verified', 'user_status', 'user_password', 'role_id');
                    $condition_to_pass = "user_id = " . $insert_id . "";
                    $arr_login_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
                    $user_data['user_id'] = $arr_login_data[0]['user_id'];
                    $user_data['user_email'] = $arr_login_data[0]['user_email'];
                    $user_data['first_name'] = $arr_login_data[0]['first_name'];
                    $user_data['last_name'] = $arr_login_data[0]['last_name'];
                    $user_data['user_type'] = $arr_login_data[0]['user_type'];
                    $user_data['role_id'] = $arr_login_data[0]['role_id'];

                    $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $arr_login_data[0]['role_id']));
                    /* serializing the user privilegse and setting into the session. While ussing user privileges use unserialize this session to get user privileges */
                    if (count($arr_privileges) > 0) {
                        foreach ($arr_privileges as $privilege) {
                            $user_privileges[] = $privilege['privilege_id'];
                        }
                    } else {
                        $user_privileges = array();
                    }
                    $user_data['user_privileges'] = serialize($user_privileges);
                    /*
                     * Set the user's session
                     */
                    $this->session->set_userdata('user_account', $user_data);
                    //Send account activation emai_id to user
//                    $login_link = '<a href="' . base_url() . '">Click here.</a>';
//                    $lang_id = 17; // Default is 17(English)
//                    $activation_link = '<a href="' . base_url() . 'user-activation/' . $activation_code . '">Click here</a>';
//                    $macros_array_detail = array();
//                    $macros_array_detail = $this->common_model->getRecords('mst_email_template_macros', 'macros,value', $condition_to_pass = '', $order_by = '', $limit = '', $debug = 0);
//                    $macros_array = array();
//                    foreach ($macros_array_detail as $row) {
//                        $macros_array[$row['macros']] = $row['value'];
//                    }
//                    if ($this->input->post('social_flag') == '1') {
//                        $reserved_words = array();
//                        $reserved_arr = array(
//                            "||USER_NAME||" => $array1[0],
//                            "||FIRST_NAME||" => $array1[0],
//                            "||LAST_NAME||" => $array1[1],
//                            "||USER_EMAIL||" => $this->input->post('user_email'),
//                            "||PASSWORD||" => $this->input->post('user_password'),
//                            "||ACTIVATION_LINK||" => $activation_link,
//                            "||SITE_URL||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                            "||SITE_PATH||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                            "||SITE_TITLE||" => $data['global']['site_title'],
//                            "||CONTACT_US||" => '<a href="' . base_url() . 'contact-us">Contact Us</a>'
//                        );
//                        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
//                        $template_title = 'registration-successful';
//                        $template_title = 'social-registration-successful';
//                        $register_msg = "Registration Successful! A confirmation email has been sent to " . '"' . $this->input->post('user_email') . '"';
//                    } else {
//                        $reserved_words = array();
//                        $reserved_arr = array(
//                            "||USER_EMAIL||" => $this->input->post('user_email'),
//                            "||PASSWORD||" => $this->input->post('user_password'),
//                            "||USER_NAME||" => $array1[0],
//                            "||FIRST_NAME||" => $array1[0],
//                            "||LAST_NAME||" => $array1[1],
//                            "||ACTIVATION_LINK||" => $activation_link,
//                            "||SITE_URL||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                            "||SITE_PATH||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                            "||SITE_TITLE||" => $data['global']['site_title'],
//                            "||CONTACT_US||" => '<a href="' . base_url() . 'contact-us">Contact Us</a>'
//                        );
//                        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
////                        $template_title = 'social-registration-successful';
//                        $template_title = 'registration-successful';
//                        $register_msg = "Registration Successful! A confirmation email has been sent to " . '"' . $this->input->post('user_email') . '"' . " , If you don't receive this confirmation email in your email inbox within a few minutes, please check your email's spam folder and then click on the Activation link in the email, to activate your " . $data['global']['site_title'] . " account";
//                    }
//                    $arr_emailtemplate_data = $this->common_model->getEmailTemplateInfo($template_title, $lang_id, $reserved_words);
//                    $recipeinets = $this->input->post('user_email');
//                    $from = array("email" => $data['global']['site_email'], "name" => $data['global']['site_title']);
//                    $subject = $arr_emailtemplate_data['subject'];
//                    $message = $arr_emailtemplate_data['content'];
//                    $mail = $this->common_model->sendEmail($recipeinets, $from, $subject, $message);
//                    if (count($mail) > 0) {
//                        $this->session->set_userdata('msg_success', $register_msg);
//                        redirect(base_url());
//                    }

                    $this->session->set_userdata('msg_success', "You have registered successfully.");
                    redirect(base_url() . "my-dashboard");
                } else {
                    $this->session->set_userdata('msg_warning', "Something went wrong, please try again.");
                    redirect(base_url());
                }
            }
        }
    }

    public function createThumbnail($filename, $image_width, $path_to_image_directory, $path_to_thumbs_directory) {
        $final_width_of_image = $image_width;
        if (preg_match('/[.](jpg)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        } elseif (preg_match('/[.](JPG)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        } elseif (preg_match('/[.](jpeg)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        } elseif (preg_match('/[.](JPEG)$/', $filename)) {
            $im = imagecreatefromjpeg($path_to_image_directory . $filename);
        } else if (preg_match('/[.](gif)$/', $filename)) {
            $im = imagecreatefromgif($path_to_image_directory . $filename);
        } elseif (preg_match('/[.](GIF)$/', $filename)) {
            $im = imagecreatefromgif($path_to_image_directory . $filename);
        } else if (preg_match('/[.](png)$/', $filename)) {
            $im = imagecreatefrompng($path_to_image_directory . $filename);
        } elseif (preg_match('/[.](PNG)$/', $filename)) {
            $im = imagecreatefrompng($path_to_image_directory . $filename);
        }

        $ox = imagesx($im);
        $oy = imagesy($im);

        $nx = $final_width_of_image;
        $ny = floor($oy * ($final_width_of_image / $ox));

        $nm = imagecreatetruecolor($nx, $ny);

        imagecopyresampled($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);

        if (!file_exists($path_to_thumbs_directory)) {
            if (!mkdir($path_to_thumbs_directory)) {
                die("There was a problem. Please try again!");
            }
        }

        imagejpeg($nm, $path_to_thumbs_directory . $filename);
        $tn = '<img src="' . $path_to_thumbs_directory . $filename . '" alt="image" />';
        $tn .= '<br />Congratulations. Your file has been successfully uploaded, and a thumbnail has been created.';
        return true;
    }

    public function saveFBImage($file_content = '', $fb_id = '') {
        $data = $this->common_model->commonFunction();
        $imageData = file_get_contents($file_content);
        file_put_contents($this->common_model->absolutePath() . 'media/front/img/user-profile-pictures/' . $fb_id . ".jpg", $imageData);
        $this->createThumbnail($fb_id . '.jpg', '100', 'media/front/img/user-profile-pictures/', 'media/front/img/user-profile-pictures/mini-thumb/');
        $this->createThumbnail($fb_id . '.jpg', '100', 'media/front/img/user-profile-pictures/', 'media/front/img/user-profile-pictures/profile-thumb/');
        return $fb_id . ".jpg";
    }
    
    /* show repairer registation form 1 */
    public function repairerSignup() {
        $data = $this->common_model->commonFunction();
        if ($this->common_model->isLoggedIn()) {
            if ($data['user_account']['user_type'] == '2') {
                $this->session->unset_userdata('user_account');
                $this->session->set_userdata("msg_success", "You are not authorized to access this area.");
                redirect(base_url());
            } else {
                redirect(base_url() . 'my-dashboard');
            }
        }
        $data['title'] = "Repairer Signup";
        if ($this->session->userdata('social_userdata') != '') {
            $social_logged_data = $this->session->userdata('social_userdata');
            $data['user_name'] = $social_logged_data['user_name'];
            $data['user_email'] = $social_logged_data['user_email'];
            $data['first_name'] = $social_logged_data['first_name'];
            $data['last_name'] = $social_logged_data['last_name'];
            if ($social_logged_data['fb_id'] != '') {
                $data['fb_id'] = $social_logged_data['fb_id'];
            } elseif ($social_logged_data['google_id'] != '') {
                $data['google_id'] = $social_logged_data['google_id'];
            } elseif ($social_logged_data['tw_id'] != '') {
                $data['twitter_id'] = $social_logged_data['tw_id'];
            }
        } else {
            $data['email_address'] = '';
            $data['first_name'] = '';
            $data['last_name'] = '';
        }

        $data['arr_categories'] = $this->common_model->getCategoriesAndSubCategories();
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/registration/repairer-register', $data);
        $this->load->view('front/includes/footer');
    }
    /* repairer registration */

    public function dealerRegister() {
        $data = $this->common_model->commonFunction();
        if ($this->input->post('user_email') != '') {
            $selected_city = reverseGeocode($this->input->post('city_name'));
            $table = 'mst_users';
            $activation_code = time();
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($this->input->post('user_password_d'), '$2y$12$' . $salt);
            $user_name = explode('@', $this->input->post('user_email'));
            echo $this->input->post('fb_app_id');
            if ($this->input->post('fb_flag') == '1') {
                $img_name = $this->saveFBImage('http://graph.facebook.com/' . $this->input->post('fb_user_id') . '/picture?type=large', $this->input->post('fb_app_id') . '_' . uniqid());
            } else {
                $img_name = "";
            }
            $str = $this->input->post('first_name');
            $array1 = explode(" ", $str);
            if ($this->input->post('social_flag') == '1') {
                $fields = array(
                    'user_email' => mysql_real_escape_string($this->input->post('user_email')),
                    'user_password' => $hash_password,
                    'first_name' => mysql_real_escape_string($array1[0]),
                    'last_name' => mysql_real_escape_string($array1[1]),
                    'gender' => mysql_real_escape_string($this->input->post('gender')),
                    'company_id_fk' => mysql_real_escape_string($this->input->post('company_id')),
                    'career_year' => mysql_real_escape_string($this->input->post('career_year')),
                    'user_age' => mysql_real_escape_string($this->input->post('user_age')),
                    'phone' => $this->input->post('contact_number'),
                    'user_type' => '3',
                    'user_status' => '1',
                    'activation_code' => $activation_code,
                    'email_verified' => '1',
                    'profile_picture' => $img_name,
                    'register_date' => date("Y-m-d H:i:s"),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
            } else {
                $fields = array(
                    'user_email' => mysql_real_escape_string($this->input->post('user_email')),
                    'user_password' => $hash_password,
                    'first_name' => mysql_real_escape_string($array1[0]),
                    'last_name' => mysql_real_escape_string($array1[1]),
                    'gender' => mysql_real_escape_string($this->input->post('gender')),
                    'company_id_fk' => mysql_real_escape_string($this->input->post('company_id')),
                    'career_year' => mysql_real_escape_string($this->input->post('career_year')),
                    'user_age' => mysql_real_escape_string($this->input->post('user_age')),
                    'phone' => $this->input->post('contact_number'),
                    'user_type' => '3',
                    'user_status' => '1',
                    'activation_code' => $activation_code,
                    'email_verified' => '1',
                    'register_date' => date("Y-m-d H:i:s"),
                    'ip_address' => $_SERVER['REMOTE_ADDR']
                );
            }

            $condition = '';
            $insert_id = $this->common_model->insertRow($fields, $table);

            if ($insert_id != '') {
                $table_to_pass = 'mst_users';
                $fields_to_pass = array('user_id', 'first_name', 'last_name', 'user_name', 'user_email', 'user_type', 'email_verified', 'user_status', 'user_password', 'role_id');
                $condition_to_pass = "user_id = " . $insert_id . "";
                $arr_login_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
                $user_data['user_id'] = $arr_login_data[0]['user_id'];
                $user_data['user_email'] = $arr_login_data[0]['user_email'];
                $user_data['first_name'] = $arr_login_data[0]['first_name'];
                $user_data['last_name'] = $arr_login_data[0]['last_name'];
                $user_data['user_type'] = $arr_login_data[0]['user_type'];
                $user_data['role_id'] = $arr_login_data[0]['role_id'];

                $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $arr_login_data[0]['role_id']));
                /* serializing the user privilegse and setting into the session. While ussing user privileges use unserialize this session to get user privileges */
                if (count($arr_privileges) > 0) {
                    foreach ($arr_privileges as $privilege) {
                        $user_privileges[] = $privilege['privilege_id'];
                    }
                } else {
                    $user_privileges = array();
                }
                $user_data['user_privileges'] = serialize($user_privileges);
                /*
                 * Set the user's session
                 */
                $this->session->set_userdata('user_account', $user_data);
//                $login_link = '<a href="' . base_url() . '">Click here.</a>';
//                $lang_id = 17; // Default is 17(English)
//                $activation_link = '<a href="' . base_url() . 'user-activation/' . $activation_code . '">Click here</a>';
//                $macros_array_detail = array();
//                $macros_array_detail = $this->common_model->getRecords('mst_email_template_macros', 'macros,value', $condition_to_pass = '', $order_by = '', $limit = '', $debug = 0);
//                $macros_array = array();
//                foreach ($macros_array_detail as $row) {
//                    $macros_array[$row['macros']] = $row['value'];
//                }
//                if ($this->input->post('social_flag') == '1') {
//                    $reserved_words = array();
//                    $reserved_arr = array(
//                        "||USER_NAME||" => $array1[0],
//                        "||FIRST_NAME||" => $array1[0],
//                        "||LAST_NAME||" => $array1[1],
//                        "||USER_EMAIL||" => $this->input->post('user_email'),
//                        "||PASSWORD||" => $this->input->post('user_password_d'),
//                        "||SITE_URL||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                        "||SITE_PATH||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                        "||SITE_TITLE||" => $data['global']['site_title'],
//                        "||CONTACT_US||" => '<a href="' . base_url() . 'contact-us">Contact Us</a>'
//                    );
//                    $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
//                    $template_title = 'social-registration-successful';
//                    $register_msg = "Registration Successful! A confirmation email has been sent to " . '"' . $this->input->post('user_email') . '"';
//                } else {
//                    $reserved_words = array();
//                    $reserved_arr = array(
//                        "||USER_EMAIL||" => $this->input->post('user_email'),
//                        "||PASSWORD||" => $this->input->post('user_password_d'),
//                        "||FIRST_NAME||" => $array1[0],
//                        "||USER_NAME||" => $array1[0],
//                        "||LAST_NAME||" => $array1[1],
//                        "||ACTIVATION_LINK||" => $activation_link,
//                        "||SITE_URL||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                        "||SITE_PATH||" => '<a href="' . base_url() . '">' . base_url() . '</a>',
//                        "||SITE_TITLE||" => $data['global']['site_title'],
//                        "||CONTACT_US||" => '<a href="' . base_url() . 'contact-us">Contact Us</a>'
//                    );
//                    $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
////                        $template_title = 'social-registration-successful';
//                    $template_title = 'registration-successful';
//                    $register_msg = "Registration Successful! A confirmation email has been sent to " . '"' . $this->input->post('user_email') . '"' . " , If you don't receive this confirmation email in your email inbox within a few minutes, please check your email's spam folder and then click on the Activation link in the email, to activate your " . $data['global']['site_title'] . " account";
//                }
//                $arr_emailtemplate_data = $this->common_model->getEmailTemplateInfo($template_title, $lang_id, $reserved_words);
//                $recipeinets = $this->input->post('user_email');
//                $from = array("email" => $data['global']['site_email'], "name" => $data['global']['site_title']);
//                $subject = $arr_emailtemplate_data['subject'];
//                $message = $arr_emailtemplate_data['content'];
//                $mail = $this->common_model->sendEmail($recipeinets, $from, $subject, $message);
//                if (count($mail) > 0) {
//                    $this->session->set_userdata('msg_success', $register_msg);
//                    redirect(base_url());
//                }
                $this->session->set_userdata('msg_success', "You have registered successfully.");
                redirect(base_url() . "mydashboard");
            } else {
                $this->session->set_userdata('msg_warning', "Something went wrong, please try again.");
                redirect(base_url());
            }
        }

        $data['site_title'] = "Create an Dealer Account";
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/registration/dealer-register', $data);
        $this->load->view('front/includes/footer');
    }

    public function employerRegisterFacebook() {
        $data = $this->common_model->commonFunction();
        $social_logged_data = $this->session->userdata('social_userdata');
        $fields = array(
            'user_name' => $social_logged_data['first_name'],
            'user_email' => $social_logged_data['user_email'],
            'first_name' => $social_logged_data['first_name'],
            'last_name' => $social_logged_data['last_name'],
            'user_password' => $social_logged_data['user_password'],
            'user_type' => '1',
            'user_status' => '1',
            'email_verified' => '1',
        );
        $insert_id = $this->common_model->insertRow($fields, 'mst_users');
        if ($insert_id != '') {
            $arr_login_data = $this->common_model->getRecords('mst_users', 'user_id,user_type,role_id,first_name,last_name,user_email,user_name', array('user_id' => $insert_id));
            $user_data['user_id'] = $arr_login_data[0]['user_id'];
            $user_data['user_email'] = $arr_login_data[0]['user_email'];
            $user_data['first_name'] = $arr_login_data[0]['first_name'];
            $user_data['last_name'] = $arr_login_data[0]['last_name'];
            $user_data['user_type'] = $arr_login_data[0]['user_type'];
            $user_data['role_id'] = $arr_login_data[0]['role_id'];

            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $arr_login_data[0]['role_id']));
            /* serializing the user privilegse and setting into the session. While ussing user privileges use unserialize this session to get user privileges */
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            } else {
                $user_privileges = array();
            }
            $user_data['user_privileges'] = serialize($user_privileges);
            $this->session->set_userdata('user_account', $user_data);
            $this->session->unset_userdata($social_logged_data);
        }
        $this->session->set_userdata('msg_success', "You have logged in successfully.");

        redirect(base_url() . "myfbDashboard/" . base64_encode($arr_login_data[0]['user_id']));
    }

    public function dealerRegisterFacebook() {
        $data = $this->common_model->commonFunction();
        $social_logged_data = $this->session->userdata('social_userdata');
        $fields = array(
            'user_name' => $social_logged_data['first_name'],
            'user_email' => $social_logged_data['user_email'],
            'first_name' => $social_logged_data['first_name'],
            'last_name' => $social_logged_data['last_name'],
            'user_password' => $social_logged_data['user_password'],
            'user_type' => '3',
            'user_status' => '1',
            'email_verified' => '1',
        );
        $insert_id = $this->common_model->insertRow($fields, 'mst_users');
        if ($insert_id != '') {
            $arr_login_data = $this->common_model->getRecords('mst_users', 'user_id,user_type,role_id,first_name,last_name,user_email,user_name', array('user_id' => $insert_id));
            $user_data['user_id'] = $arr_login_data[0]['user_id'];
            $user_data['user_email'] = $arr_login_data[0]['user_email'];
            $user_data['first_name'] = $arr_login_data[0]['first_name'];
            $user_data['last_name'] = $arr_login_data[0]['last_name'];
            $user_data['user_type'] = $arr_login_data[0]['user_type'];
            $user_data['role_id'] = $arr_login_data[0]['role_id'];

            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $arr_login_data[0]['role_id']));
            /* serializing the user privilegse and setting into the session. While ussing user privileges use unserialize this session to get user privileges */
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            } else {
                $user_privileges = array();
            }
            $user_data['user_privileges'] = serialize($user_privileges);
            $this->session->set_userdata('user_account', $user_data);
            $this->session->unset_userdata($social_logged_data);
        }
        $this->session->set_userdata('msg_success', "You have logged in successfully.");
        redirect(base_url() . "myfbDashboard/" . base64_encode($arr_login_data[0]['user_id']));
    }

    /*
     * Customer's account activation by email
     */

    public function userActivation($activation_code) {
        $this->load->model('register_model');
        $table_to_pass = 'mst_users';
        $fields_to_pass = array('user_id', 'first_name', 'last_name', 'user_name', 'user_email', 'user_type', 'email_verified', 'user_status');
        $condition_to_pass = array("activation_code" => $activation_code);
        /* get user details to verify the email address */
        $arr_login_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        if (count($arr_login_data)) {
            if ($arr_login_data[0]['email_verified'] == 1) {
                $this->session->set_userdata('msg_error', "You have already activated your account. Please log in.");
            } else {

                $user_detail = $this->common_model->getRecords("mst_users", "user_id", array("activation_code" => $activation_code));
                $this->load->model('admin_model');
                /* Removing the user if he is exists in inactiveated list */
                $this->register_model->updateInactiveUserFile($this->common_model->absolutePath(), 1, intval($user_detail[0]['user_id']));
                $table_name = 'mst_users';
                $update_data = array("user_status" => '1', 'email_verified' => '1');
                $condition_to_pass = array("activation_code" => $activation_code);
                $this->common_model->updateRow($table_name, $update_data, $condition_to_pass);
                $this->session->set_userdata('msg_success', "Your account has been activated successfully. Please log In.");
            }
        } else {
            $this->session->set_userdata('msg_error', "Invalid activation link.");
        }
        redirect(base_url());
    }

    /*
     * Check email duplication
     */

    public function chkEmailDuplicate() {
        $this->load->model('register_model');
        $user_email = $this->input->post('user_email');
        $table_to_pass = 'mst_users';
        $fields_to_pass = array('user_id', 'user_email');
        $condition_to_pass = array("user_email" => $user_email);

        if ($this->input->post('user_email_old') == $this->input->post('user_email')) {
            echo 'true';
        } else {

            $arr_login_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            if (count($arr_login_data)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    /*
     * Check email availability 
     */

    public function chkEmailExist() {
        ob_clean();
        $this->load->model('register_model');
        $user_email = $this->input->post('forgot_user_email');
        $table_to_pass = 'mst_users';
        $fields_to_pass = array('user_id', 'user_email');
        if ($this->input->post('action') != "") {
            $condition_to_pass = "(user_type!=2) and (`user_email` = '" . $user_email . "' or `user_name` = '" . $user_email . "')";
        } else {
            $condition_to_pass = array("user_email" => $user_email);
        }
        $arr_login_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        if (count($arr_login_data)) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    /*
     * Chekck duplicate username
     */

    public function chkUserDuplicate() {
        $this->load->model('register_model');
        $user_name = $this->input->post('user_name');
        $table_to_pass = 'mst_users';
        $fields_to_pass = array('user_id', 'user_name');
        $condition_to_pass = array("user_name" => $user_name);
        $arr_login_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        if (count($arr_login_data)) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    /*
     * create captcha image
     */

    public function generateCaptcha($rand) {
        ob_clean();
        $data = $this->common_model->commonFunction();
        $arr1 = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $arr2 = array();
        foreach ($arr1 as $val)
            $arr2[] = strtoupper($val);
        $arr3 = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $str = "";
        $arr_all_characters = array_merge($arr1, $arr2, $arr3);
        for ($i = 0; $i < 5; $i++) {
            $str.=$arr_all_characters[array_rand($arr_all_characters)] . "";
        }
        $this->session->set_userdata('security_answer', $str);
        putenv('GDFONTPATH=' . realpath('.'));
        //$font = '/var/www/ci-pipl-code-library/user-module/media/front/captcha/ariblk.ttf';
        $font = $data['absolute_path'] . 'media/front/captcha/ariblk.ttf';
        $IMGVER_IMAGE = imagecreatefromjpeg(base_url() . "media/front/captcha/bg1.jpg");
        $IMGVER_COLOR_WHITE = imagecolorallocate($IMGVER_IMAGE, 0, 0, 0);
        $text = $str;
        $IMGVER_COLOR_BLACK = imagecolorallocate($IMGVER_IMAGE, 255, 255, 255);
        imagefill($IMGVER_IMAGE, 0, 0, $IMGVER_COLOR_BLACK);
        imagettftext($IMGVER_IMAGE, 24, 0, 20, 28, $IMGVER_COLOR_WHITE, $font, $text);
        //header("Content-type: image/jpeg");
        imagejpeg($IMGVER_IMAGE);
    }

    /*
     * Check the captcha validation 
     */

    public function checkCaptcha() {
        if ($this->input->post('input_captcha_value') == $this->session->userdata('security_answer')) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    /*
     * Login into website
     */

    public function signin() {

        $data = $this->common_model->commonFunction();
        $user_id = $data['user_account']['user_id'];
        if ($this->common_model->isLoggedIn()) {

            if ($data['user_account']['user_type'] == '2') {
                $this->session->unset_userdata('user_account');
                $this->session->set_userdata("msg_success", "You are not authorized to access this area.");
                redirect(base_url());
            } else if ($data['user_account']['user_type'] == '1') {
                if ($this->input->post('bid_value') == '11') {
                    redirect(base_url() . 'add-place-bids');
                } elseif ($this->input->post('question_value') == '22') {
                    redirect(base_url() . 'add-view-QA');
                } else {
                    redirect(base_url() . 'my-dashboard');
                }
            } else {
                redirect(base_url() . 'mydashboard');
            }
        }
        if ($this->input->post('user_email') != '') {
            $table_to_pass = 'mst_users';
            $fields_to_pass = array('user_id', 'first_name', 'last_name', 'user_name', 'user_email', 'user_type', 'email_verified', 'user_status', 'user_password', 'role_id');
            $condition_to_pass = "user_email = '" . addslashes($this->input->post('user_email')) . "'";
            $arr_login_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            if (count($arr_login_data)) {
                $crypt = crypt($this->input->post('user_password'), $arr_login_data[0]['user_password']);
                if ($crypt != $arr_login_data[0]['user_password']) {
                    $this->session->set_userdata('msg_error', "Please enter correct password.");
                    redirect(base_url());
                    exit;
                } elseif ($arr_login_data[0]['email_verified'] == 1) {
                    if ($arr_login_data[0]['user_status'] == 2) {
                        $this->session->set_userdata('msg_error', "Your account has been blocked by administrator.");
                        redirect(base_url());
                    } else {
                        $user_data['user_id'] = $arr_login_data[0]['user_id'];
                        $user_data['user_email'] = $arr_login_data[0]['user_email'];
                        $user_data['first_name'] = $arr_login_data[0]['first_name'];
                        $user_data['last_name'] = $arr_login_data[0]['last_name'];
                        $user_data['user_type'] = $arr_login_data[0]['user_type'];
                        $user_data['role_id'] = $arr_login_data[0]['role_id'];

                        $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $arr_login_data[0]['role_id']));
                        /* serializing the user privilegse and setting into the session. While ussing user privileges use unserialize this session to get user privileges */
                        if (count($arr_privileges) > 0) {
                            foreach ($arr_privileges as $privilege) {
                                $user_privileges[] = $privilege['privilege_id'];
                            }
                        } else {
                            $user_privileges = array();
                        }
                        $user_data['user_privileges'] = serialize($user_privileges);
                        /*
                         * Set the user's session
                         */
                        $this->session->set_userdata('user_account', $user_data);
						$_SESSION['user_id']=$arr_login_data[0]['user_id'];
                        if ($this->input->post('remember_me') == 'remember_me') {
                            $this->load->helper('cookie');
                            $cookie = array(
                                'name' => 'user_email',
                                'value' =>
                                $this->input->post('user_email'),
                                'expire' => '86500'
                            );
                            $this->input->set_cookie($cookie);
                            $cookie = array(
                                'name' => 'user_password',
                                'value' =>
                                $this->input->post('user_password'),
                                'expire' => '86500'
                            );
                            $this->input->set_cookie($cookie);
                        } else {
                            $this->load->helper('cookie');
                            delete_cookie("user_email");
                            delete_cookie("user_password");
                        }

                        $this->common_model->updateRow('mst_users', array('user_online' => 'Yes'), array('user_id' => intval($user_data['user_id'])));
                        if ($user_data['user_type'] == '1') {
                            $this->session->set_userdata('msg_success', "You have logged in successfully.");
                            if ($this->input->post('bid_value') == '11') {
                                redirect(base_url() . 'add-place-bids');
                            } elseif ($this->input->post('question_value') == '22') {
                                redirect(base_url() . 'add-view-QA');
                            } else {
                                redirect(base_url() . 'my-dashboard');
                            }
                        } elseif ($user_data['user_type'] == '3') {
                            $this->session->set_userdata('msg_success', "You have logged in successfully.");
                            redirect(base_url() . 'mydashboard');
                        }
                    }
                } else {
                    $this->session->set_userdata('msg_error', "Please activate your account.");
                    redirect(base_url());
                }
            } else {
                $this->session->set_userdata('msg_error', "Invalid email address.");
                redirect(base_url());
            }
        } else {
            $this->session->set_userdata('msg_error', "Opps !!! Something went wrong. Please try again.");
            redirect(base_url());
        }
    }

    /*
     * Send the reset password link
     */

    public function passwordRecovery() {
        $data = $this->common_model->commonFunction();
        if ($this->common_model->isLoggedIn()) {
            if ($data['user_account']['user_type'] == '2') {
                $this->session->unset_userdata('user_account');
                $this->session->set_userdata("msg_success", "You are not authorized to access this area.");
                redirect(base_url());
            } else {
                redirect(base_url() . 'my-dashboard');
            }
        }
        if ($this->input->post('forgot_user_email') != '') {
            /* get user information to send password detail */
            $table_to_pass = 'mst_users';
            $fields_to_pass = array('user_id', 'first_name', 'last_name', 'user_name', 'user_email', 'user_password');
            $condition_to_pass = "`user_email` = '" . $this->input->post('forgot_user_email') . "'";

            $arr_password_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
            if (count($arr_password_data)) {
                $code = rand(9991, 999899);
                $activation_code = time();
                $table_name = 'mst_users';
                $update_data = array('reset_password_code' => $activation_code, 'password_change_security_code' => $code);
                $condition_to_pass = array("user_email" => $arr_password_data[0]['user_email']);
                $this->common_model->updateRow($table_name, $update_data, $condition_to_pass);
                $reset_password_link = '<a href="' . base_url() . 'reset-password/' . base64_encode($activation_code) . '">Click here</a>';
                $lang_id = $this->session->userdata('lang_id');
                if (isset($lang_id) && $lang_id != '') {
                    $lang_id = $this->session->userdata('lang_id');
                } else {
                    $lang_id = 17; // Default is 17(English)
                }
                $reserved_words = array
                    (
                    "||USER_NAME||" => $arr_password_data[0]['first_name'],
                    "||FIRST_NAME||" => $arr_password_data[0]['first_name'],
                    "||LAST_NAME||" => $arr_password_data[0]['last_name'],
                    "||USER_EMAIL||" => $arr_password_data[0]['user_email'],
                    "||RESET_PASSWORD_LINK||" => $reset_password_link,
                    "||SITE_TITLE||" => $data['global']['site_title'],
                    "||CODE||" => $code,
                    "||SITE_PATH||" => base_url()
                );
                $template_title = 'forgot-password';
                $arr_emailtemplate_data = $this->common_model->getEmailTemplateInfo($template_title, $lang_id, $reserved_words);
                $recipeinets = $arr_password_data[0]['user_email'];
                $from = array("email" => $data['global']['site_email'], "name" => $data['global']['site_title']);
                $subject = $arr_emailtemplate_data['subject'];
                $message = $arr_emailtemplate_data['content'];
                $mail = $this->common_model->sendEmail($recipeinets, $from, $subject, $message);
                if ($mail) {
                    $this->session->set_userdata('msg_success', "We have sent reset password link on your email " . $arr_password_data[0]['user_email']);
                    redirect(base_url());
                }
            }
        }
        $data['site_title'] = "Forgot Password";
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/forgot-password/forgot-password');
        $this->load->view('front/includes/footer');
    }

    public function checkForValidCodeOnchangePassword() {
        $data = $this->common_model->commonFunction();
        $code = $this->input->post('security_code');
        $data['user_session'] = $this->session->userdata('user_account');
        if ($code != '') {
            $table_name = 'mst_users';
            $select_data = array('user_id,password_change_security_code');
            $condition = array('password_change_security_code' => $code);
            $data['user_info'] = $this->common_model->getRecords($table_name, $select_data, $condition);

            if (count($data['user_info']) > 0) {
                if ($data['user_info'][0]['password_change_security_code'] != '0' && $data['user_info'][0]['password_change_security_code'] != '') {
                    echo "true";
                    exit;
                } else {
                    echo "false";
                    exit;
                }
            } else {
                echo "false";
                exit;
            }
        }
        echo "false";
        exit;
    }

    /*
     * Change new user's password
     */

    public function resetPassword($activation_code) {

        $data = $this->common_model->commonFunction();
        if ($activation_code != '') {
            $data['activation_code'] = $activation_code;
        }
        /* cheaking password link expirted or not using reset_password_code; */
        $user_detail = $this->common_model->getRecords("mst_users", "user_id", array("reset_password_code" => base64_decode($data['activation_code'])));

        if (count($user_detail) == 0) {
            $this->session->set_userdata('msg_error', "Your reset password link has been expired.");
            redirect(base_url());
            exit;
        }

        if ($this->input->post('user_password') != '') {
            // generating the password by using hashing technique and applyng salt on it
            //crypt has method is used. 2y is crypt algorith selector
            //12 is workload factor on core processor.
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($this->input->post('user_password'), '$2y$12$' . $salt);

            $table_to_pass = 'mst_users';
            $fields_to_pass = array('user_id', 'first_name', 'last_name', 'user_name', 'user_email', 'user_password');
            $condition_to_pass = array("reset_password_code" => $this->input->post('activation_code'));
            $arr_password_data = $this->register_model->getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

            if (count($arr_password_data) > 0) {
                $table_name = 'mst_users';
                $update_data = array('user_password' => $hash_password, 'reset_password_code' => '','pwd' => $this->input->post('user_password'));
                if (isset($arr_password_data[0]['user_id']) && $arr_password_data[0]['user_id'] != 0) {
                    $condition_to_pass = array("user_id" => $arr_password_data[0]['user_id']);
                    $this->common_model->updateRow($table_name, $update_data, $condition_to_pass);
                    $this->session->set_userdata('msg_success', "Your password has been reset successfully. Please login.");
                }
                redirect(base_url());
                exit;
            } else {
                $this->session->set_userdata('msg_error', "Your reset password link has been expired.");
                redirect(base_url());
                exit;
            }
        }
        $data['header'] = array("title" => "Reset Your Password", "keywords" => "", "description" => "");
        $data['site_title'] = "Reset Password";
        $this->load->view('front/includes/header', $data);
        $this->load->view('front/forgot-password/new-password', $data);
        $this->load->view('front/includes/footer');
    }

    /* Random password generating for fb user */

    private function randamPass($num = 8) {
        $alpha_num = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $generated_str = '';
        for ($i = 0; $i < $num; $i++) {
            $generated_str .= substr($alpha_num, mt_rand(0, strlen($alpha_num) - 1), 1);
        }
        return $generated_str;
    }

}

/* End of file register.php */
    /* Location: ./application/controllers/register.php */ 
