<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
            exit();
        }
//        $this->session->unset_userdata("msg");
    }

    public function eventList() {
        /* checking admin is logged in or not */
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
        }
        /* using the email template model */
        $data = $this->common_model->commonFunction();
        $arr_privileges = array();
        $data['global'] = $this->common_model->getGlobalSettings();
        //checking for admin privilages
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('7', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage user!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if (count($this->input->post()) > 0) {
            if ($this->input->post('btn_delete_all') != "") {
                /* getting all ides selected */
                $arr_event_ids = $this->input->post('checkbox');
                if (count($arr_event_ids) > 0 && is_array($arr_event_ids)) {
                    if (count($arr_event_ids) > 0) {
                        /* deleting the property selected */
                        $this->common_model->deleteRows($arr_event_ids, "mst_events", "event_id");
                    }
                    $this->session->set_userdata("msg", "<span class='success'>Event deleted successfully!</span>");
                }
            }
        }
        $data['arr_event_data'] = $this->common_model->getRecords('mst_events', $fields = 'event_id,event_name,event_short_description,event_description,added_date,event_image', $condition = '', $order_by = 'event_id DESC', $limit = '', $debug = 0);
        $data["title"] = "Manage Events";
        $this->load->view('backend/events/list', $data);
    }

    public function eventEdit($event_id = '') {
        /* checking admin is logged in or not */
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
        }
        /* using the email template model */
        $data = $this->common_model->commonFunction();
        $data['global'] = $this->common_model->getGlobalSettings();
        //checking for admin privilages
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('7', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage news article!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="validationError">', '</p>');
        $this->form_validation->set_rules('event_name', 'name', 'required');
        $this->form_validation->set_rules('event_date_time', 'event date time', 'required');
        $this->form_validation->set_rules('registration_open_time', 'registration open time', 'required');
        $event_name = $this->input->post("event_name");
        if ($this->form_validation->run() == true && $event_name != "") {
            $data1['user_session'] = $this->session->userdata('user_account');
            $edit_id = $this->input->post("edit_id");
            if ($edit_id != "") {
                if ($_FILES['event_image']['name'][0] != '') {
                    $arr_file = $this->findExtension($_FILES['event_image']['name']);
                    $image_name = time() . '.' . $arr_file['ext'];
                    $upload_dir = './media/backend/img/event_image/';
                    $old_name = $upload_dir . $this->input->post('hidden_image');
                    unlink($old_name);
                    $config['upload_path'] = $upload_dir;
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|ico|bmp';
                    $config['max_width'] = '102400';
                    $config['max_height'] = '76800';
                    $config['file_name'] = $image_name;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('event_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_userdata('msg', $error['error']);
                        redirect(base_url() . 'backend/event-add');
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        $absolute_path = $this->common_model->absolutePath();
                        $image_path = $absolute_path . $upload_dir;
                        $image_main = $image_path . "/" . $image_name;
                        $thumbs_image = $image_path . "/thumbs/" . $image_name;
                        $thumbs_image1 = $image_path . "/recent-thumbs/" . $image_name;

                        $str_console = "convert " . $image_main . " -resize 795!X400! " . $thumbs_image;
                        exec($str_console);

                        $str_console1 = "convert " . $image_main . " -resize 100!X50! " . $thumbs_image1;
                        exec($str_console1);
                    }
                    $arr_post_data = array(
                        "event_name" => trim(stripslashes($this->input->post('event_name'))),
//                        'event_short_description' => stripslashes($this->input->post('event_short_description')),
                        'event_description' => trim(stripslashes($this->input->post('event_description'))),
                        'event_address' => trim(stripslashes($this->input->post('event_venue'))),
                        'parking_status' => (stripslashes($this->input->post('parking_status'))),
                        'event_date_time' => trim(stripslashes($this->input->post('event_date_time'))),
                        'registration_open_time' => trim(stripslashes($this->input->post('registration_open_time'))),
                        'event_image' => $image_name,
                        'posted_by' => $data1['user_session']['user_id'],
                    );
                    $table_name = "mst_events";
                    $arr_update_condition = array("event_id" => $edit_id);
                    $this->common_model->updateRow($table_name, $arr_post_data, $arr_update_condition);
                } else {
                    $arr_post_data = array(
                        "event_name" => trim(stripslashes($this->input->post('event_name'))),
                        'event_date_time' => (stripslashes($this->input->post('event_date_time'))),
                        'registration_open_time' => (stripslashes($this->input->post('registration_open_time'))),
                        'event_description' => trim(stripslashes($this->input->post('event_description'))),
                        'event_address' => trim(stripslashes($this->input->post('event_venue'))),
                        'parking_status' => (stripslashes($this->input->post('parking_status'))),
                        'posted_by' => $data1['user_session']['user_id'],
                    );
                    $table_name = "mst_events";
                    $arr_update_condition = array("event_id" => $edit_id);
                    $this->common_model->updateRow($table_name, $arr_post_data, $arr_update_condition);
                    $this->session->set_userdata("msg", "<span class='success'>Record updated successfully!</span>");
                    redirect(base_url() . "backend/event-list");
                }
            } else {
                // this is insert request
                if ($_FILES['event_image']['name'][0] != '') {
                    $arr_file = $this->findExtension($_FILES['event_image']['name']);
                    $image_name = str_replace(' ', '_', $arr_file['file_name'] . '-' . time() . '.' . $arr_file['ext']);
                    $upload_dir = './media/backend/img/event_image/';
                    $old_name = $upload_dir . $this->input->post('hidden_image');
                    unlink($old_name);
                    $config['upload_path'] = $upload_dir;
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|ico|bmp';
                    $config['max_width'] = '102400';
                    $config['max_height'] = '76800';
                    $config['file_name'] = $image_name;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('event_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_userdata('msg', $error['error']);
                        redirect(base_url() . 'backend/event-add');
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        $absolute_path = $this->common_model->absolutePath();
                        $image_path = $absolute_path . $upload_dir;
                        $image_main = $image_path . "/" . $image_name;
                        $thumbs_image = $image_path . "/thumbs/" . $image_name;
                        $thumbs_image1 = $image_path . "/recent-thumbs/" . $image_name;

                        $str_console1 = "convert " . $image_main . " -resize 795!X400! " . $thumbs_image;
                        exec($str_console1);

                        $str_console1 = "convert " . $image_main . " -resize 100!X50! " . $thumbs_image1;
                        exec($str_console1);
                    }
                }
                $arr_post_data = array(
                    "event_name" => trim(stripslashes($this->input->post('event_name'))),
                    'event_description' => trim(stripslashes($this->input->post('event_description'))),
                    'event_address' => trim(stripslashes($this->input->post('event_venue'))),
                    'parking_status' => (stripslashes($this->input->post('parking_status'))),
                    'event_date_time' => (stripslashes($this->input->post('event_date_time'))),
                    'registration_open_time' => (stripslashes($this->input->post('registration_open_time'))),
                    'event_image' => $image_name,
                    'posted_by' => $data1['user_session']['user_id'],
                    'added_date' => date("Y-m-d H:i:s"),
                );
                $table_name = "mst_events";
                $this->common_model->insertRow($arr_post_data, $table_name);
                $this->session->set_userdata("msg", "<span class='success'>Record added successfully!</span>");
            }
            redirect(base_url() . "backend/event-list");
        }
        if ($event_id == "") {
            if ($this->input->post("edit_id") == '') {
                $data["title"] = "Add Event ";
                $this->load->view('backend/events/add', $data);
            } else {
                $data["title"] = "Edit Event";
                $data["event_id"] = $event_id;
                $arr_event_data = $this->common_model->getRecords('mst_events', $fields = 'event_id,event_name,event_short_description,event_description,added_date,event_image,event_address,parking_status', array("event_id" => $this->input->post("edit_id")), $order_by = '', $limit = '', $debug = 0);
                $data["arr_event_data"] = $arr_event_data[0];
                $this->load->view('backend/events/edit', $data);
            }
        } else {
            $data["title"] = "Edit Event";
            $data["event_id"] = $event_id;
            $arr_event_data = $this->common_model->getRecords('mst_events', $fields = 'event_id,event_name,event_date_time,registration_open_time,event_description,added_date,event_image,event_address,parking_status', array("event_id" => $event_id), $order_by = '', $limit = '', $debug = 0);
            $data["arr_event_data"] = $arr_event_data[0];
            $this->load->view('backend/events/edit', $data);
        }
    }

    public function findExtension($filename) {
        $filename = strtolower($filename);
        $exts = explode(".", $filename);
        $file_name = '';
        for ($i = 0; $i <= count($exts) - 2; $i++) {
            $file_name .=$exts[$i];
        }
        $n = count($exts) - 1;
        $exts = $exts[$n];
        $arr_return = array(
            'file_name' => $file_name,
            'ext' => $exts
        );
        return $arr_return;
    }

}

/* End of file home.php */
/* Location: ./application/controllers/news_article.php */