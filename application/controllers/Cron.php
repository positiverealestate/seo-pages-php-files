<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ('soapclient/SforcePartnerClient.php');
require_once ('soapclient/SforceEnterpriseClient.php');

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('user_model');
    }

    public function updateSFUserData() {

        try {

            $mySforceConnection = new SforceEnterpriseClient();
            $mySforceConnection->createConnection("soapclient/enterprise.wsdl.xml");

            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (($this->session->userdata('enterpriseSessionId')) !== null) {
                $location = $this->session->userdata('enterpriseLocation');
                $sessionId = $this->session->userdata('enterpriseSessionId');

                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);
            } else {
                $ssas = $mySforceConnection->login(USERNAME, PASSWORD);

                $this->session->set_userdata('enterpriseLocation', $mySforceConnection->getLocation());
                $this->session->set_userdata('enterpriseSessionId', $mySforceConnection->getSessionId());
            }

            $query = "SELECT Id, FirstName, LastName, Phone, Email, Languages__c from Contact";
            $response = $mySforceConnection->query($query);

            foreach ($response->records as $record) {
                //echo $record->Id.": ".$record->FirstName." ".$record->LastName." ".$record->Phone." ".$record->Email." ".$record->Languages__c;
                $arrUserDetail = $this->common_model->getRecords("mst_users", "", array("salesforce_id" => $record->Id));
                if (is_array($arrUserDetail) && count($arrUserDetail)) {
                    $arr_to_update = array(
                        "first_name" => $record->FirstName,
                        "last_name" => $record->LastName,
                        "user_email" => $record->Email,
                        "user_status" => '1',
                        "email_verified" => '1'
                    );
                    if ($arrUserDetail[0]['user_id']) {
                        $this->common_model->updateRow("mst_users", $arr_to_update, array("user_id" => intval($arrUserDetail[0]['user_id'])));
                    }
                } else {
                    $randPassword = time() . rand();
                    $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
                    $password = crypt($randPassword, '$2y$12$' . $salt);
                    $arr_to_insert = array(
                        "salesforce_id" => $record->Id,
                        "first_name" => $record->FirstName,
                        "last_name" => $record->LastName,
                        "user_email" => ($record->Email) ? $record->Email : "",
                        "user_password" => $password,
                        "pwd" => $randPassword,
                        'user_type' => '1',
                        'user_status' => '1',
                        'email_verified' => '1',
                        'role_id' => '0',
                        'register_date' => date("Y-m-d H:i:s")
                    );
                    /* inserting user details into the database */
                    $last_insert_id = $this->common_model->insertRow($arr_to_insert, "mst_users");
                }
            }
        } catch (Exception $e) {
            echo "Exception " . $e->faultstring . "<br/><br/>\n";
            echo "Last Request:<br/><br/>\n";
            echo $mySforceConnection->getLastRequestHeaders();
            echo "<br/><br/>\n";
            echo $mySforceConnection->getLastRequest();
            echo "<br/><br/>\n";
            echo "Last Response:<br/><br/>\n";
            echo $mySforceConnection->getLastResponseHeaders();
            echo "<br/><br/>\n";
            echo $mySforceConnection->getLastResponse();
        }
    }

    public function updateCLPropertyData() {
        $this->load->model('property_model');

        $data['global'] = $this->common_model->getGlobalSettings();
        $client_id = $data['global']['client_id'];
        $client_secret = $data['global']['client_secret'];

        $get_all_properties = $this->property_model->getCronUpdatePropertyDetails($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);

        foreach ($get_all_properties as $property) {

            $url = "https://access-uat-api.corelogic.asia/access/oauth/token?client_id=" . $client_id . "&client_secret=" . $client_secret . "&grant_type=client_credentials";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $response = json_decode($response);
            $token = $response->access_token;
            curl_close($ch);
            $propertyId = $property['corelogic_property_id'];

            $url1 = "https://property-uat-api.corelogic.asia/bsg-au/v1/property/" . $propertyId . ".json?returnFields=address%2Cattributes%2CavmDetailList%2CcurrentOwnershipList%2CcontactList%2C%20developmentApplicationList%2CexternalReferenceList%2CfeatureList%2CforRentPropertyCampaignList%2CforSaleAgencyCampaignList%2CforSalePropertyCampaignList%2Clegal%2CparcelList%2CpropertyPhotoList%2CsaleList%2Csite%2Ctitle&access_token=" . $token;
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $url1);
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            $response1 = curl_exec($ch1);
            curl_close($ch);
            $response1 = json_decode($response1);

            $purchase_SQM = 0;
            $value_SQM = 0;
            if (count($response1->property->saleList) > 0) {
                //calculaing the purchse and current sqm value.
                if (strtoupper($property['property_type']) == 'LAND') {
                    //calculating purchase sqm
                    if ($property['purchase_price'] != 0 && $property['purchase_price'] != '') {
                        $purchase_SQM = ($property['purchase_price']) / ($property['land_size']);
                    }
                    if ($property['purchase_price'] != 0 && $property['purchase_price'] != '') {
                        $value_SQM = ($property['valuation']) / ($property['land_size']);
                    }

                    $valuation = $property['valuation'];
                    $land_growth_value = ((($value_SQM - $purchase_SQM)) / $property['land_size']);
                    $capital_growth = round(((($valuation - $property['purchase_price']) / $property['purchase_price']) * 100), 1);
                } else {
                    if ($property['purchase_price'] != 0 && $property['purchase_price'] != '') {
                        $purchase_SQM = ($property['purchase_price'] * 0.3) / ($property['land_size']);
                    }
                    if ($property['purchase_price'] != 0 && $property['purchase_price'] != '') {
                        $value_SQM = ($property['valuation'] * 0.3) / ($property['land_size']);
                    }
                    $valuation = $property['valuation'];
                    $land_growth_value = ((($value_SQM - $purchase_SQM)) / $property['land_size']);
                    $capital_growth = round(((($valuation - $property['purchase_price']) / $property['purchase_price']) * 100), 1);
                }
                if ($purchase_SQM != 0 && $value_SQM != 0) {
                    $table_name = 'mst_properties';
                    $update_data = array('value_SQM' => round($value_SQM), 'capital_growth' => $capital_growth);
                    $condition = array('corelogic_property_id' => $propertyId);
                    $this->common_model->updateRow($table_name, $update_data, $condition);
                    /* End :: Update Sqm value */

                    /* Start :: Update valuation from  value */
                    $table = 'trans_graph_details';
                    $insert_data = array(
                        'value' => $land_growth_value,
                        'property_id' => $property['property_id'],
                        'user_id' => $property['user_id'],
                        'type' => '2',
                        'date' => date('Y-m-d'),
                    );
                    $this->common_model->insertRow($insert_data, $table);
                    /* End :: Update Sqm value */
                }
            }
        }
        $this->session->set_userdata("msg", "<span class='success'>Records has been updated successfully!</span>");
        redirect(base_url() . "backend/home");
    }

    function updateDataWeekly() {
        $this->load->model('web_services_model');
        $property_details = $this->web_services_model->getCronRecords();
        foreach ($property_details as $property) {

            $property_id = $property['property_id'];
            $user_id = $property['user_id'];

            $income_details = end($this->common_model->getRecords('trans_incomes', $fields_to_pass = 'sum(weekly_rent) as total_weekly_rent,income_id', array('user_id' => intval($user_id), 'property_id' => intval($property_id)), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));

            /** Graph Value * */
            $current_date = date('Y-m-d');
            $start_date = strtotime($property['income_cron_updated']);

            $interval_date = strtotime("+7 day", $start_date);
            $updated_date = date('Y-m-d', $interval_date);

            if ($current_date <= $updated_date) {
                /** Graph Value * */
                /* Start :: Update valuation from  value */
                $table = 'trans_graph_details';
                $insert_data = array(
                    'value' => $income_details['total_weekly_rent'],
                    'property_id' => $property['property_id'],
                    'income_id_fk' => $income_details['income_id'],
                    'user_id' => $property['user_id'],
                    'type' => '4',
                    'date' => $updated_date
                );

                $this->common_model->insertRow($insert_data, $table);

                $table_name = 'mst_properties';
                $update_data = array(
                    'cron_updated_status' => '1',
                    'income_cron_updated' => $updated_date
                );
                $condition = array(
                    'property_id' => $property['property_id'],
                    'user_id' => $property['user_id'],
                );

                $this->common_model->updateRow($table_name, $update_data, $condition);

                /* End :: Update Sqm value */
            }
        }

        $this->session->set_userdata("msg", "<span class='success'>Records has been updated successfully!</span>");
        redirect(base_url() . "backend/home");
    }

}
