<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ('soapclient/SforcePartnerClient.php');
require_once ('soapclient/SforceHeaderOptions.php');

class Income extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->load->model('property_model');
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
            exit();
        }
    }

    /* Start :: Function to list all the Liability */

    public function listIncome($property_id) {
        $property_id = base64_decode($property_id);
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to manage income!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        if (count($this->input->post()) > 0) {
            if ($this->input->post('btn_delete_all') != "") {
                /* getting all ides selected */
                $arr_income_ids = $this->input->post('checkbox');
                if (count($arr_income_ids) > 0 && is_array($arr_income_ids)) {
                    foreach ($arr_income_ids as $income_id) {
                        /* getting user details */
                        $arr_income_detail = $this->common_model->getRecords("trans_incomes", "", array("income_id" => intval($income_id)));
                        if (count($arr_income_detail) > 0) {
                            /* Delete details from salesforce */
                            $mySforceConnection = new SforcePartnerClient();
                            $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                            // Simple example of session management - first call will do
                            // login, refresh will use session ID and location cached in
                            // PHP session
                            if (($this->session->userdata('partnerSessionId')) !== null) {
                                $location = $this->session->userdata('partnerLocation');
                                $sessionId = $this->session->userdata('partnerSessionId');
                                $mySforceConnection->setEndpoint($location);
                                $mySforceConnection->setSessionHeader($sessionId);
                            } else {
                                $mySforceConnection->login(USERNAME, PASSWORD);
                                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                            }
                            $salesforceId = $arr_income_detail[0]['salesforce_income_id'];
                            if ($salesforceId != null) {
                                $response = $mySforceConnection->delete($salesforceId);
                            }
                        }
                    }
                    if (count($arr_income_ids) > 0) {
                        /* deleting the property selected */
                        $this->common_model->deleteRows($arr_income_ids, "trans_incomes", "income_id");
                    }
                    $this->session->set_userdata("msg", "<span class='success'>Income deleted successfully!</span>");
                }
            }
        }
        /* using the property model */
        $data['title'] = "Manage Incomes";
        $data['property_id'] = $property_id;
        $data['arr_income_list'] = $this->property_model->getPropertyDetailsById($table_to_pass = '', $fields_to_pass, array('inc.property_id' => intval($property_id)), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0);
        $this->load->view('backend/income/list', $data);
    }
    /* Start :: Add Liability function */

    public function addIncome($property_id) {
        $property_id = base64_decode($property_id);
        /* Getting Common data */
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage income!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        $arr_property_detail = end($this->common_model->getRecords("mst_properties", 'property_id,valuation,user_id,salesforce_property_id', array("property_id" => intval($property_id))));
        $arr_user_detail = end($this->common_model->getRecords("mst_users", 'user_id,salesforce_user_id', array("user_id" => intval($arr_property_detail['user_id']))));
        if (count($this->input->post()) > 0) {
            $user_id = $arr_property_detail['user_id'];
            $salesforce_property_id = $arr_property_detail['salesforce_property_id'];
            $salesforce_user_id = $arr_user_detail['salesforce_user_id'];
            $weekly_rent = $this->input->post('weekly_rent');
            if ($user_id != "" && $property_id != '') {
            /* Start :: Insert property dept data */
                $arr_fields = array(
                    "property_id" => $property_id,
                    "user_id" => $user_id,
                    "weekly_rent" => $weekly_rent,
                    "added_date" => date("Y-m-d")
                );
                $last_income_id = $this->common_model->insertRow($arr_fields, 'trans_incomes');
                /* Start :: Salesforce Integration */
                $mySforceConnection = new SforcePartnerClient();
                $mySoapClient = $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
                // Simple example of session management - first call will do
                // login, refresh will use session ID and location cached in
                // PHP session
                if (($this->session->userdata('partnerSessionId')) !== null) {
                    $location = $this->session->userdata('partnerLocation');
                    $sessionId = $this->session->userdata('partnerSessionId');
                    $mySforceConnection->setEndpoint($location);
                    $mySforceConnection->setSessionHeader($sessionId);
                } else {
                    $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);
                    $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                    $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
                }
                /** Insert Income */
                $arr_income_fields = array(
                    'User_ID__c' => $salesforce_user_id,
                    'Property_ID__c' => $salesforce_property_id,
                    'Weekly_Rent__c' => $weekly_rent,
                );
                $sIncomeObject = new SObject();
                $sIncomeObject->fields = $arr_income_fields;
                $sIncomeObject->type = 'Income__c';
                $createIncomeResponse = $mySforceConnection->create(array($sIncomeObject));
                $salesforceIncomeId = $createIncomeResponse[0]->id;

                $table_name = 'trans_incomes';
                $update_data = array(
                    'salesforce_income_id' => $salesforceIncomeId
                );
                $condition = array(
                    'income_id' => $last_income_id
                );
                if ($last_income_id) {
                    $this->common_model->updateRow($table_name, $update_data, $condition);
                }
                $this->session->set_userdata("msg", "<span class='success'>Property added successfully!</span>");
                redirect(base_url() . "backend/income/list/" . base64_encode($property_id));
                exit();
            }
        }
        $arr_privileges = array();
        /* getting all privileges */
        $data['property_id'] = $property_id;
        $data['title'] = "Add Income";
        $this->load->view('backend/income/add', $data);
    }

    /* End :: Add Liability function */
    /* Start :: Edit Liability function */

    public function editIncome($edit_id = '') {
        $data = $this->common_model->commonFunction();
        /* checking user has privilige for the Manage Admin */
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('5', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to manage income!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        $weekly_rent = $this->input->post('weekly_rent');
        $income_id = intval($this->input->post('edit_id'));
        if ($weekly_rent != '') {
        /* Start :: Insert property dept data */
            $update_data = array(
                "weekly_rent" => $weekly_rent,
            );
            $condition = array("income_id" => $income_id);
            $this->common_model->updateRow('trans_incomes', $update_data, $condition);
            /* Start :: Salesforce Integration */
            $mySforceConnection = new SforcePartnerClient();
            $mySoapClient = $mySforceConnection->createConnection("soapclient/partner.wsdl.xml");
            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (($this->session->userdata('partnerSessionId')) !== null) {
                $location = $this->session->userdata('partnerLocation');
                $sessionId = $this->session->userdata('partnerSessionId');
                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);
            } else {
                $mylogin = $mySforceConnection->login(USERNAME, PASSWORD);
                $this->session->set_userdata('partnerLocation', $mySforceConnection->getLocation());
                $this->session->set_userdata('partnerSessionId', $mySforceConnection->getSessionId());
            }
            /** Update Income */
            $arr_property_income = $this->common_model->getRecords('trans_incomes', '', array("income_id" => $edit_id, "user_id" => $user_id));
            $arr_property_income = end($arr_property_income);
            $arr_income_fields = array(
                'Weekly_Rent__c' => $weekly_rent,
            );
            $sIncomeObject = new SObject();
            $sIncomeObject->fields = $arr_income_fields;
            $sIncomeObject->Id = $arr_property_income['salesforce_income_id'];
            $sIncomeObject->type = 'Income__c';
            $createIncomeResponse = $mySforceConnection->update(array($sIncomeObject));
            /* End :: Salesforce Integration */
            $this->session->set_userdata("msg", "<span class='success'>Property updated successfully!</span>");
            redirect(base_url() . "backend/income/list");
            exit();
        }

        $arr_privileges = array();
        /* getting all privileges */
        $data['arr_privileges'] = $this->common_model->getRecords('mst_privileges');
        /* getting user details from $edit_id from function parameter */
        $data['arr_income'] = end($this->common_model->getRecords('trans_incomes', $fields_to_pass = 'weekly_rent,income_id,property_id', array('income_id' => intval(base64_decode($edit_id))), $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0));
        $data['title'] = "Edit Income";
        $data['income_id'] = $edit_id;
        $data['property_id'] = $data['arr_income']['property_id'];
        $this->load->view('backend/income/edit', $data);
    }
    /* End :: Edit Liability function */
}
