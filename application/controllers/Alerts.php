<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Alerts extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
            exit();
        }
    }

    public function alertList() {
        /*         * checking admin is logged in or not ** */
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
        }
        /*         * using the email template model ** */

        $data = $this->common_model->commonFunction();
        $arr_privileges = array();
        $data['global'] = $this->common_model->getGlobalSettings();

        //checking for admin privilages
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('17', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage alerts!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        
         if (count($this->input->post()) > 0) {
            if ($this->input->post('btn_delete_all') != "") {
                /* getting all ides selected */
                $arr_alert_ids = $this->input->post('checkbox');
                if (count($arr_alert_ids) > 0 && is_array($arr_alert_ids)) {

                    if (count($arr_alert_ids) > 0) {
                        /* deleting the property selected */
                        $this->common_model->deleteRows($arr_alert_ids, "mst_alerts", "alert_id");
                    }
                    $this->session->set_userdata("msg", "<span class='success'>Alerts deleted successfully!</span>");
                }
            }
        }
        $data['arr_alert_data'] = $this->common_model->getRecords('mst_alerts', $fields = 'alert_id,alert_name,alert_short_description,added_date', $condition = '', $order_by = 'alert_id DESC', $limit = '', $debug = 0);
        
        $data["title"] = "Manage Alerts";
        $this->load->view('backend/alerts/list', $data);
    }

    public function alertEdit($alert_id = '') {
        
        /*         * checking admin is logged in or not ** */
        if (!$this->common_model->isLoggedIn()) {
            redirect(base_url() . "backend/login");
        }
        /*         * using the email template model ** */       
        $data = $this->common_model->commonFunction();
        $data['global'] = $this->common_model->getGlobalSettings();
        //checking for admin privilages
        if ($data['user_account']['role_id'] != 1) {
            $arr_privileges = $this->common_model->getRecords('trans_role_privileges', 'privilege_id', array("role_id" => $data['user_account']['role_id']));
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege['privilege_id'];
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('17', $arr_login_admin_privileges) == FALSE) {
                /* an admin which is not super admin not privileges to access Manage Role
                 * setting session for displaying notiication message. */
                $this->session->set_userdata("permission_msg", "<span class='error'>You doesn't have priviliges to  manage news article!</span>");
                redirect(base_url() . "backend/home");
                exit();
            }
        }
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="validationError">', '</p>');
        $this->form_validation->set_rules('alert_name', 'name', 'required');
        $this->form_validation->set_rules('alert_short_description', 'short description', 'required');
        $alert_name = $this->input->post("alert_name");
        
        if ($this->form_validation->run() == true && $alert_name != "") {
           $data1['user_session'] = $this->session->userdata('user_account');        
            $edit_id = $this->input->post("edit_id");
            if ($edit_id != "") {
                $arr_post_data = array(
                    "alert_name" => trim(stripslashes($this->input->post('alert_name'))),
                    'alert_short_description' => trim(stripslashes($this->input->post('alert_short_description'))),
                    'posted_by'=>$data1['user_session']['user_id'],
                );
                $table_name = "mst_alerts";
                $arr_update_condition = array("alert_id" => $edit_id);
                $this->common_model->updateRow($table_name, $arr_post_data, $arr_update_condition);
                $this->session->set_userdata("msg", "<span class='success'>Record updated successfully!</span>");
             
                redirect(base_url()."backend/alert-list");
                
            } else {
                $arr_post_data = array(
                    "alert_name" => trim(stripslashes($this->input->post('alert_name'))),
                    'alert_short_description' => trim(stripslashes($this->input->post('alert_short_description'))),
                    'posted_by'=>$data1['user_session']['user_id'],
                    'added_date' => date("Y-m-d H:i:s"),
                );
                
                $table_name = "mst_alerts";
                $new_post_id = $this->common_model->insertRow($arr_post_data, $table_name);
                $this->session->set_userdata("msg", "<span class='success'>Record added successfully!</span>");
            }
            
            redirect(base_url() . "backend/alert-list");
        }
        if ($alert_id == "") {
            if ($this->input->post("edit_id") == '') {
                $data["title"] = "Add Alerts";
                $this->load->view('backend/alerts/add', $data);
            } else {
                $data["title"] = "Edit Alerts";
                
                $data["alert_id"] = $alert_id;
                $arr_alert_data = $this->common_model->getRecords('mst_news_article', $fields = 'alert_id,alert_name,alert_short_description,added_date', array("alert_id" => $this->input->post("edit_id")), $order_by = '', $limit = '', $debug = 0);
                $data["arr_alert_data"] = $arr_alert_data[0];
                $this->load->view('backend/alerts/edit', $data);
            }
        } else {
            $data["title"] = "Edit Alerts";
            $data["alert_id"] = $alert_id;
            $arr_alert_data = $this->common_model->getRecords('mst_alerts', $fields = 'alert_id,alert_name,alert_short_description,added_date', array("alert_id" => $alert_id), $order_by = '', $limit = '', $debug = 0);
            $data["arr_alert_data"] = $arr_alert_data[0];
            $this->load->view('backend/alerts/edit', $data);
        }
    }

    function findExtension($filename) {
        $filename = strtolower($filename);
        $exts = explode(".", $filename);
        $file_name = '';
        for ($i = 0; $i <= count($exts) - 2; $i++) {
            $file_name .=$exts[$i];
        }
        $n = count($exts) - 1;
        $exts = $exts[$n];
        $arr_return = array(
            'file_name' => $file_name,
            'ext' => $exts
        );
        return $arr_return;
    }

}

/* End of file home.php */
/* Location: ./application/controllers/news_article.php */