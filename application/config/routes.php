<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

/* Start ::  admin login, dashboard and forgot password */
$route['default_controller'] = "home/index";
$route['admin'] = "admin/index";
$route['backend/change-languageversion-for-functionality'] = "admin/change_language_for_functionality";
$route['backend'] = "admin/index";
$route['backend/login'] = "admin/index";
$route['backend/index'] = "admin/index";
$route['backend/home'] = "admin/home";
$route['backend/dashboard'] = "admin/home";
$route['backend/log-out'] = "admin/logout";
$route['backend/forgot-password'] = "admin/forgotPassword";
$route['backend/reset-password/(:any)'] = "admin/resetPassword/$1";
$route['backend/forgot-password-email'] = "admin/checkForgotPasswordEmail";
/* End :: admin login, dashboard and forgot password */

/* Start :: Global Settings:   */
$route['backend/global-settings/edit/(:any)/(:any)'] = "global_setting/editGlobalSettings/$1/$2";
$route['backend/global-settings/list']="global_setting/listGlobalSettings";
$route['backend/global-settings/edit-parameter-language/(:any)'] = "global_setting/editParameterLanguage/$1";
$route['backend/global-settings/get-global-parameter-language'] = "global_setting/getGlobalParameterLanguage";
/* End :: Global Settings */

/* Start :: Manage Role */
$route['backend/role/list']="role/listRole";
$route['backend/role/edit/(:any)']="role/addRole/$1";
$route['backend/role/add']="role/addRole";
$route['backend/role/check-role']="role/checkRole";
/* End :: Manage Role */

/* Start :: Manage Admin  */
$route['backend/admin/list']="admin/listAdmin";
$route['backend/admin/change-status']="admin/changeStatus";
$route['backend/admin/add']="admin/addAdmin";
$route['backend/admin/check-admin-username']="admin/checkAdminUsername";
$route['backend/admin/check-admin-email']="admin/checkAdminEmail";
$route['backend/admin/account-activate/(:any)']="admin/activateAccount/$1";
$route['backend/admin/edit/(:any)']="admin/editAdmin/$1";
$route['backend/admin/profile']="admin/adminProfile";
$route['backend/admin/edit-profile']="admin/editProfile";
$route['backend/country-change-language/(:any)'] = "countries/changeLanguage/$1";
$route['backend/country/country-name']="countries/getAllCountryNames";
/* End :: Manage Admin */

/* Start :: Manage Users  */
$route['backend/user/list']="user/listUser";
$route['backend/user/edit/(:any)']="user/editUser/$1";
$route['backend/user/view/(:any)']="user/userProfile/$1";
$route['backend/user/add']="user/addUser";
$route['backend/user/change-custodian-status']="user/changeCustodianStatus";

$route["backend/uploadUserUsingExcel"] = "user/uploadUserUsingExcel";
$route['backend/download-excel']="user/downloadExcel";
/* End :: Manage Users */

/* Start :: Manage Property  */
$route['backend/property/list']="property/listProperty";
$route['backend/property/add']="property/addProperty";
$route['backend/property/edit/(:any)']="property/editProperty/$1";
$route['backend/delete-property-image/(:any)/(:any)']="property/deleteBackendPropertyImages/$1/$2";
$route['backend/property/chk-name-exist'] = "property/chkPropertyNameExist";
$route['backend/property/chk-edit-name-exist'] = "property/chkEditPropertyNameExist";

$route['backend/property/change-promoted-status']="property/changePromotedStatus";
$route['get-user-lat-lng'] = "property/getUserCityLatLng";
$route['get-user-geo-location'] = "property/getUserGeoLocation";

/* Start :: Corelogic Function Routes */
$route['corelogic-access-locality'] = "corelogic/accessLocality";
$route['corelogic-get-property-details'] = "corelogic/getPropertyDetails";
/* End :: Manage Property */

/* Start :: Manage Property Liabilities */
$route['backend/liability/list/(:any)'] = "liability/listLiability/$1";
$route['backend/liability/add/(:any)'] = "liability/addLiability/$1";
$route['backend/liability/edit/(:any)'] = "liability/editLiability/$1";
$route['backend/liability/view/(:any)'] = "liability/viewLiability/$1";
/* End :: Manage Property Liabilities */

/* Start :: Manage Property Liabilities */
$route['backend/income/list/(:any)'] = "income/listIncome/$1";
$route['backend/income/add/(:any)'] = "income/addIncome/$1";
$route['backend/income/edit/(:any)'] = "income/editIncome/$1";
$route['backend/income/view/(:any)'] = "income/viewIncome/$1";
/* End :: Manage Property Liabilities */

/* Start :: Manage email template routes */
$route['backend/email-template/list']="email_template/index";
$route['backend/edit-email-template/(:any)']="email_template/editEmailTemplate/$1";
/* End :: Manage email template routes end */

/* Start :: Manage Cms */
$route['backend/cms']="cms/listCMS";
$route['backend/cms/edit-cms/(:any)']="cms/editCMS/$1";
$route['backend/cms/edit-cms-language/(:any)'] = "cms/editCmsLanguage/$1";
$route['backend/cms/get-cms-language'] = "cms/getCmsLanguage";
/* Common  Editor validation while uploading image file  route  here : */
$route['upload-image'] = "cms/uploadImage";
/* End :: Manage Cms */

/* Start :: Contact Us */
$route['backend/contact-us'] = "contact_us/listContactUs";
$route['backend/contact-us/view/(:any)'] = "contact_us/view/$1";
$route['backend/contact-us/reply/(:any)'] = "contact_us/reply/$1";
/* End :: Contact Us */

/* Start :: Manage Blogs */
$route['backend/blog'] = "blog/manage_blog_posts";
$route['backend/blog/add-post'] = "blog/edit_post";
$route['backend/blog/edit-post/(:any)'] = "blog/edit_post/$1";
$route['backend/blog/delete-post'] = "blog/delete_post";
$route['backend/blog/validate-page-url']="blog/validatePageUrl"; 
/* End :: Manage Blogs */

/* Start :: Manage News Article at backend start here  */
$route['backend/news-article-list'] = "News_article/newsArticleList";
$route['backend/news-article-add'] = "News_article/newsArticleEdit";
$route['backend/news-article-add/(:any)'] = "News_article/newsArticleEdit/$1";
/* End :: Manage News Article at backend  end here  */

/* Start :: Manage Events at backend start here  */
$route['backend/event-list'] = "events/eventList";
$route['backend/event-add'] = "events/eventEdit";
$route['backend/event-add/(:any)'] = "events/eventEdit/$1";
/* End :: Manage Events at backend  end here  */

/* Start :: Manage Alerts at backend start here  */
$route['backend/alert-list'] = "alerts/alertList";
$route['backend/alert-add'] = "alerts/alertEdit";
$route['backend/alert-add/(:any)'] = "alerts/alertEdit/$1";
/* End ::  Manage Alerts at backend  end here  */

/* Home Page Section :   */
$route['backend/seo-page-banner/list']="banner/listHomePageSettings";
$route['backend/seo-page-banner/change-status']="banner/changeStatus";
$route['backend/seo-page-banner/edit/(:any)'] = "banner/editSeoPageBanner/$1";
$route['backend/seo-page-banner/view/(:any)'] = "banner/ViewSeoPageBanner/$1";
$route['backend/seo-page-banner/check-section-name'] = "banner/checkBannerName";

/* Home Page Section End Here */ 

/* Start :: FrontEnd Routes Start */
$route['cms/(:any)'] = "cms/cmsPage/$1";
$route['contact-us'] = "contact_us/index";
$route['reset-password/(:any)']="register/resetPassword/$1";
$route['reset-password']="register/resetPassword";
$route['get-all-state']="home/getAllState";
$route['get-particular-state-suburbs']="home/getParticularStateSuburbs";
$route['get-seo-page-data']="home/getSeoPageData";
$route['get-APM-api-data']="home/getAPMData";
$route['get-user-lat-lng'] = "home/getUserCityLatLng";
$route['get-suburb-state-zipcode'] = "home/getSuburbStatesZipcode";
$route['seo-page-cron'] = "home/seoPageDataCron";
$route['generate-apn-cache-cron']="home/generateAPNCache";
$route['search-state-data'] = "home/searchStateData";
/* End :: FrontEnd Routes Start */

/* Start :: Webservices routes start */
$route['ws-pre-login'] = "web_services/wsAppLogin";
$route['ws-forgot-password'] = "web_services/wsForgotPassword";
$route['ws-cms/(:any)'] = "web_services/wsCms/$1";
$route['ws-client-contact-us'] = "web_services/wsClientContactUs";
$route['ws-pre-property-list-by-type'] = "web_services/wsGetPrePropertyListByType";
$route['ws-pre-add-property'] = "web_services/wsAddPreProperty";
$route['ws-pre-edit-property'] = "web_services/wsEditPreProperty";
$route['ws-pre-property-edit-details-by-id'] = "web_services/wsGetPrePropertyEditDetailById";
$route['ws-pre-delete-property'] = "web_services/wsDeletePreProperty";
$route['ws-pre-delete-property/(:any)'] = "web_services/wsDeletePreProperty/$1";
$route['ws-pre-get-loan-rent-equity-review-list'] = "web_services/wsPreGetLoanRentEquityReviewList";
$route['ws-pre-loan-rent-review'] = "web_services/wsPreGetLoanRentEquityReview";
$route['ws-pre-goals'] = "web_services/wsPreGoals";
$route['ws-pre-goals-add'] = "web_services/wsPreGoalsAdd";
$route['ws-pre-goals-edit'] = "web_services/wsPreGoalsEdit";
$route['ws-pre-goals-delete'] = "web_services/wsPreDeleteGoal";
$route['ws-pre-get-goals-value-for-edit'] = "web_services/wsPreGetGoalValueForEdit";
$route['ws-pre-get-goals-value-for-current-status'] = "web_services/wsPreGetGoalValueCurrentStatus";
$route['ws-pre-get-all-property-wealth-details'] = "web_services/wsPreGetPreAllPropertyWealthDetails";
$route['ws-pre-news-event-list'] = "web_services/wsPreNewsEventList";
$route['ws-pre-news-event-details-by-id'] = "web_services/wsPreGetNewsEventsDetailsById";
$route['ws-pre-all-property-income-details'] = "web_services/getAllPropertyIncomeDetails";
$route['ws-pre-mortage-calculator'] = "web_services/getMortageCalculator";
$route['ws-pre-depreciation-calculator'] = "web_services/getDepreciationCalculator";
$route['ws-pre-tax-calculator'] = "web_services/getTaxCalculator";
$route['ws-pre-get-current-date'] = "web_services/getCurrentDate";


// new_asset_PFS 

$route['ws-pre-new-asset-pfs'] = "web_services/getPreNewAssetPFS";
$route['ws-pre-person-edit-pfs'] = "web_services/wsPrePersonAddEdit";
$route['ws-pre-SMSF-insurance-edit-pfs'] = "web_services/wsPreSMSFInsuranceAddEdit";
$route['ws-pre-income-dependants-edit-pfs'] = "web_services/wsPreIncomeDependantAddEdit";
$route['ws-pre-liabilities-edit-pfs'] = "web_services/wsPreLiabilitiesAddEdit";
$route['ws-pre-dashboard-details'] = "web_services/wsGetDashboardDetails";
/* End :: Webservices routes start */

$route['get-call-to-action-urls'] = 'home/CTAUrls';

/* Start :: Database Error Page */
$route['page-not-found']="cms/databaseError";

/* End :: Database Error Page */

$route['(:any)']="home/seoPage/$1/";
$route['(:any)/(:any)']="home/seoPage/$1/$2";
$route['(:any)/(:any)/(:any)']="home/seoPage/$1/$2/$3";
//$route['(:any)/(:any)/(:any)']="home/seoPage/$1/$2/$3";

