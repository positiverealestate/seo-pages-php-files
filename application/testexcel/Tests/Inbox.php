<?php
include ("AbstractDB.php");

class Inbox extends AbstractDB 
{
	protected	$result;	
	private	$query;
	protected $result_U;
	protected $row_U;

	public function __construct() 
	{
		parent::__construct();
		$this->result = NULL;
		$this->table="customers";
		return true;
	}
	
    public function numofrows()
	{
		$num=mysqli_num_rows($this->result);
		return $num;
	}
	
	public function getRow()
      { 
	     while($this->row=mysqli_fetch_assoc($this->result))
		   {
		     return true;
		   }
		   
		 return false;   
	  }
	  public function getAllProductData()
	  {
	     $this->result=$this->query("select * from u_supply_product_details");
	  }
	  
	  public function inserData($arr_ins){
	 $this->query("truncate table u_supply_product_details_bk");
	 $validInsert=true;
	  $insertQuery='';
	   for ($i =2; $i <= $arr_ins[0]['numRows']; $i++) {
       $insertQuery ="insert into u_supply_product_details_bk values(";
	    $failInsert=0;
	    for ($j = 1; $j <= $arr_ins[0]['numCols']; $j++) {
		
		if($j>1)
		{
			$insertQuery.=",";
		}
		
		$insertQuery.="'".str_replace("'","&rsquo;",$arr_ins[0]['cells'][$i][$j])."'";
			
			if($arr_ins[0]['cells'][$i][$j]=="")
			{
				$failInsert++;
			}
			
		}
		
		$insertQuery.=");";
		if($failInsert>=$arr_ins[0]['numCols'])
		{
			
		}
		else
		{
			if(!$this->query($insertQuery))
			{
				$validInsert=false;
			}
		}
		
		}
		
		if($validInsert)
		{
			$this->query("truncate table u_supply_product_details");
			if(!$this->query("Insert into u_supply_product_details select * from u_supply_product_details_bk"))
                        {
                           echo mysqli_error($this->mysql);
                        }
		}
		return $validInsert;
		//
	  }
	  public function getAllCategoryData()
	  {  
	     $this->result=$this->query("select * from cb_wine_types");
	  }
	  public function inserCategoryData($arr_ins){
	 $this->query("truncate table u_supply_category_details_bk");
	 $validInsert=true;
	  $insertQuery='';
	   for ($i =2; $i <= $arr_ins[0]['numRows']; $i++) {
       $insertQuery ="insert into u_supply_category_details_bk values(";
	    $failInsert=0;
	    for ($j = 1; $j <= $arr_ins[0]['numCols']; $j++) {
		
		if($j>1)
		{
			$insertQuery.=",";
		}
		
		$insertQuery.="'".str_replace("'","&rsquo;",$arr_ins[0]['cells'][$i][$j])."'";
			
			if($arr_ins[0]['cells'][$i][$j]=="")
			{
				$failInsert++;
			}
			
		}
		
		$insertQuery.=");";
		if($failInsert>=$arr_ins[0]['numCols'])
		{
			
		}
		else
		{
			if(!$this->query($insertQuery))
			{
				$validInsert=false;
			}
		}
		
		}
		
		if($validInsert)
		{
			$this->query("truncate table u_supply_category_details");
			$this->query("Insert into u_supply_category_details select * from u_supply_category_details_bk");
		}
		return $validInsert;
		//
	  }
	  public function getAllSubcategoryData()
	  {
	     $this->result=$this->query("select * from u_supply_subcategory_details");
	  }
	  public function getAllSubsubcategoryData()
	  { 
	     $this->result=$this->query("select * from u_supply_sub_subcat1");
	  }
	  public function getAllSubsubcategory1Data()
	  {
	     $this->result=$this->query("select * from u_supply_sub_subcat2");
	  }
	  public function inserSubcategoryData($arr_ins){
	 $this->query("truncate table u_supply_subcategory_details_bk");
	 $validInsert=true;
	  $insertQuery='';
	   for ($i =2; $i <= $arr_ins[0]['numRows']; $i++) {
       $insertQuery ="insert into u_supply_subcategory_details_bk values(";
	    $failInsert=0;
	    for ($j = 1; $j <= $arr_ins[0]['numCols']; $j++) {
		
		if($j>1)
		{
			$insertQuery.=",";
		}
		
		$insertQuery.="'".str_replace("'","&rsquo;",$arr_ins[0]['cells'][$i][$j])."'";
			
			if($arr_ins[0]['cells'][$i][$j]=="")
			{
				$failInsert++;
			}
			
		}
		
		$insertQuery.=");";
		if($failInsert>=$arr_ins[0]['numCols'])
		{
			
		}
		else
		{
			if(!$this->query($insertQuery))
			{
				$validInsert=false;
			}
		}
		
		}
		
		if($validInsert)
		{
			$this->query("truncate table u_supply_subcategory_details");
			$this->query("Insert into u_supply_subcategory_details select * from u_supply_subcategory_details_bk");
		}
		return $validInsert;
		//
	  }
	  public function inserSubsubcategoryData($arr_ins){
	 $this->query("truncate table u_supply_sub_subcat1_bk");
	 $validInsert=true;
	  $insertQuery='';
	   for ($i =2; $i <= $arr_ins[0]['numRows']; $i++) {
       $insertQuery ="insert into u_supply_sub_subcat1_bk values(";
	    $failInsert=0;
	    for ($j = 1; $j <= $arr_ins[0]['numCols']; $j++) {
		
		if($j>1)
		{
			$insertQuery.=",";
		}
		
		$insertQuery.="'".str_replace("'","&rsquo;",$arr_ins[0]['cells'][$i][$j])."'";
			
			if($arr_ins[0]['cells'][$i][$j]=="")
			{
				$failInsert++;
			}
			
		}
		$insertQuery.=");";
		if($failInsert>=$arr_ins[0]['numCols'])
		{
			
		}
		else
		{
			if(!$this->query($insertQuery))
			{
				$validInsert=false;
			}
		}
		
		}
		
		if($validInsert)
		{
			$this->query("truncate table u_supply_sub_subcat1");
			$this->query("Insert into u_supply_sub_subcat1 select * from u_supply_sub_subcat1_bk");
		}
		return $validInsert;
		//
	  }
	  public function inserSubsubcategory1Data($arr_ins){
	 $this->query("truncate table u_supply_sub_subcat2_bk");
	 $validInsert=true;
	  $insertQuery='';
	   for ($i =2; $i <= $arr_ins[0]['numRows']; $i++) {
       $insertQuery ="insert into u_supply_sub_subcat2_bk values(";
	    $failInsert=0;
	    for ($j = 1; $j <= $arr_ins[0]['numCols']; $j++) {
		
		if($j>1)
		{
			$insertQuery.=",";
		}
		
		$insertQuery.="'".str_replace("'","&rsquo;",$arr_ins[0]['cells'][$i][$j])."'";
			
			if($arr_ins[0]['cells'][$i][$j]=="")
			{
				$failInsert++;
			}
			
		}
		
		$insertQuery.=");";
		if($failInsert>=$arr_ins[0]['numCols'])
		{
			
		}
		else
		{
			if(!$this->query($insertQuery))
			{
				$validInsert=false;
			}
		}
		
		}
		
		if($validInsert)
		{
			$this->query("truncate table u_supply_sub_subcat2");
			$this->query("Insert into u_supply_sub_subcat2 select * from u_supply_sub_subcat2_bk");
		}
		return $validInsert;
		//
	  }
function backup()
{
	return $this->result=$this->query("call getAllProdRecs()");
	
}	   
	  	  	    
			
}//end class	
?>