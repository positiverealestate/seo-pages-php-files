<?php  

require_once dirname(__FILE__). '/AbstractDB.php'; 

class Inbox extends AbstractDB 
{
	protected	$result;	
	private	$query;
	protected $result_U;
	protected $row_U;

	public function __construct() 
	{
		parent::__construct();
		$this->result = NULL;
		$this->table="customers";
		return true;
	}
	
    public function numofrows()
	{
		$num=mysqli_num_rows($this->result);
		return $num;
	}
	
	public function getRow()
      { 
	     while($this->row=mysqli_fetch_assoc($this->result))
		   {
		     return true;
		   }
		   
		 return false;   
	  }
	  public function getAllProductData()
	  {
	     $this->result=$this->query("select * from u_supply_product_details ");
	  }
	  
	  public function inserData($arr_ins){
	 $this->query("truncate table u_supply_product_details_bk");
	 $validInsert=true;
	  $insertQuery='';
	   for ($i =2; $i <= $arr_ins[0]['numRows']; $i++) {
       $insertQuery ="insert into u_supply_product_details_bk values(";
	    $failInsert=0;
	    for ($j = 1; $j <= $arr_ins[0]['numCols']; $j++) {
		
		if($j>1)
		{
			$insertQuery.=",";
		}
		
		$insertQuery.="'".str_replace("'","&rsquo;",$arr_ins[0]['cells'][$i][$j])."'";
			
			if($arr_ins[0]['cells'][$i][$j]=="")
			{
				$failInsert++;
			}
			
		}
		
		$insertQuery.=");";
		if($failInsert>=$arr_ins[0]['numCols'])
		{
			
		}
		else
		{
			if(!$this->query($insertQuery))
			{
				$validInsert=false;
			}
		}
		
		}
		
		if($validInsert)
		{
			$this->query("truncate table u_supply_product_details");
			$this->query("Insert into u_supply_product_details select * from u_supply_product_details_bk");
		}
		return $validInsert;
		//
	  }

			
}//end class	
?>