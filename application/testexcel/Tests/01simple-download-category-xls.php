<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2010 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2010 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.4, 2010-08-26
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../Classes/PHPExcel.php';

function replQuote($str)
{
return str_replace("&rsquo;","'",$str);
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

include('Inbox.php');
$inbox=new Inbox();
$inbox->getAllCategoryData();
$allData=$inbox->getResultArr();

// Set properties
$objPHPExcel->getProperties()->setCreator("Panacea Infotech Private Limited")
							 ->setLastModifiedBy("Panacea Infotech Private Limited")
							 ->setTitle("Backup Data")
							 ->setSubject("Backup Data")
							 ->setDescription("Backup of Wine Types")
							 ->setKeywords("Beer Types")
							 ->setCategory("Backups");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'wine_type_id')
            ->setCellValue('B1', 'wine_type_title');
           /* ->setCellValue('C1', 'title')
            ->setCellValue('D1', 'description')
			->setCellValue('E1', 'image_path')
			->setCellValue('F1', 'created_on')
			->setCellValue('G1', 'update_on')
			->setCellValue('H1', 'view');*/



// Miscellaneous glyphs, UTF-8
$i=1;
foreach($allData as $ad)
{
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.($i+1), $ad['wine_type_id'])
			->setCellValue('B'.($i+1), replQuote($ad['wine_type_title']));
			/*->setCellValue('C'.($i+1), replQuote($ad['title']))
			->setCellValue('D'.($i+1), replQuote($ad['description']))
			->setCellValue('E'.($i+1), replQuote($ad['image_path']))
			->setCellValue('F'.($i+1), replQuote($ad['created_on']))
			->setCellValue('G'.($i+1), replQuote($ad['update_on']))
			->setCellValue('H'.($i+1), replQuote($ad['view']));*/


			
						
$i++;
}




//$objPHPExcel->setActiveSheetIndex(0)
           // ->setCellValue('A4', 'Miscellaneous glyphs');
            

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Backup as on - '.date("jS M Y"));


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client�s web browser (Excel5)
/*header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Backup_Products.xls"');
header('Cache-Control: max-age=0');*/

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter->save('php://output');
$objWriter->save('Backup_categories.xls');

?>
<a href="Backup_categories.xls" target="_blank">Download File</a><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
