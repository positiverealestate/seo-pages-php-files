#!/usr/bin/php
<?php
ob_start();
error_reporting(E_ALL);
set_time_limit(0);
ini_set("memory_limit","10000M");
date_default_timezone_set('Europe/London');
include("/home/uneek0/public_html/testexcel/Tests/Inbox.php");
include ("/home/uneek0/public_html/testexcel/Classes/PHPExcel.php");

function replQuote($str)
{
return str_replace("&rsquo;","'",$str);
}

$objPHPExcel = new PHPExcel();


$inbox=new Inbox();
$inbox->getAllProductData();
mail('mayur@panaceatek.com','test','success');
$allData=$inbox->getResultArr();

$objPHPExcel->getProperties()->setCreator("Panacea Infotech Private Limited")
							 ->setLastModifiedBy("Panacea Infotech Private Limited")
							 ->setTitle("Backup Data")
							 ->setSubject("Backup Data")
							 ->setDescription("Backup of products data")
							 ->setKeywords("Products data")
							 ->setCategory("Backups");

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'id')
            ->setCellValue('B1', 'Product')
            ->setCellValue('C1', 'SKU')
            ->setCellValue('D1', 'UPC')
			->setCellValue('E1', 'Oem')
			->setCellValue('F1', 'Supplier')
			->setCellValue('G1', 'Price')
			->setCellValue('H1', 'Cost')
			->setCellValue('I1', 'Taxable')
			->setCellValue('J1', 'Quantity')
			->setCellValue('K1', 'Brief')
			->setCellValue('L1', 'Description')
			->setCellValue('M1', 'Thumbnail')
			->setCellValue('N1', 'Photo')
			->setCellValue('O1', 'NaturalSearchKeywords')
			->setCellValue('P1', 'NaturalSearchDescription')
			->setCellValue('Q1', 'Category')
			->setCellValue('R1', 'Keywords')
			->setCellValue('S1', 'DomesticShippingType')
			->setCellValue('T1', 'DomesticShippingService1')
			->setCellValue('U1', 'DomesticShippingCost1')
			->setCellValue('V1', 'DomesticShippingAdditionalCost1')
			->setCellValue('W1', 'DomesticShippingService2')
			->setCellValue('X1', 'DomesticShippingCost2')
			->setCellValue('Y1', 'DomesticShippingAdditionalCost2')
			->setCellValue('Z1', 'DomesticShippingService3')
			->setCellValue('AA1', 'DomesticShippingCost3')
			->setCellValue('AB1', 'DomesticShippingAdditionalCost3')
			->setCellValue('AC1', 'DomesticShippingServiceCanada1')
			->setCellValue('AD1', 'DomesticShippingCostCanada1')
			->setCellValue('AE1', 'DomesticShippingAdditionalCostCanada1')
			->setCellValue('AF1', 'DomesticShippingServiceCanada2')
			->setCellValue('AG1', 'DomesticShippingCostCanada2')
			->setCellValue('AH1', 'DomesticShippingAdditionalCostCanada2')
			->setCellValue('AI1', 'DomesticShippingServiceCanada3')
			->setCellValue('AJ1', 'DomesticShippingCostCanada3')
			->setCellValue('AK1', 'DomesticShippingAdditionalCostCanada3')
			->setCellValue('AL1', 'InternationalShippingType')
			->setCellValue('AM1', 'InternationalShippingService1')
			->setCellValue('AN1', 'InternationalShippingCost1')
			->setCellValue('AO1', 'InternationalShippingAdditionalCost1')
			->setCellValue('AP1', 'InternationalShippingCountryCodes1')
			->setCellValue('AQ1', 'InternationalShippingService2')
			->setCellValue('AR1', 'InternationalShippingCost2')
			->setCellValue('AS1', 'InternationalShippingAdditionalCost2')
			->setCellValue('AT1', 'InternationalShippingCountryCodes2')
			->setCellValue('AU1', 'InternationalShippingService3')
			->setCellValue('AV1', 'InternationalShippingCost3')
			->setCellValue('AW1', 'InternationalShippingAdditionalCost3')
			->setCellValue('AX1', 'InternationalShippingCountryCodes3')
			->setCellValue('AY1', 'Featured')
			->setCellValue('AZ1', 'Active')
			->setCellValue('BA1', 'wholesale_price')
            ->setCellValue('BB1', 'main_cat_id')
			->setCellValue('BC1', 'cat_id')
			->setCellValue('BD1', 'sub_cat_id')
			->setCellValue('BE1', 'sub_subsub_cat1_id')
			->setCellValue('BF1', 'sub_subsub_cat2_id')
			->setCellValue('BG1', 'image_path')
			->setCellValue('BH1', 'created_on')
			->setCellValue('BI1', 'last_updated_on')
			->setCellValue('BJ1', 'view');			


$m=1;


foreach($allData as $ad)
{
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.($m+1), $ad['id'])
			->setCellValue('B'.($m+1), replQuote($ad['Product']))
			->setCellValue('C'.($m+1), replQuote($ad['SKU']))
			->setCellValue('D'.($m+1), replQuote($ad['UPC']))
			->setCellValue('E'.($m+1), replQuote($ad['Oem']))
			->setCellValue('F'.($m+1), replQuote($ad['Supplier']))
			->setCellValue('G'.($m+1), replQuote($ad['Price']))
			->setCellValue('H'.($m+1), replQuote($ad['Cost']))
			->setCellValue('I'.($m+1), replQuote($ad['Taxable']))
			->setCellValue('J'.($m+1), replQuote($ad['Quantity']))
			->setCellValue('K'.($m+1), replQuote($ad['Brief']))
			->setCellValue('L'.($m+1), replQuote($ad['Description']))
			->setCellValue('M'.($m+1), replQuote($ad['Thumbnail']))
			->setCellValue('N'.($m+1), replQuote($ad['Photo']))
			->setCellValue('O'.($m+1), replQuote($ad['NaturalSearchKeywords']))
			->setCellValue('P'.($m+1), replQuote($ad['NaturalSearchDescription']))
			->setCellValue('Q'.($m+1), replQuote($ad['Category']))
			->setCellValue('R'.($m+1), replQuote($ad['Keywords']))
			->setCellValue('S'.($m+1), replQuote($ad['DomesticShippingType']))
			->setCellValue('T'.($m+1), replQuote($ad['DomesticShippingService1']))
			->setCellValue('U'.($m+1), replQuote($ad['DomesticShippingCost1']))
			->setCellValue('V'.($m+1), replQuote($ad['DomesticShippingAdditionalCost1']))
			->setCellValue('W'.($m+1), replQuote($ad['DomesticShippingService2']))
			->setCellValue('X'.($m+1), replQuote($ad['DomesticShippingCost2']))
			->setCellValue('Y'.($m+1), replQuote($ad['DomesticShippingAdditionalCost2']))
			->setCellValue('Z'.($m+1), replQuote($ad['DomesticShippingService3']))
            ->setCellValue('AA'.($m+1), replQuote($ad['DomesticShippingCost3']))
			->setCellValue('AB'.($m+1), replQuote($ad['DomesticShippingAdditionalCost3']))
			->setCellValue('AC'.($m+1), replQuote($ad['DomesticShippingServiceCanada1']))
			->setCellValue('AD'.($m+1), replQuote($ad['DomesticShippingCostCanada1']))
			->setCellValue('AE'.($m+1), replQuote($ad['DomesticShippingAdditionalCostCanada1']))
			->setCellValue('AF'.($m+1), replQuote($ad['DomesticShippingServiceCanada2']))
			->setCellValue('AG'.($m+1), replQuote($ad['DomesticShippingCostCanada2']))
			->setCellValue('AH'.($m+1), replQuote($ad['DomesticShippingAdditionalCostCanada2']))
			->setCellValue('AI'.($m+1), replQuote($ad['DomesticShippingServiceCanada3']))
			->setCellValue('AJ'.($m+1), replQuote($ad['DomesticShippingCostCanada3']))
			->setCellValue('AK'.($m+1), replQuote($ad['DomesticShippingAdditionalCostCanada3']))
			->setCellValue('AL'.($m+1), replQuote($ad['InternationalShippingType']))
			->setCellValue('AM'.($m+1), replQuote($ad['InternationalShippingService1']))
			->setCellValue('AN'.($m+1), replQuote($ad['InternationalShippingCost1']))
			->setCellValue('AO'.($m+1), replQuote($ad['InternationalShippingAdditionalCost1']))
			->setCellValue('AP'.($m+1), replQuote($ad['InternationalShippingCountryCodes1']))
			->setCellValue('AQ'.($m+1), replQuote($ad['InternationalShippingService2']))
			->setCellValue('AR'.($m+1), replQuote($ad['InternationalShippingCost2']))
			->setCellValue('AS'.($m+1), replQuote($ad['InternationalShippingAdditionalCost2']))
			->setCellValue('AT'.($m+1), replQuote($ad['InternationalShippingCountryCodes2']))
			->setCellValue('AU'.($m+1), replQuote($ad['InternationalShippingService3']))
			->setCellValue('AV'.($m+1), replQuote($ad['InternationalShippingCost3']))
			->setCellValue('AW'.($m+1), replQuote($ad['InternationalShippingAdditionalCost3']))
			->setCellValue('AX'.($m+1), replQuote($ad['InternationalShippingCountryCodes3']))
			->setCellValue('AY'.($m+1), replQuote($ad['Featured']))
			->setCellValue('AZ'.($m+1), replQuote($ad['Active']))
			->setCellValue('BA'.($m+1), replQuote($ad['wholesale_price']))
			->setCellValue('BB'.($m+1), replQuote($ad['main_cat_id']))
			->setCellValue('BC'.($m+1), replQuote($ad['cat_id']))
			->setCellValue('BD'.($m+1), replQuote($ad['sub_cat_id']))
			->setCellValue('BE'.($m+1), replQuote($ad['sub_subsub_cat1_id']))
			->setCellValue('BF'.($m+1), replQuote($ad['sub_subsub_cat2_id']))
			->setCellValue('BG'.($m+1), replQuote($ad['image_path']))
			->setCellValue('BH'.($m+1), replQuote($ad['created_on']))
			->setCellValue('BI'.($m+1), replQuote($ad['last_updated_on']))
			->setCellValue('BJ'.($m+1), replQuote($ad['view']));
		
						
$m++;
}

$objPHPExcel->getActiveSheet()->setTitle('Backup as on - '.date("jS M Y"));
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('/home/uneek0/public_html/testexcel/Tests/Backup_Products.xls');
?>
<a href="Backup_Products.xls" target="_blank">Download File</a>