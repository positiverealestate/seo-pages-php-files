<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2010 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2010 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.4, 2010-08-26
 */
ob_start();

/** Error reporting */
error_reporting(E_ALL);
set_time_limit(0);
ini_set("memory_limit","100M");
date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../Classes/PHPExcel.php';

function replQuote($str)
{
return str_replace("&rsquo;","'",$str);
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

include('Inbox.php');
$inbox=new Inbox();
$inbox->getAllProductData();
$allData=$inbox->getResultArr();

// Set properties
$objPHPExcel->getProperties()->setCreator("Panacea Infotech Private Limited")
							 ->setLastModifiedBy("Panacea Infotech Private Limited")
							 ->setTitle("Backup Data")
							 ->setSubject("Backup Data")
							 ->setDescription("Backup of products data")
							 ->setKeywords("Products data")
							 ->setCategory("Backups");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'id')
            ->setCellValue('B1', 'Product')
            ->setCellValue('C1', 'SKU')
            ->setCellValue('D1', 'UPC')
			->setCellValue('E1', 'Oem')
			->setCellValue('F1', 'Supplier')
			->setCellValue('G1', 'Price')
			->setCellValue('H1', 'Cost')
			->setCellValue('I1', 'Taxable')
			->setCellValue('J1', 'Quantity')
			->setCellValue('K1', 'Brief')
			->setCellValue('L1', 'Description')
			->setCellValue('M1', 'Thumbnail')
			->setCellValue('N1', 'Photo')
			->setCellValue('O1', 'NaturalSearchKeywords')
			->setCellValue('P1', 'NaturalSearchDescription')
			->setCellValue('Q1', 'Category')
			->setCellValue('R1', 'Keywords')
			->setCellValue('S1', 'DomesticShippingType')
			->setCellValue('T1', 'DomesticShippingService1')
			->setCellValue('U1', 'DomesticShippingCost1')
			->setCellValue('V1', 'DomesticShippingAdditionalCost1')
			->setCellValue('W1', 'DomesticShippingService2')
			->setCellValue('X1', 'DomesticShippingCost2')
			->setCellValue('Y1', 'DomesticShippingAdditionalCost2')
			->setCellValue('Z1', 'DomesticShippingService3')
			->setCellValue('AA1', 'DomesticShippingCost3')
			->setCellValue('AB1', 'DomesticShippingAdditionalCost3')
			->setCellValue('AC1', 'DomesticShippingServiceCanada1')
			->setCellValue('AD1', 'DomesticShippingCostCanada1')
			->setCellValue('AE1', 'DomesticShippingAdditionalCostCanada1')
			->setCellValue('AF1', 'DomesticShippingServiceCanada2')
			->setCellValue('AG1', 'DomesticShippingCostCanada2')
			->setCellValue('AH1', 'DomesticShippingAdditionalCostCanada2')
			->setCellValue('AI1', 'DomesticShippingServiceCanada3')
			->setCellValue('AJ1', 'DomesticShippingCostCanada3')
			->setCellValue('AK1', 'DomesticShippingAdditionalCostCanada3')
			->setCellValue('AL1', 'InternationalShippingType')
			->setCellValue('AM1', 'InternationalShippingService1')
			->setCellValue('AN1', 'InternationalShippingCost1')
			->setCellValue('AO1', 'InternationalShippingAdditionalCost1')
			->setCellValue('AP1', 'InternationalShippingCountryCodes1')
			->setCellValue('AQ1', 'InternationalShippingService2')
			->setCellValue('AR1', 'InternationalShippingCost2')
			->setCellValue('AS1', 'InternationalShippingAdditionalCost2')
			->setCellValue('AT1', 'InternationalShippingCountryCodes2')
			->setCellValue('AU1', 'InternationalShippingService3')
			->setCellValue('AV1', 'InternationalShippingCost3')
			->setCellValue('AW1', 'InternationalShippingAdditionalCost3')
			->setCellValue('AX1', 'InternationalShippingCountryCodes3')
			->setCellValue('AY1', 'Featured')
			->setCellValue('AZ1', 'Active')
			->setCellValue('BA1', 'wholesale_price')
            ->setCellValue('BB1', 'main_cat_id')
			->setCellValue('BC1', 'cat_id')
			->setCellValue('BD1', 'sub_cat_id')
			->setCellValue('BE1', 'sub_subsub_cat1_id')
			->setCellValue('BF1', 'sub_subsub_cat2_id')
			->setCellValue('BG1', 'image_path')
			->setCellValue('BH1', 'created_on')
			->setCellValue('BI1', 'last_updated_on')
			->setCellValue('BJ1', 'view');			


// Miscellaneous glyphs, UTF-8
$i=1;

foreach($allData as $ad)
{
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.($i+1), $ad['id'])
			->setCellValue('B'.($i+1), replQuote($ad['Product']))
			->setCellValue('C'.($i+1), replQuote($ad['SKU']))
			->setCellValue('D'.($i+1), replQuote($ad['UPC']))
			->setCellValue('E'.($i+1), replQuote($ad['Oem']))
			->setCellValue('F'.($i+1), replQuote($ad['Supplier']))
			->setCellValue('G'.($i+1), replQuote($ad['Price']))
			->setCellValue('H'.($i+1), replQuote($ad['Cost']))
			->setCellValue('I'.($i+1), replQuote($ad['Taxable']))
			->setCellValue('J'.($i+1), replQuote($ad['Quantity']))
			->setCellValue('K'.($i+1), replQuote($ad['Brief']))
			->setCellValue('L'.($i+1), replQuote($ad['Description']))
			->setCellValue('M'.($i+1), replQuote($ad['Thumbnail']))
			->setCellValue('N'.($i+1), replQuote($ad['Photo']))
			->setCellValue('O'.($i+1), replQuote($ad['NaturalSearchKeywords']))
			->setCellValue('P'.($i+1), replQuote($ad['NaturalSearchDescription']))
			->setCellValue('Q'.($i+1), replQuote($ad['Category']))
			->setCellValue('R'.($i+1), replQuote($ad['Keywords']))
			->setCellValue('S'.($i+1), replQuote($ad['DomesticShippingType']))
			->setCellValue('T'.($i+1), replQuote($ad['DomesticShippingService1']))
			->setCellValue('U'.($i+1), replQuote($ad['DomesticShippingCost1']))
			->setCellValue('V'.($i+1), replQuote($ad['DomesticShippingAdditionalCost1']))
			->setCellValue('W'.($i+1), replQuote($ad['DomesticShippingService2']))
			->setCellValue('X'.($i+1), replQuote($ad['DomesticShippingCost2']))
			->setCellValue('Y'.($i+1), replQuote($ad['DomesticShippingAdditionalCost2']))
			->setCellValue('Z'.($i+1), replQuote($ad['DomesticShippingService3']))
            ->setCellValue('AA'.($i+1), replQuote($ad['DomesticShippingCost3']))
			->setCellValue('AB'.($i+1), replQuote($ad['DomesticShippingAdditionalCost3']))
			->setCellValue('AC'.($i+1), replQuote($ad['DomesticShippingServiceCanada1']))
			->setCellValue('AD'.($i+1), replQuote($ad['DomesticShippingCostCanada1']))
			->setCellValue('AE'.($i+1), replQuote($ad['DomesticShippingAdditionalCostCanada1']))
			->setCellValue('AF'.($i+1), replQuote($ad['DomesticShippingServiceCanada2']))
			->setCellValue('AG'.($i+1), replQuote($ad['DomesticShippingCostCanada2']))
			->setCellValue('AH'.($i+1), replQuote($ad['DomesticShippingAdditionalCostCanada2']))
			->setCellValue('AI'.($i+1), replQuote($ad['DomesticShippingServiceCanada3']))
			->setCellValue('AJ'.($i+1), replQuote($ad['DomesticShippingCostCanada3']))
			->setCellValue('AK'.($i+1), replQuote($ad['DomesticShippingAdditionalCostCanada3']))
			->setCellValue('AL'.($i+1), replQuote($ad['InternationalShippingType']))
			->setCellValue('AM'.($i+1), replQuote($ad['InternationalShippingService1']))
			->setCellValue('AN'.($i+1), replQuote($ad['InternationalShippingCost1']))
			->setCellValue('AO'.($i+1), replQuote($ad['InternationalShippingAdditionalCost1']))
			->setCellValue('AP'.($i+1), replQuote($ad['InternationalShippingCountryCodes1']))
			->setCellValue('AQ'.($i+1), replQuote($ad['InternationalShippingService2']))
			->setCellValue('AR'.($i+1), replQuote($ad['InternationalShippingCost2']))
			->setCellValue('AS'.($i+1), replQuote($ad['InternationalShippingAdditionalCost2']))
			->setCellValue('AT'.($i+1), replQuote($ad['InternationalShippingCountryCodes2']))
			->setCellValue('AU'.($i+1), replQuote($ad['InternationalShippingService3']))
			->setCellValue('AV'.($i+1), replQuote($ad['InternationalShippingCost3']))
			->setCellValue('AW'.($i+1), replQuote($ad['InternationalShippingAdditionalCost3']))
			->setCellValue('AX'.($i+1), replQuote($ad['InternationalShippingCountryCodes3']))
			->setCellValue('AY'.($i+1), replQuote($ad['Featured']))
			->setCellValue('AZ'.($i+1), replQuote($ad['Active']))
			->setCellValue('BA'.($i+1), replQuote($ad['wholesale_price']))
			->setCellValue('BB'.($i+1), replQuote($ad['main_cat_id']))
			->setCellValue('BC'.($i+1), replQuote($ad['cat_id']))
			->setCellValue('BD'.($i+1), replQuote($ad['sub_cat_id']))
			->setCellValue('BE'.($i+1), replQuote($ad['sub_subsub_cat1_id']))
			->setCellValue('BF'.($i+1), replQuote($ad['sub_subsub_cat2_id']))
			->setCellValue('BG'.($i+1), replQuote($ad['image_path']))
			->setCellValue('BH'.($i+1), replQuote($ad['created_on']))
			->setCellValue('BI'.($i+1), replQuote($ad['last_updated_on']))
			->setCellValue('BJ'.($i+1), replQuote($ad['view']));
			
						
$i++;
}



// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Backup as on - '.date("jS M Y"));


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter->save('php://output');
$objWriter->save('Backup_Products.xls');

?>