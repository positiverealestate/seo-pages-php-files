<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2010 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2010 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.4, 2010-08-26
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

include('Inbox.php');
$inbox=new Inbox();
$inbox->getAllProductData();
$allData=$inbox->getResultArr();
// Set properties
$objPHPExcel->getProperties()->setCreator("Panacea Infotech Private Limited")
							 ->setLastModifiedBy("Panacea Infotech Private Limited")
							 ->setTitle("Backup Data")
							 ->setSubject("Backup Data")
							 ->setDescription("Backup of products data")
							 ->setKeywords("Products data")
							 ->setCategory("Backups");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'id')
            ->setCellValue('B1', 'Product')
            ->setCellValue('C1', 'SKU')
            ->setCellValue('D1', 'UPC')
			->setCellValue('E1', 'Oem')
			->setCellValue('F1', 'Supplier')
			->setCellValue('G1', 'Price')
			->setCellValue('H1', 'Cost')
			->setCellValue('I1', 'Taxable')
			->setCellValue('J1', 'Quantity')
			->setCellValue('K1', 'Brief')
			->setCellValue('L1', 'Description')
			->setCellValue('M1', 'Thumbnail')
			->setCellValue('N1', 'Photo')
			->setCellValue('O1', 'NaturalSearchKeywords')
			->setCellValue('P1', 'NaturalSearchDescription')
			->setCellValue('Q1', 'Category')
			->setCellValue('R1', 'Keywords')
			->setCellValue('S1', 'DomesticShippingType')
			->setCellValue('T1', 'DomesticShippingService1')
			->setCellValue('U1', 'DomesticShippingCost1')
			->setCellValue('V1', 'DomesticShippingAdditionalCost1')
			->setCellValue('W1', 'DomesticShippingService2')
			->setCellValue('X1', 'DomesticShippingCost2')
			->setCellValue('Y1', 'DomesticShippingAdditionalCost2')
			->setCellValue('Z1', 'DomesticShippingService3')
			->setCellValue('AA1', 'DomesticShippingCost3')
			->setCellValue('AB1', 'DomesticShippingAdditionalCost3')
			->setCellValue('AC1', 'InternationalShippingType')
			->setCellValue('AD1', 'InternationalShippingService1')
			->setCellValue('AE1', 'InternationalShippingCost1')
			->setCellValue('AF1', 'InternationalShippingAdditionalCost1')
			->setCellValue('AG1', 'InternationalShippingCountryCodes1')
			->setCellValue('AH1', 'InternationalShippingService2')
			->setCellValue('AI1', 'InternationalShippingCost2')
			->setCellValue('AJ1', 'InternationalShippingAdditionalCost2')
			->setCellValue('AK1', 'InternationalShippingCountryCodes2')
			->setCellValue('AL1', 'InternationalShippingService3')
			->setCellValue('AM1', 'InternationalShippingCost3')
			->setCellValue('AN1', 'InternationalShippingAdditionalCost3')
			->setCellValue('AO1', 'InternationalShippingCountryCodes3')
			->setCellValue('AP1', 'Featured')
			->setCellValue('AQ1', 'Active')
			->setCellValue('AR1', 'wholesale_price')
			->setCellValue('AS1', 'main_cat_id')
			->setCellValue('AT1', 'cat_id')
			->setCellValue('AU1', 'sub_cat_id')
			->setCellValue('AV1', 'sub_subsub_cat1_id')
			->setCellValue('AW1', 'sub_subsub_cat2_id')
			->setCellValue('AX1', 'image_path')
			->setCellValue('AY1', 'created_on')
			->setCellValue('AZ1', 'last_updated_on')
			->setCellValue('BA1', 'view');

// Miscellaneous glyphs, UTF-8
foreach($allData as $d)
{

echo  $d;
echo'<hr/>';

}

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs');
            

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>