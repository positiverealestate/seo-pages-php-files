<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Suburbs_Model extends CI_Model {

    function getSuburbsList($limit = 0, $offset = 0) {

        $fields_to_pass = "*";
        $this->db->select($fields_to_pass);
        $this->db->from('mst_suburbs');
        if ($limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->group_by('suburb_name', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getSuburbRegionList() {
        $fields_to_pass = "A.country_id,A.country_name,A.iso";
        $this->db->select($fields_to_pass);
        $this->db->from('mst_countries as A');
        $this->db->group_by('A.country_name', 'DESC');
        $query = $this->db->get();
        $error = $this->db->_error_message();
        $error_number = $this->db->_error_number();
        if ($error) {
            $controller = $this->router->fetch_class();
            $method = $this->router->fetch_method();
            $error_details = array(
                'error_name' => $error,
                'error_number' => $error_number,
                'model_name' => 'suburbs_model',
                'model_method_name' => 'getSuburbRegionList',
                'controller_name' => $controller,
                'controller_method_name' => $method
            );
            $this->common_model->errorSendEmail($error_details);
            redirect(base_url() . 'page-not-found');
        }

        return $query->result_array();
    }

    function getNamesList($suburb_name) {
        $condition_to_pass = array("suburb_name" => $suburb_name);
        $fields_to_pass = "A.*";
        $this->db->select($fields_to_pass);
        $this->db->from('mst_suburbs as A');
        $this->db->where($condition_to_pass);
        $this->db->order_by('A.suburb_id', 'DESC');
        $query = $this->db->get();
        $error = $this->db->error();
       if(count($error)>0)
       {
            $error_number = isset($error['code'])?$error['code']:'';
            $error_message=isset($error['message'])?$error['message']:'';
            if ($error_number) {
                    $controller = $this->router->fetch_class();
                    $method = $this->router->fetch_method();
                    $error_details = array(
                            'error_name' => $error,
                            'error_number' => $error_number,
                            'model_name' => 'user_model',
                            'model_method_name' => 'getUserInformation',
                            'controller_name' => $controller,
                            'controller_method_name' => $method
                    );
                    $this->common_model->errorSendEmail($error_details);
                    redirect(base_url() . 'page-not-found');
       }
	   }
        return $query->result_array();
    }

    #function to get cms from the database if edit_id and lang_id black then it will return all reacords   

    function getSuburbDetails($suburb_id) {
        $fields_to_pass = "A.*";
        $condition_to_pass = array("A.suburb_id" => $suburb_id);
        $this->db->select($fields_to_pass);
        $this->db->from('mst_suburbs as A');
        $this->db->where($condition_to_pass);
        $query = $this->db->get();
        $error = $this->db->error();
       if(count($error)>0)
       {
        $error_number = isset($error['code'])?$error['code']:'';
        $error_message=isset($error['message'])?$error['message']:'';
        if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                        'error_name' => $error,
                        'error_number' => $error_number,
                        'model_name' => 'user_model',
                        'model_method_name' => 'getUserInformation',
                        'controller_name' => $controller,
                        'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url() . 'page-not-found');
		   }
	   }
        return $query->result_array();
    }

    public function getAllSuburbsLists($condition_to_pass) {
        $this->db->select('*');
        $this->db->from('trans_country_lang');
        $query = $this->db->where('country_id_fk', $condition_to_pass);

        $this->db->order_by('country_lang_id DESC');
        $query = $this->db->get();
        /*         * * this is to print error message ** */
        $error = $this->db->_error_message();
        /*         * * this is to print number ** */
        $error_number = $this->db->_error_number();
        if ($error) {
            $controller = $this->router->fetch_class();
            $method = $this->router->fetch_method();
            $error_details = array(
                'error_name' => $error,
                'error_number' => $error_number,
                'model_name' => 'suburbs_model',
                'method_name' => 'getAllSuburbsLists',
                'controller_name' => $controller,
                'controller_method_name' => $method
            );
            $this->common_model->errorSendEmail($error_details);
            redirect(base_url() . 'error-redirect');
        }
        return $query->result_array();
    }

    public function getSuburbInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('*');
        $this->db->from('trans_country_lang');

        if ($condition_to_pass != '') {
            $this->db->where($condition_to_pass);
        }
        $query = $this->db->get();
        /*         * * this is to print error message ** */
        $error = $this->db->_error_message();

        /*         * * this is to print number ** */
        $error_number = $this->db->_error_number();
        if ($error) {
            $controller = $this->router->fetch_class();
            $method = $this->router->fetch_method();
            $error_details = array(
                'error_name' => $error,
                'error_number' => $error_number,
                'model_name' => 'suburbs_model',
                'method_name' => 'getSuburbInformation',
                'controller_name' => $controller,
                'controller_method_name' => $method
            );
            $this->common_model->errorSendEmail($error_details);
            redirect(base_url() . 'error-redirect');
        }
        return $query->result_array();
    }
    
    public function checkEditId($table_name, $field_value, $field_name) {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($field_name, $field_value);
        $query = $this->db->get();
        $result = $query->result_array();
        if (count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function insertCustomRow($sql) {
        return $this->db->query($sql);
    }
}
