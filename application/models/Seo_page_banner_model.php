<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Seo_Page_Banner_Model extends CI_Model {

    public function getBanner() {
        $this->db->select('banner.*,b.*');
        $this->db->from('mst_home_page_sections as banner');
        $this->db->join('trans_home_page_section as b', 'banner.section_name_id= b.section_name_id', 'inner');
//        $this->db->where('email.status', "1");
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getBannerDetails($banner_id) {
        $this->db->select('banner.*,b.*');
        $this->db->from('mst_home_page_sections as banner');
        $this->db->join('trans_home_page_section as b', 'banner.section_name_id= b.section_name_id', 'inner');
        $this->db->where('banner.section_name_id', $banner_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getBannerDetailsByName($banner_name) {
        $this->db->select('banner.*,b.*');
        $this->db->from('mst_home_page_sections as banner');
        $this->db->join('trans_home_page_section as b', 'banner.section_name_id= b.section_name_id', 'inner');
        $this->db->where('banner.name', $banner_name);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getSuburbData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('demographic_id,state,postcode,suburb,MAX(cast(population as unsigned)) populations');
        $this->db->from('mst_demographic_data');

        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

//          if ($order_by_to_pass != '')
        $this->db->order_by('populations DESC');
        $this->db->group_by('suburb');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }

    public function getParticularStateSuburbData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('demographic_id,state,postcode,suburb');
        $this->db->from('mst_demographic_data');

        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

//          if ($order_by_to_pass != '')
        $this->db->order_by('suburb ASC');
        $this->db->group_by('suburb');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }

    public function getStateData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('demographic_id,state,country');
        $this->db->from('mst_demographic_data');

        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

        if ($order_by_to_pass != '')
            $this->db->order_by($order_by_to_pass);

        $this->db->group_by('state');
        $this->db->group_by('country');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }

    public function getStateDetails($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('demographic_id,state,country');
        $this->db->from('mst_demographic_data');

        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

        if ($order_by_to_pass != '')
            $this->db->order_by($order_by_to_pass);

        $this->db->group_by('state');
        $this->db->group_by('country');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }

    public function getAllSuburbData($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('state,postcode,suburb');
        $this->db->from('mst_demographic_data');

        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

//          if ($order_by_to_pass != '')
        $this->db->order_by('suburb ASC');
        $this->db->group_by('suburb');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }

    public function getCronRecords($table_to_pass = '', $fields_to_pass = '', $condition_to_pass = '', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('page_link,status');
        $this->db->from('mst_cron_details');

        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);


        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }

    public function getSearchStateRecords($search_val) {
        $this->db->select('state,postcode,suburb,demographic_id');
        $this->db->from('mst_demographic_data');
        $this->db->where("suburb LIKE '%$search_val%'");

        $query = $this->db->get();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }
    
    public function getCronSuburbData($limit) {
        
        $this->db->select('*');
        $this->db->from('mst_demographic_data');
        $this->db->limit(100, $limit);

        $query = $this->db->get();
        
        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'seo_page_banner_model',
                    'model_method_name' => 'getSuburbData',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        
        return $query->result_array();
    }

}
