<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Property_Model extends CI_Model {

    public function getPropertyDetails($table_to_pass='', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select($fields_to_pass, FALSE);
        $this->db->from('mst_properties as p');
        //$this->db->join('trans_property_images as pi','pi.property_id=p.property_id','inner');
        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

        if ($order_by_to_pass != '')
            $this->db->order_by($order_by_to_pass);
        
        //$this->db->group_by('pi.property_id');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();
        
       $error = $this->db->error();
       if(count($error)>0)
       {
            $error_number = isset($error['code'])?$error['code']:'';
            $error_message=isset($error['message'])?$error['message']:'';
            if ($error_number) {
                    $controller = $this->router->fetch_class();
                    $method = $this->router->fetch_method();
                    $error_details = array(
                            'error_name' => $error,
                            'error_number' => $error_number,
                            'model_name' => 'property_model',
                            'model_method_name' => 'getPropertyInformation',
                            'controller_name' => $controller,
                            'controller_method_name' => $method
                    );
                    $this->common_model->errorSendEmail($error_details);
                    redirect(base_url());
        }
	   }
        return $query->result_array();
    }
    
    public function getPropertyImageDetails($table_to_pass='', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select($fields_to_pass, FALSE);
        $this->db->from('trans_property_images');
        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

        if ($order_by_to_pass != '')
            $this->db->order_by($order_by_to_pass);
        
        $this->db->group_by('property_id');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();
        
       $error = $this->db->error();
       if(count($error)>0)
       {
            $error_number = isset($error['code'])?$error['code']:'';
            $error_message=isset($error['message'])?$error['message']:'';
            if ($error_number) {
                    $controller = $this->router->fetch_class();
                    $method = $this->router->fetch_method();
                    $error_details = array(
                            'error_name' => $error,
                            'error_number' => $error_number,
                            'model_name' => 'property_model',
                            'model_method_name' => 'getPropertyInformation',
                            'controller_name' => $controller,
                            'controller_method_name' => $method
                    );
                    $this->common_model->errorSendEmail($error_details);
                    redirect(base_url());
        }
	   }
        return $query->result_array();
    }
    
    public function getPropertyDetailsById($table_to_pass='', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('p.*,u.salesforce_user_id,inc.weekly_rent,inc.income_id,dbt.loan_amount,dbt.interest_rate,dbt.loan_type,dbt.rate_term,dbt.payment_frequency,dbt.next_payment_date,dbt.next_payment_amount');
        $this->db->from('mst_properties as p');
        $this->db->join('trans_debts as dbt','dbt.property_id=p.property_id','left');
        $this->db->join('trans_incomes as inc','inc.property_id=p.property_id','left');
        $this->db->join('mst_users as u','u.user_id=p.user_id','left');
        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

        if ($order_by_to_pass != '')
            $this->db->order_by($order_by_to_pass);
        
//        $this->db->group_by('p.property_id');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();
        
       $error = $this->db->error();
       if(count($error)>0)
       {
            $error_number = isset($error['code'])?$error['code']:'';
            $error_message=isset($error['message'])?$error['message']:'';
            if ($error_number) {
                    $controller = $this->router->fetch_class();
                    $method = $this->router->fetch_method();
                    $error_details = array(
                            'error_name' => $error,
                            'error_number' => $error_number,
                            'model_name' => 'property_model',
                            'model_method_name' => 'getPropertyInformation',
                            'controller_name' => $controller,
                            'controller_method_name' => $method
                    );
                    $this->common_model->errorSendEmail($error_details);
                    redirect(base_url());
        }
	   }
        return $query->result_array();
    }
    
    public function getCronUpdatePropertyDetails($table_to_pass='', $fields_to_pass='', $condition_to_pass='', $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select('corelogic_property_id,property_id,user_id,property_name,valuation');
        $this->db->from('mst_properties');
//        if ($condition_to_pass != '')
            $this->db->where('property_type','Land');
            $this->db->or_where('property_type','House & Land');

        if ($order_by_to_pass != '')
            $this->db->order_by($order_by_to_pass);
        
        $this->db->group_by('property_id');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();
        
       $error = $this->db->error();
       if(count($error)>0)
       {
            $error_number = isset($error['code'])?$error['code']:'';
            $error_message=isset($error['message'])?$error['message']:'';
            if ($error_number) {
                    $controller = $this->router->fetch_class();
                    $method = $this->router->fetch_method();
                    $error_details = array(
                            'error_name' => $error,
                            'error_number' => $error_number,
                            'model_name' => 'property_model',
                            'model_method_name' => 'getPropertyInformation',
                            'controller_name' => $controller,
                            'controller_method_name' => $method
                    );
                    $this->common_model->errorSendEmail($error_details);
                    redirect(base_url());
        }
	   }
        return $query->result_array();
    }
    
    public function deleteImages($property_id) {
        $this->db->where("property_id", $property_id);
        $this->db->delete("trans_property_images");
    }
}