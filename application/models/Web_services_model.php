<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Web_Services_Model extends CI_Model {
	
    public function getUserInformation($table_to_pass, $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
    $this->db->select($fields_to_pass, FALSE);
    $this->db->from($table_to_pass);
    if ($condition_to_pass != '')
        $this->db->where($condition_to_pass);

    if ($order_by_to_pass != '')
        $this->db->order_by($order_by_to_pass);

    if ($limit_to_pass != '')
        $this->db->limit($limit_to_pass);

    $query = $this->db->get();

    if ($debug_to_pass)
        echo $this->db->last_query();

    return $query->result_array();
}

    public function getCmsPageDetailsFront($condition) {
  $fields_to_pass = "A.cms_id,A.page_alias,B.page_title,B.page_content,A.status,B.page_seo_title_lang,B.page_meta_keyword,B.page_meta_description,B.page_title";        
    $this->db->select($fields_to_pass);
    $this->db->from('mst_cms as A');     
    $this->db->join('trans_cms as B','A.cms_id=B.cms_id','left');
            $this->db->where($condition);
    $query = $this->db->get();
    return $query->result_array();
}

    public function insertContactUs($arr_fields) {
        $this->db->insert("mst_contact_us", $arr_fields);
        return $this->db->insert_id();
    }
    
    public function insertProperty($arr_fields) {
        $this->db->insert("mst_properties", $arr_fields);
        return $this->db->insert_id();
    }
    
    public function getPropertyDetailsByType($table_to_pass='', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select($fields_to_pass, FALSE);
        $this->db->from('mst_properties as p');
        $this->db->join('trans_property_images as pi','pi.property_id=p.property_id','left');
        $this->db->join('trans_incomes as ti','ti.property_id=p.property_id','left');
        $this->db->join('trans_debts as td','td.property_id=p.property_id','left');
        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);
            $this->db->order_by('property_id','DESC');
        
        $this->db->group_by('p.property_id');

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();
        
       $error = $this->db->error();
       if(count($error)>0)
       {
            $error_number = isset($error['code'])?$error['code']:'';
            $error_message=isset($error['message'])?$error['message']:'';
            if ($error_number) {
                    $controller = $this->router->fetch_class();
                    $method = $this->router->fetch_method();
                    $error_details = array(
                            'error_name' => $error,
                            'error_number' => $error_number,
                            'model_name' => 'property_model',
                            'model_method_name' => 'getPropertyInformation',
                            'controller_name' => $controller,
                            'controller_method_name' => $method
                    );
                    $this->common_model->errorSendEmail($error_details);
                    redirect(base_url());
        }
	   }
        return $query->result_array();
    }
    
    public function deleteProperty($property_id) {
        $this->db->where("property_id", $property_id);
        $this->db->delete("mst_properties");
    }
    
    public function getCheckRecordsExists($chk_name_condition,$user_id) {
//        error_reporting(E_ALL);
        $this->db->select('*');
        $this->db->from('mst_properties');
        
        $this->db->where($chk_name_condition);
//        $this->db->where('user_id',$user_id);

        $query = $this->db->get();
//        echo $this->db->last_query();die;
        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'web_services_model',
                    'model_method_name' => 'getCheckRecordsExists',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }
    
    public function getPropertyDetails($property_id) {
        $this->db->select('p.property_id,p.property_name,p.weekly_rent,p.property_address,p.current_equity,td.loan_amount,td.interest_rate,td.loan_type,td.rate_term,p.user_id,u.first_name,u.last_name');
        $this->db->from('mst_properties as p');
        $this->db->join('mst_users as u', 'u.user_id=p.user_id', 'left');
        $this->db->join('trans_debts as td', 'td.property_id=p.property_id', 'left');
        
        $this->db->where_in("p.property_id",  explode(",", $property_id));
        $query = $this->db->get();
        
        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'property_model',
                    'model_method_name' => 'getPropertyInformation',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }
    
    public function getGoalsDetails($table_to_pass = '', $fields_to_pass, $condition_to_pass, $order_by_to_pass = '', $limit_to_pass = '', $debug_to_pass = 0) {
        $this->db->select($fields_to_pass, FALSE);
        $this->db->from('trans_goals');

        if ($condition_to_pass != '')
            $this->db->where($condition_to_pass);

        if ($order_by_to_pass != '')
            $this->db->order_by($order_by_to_pass);

        if ($limit_to_pass != '')
            $this->db->limit($limit_to_pass);

        $query = $this->db->get();

        if ($debug_to_pass)
            echo $this->db->last_query();

        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'property_model',
                    'model_method_name' => 'getPropertyInformation',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }
    
    public function insertGoals($arr_fields) {
        $this->db->insert("trans_goals", $arr_fields);
        return $this->db->insert_id();
    }
    
    public function deleteGoal($goal_id) {
        $this->db->where("goal_id", $goal_id);
        $this->db->delete("trans_goals");
    }
    
    public function getAllDebtRecords($user_id) {
        $this->db->select('sum(loan_amount) as debt_value');
        $this->db->from('trans_debts as d');
        $this->db->join('mst_properties as p','p.property_id= d.property_id','left');
        $this->db->where('d.user_id',$user_id);
        $this->db->where('p.ppor_status','Yes');
        $query = $this->db->get();
        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'property_model',
                    'model_method_name' => 'getPropertyInformation',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }
    
    public function getGraphRecords($user_id,$type,$date) {
//        error_reporting(E_ALL);
        $this->db->select('sum(gd.value) as value_total,gd.date,p.property_id');
        $this->db->from('trans_graph_details as gd');
        $this->db->join('mst_properties as p','gd.property_id=p.property_id','left');
        $this->db->where('gd.user_id',$user_id);
        $this->db->where('gd.type',$type);
        $this->db->where('p.ppor_status','Yes');
        if($date !=''){
            $this->db->where('YEAR(date)', $date);
        }
//        $this->db->where('date <= DATE_SUB(CURDATE(), INTERVAL 1 month)');
//        $this->db->group_by('date');
        $query = $this->db->get();
        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'property_model',
                    'model_method_name' => 'getPropertyInformation',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }
    
    public function getCapitalGraphRecords($user_id,$type,$property_id) {
//        error_reporting(E_ALL);
        $this->db->select('sum(gd.value) as value_total,gd.date,p.property_id');
        $this->db->from('trans_graph_details as gd');
        $this->db->join('mst_properties as p','gd.property_id=p.property_id','left');
        $this->db->where('gd.user_id',$user_id);
        $this->db->where('gd.type',$type);
        $this->db->where('gd.property_id',$property_id);
        $this->db->where('p.ppor_status','Yes');
        if($date !=''){
            $this->db->where('YEAR(date)', $date);
        }
//        $this->db->where('date <= DATE_SUB(CURDATE(), INTERVAL 1 month)');
//        $this->db->group_by('date');
        $query = $this->db->get();
        $error = $this->db->error();
        if (count($error) > 0) {
            $error_number = isset($error['code']) ? $error['code'] : '';
            $error_message = isset($error['message']) ? $error['message'] : '';
            if ($error_number) {
                $controller = $this->router->fetch_class();
                $method = $this->router->fetch_method();
                $error_details = array(
                    'error_name' => $error,
                    'error_number' => $error_number,
                    'model_name' => 'property_model',
                    'model_method_name' => 'getPropertyInformation',
                    'controller_name' => $controller,
                    'controller_method_name' => $method
                );
                $this->common_model->errorSendEmail($error_details);
                redirect(base_url());
            }
        }
        return $query->result_array();
    }
}