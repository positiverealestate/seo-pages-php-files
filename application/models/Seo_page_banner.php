<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Seo_page_banner extends CI_Model{
    
    public function getBanner() {
     $this->db->select('banner.*,b.*');
        $this->db->from('mst_home_page_sections as banner');
        $this->db->join('trans_home_page_section as b', 'banner.section_name_id= b.section_name_id', 'inner');
//        $this->db->where('email.status', "1");
        $result = $this->db->get();
        return $result->result_array();
}
    public function getBannerDetails($banner_id) {
     $this->db->select('banner.*,b.*');
        $this->db->from('mst_home_page_sections as banner');
        $this->db->join('trans_home_page_section as b', 'banner.section_name_id= b.section_name_id', 'inner');
        $this->db->where('banner.section_name_id', $banner_id);
        $result = $this->db->get();
        return $result->result_array();
}
    public function getBannerDetailsByName($banner_name) {
     $this->db->select('banner.*,b.*');
        $this->db->from('mst_home_page_sections as banner');
        $this->db->join('trans_home_page_section as b', 'banner.section_name_id= b.section_name_id', 'inner');
        $this->db->where('banner.name', $banner_name);
        $result = $this->db->get();
        return $result->result_array();
}
}
