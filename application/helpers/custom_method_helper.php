<?php

function timeAgo($ptime = '') {
    $etime = time() - $ptime;

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array(365 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    $a_plural = array('year' => 'years',
        'month' => 'months',
        'day' => 'days',
        'hour' => 'hours',
        'minute' => 'minutes',
        'second' => 'seconds'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function timeLeft($now, $future) {
    if ($future <= $now) {
        // Time has already elapsed 
        return FALSE;
    } else {
        // Get difference between times 
        $time = $future - $now;
        $minutesFloat = $time / 60;
        $minutes = floor($minutesFloat);
        $hoursFloat = $minutes / 60;
        $hours = floor($hoursFloat);
        $daysFloat = $hours / 24;
        $days = floor($daysFloat);
        return array(
            'days' => $days,
            'hours' => round(($daysFloat - $days) * 24),
            'minutes' => round(($hoursFloat - $hours) * 60),
            'seconds' => round(($minutesFloat - $minutes) * 60)
        );
    }
}

/*
 * get file extention
 */

function findExtension($filename) {
    $filename = strtolower($filename);
    // $file = str_replace(" ", "-", $filename);
    $exts = explode(".", $filename);
    $file_name = '';
    for ($i = 0; $i <= count($exts) - 2; $i++) {
        $file_name .=$exts[$i];
    }
    $n = count($exts) - 1;
    $exts = $exts[$n];
    $arr_return = array(
        'file_name' => $file_name,
        'ext' => $exts
    );
    return $arr_return;
}

//function for create thumb
function createThumbnail($filename, $image_width, $path_to_image_directory, $path_to_thumbs_directory) {
    $filename = strtolower($filename);
    $final_width_of_image = $image_width;
    if (preg_match('/[.](jpg)$/', $filename)) {
        $im = imagecreatefromjpeg($path_to_image_directory . $filename);
    } elseif (preg_match('/[.](JPG)$/', $filename)) {
        $im = imagecreatefromjpeg($path_to_image_directory . $filename);
    } elseif (preg_match('/[.](jpeg)$/', $filename)) {
        $im = imagecreatefromjpeg($path_to_image_directory . $filename);
    } elseif (preg_match('/[.](JPEG)$/', $filename)) {
        $im = imagecreatefromjpeg($path_to_image_directory . $filename);
    } else if (preg_match('/[.](gif)$/', $filename)) {
        $im = imagecreatefromgif($path_to_image_directory . $filename);
    } elseif (preg_match('/[.](GIF)$/', $filename)) {
        $im = imagecreatefromgif($path_to_image_directory . $filename);
    } else if (preg_match('/[.](png)$/', $filename)) {
        $im = imagecreatefrompng($path_to_image_directory . $filename);
    } elseif (preg_match('/[.](PNG)$/', $filename)) {
        $im = imagecreatefrompng($path_to_image_directory . $filename);
    }

    $ox = imagesx($im);
    $oy = imagesy($im);

    $nx = $final_width_of_image;
    $ny = floor($oy * ($final_width_of_image / $ox));

    $nm = imagecreatetruecolor($nx, $ny);

    imagecopyresampled($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);

    if (!file_exists($path_to_thumbs_directory)) {
        if (!mkdir($path_to_thumbs_directory)) {
            die("There was a problem. Please try again!");
        }
    }

    imagejpeg($nm, $path_to_thumbs_directory . $filename);
    $tn = '<img src="' . $path_to_thumbs_directory . $filename . '" alt="image" />';
    $tn .= '<br />Congratulations. Your file has been successfully uploaded, and a thumbnail has been created.';
    return true;
}

// Get SEO URL function here
function seoUrl($string) {
    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = strtolower($string);
    //Strip any unwanted characters
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

//function for get country state from address
function reverseGeocode($address) {
    $address = str_replace(" ", "+", "$address");
    $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
    $result = file_get_contents("$url");
    $json = json_decode($result);
    foreach ($json->results as $result) {
        foreach ($result->address_components as $addressPart) {
            if ((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types))) {
                $city = $addressPart->long_name;
            } else if ((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types))) {
                $state = $addressPart->long_name;
            } else if ((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types))) {
                $country = $addressPart->long_name;
                $country_iso = $addressPart->short_name;
            } else if (in_array('postal_code', $addressPart->types)) {
                $postal_code = $addressPart->long_name;
            }
        }
        $lat = $result->geometry->location->lat;
        $lng = $result->geometry->location->lng;
    }
    $arr_address_components = array(
        'city' => ($city != '') ? $city : '',
        'state' => ($state != '') ? $state : '',
        'country' => ($country != '') ? $country : '',
        'country_iso' => ($country_iso != '') ? $country_iso : '',
        'zipcode' => ($postal_code != '') ? $postal_code : '',
        'latitude' => ($lat != '') ? $lat : '',
        'longitude' => ($lng != '') ? $lng : '',
    );
    return $arr_address_components;
}

//function to convert currency

function currencyConversion($amt, $currency, $exchange_rate) {
    $number = round(($amt * $exchange_rate), 2);
    $amount = explode('.', $number);
    if ($amount[1] < '10') {
        return $amount[0];
    } else {
        return round($number, 2);
    }
}

function exchangeRate($from, $to, $amount) {
    $data = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from&to=$to");
    preg_match("/<span class=bld>(.*)<\/span>/", $data, $converted);
    $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
    return number_format(round($converted, 3), 2);
}

/*
 * remove time from datetime format for globalsetting date_format
 */

function removeTimeFromDate($date_format) {
    $date_format = explode('g', $date_format);
    $only_date = explode(',', $date_format[0]);
    return $only_date[0];
}

//calculate distance
function calculateDistance($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
    $theta = $longitude1 - $longitude2;
    $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
    $distance = acos($distance);
    $distance = rad2deg($distance);
    $distance = $distance * 60 * 1.1515;
    switch ($unit) {
        case 'Mi': break;
        case 'Km' : $distance = $distance * 1.609344;
    }
//    echo '<pre>';print_r($latitude1);
//    echo '<pre>';print_r($latitude2);die;
//    if ($latitude1 == $latitude2) {
//        $distance = 0;
//        return $distance;
//    } else {
    return (round($distance, 2));
//    }
}

function dd($data) {
    echo '<pre>';
    print_r($data);
    exit();
}

?>