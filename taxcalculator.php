<?php
include_once('simple_html_dom.php');
$yearstring = (isset($_GET["yr"]))?$_GET["yr"]:"1617";
$dirfile = "/home/positive/public_html/property/media/front/img/taxcalculator-data/";

if(file_exists($dirfile.$yearstring.".json")){

  echo file_get_contents($dirfile.$yearstring.".json");
  die;

} else{
$html = file_get_html("http://www.taxcalc.com.au/".$yearstring.".html");

}




$headings = array();
$table_data = array();
$textdata = array();
$main_data = $html->find('div.mid',0);



$last_tag = "";
foreach($main_data->nodes as $node){

	if($node->tag === 'text')
	{

		if(strlen(trim($node->innertext)) > 0)
		{
			if($last_tag === 'br'){

				$textdata[count($textdata)-1] = $textdata[count($textdata)-1]."\n".format_string($node->innertext);

			} else
			{
				$textdata[] = format_string($node->innertext);
			}
		}
	}

	$last_tag = $node->tag;
}
// Table 1
// get heading
$head_first = $main_data->find('span.tablehead',0);
$head_second = $main_data->find('span.tablehead',1);

// get tables
$table_first = $main_data->find('table',0);
$table_second = $main_data->find('table',1);

// get first table headings
$table_first_heading = $table_first->find('caption',0);
$table_first_column_heading = $table_first->find('tbody tr.taxtblhead',0);

$table_first_column_heading_array = array();

foreach($table_first_column_heading->children() as $childTd){
  $table_first_column_heading_array[]=  parseHTMLString($childTd->plaintext);
}

// Table 1
// iterate data
$key=0;
foreach($table_first->children() as $childtr)
{
	if($childtr->class === 'even' || $childtr->class === 'odd'){
		foreach($childtr->children as $td){
			$tableData['firstTable'][$key][] = parseHTMLString($td->plaintext);
		}
    $key++;
	}
}
// get first table footer
$table_first_footer = $table_first->find('tbody tr.rowhighlight',0);


// Table 2
// get second table headings
$table_second_heading = $table_first->find('caption',0);
$table_second_column_heading = $table_first->find('tbody tr.taxtblhead',0);

$table_second_column_heading_array = array();

foreach($table_second_column_heading->children() as $childTd){
  $table_second_column_heading_array[]=  parseHTMLString($childTd->plaintext);
}


// iterate second data
$key=0;
foreach($table_second->children() as $childtr)
{
  if($childtr->class === 'even' || $childtr->class === 'odd'){
    foreach($childtr->children as $td){
      $tableData['secondTable'][$key][] = parseHTMLString($td->plaintext);

    }
    $key++;
  }
}
// get second table footer
$table_second_footer = $table_second->lastChild();

$arrToReturn = array();

$arrToReturn['first_table_title'] = parseHTMLString($head_first->plaintext);
$arrToReturn['first_table_heading'] = parseHTMLString($table_first_heading->plaintext);
$arrToReturn['first_table_column_headings'] = $table_first_column_heading_array;
$arrToReturn['first_table_datarows'] = $tableData['firstTable'];
$arrToReturn['first_table_footer'] = parseHTMLString($table_first_footer->plaintext);
$arrToReturn['first_table_below_text'] = (count($textdata)>0)?$textdata[0]:"";


$arrToReturn['second_table_title'] = parseHTMLString($head_second->plaintext);
$arrToReturn['second_table_heading'] = parseHTMLString($table_second_heading->plaintext);
$arrToReturn['second_table_column_headings'] = $table_second_column_heading_array;
$arrToReturn['second_table_datarows'] = $tableData['secondTable'];
$arrToReturn['second_table_footer'] = parseHTMLString($table_second_footer->plaintext);
$arrToReturn['second_table_below_text'] = (count($textdata)>1)?$textdata[1]:"";

$main_data->outertext = '<link href="https://d1lewodvjpqn1n.cloudfront.net/images/stylev7.css" rel="stylesheet" type="text/css" />'.$main_data->outertext;

$fp = fopen($dirfile.$yearstring.".html","w+");
fwrite($fp, $main_data->outertext);
fclose($fp);


$arrToReturn['html_file']="media/front/img/taxcalculator-data/".$yearstring.".html";

$fp = fopen($dirfile.$yearstring.".json","w+");
fwrite($fp, json_encode($arrToReturn));
fclose($fp);

echo json_encode($arrToReturn);

function format_string($str){

	$string = preg_replace('/\s*$^\s*/m', "", $str);
	$string = (preg_replace('/[ \t]+/', ' ', $string));

	$i=0;

	while($i<25){
		$string = trim($string);
		$i++;
	}
	return $string;
}

function parseHTMLString($Htmlstr){
	return html2text(format_string($Htmlstr));
}

function html2text($Document) {
    $Rules = array ('@<script[^>]*?>.*?</script>@si',
                    '@<[\/\!]*?[^<>]*?>@si',
                    '@([\r\n])[\s]+@',
                    '@&(quot|#34);@i',
                    '@&(amp|#38);@i',
                    '@&(lt|#60);@i',
                    '@&(gt|#62);@i',
                    '@&(nbsp|#160);@i',
                    '@&(iexcl|#161);@i',
                    '@&(cent|#162);@i',
                    '@&(pound|#163);@i',
                    '@&(copy|#169);@i',
                    '@&(reg|#174);@i',
                    '@&(#8211);@i'
             );
    $Replace = array ('',
                      '',
                      '',
                      '',
                      '&',
                      '<',
                      '>',
                      ' ',
                      chr(161),
                      chr(162),
                      chr(163),
                      chr(169),
                      chr(174),
                      "-"
                );
  $str_updated = preg_replace($Rules, $Replace, $Document);
 
  return $str_updated;
}


?> 